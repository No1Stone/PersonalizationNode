package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProfile is a Querydsl query type for Profile
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfile extends EntityPathBase<Profile> {

    private static final long serialVersionUID = -734446019L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProfile profile = new QProfile("profile");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath accomplishmentsInfo = createString("accomplishmentsInfo");

    public final StringPath bio = createString("bio");

    public final StringPath bthDay = createString("bthDay");

    public final StringPath bthMonth = createString("bthMonth");

    public final StringPath bthYear = createString("bthYear");

    public final StringPath caeInfo = createString("caeInfo");

    public final StringPath currentCityCountryCd = createString("currentCityCountryCd");

    public final StringPath currentCityNm = createString("currentCityNm");

    public final StringPath fullName = createString("fullName");

    public final StringPath headline = createString("headline");

    public final StringPath hireMeYn = createString("hireMeYn");

    public final StringPath homeTownCountryCd = createString("homeTownCountryCd");

    public final StringPath homeTownNm = createString("homeTownNm");

    public final StringPath ipiInfo = createString("ipiInfo");

    public final StringPath ipnInfo = createString("ipnInfo");

    public final StringPath isniInfo = createString("isniInfo");

    public final StringPath mattermostId = createString("mattermostId");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath nroInfo = createString("nroInfo");

    public final StringPath nroTypeCd = createString("nroTypeCd");

    public final QProfileAnrPitching profileAnrPitching;

    public final StringPath profileBgdImgUrl = createString("profileBgdImgUrl");

    public final ListPath<ProfileCareer, QProfileCareer> profileCareer = this.<ProfileCareer, QProfileCareer>createList("profileCareer", ProfileCareer.class, QProfileCareer.class, PathInits.DIRECT2);

    public final ListPath<ProfileEducation, QProfileEducation> profileEducation = this.<ProfileEducation, QProfileEducation>createList("profileEducation", ProfileEducation.class, QProfileEducation.class, PathInits.DIRECT2);

    public final ListPath<ProfileGender, QProfileGender> profileGender = this.<ProfileGender, QProfileGender>createList("profileGender", ProfileGender.class, QProfileGender.class, PathInits.DIRECT2);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImgUrl = createString("profileImgUrl");

    public final ListPath<ProfileInterestMeta, QProfileInterestMeta> profileInterestMeta = this.<ProfileInterestMeta, QProfileInterestMeta>createList("profileInterestMeta", ProfileInterestMeta.class, QProfileInterestMeta.class, PathInits.DIRECT2);

    public final ListPath<ProfileLanguage, QProfileLanguage> profileLanguage = this.<ProfileLanguage, QProfileLanguage>createList("profileLanguage", ProfileLanguage.class, QProfileLanguage.class, PathInits.DIRECT2);

    public final ListPath<ProfileOntheweb, QProfileOntheweb> profileOntheweb = this.<ProfileOntheweb, QProfileOntheweb>createList("profileOntheweb", ProfileOntheweb.class, QProfileOntheweb.class, PathInits.DIRECT2);

    public final ListPath<ProfilePosition, QProfilePosition> profilePosition = this.<ProfilePosition, QProfilePosition>createList("profilePosition", ProfilePosition.class, QProfilePosition.class, PathInits.DIRECT2);

    public final ListPath<ProfileSkill, QProfileSkill> profileSkill = this.<ProfileSkill, QProfileSkill>createList("profileSkill", ProfileSkill.class, QProfileSkill.class, PathInits.DIRECT2);

    public final ListPath<ProfileTimezone, QProfileTimezone> profileTimezone = this.<ProfileTimezone, QProfileTimezone>createList("profileTimezone", ProfileTimezone.class, QProfileTimezone.class, PathInits.DIRECT2);

    public final ListPath<ProfileUseInfo, QProfileUseInfo> profileUseInfo = this.<ProfileUseInfo, QProfileUseInfo>createList("profileUseInfo", ProfileUseInfo.class, QProfileUseInfo.class, PathInits.DIRECT2);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath stageNameYn = createString("stageNameYn");

    public final StringPath useYn = createString("useYn");

    public final StringPath verifyYn = createString("verifyYn");

    public QProfile(String variable) {
        this(Profile.class, forVariable(variable), INITS);
    }

    public QProfile(Path<? extends Profile> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProfile(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProfile(PathMetadata metadata, PathInits inits) {
        this(Profile.class, metadata, inits);
    }

    public QProfile(Class<? extends Profile> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.profileAnrPitching = inits.isInitialized("profileAnrPitching") ? new QProfileAnrPitching(forProperty("profileAnrPitching")) : null;
    }

}

