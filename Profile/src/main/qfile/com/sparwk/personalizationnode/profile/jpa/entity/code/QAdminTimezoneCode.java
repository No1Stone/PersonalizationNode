package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminTimezoneCode is a Querydsl query type for AdminTimezoneCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminTimezoneCode extends EntityPathBase<AdminTimezoneCode> {

    private static final long serialVersionUID = 780390752L;

    public static final QAdminTimezoneCode adminTimezoneCode = new QAdminTimezoneCode("adminTimezoneCode");

    public final StringPath city = createString("city");

    public final StringPath continent = createString("continent");

    public final NumberPath<Long> diffTime = createNumber("diffTime", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath timezoneName = createString("timezoneName");

    public final NumberPath<Long> timezoneSeq = createNumber("timezoneSeq", Long.class);

    public final StringPath useYn = createString("useYn");

    public final StringPath utcHour = createString("utcHour");

    public QAdminTimezoneCode(String variable) {
        super(AdminTimezoneCode.class, forVariable(variable));
    }

    public QAdminTimezoneCode(Path<? extends AdminTimezoneCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminTimezoneCode(PathMetadata metadata) {
        super(AdminTimezoneCode.class, metadata);
    }

}

