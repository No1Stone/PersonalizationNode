package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCareer is a Querydsl query type for ProfileCareer
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCareer extends EntityPathBase<ProfileCareer> {

    private static final long serialVersionUID = -628109285L;

    public static final QProfileCareer profileCareer = new QProfileCareer("profileCareer");

    public final StringPath careerEndDt = createString("careerEndDt");

    public final StringPath careerOrganizationNm = createString("careerOrganizationNm");

    public final NumberPath<Long> careerSeq = createNumber("careerSeq", Long.class);

    public final StringPath careerStartDt = createString("careerStartDt");

    public final StringPath deptRoleNm = createString("deptRoleNm");

    public final StringPath filePath = createString("filePath");

    public final StringPath locationCityCountryCd = createString("locationCityCountryCd");

    public final StringPath locationCityNm = createString("locationCityNm");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath presentYn = createString("presentYn");

    public final NumberPath<Long> profileCareerSeq = createNumber("profileCareerSeq", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QProfileCareer(String variable) {
        super(ProfileCareer.class, forVariable(variable));
    }

    public QProfileCareer(Path<? extends ProfileCareer> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCareer(PathMetadata metadata) {
        super(ProfileCareer.class, metadata);
    }

}

