package com.sparwk.personalizationnode.profile.jpa.entity.project;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProject is a Querydsl query type for Project
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProject extends EntityPathBase<Project> {

    private static final long serialVersionUID = -1917370920L;

    public static final QProject project = new QProject("project");

    public final StringPath avatarFileUrl = createString("avatarFileUrl");

    public final StringPath completeYn = createString("completeYn");

    public final StringPath genderCd = createString("genderCd");

    public final StringPath leaveYn = createString("leaveYn");

    public final NumberPath<Long> maxVal = createNumber("maxVal", Long.class);

    public final NumberPath<Long> minVal = createNumber("minVal", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath pitchProjTypeCd = createString("pitchProjTypeCd");

    public final StringPath projAvalYn = createString("projAvalYn");

    public final StringPath projCondCd = createString("projCondCd");

    public final DatePath<java.time.LocalDate> projDdlDt = createDate("projDdlDt", java.time.LocalDate.class);

    public final StringPath projDesc = createString("projDesc");

    public final ListPath<ProjectMemb, QProjectMemb> projectMemb = this.<ProjectMemb, QProjectMemb>createList("projectMemb", ProjectMemb.class, QProjectMemb.class, PathInits.DIRECT2);

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public final StringPath projIndivCompType = createString("projIndivCompType");

    public final StringPath projInvtDesc = createString("projInvtDesc");

    public final StringPath projInvtTitle = createString("projInvtTitle");

    public final NumberPath<Long> projOwner = createNumber("projOwner", Long.class);

    public final StringPath projPublYn = createString("projPublYn");

    public final StringPath projPwd = createString("projPwd");

    public final StringPath projTitle = createString("projTitle");

    public final StringPath projWhatFor = createString("projWhatFor");

    public final StringPath projWorkLocat = createString("projWorkLocat");

    public final StringPath recruitYn = createString("recruitYn");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QProject(String variable) {
        super(Project.class, forVariable(variable));
    }

    public QProject(Path<? extends Project> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProject(PathMetadata metadata) {
        super(Project.class, metadata);
    }

}

