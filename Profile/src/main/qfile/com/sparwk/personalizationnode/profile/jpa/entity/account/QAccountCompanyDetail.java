package com.sparwk.personalizationnode.profile.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountCompanyDetail is a Querydsl query type for AccountCompanyDetail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountCompanyDetail extends EntityPathBase<AccountCompanyDetail> {

    private static final long serialVersionUID = 478476046L;

    public static final QAccountCompanyDetail accountCompanyDetail = new QAccountCompanyDetail("accountCompanyDetail");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath addr1 = createString("addr1");

    public final StringPath addr2 = createString("addr2");

    public final StringPath city = createString("city");

    public final StringPath companyName = createString("companyName");

    public final StringPath contctEmail = createString("contctEmail");

    public final StringPath contctFirstName = createString("contctFirstName");

    public final StringPath contctLastName = createString("contctLastName");

    public final StringPath contctMidleName = createString("contctMidleName");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath postCd = createString("postCd");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final StringPath region = createString("region");

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QAccountCompanyDetail(String variable) {
        super(AccountCompanyDetail.class, forVariable(variable));
    }

    public QAccountCompanyDetail(Path<? extends AccountCompanyDetail> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountCompanyDetail(PathMetadata metadata) {
        super(AccountCompanyDetail.class, metadata);
    }

}

