package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QViewPartnerStudio is a Querydsl query type for ViewPartnerStudio
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QViewPartnerStudio extends EntityPathBase<ViewPartnerStudio> {

    private static final long serialVersionUID = 1731981741L;

    public static final QViewPartnerStudio viewPartnerStudio = new QViewPartnerStudio("viewPartnerStudio");

    public final StringPath addr1 = createString("addr1");

    public final StringPath addr2 = createString("addr2");

    public final StringPath city = createString("city");

    public final StringPath companyType = createString("companyType");

    public final StringPath locationCd = createString("locationCd");

    public final StringPath locationCdName = createString("locationCdName");

    public final NumberPath<Long> mappingProfileId = createNumber("mappingProfileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final StringPath name = createString("name");

    public final StringPath postCd = createString("postCd");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath region = createString("region");

    public final StringPath type = createString("type");

    public QViewPartnerStudio(String variable) {
        super(ViewPartnerStudio.class, forVariable(variable));
    }

    public QViewPartnerStudio(Path<? extends ViewPartnerStudio> path) {
        super(path.getType(), path.getMetadata());
    }

    public QViewPartnerStudio(PathMetadata metadata) {
        super(ViewPartnerStudio.class, metadata);
    }

}

