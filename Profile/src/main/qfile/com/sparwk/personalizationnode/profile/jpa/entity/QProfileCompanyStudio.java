package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompanyStudio is a Querydsl query type for ProfileCompanyStudio
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompanyStudio extends EntityPathBase<ProfileCompanyStudio> {

    private static final long serialVersionUID = -2036040330L;

    public static final QProfileCompanyStudio profileCompanyStudio = new QProfileCompanyStudio("profileCompanyStudio");

    public final StringPath addr1 = createString("addr1");

    public final StringPath addr2 = createString("addr2");

    public final StringPath businessLocationCd = createString("businessLocationCd");

    public final StringPath city = createString("city");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> ownerId = createNumber("ownerId", Long.class);

    public final StringPath postCd = createString("postCd");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final StringPath region = createString("region");

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath skipYn = createString("skipYn");

    public final NumberPath<Long> studioId = createNumber("studioId", Long.class);

    public final StringPath studioName = createString("studioName");

    public QProfileCompanyStudio(String variable) {
        super(ProfileCompanyStudio.class, forVariable(variable));
    }

    public QProfileCompanyStudio(Path<? extends ProfileCompanyStudio> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompanyStudio(PathMetadata metadata) {
        super(ProfileCompanyStudio.class, metadata);
    }

}

