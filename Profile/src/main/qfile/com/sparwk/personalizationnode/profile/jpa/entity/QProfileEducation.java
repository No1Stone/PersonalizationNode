package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileEducation is a Querydsl query type for ProfileEducation
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileEducation extends EntityPathBase<ProfileEducation> {

    private static final long serialVersionUID = 1089076715L;

    public static final QProfileEducation profileEducation = new QProfileEducation("profileEducation");

    public final StringPath eduCourseNm = createString("eduCourseNm");

    public final StringPath eduEndDt = createString("eduEndDt");

    public final StringPath eduOrganizationNm = createString("eduOrganizationNm");

    public final NumberPath<Long> eduSeq = createNumber("eduSeq", Long.class);

    public final StringPath eduStartDt = createString("eduStartDt");

    public final StringPath filePath = createString("filePath");

    public final StringPath locationCityCountryCd = createString("locationCityCountryCd");

    public final StringPath locationCityNm = createString("locationCityNm");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath presentYn = createString("presentYn");

    public final NumberPath<Long> profileEduSeq = createNumber("profileEduSeq", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QProfileEducation(String variable) {
        super(ProfileEducation.class, forVariable(variable));
    }

    public QProfileEducation(Path<? extends ProfileEducation> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileEducation(PathMetadata metadata) {
        super(ProfileEducation.class, metadata);
    }

}

