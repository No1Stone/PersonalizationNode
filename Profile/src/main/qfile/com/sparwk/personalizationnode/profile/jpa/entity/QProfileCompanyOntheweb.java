package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompanyOntheweb is a Querydsl query type for ProfileCompanyOntheweb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompanyOntheweb extends EntityPathBase<ProfileCompanyOntheweb> {

    private static final long serialVersionUID = -1861042302L;

    public static final QProfileCompanyOntheweb profileCompanyOntheweb = new QProfileCompanyOntheweb("profileCompanyOntheweb");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath snsTypeCd = createString("snsTypeCd");

    public final StringPath snsUrl = createString("snsUrl");

    public final StringPath useYn = createString("useYn");

    public QProfileCompanyOntheweb(String variable) {
        super(ProfileCompanyOntheweb.class, forVariable(variable));
    }

    public QProfileCompanyOntheweb(Path<? extends ProfileCompanyOntheweb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompanyOntheweb(PathMetadata metadata) {
        super(ProfileCompanyOntheweb.class, metadata);
    }

}

