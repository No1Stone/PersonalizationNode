package com.sparwk.personalizationnode.profile.jpa.entity.account;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAccount is a Querydsl query type for Account
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccount extends EntityPathBase<Account> {

    private static final long serialVersionUID = -1612224512L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAccount account = new QAccount("account");

    public final StringPath accntEmail = createString("accntEmail");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath accntLoginActiveYn = createString("accntLoginActiveYn");

    public final StringPath accntPass = createString("accntPass");

    public final NumberPath<Long> accntRepositoryId = createNumber("accntRepositoryId", Long.class);

    public final StringPath accntTypeCd = createString("accntTypeCd");

    public final QAccountCompanyDetail accountCompanyDetail;

    public final ListPath<AccountCompanyLocation, QAccountCompanyLocation> accountCompanyLocation = this.<AccountCompanyLocation, QAccountCompanyLocation>createList("accountCompanyLocation", AccountCompanyLocation.class, QAccountCompanyLocation.class, PathInits.DIRECT2);

    public final ListPath<AccountCompanyType, QAccountCompanyType> accountCompanyType = this.<AccountCompanyType, QAccountCompanyType>createList("accountCompanyType", AccountCompanyType.class, QAccountCompanyType.class, PathInits.DIRECT2);

    public final StringPath countryCd = createString("countryCd");

    public final NumberPath<Long> lastUseProfileId = createNumber("lastUseProfileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath personalInfoCollectionYn = createString("personalInfoCollectionYn");

    public final StringPath phoneNumber = createString("phoneNumber");

    public final ListPath<com.sparwk.personalizationnode.profile.jpa.entity.Profile, com.sparwk.personalizationnode.profile.jpa.entity.QProfile> profile = this.<com.sparwk.personalizationnode.profile.jpa.entity.Profile, com.sparwk.personalizationnode.profile.jpa.entity.QProfile>createList("profile", com.sparwk.personalizationnode.profile.jpa.entity.Profile.class, com.sparwk.personalizationnode.profile.jpa.entity.QProfile.class, PathInits.DIRECT2);

    public final ListPath<com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompany, com.sparwk.personalizationnode.profile.jpa.entity.QProfileCompany> profileCompany = this.<com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompany, com.sparwk.personalizationnode.profile.jpa.entity.QProfileCompany>createList("profileCompany", com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompany.class, com.sparwk.personalizationnode.profile.jpa.entity.QProfileCompany.class, PathInits.DIRECT2);

    public final StringPath provideInfoTosparwkYn = createString("provideInfoTosparwkYn");

    public final StringPath provideInfoTothirdYn = createString("provideInfoTothirdYn");

    public final StringPath receiveMarketingInfoYn = createString("receiveMarketingInfoYn");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath transferInfoToabroadYn = createString("transferInfoToabroadYn");

    public final StringPath useYn = createString("useYn");

    public final StringPath verifyPhoneYn = createString("verifyPhoneYn");

    public final StringPath verifyYn = createString("verifyYn");

    public QAccount(String variable) {
        this(Account.class, forVariable(variable), INITS);
    }

    public QAccount(Path<? extends Account> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QAccount(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QAccount(PathMetadata metadata, PathInits inits) {
        this(Account.class, metadata, inits);
    }

    public QAccount(Class<? extends Account> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.accountCompanyDetail = inits.isInitialized("accountCompanyDetail") ? new QAccountCompanyDetail(forProperty("accountCompanyDetail")) : null;
    }

}

