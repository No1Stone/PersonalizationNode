package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminSnsCode is a Querydsl query type for AdminSnsCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminSnsCode extends EntityPathBase<AdminSnsCode> {

    private static final long serialVersionUID = -1621764245L;

    public static final QAdminSnsCode adminSnsCode = new QAdminSnsCode("adminSnsCode");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath snsCd = createString("snsCd");

    public final StringPath snsIconUrl = createString("snsIconUrl");

    public final StringPath snsName = createString("snsName");

    public final NumberPath<Long> snsSeq = createNumber("snsSeq", Long.class);

    public final StringPath useYn = createString("useYn");

    public QAdminSnsCode(String variable) {
        super(AdminSnsCode.class, forVariable(variable));
    }

    public QAdminSnsCode(Path<? extends AdminSnsCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminSnsCode(PathMetadata metadata) {
        super(AdminSnsCode.class, metadata);
    }

}

