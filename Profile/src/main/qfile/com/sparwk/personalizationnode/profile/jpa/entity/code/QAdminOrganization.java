package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminOrganization is a Querydsl query type for AdminOrganization
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminOrganization extends EntityPathBase<AdminOrganization> {

    private static final long serialVersionUID = 833616141L;

    public static final QAdminOrganization adminOrganization = new QAdminOrganization("adminOrganization");

    public final StringPath countryCd = createString("countryCd");

    public final StringPath description = createString("description");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> orgCd = createNumber("orgCd", Long.class);

    public final StringPath orgName = createString("orgName");

    public final StringPath orgType = createString("orgType");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath useYn = createString("useYn");

    public QAdminOrganization(String variable) {
        super(AdminOrganization.class, forVariable(variable));
    }

    public QAdminOrganization(Path<? extends AdminOrganization> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminOrganization(PathMetadata metadata) {
        super(AdminOrganization.class, metadata);
    }

}

