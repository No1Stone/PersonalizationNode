package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileGroupOntehweb is a Querydsl query type for ProfileGroupOntehweb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileGroupOntehweb extends EntityPathBase<ProfileGroupOntehweb> {

    private static final long serialVersionUID = 1414435550L;

    public static final QProfileGroupOntehweb profileGroupOntehweb = new QProfileGroupOntehweb("profileGroupOntehweb");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath snsTypeCd = createString("snsTypeCd");

    public final StringPath snsUrl = createString("snsUrl");

    public final StringPath useYn = createString("useYn");

    public QProfileGroupOntehweb(String variable) {
        super(ProfileGroupOntehweb.class, forVariable(variable));
    }

    public QProfileGroupOntehweb(Path<? extends ProfileGroupOntehweb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileGroupOntehweb(PathMetadata metadata) {
        super(ProfileGroupOntehweb.class, metadata);
    }

}

