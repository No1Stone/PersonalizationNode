package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileGroupTimezone is a Querydsl query type for ProfileGroupTimezone
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileGroupTimezone extends EntityPathBase<ProfileGroupTimezone> {

    private static final long serialVersionUID = 1196126299L;

    public static final QProfileGroupTimezone profileGroupTimezone = new QProfileGroupTimezone("profileGroupTimezone");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> profileTimezoneBoxNum = createNumber("profileTimezoneBoxNum", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath setTimeAutoYn = createString("setTimeAutoYn");

    public final NumberPath<Long> timezoneSeq = createNumber("timezoneSeq", Long.class);

    public QProfileGroupTimezone(String variable) {
        super(ProfileGroupTimezone.class, forVariable(variable));
    }

    public QProfileGroupTimezone(Path<? extends ProfileGroupTimezone> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileGroupTimezone(PathMetadata metadata) {
        super(ProfileGroupTimezone.class, metadata);
    }

}

