package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompanyPartner is a Querydsl query type for ProfileCompanyPartner
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompanyPartner extends EntityPathBase<ProfileCompanyPartner> {

    private static final long serialVersionUID = -1901495000L;

    public static final QProfileCompanyPartner profileCompanyPartner = new QProfileCompanyPartner("profileCompanyPartner");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> partnerId = createNumber("partnerId", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath relationStatus = createString("relationStatus");

    public final StringPath relationType = createString("relationType");

    public QProfileCompanyPartner(String variable) {
        super(ProfileCompanyPartner.class, forVariable(variable));
    }

    public QProfileCompanyPartner(Path<? extends ProfileCompanyPartner> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompanyPartner(PathMetadata metadata) {
        super(ProfileCompanyPartner.class, metadata);
    }

}

