package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminSongDetailCode is a Querydsl query type for AdminSongDetailCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminSongDetailCode extends EntityPathBase<AdminSongDetailCode> {

    private static final long serialVersionUID = 229073453L;

    public static final QAdminSongDetailCode adminSongDetailCode = new QAdminSongDetailCode("adminSongDetailCode");

    public final StringPath dcode = createString("dcode");

    public final StringPath description = createString("description");

    public final StringPath format = createString("format");

    public final NumberPath<Long> hit = createNumber("hit", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath pcode = createString("pcode");

    public final StringPath popularYn = createString("popularYn");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final NumberPath<Long> songDetailCodeSeq = createNumber("songDetailCodeSeq", Long.class);

    public final NumberPath<Integer> sortIndex = createNumber("sortIndex", Integer.class);

    public final StringPath useYn = createString("useYn");

    public final StringPath val = createString("val");

    public QAdminSongDetailCode(String variable) {
        super(AdminSongDetailCode.class, forVariable(variable));
    }

    public QAdminSongDetailCode(Path<? extends AdminSongDetailCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminSongDetailCode(PathMetadata metadata) {
        super(AdminSongDetailCode.class, metadata);
    }

}

