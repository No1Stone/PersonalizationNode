package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminCountryCode is a Querydsl query type for AdminCountryCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminCountryCode extends EntityPathBase<AdminCountryCode> {

    private static final long serialVersionUID = 728551305L;

    public static final QAdminCountryCode adminCountryCode = new QAdminCountryCode("adminCountryCode");

    public final StringPath continentCode = createString("continentCode");

    public final StringPath country = createString("country");

    public final StringPath countryCd = createString("countryCd");

    public final NumberPath<Long> countrySeq = createNumber("countrySeq", Long.class);

    public final StringPath description = createString("description");

    public final StringPath dial = createString("dial");

    public final StringPath iso2 = createString("iso2");

    public final StringPath iso3 = createString("iso3");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath nmr = createString("nmr");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath useYn = createString("useYn");

    public QAdminCountryCode(String variable) {
        super(AdminCountryCode.class, forVariable(variable));
    }

    public QAdminCountryCode(Path<? extends AdminCountryCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminCountryCode(PathMetadata metadata) {
        super(AdminCountryCode.class, metadata);
    }

}

