package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileSkill is a Querydsl query type for ProfileSkill
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileSkill extends EntityPathBase<ProfileSkill> {

    private static final long serialVersionUID = 548993556L;

    public static final QProfileSkill profileSkill = new QProfileSkill("profileSkill");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath skillDetailTypeCd = createString("skillDetailTypeCd");

    public final StringPath skillTypeCd = createString("skillTypeCd");

    public QProfileSkill(String variable) {
        super(ProfileSkill.class, forVariable(variable));
    }

    public QProfileSkill(Path<? extends ProfileSkill> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileSkill(PathMetadata metadata) {
        super(ProfileSkill.class, metadata);
    }

}

