package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileUseInfo is a Querydsl query type for ProfileUseInfo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileUseInfo extends EntityPathBase<ProfileUseInfo> {

    private static final long serialVersionUID = 1301138904L;

    public static final QProfileUseInfo profileUseInfo = new QProfileUseInfo("profileUseInfo");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> LastUseDt = createDateTime("LastUseDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> lastUseProfileId = createNumber("lastUseProfileId", Long.class);

    public QProfileUseInfo(String variable) {
        super(ProfileUseInfo.class, forVariable(variable));
    }

    public QProfileUseInfo(Path<? extends ProfileUseInfo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileUseInfo(PathMetadata metadata) {
        super(ProfileUseInfo.class, metadata);
    }

}

