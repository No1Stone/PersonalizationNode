package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileGender is a Querydsl query type for ProfileGender
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileGender extends EntityPathBase<ProfileGender> {

    private static final long serialVersionUID = -510018722L;

    public static final QProfileGender profileGender = new QProfileGender("profileGender");

    public final StringPath genderCd = createString("genderCd");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public QProfileGender(String variable) {
        super(ProfileGender.class, forVariable(variable));
    }

    public QProfileGender(Path<? extends ProfileGender> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileGender(PathMetadata metadata) {
        super(ProfileGender.class, metadata);
    }

}

