package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProfileCompany is a Querydsl query type for ProfileCompany
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompany extends EntityPathBase<ProfileCompany> {

    private static final long serialVersionUID = -1895004288L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProfileCompany profileCompany = new QProfileCompany("profileCompany");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath bio = createString("bio");

    public final StringPath comInfoAddress = createString("comInfoAddress");

    public final StringPath comInfoCountryCd = createString("comInfoCountryCd");

    public final StringPath comInfoEmail = createString("comInfoEmail");

    public final StringPath comInfoFound = createString("comInfoFound");

    public final StringPath comInfoLat = createString("comInfoLat");

    public final StringPath comInfoLocation = createString("comInfoLocation");

    public final StringPath comInfoLon = createString("comInfoLon");

    public final StringPath comInfoOverview = createString("comInfoOverview");

    public final StringPath comInfoPhone = createString("comInfoPhone");

    public final StringPath comInfoWebsite = createString("comInfoWebsite");

    public final StringPath headline = createString("headline");

    public final StringPath ipiNumber = createString("ipiNumber");

    public final StringPath ipiNumberVarifyYn = createString("ipiNumberVarifyYn");

    public final StringPath mattermostId = createString("mattermostId");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath profileBgdImgUrl = createString("profileBgdImgUrl");

    public final QProfileCompanyContact profileCompanyContact;

    public final StringPath profileCompanyName = createString("profileCompanyName");

    public final ListPath<ProfileCompanyOntheweb, QProfileCompanyOntheweb> profileCompanyOntheweb = this.<ProfileCompanyOntheweb, QProfileCompanyOntheweb>createList("profileCompanyOntheweb", ProfileCompanyOntheweb.class, QProfileCompanyOntheweb.class, PathInits.DIRECT2);

    public final ListPath<ProfileCompanyPartner, QProfileCompanyPartner> profileCompanyPartner = this.<ProfileCompanyPartner, QProfileCompanyPartner>createList("profileCompanyPartner", ProfileCompanyPartner.class, QProfileCompanyPartner.class, PathInits.DIRECT2);

    public final ListPath<ProfileCompanyRoster, QProfileCompanyRoster> profileCompanyRoster = this.<ProfileCompanyRoster, QProfileCompanyRoster>createList("profileCompanyRoster", ProfileCompanyRoster.class, QProfileCompanyRoster.class, PathInits.DIRECT2);

    public final ListPath<ProfileCompanyStudio, QProfileCompanyStudio> profileCompanyStudio = this.<ProfileCompanyStudio, QProfileCompanyStudio>createList("profileCompanyStudio", ProfileCompanyStudio.class, QProfileCompanyStudio.class, PathInits.DIRECT2);

    public final ListPath<ProfileCompanyTimezone, QProfileCompanyTimezone> profileCompanyTimezone = this.<ProfileCompanyTimezone, QProfileCompanyTimezone>createList("profileCompanyTimezone", ProfileCompanyTimezone.class, QProfileCompanyTimezone.class, PathInits.DIRECT2);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImgUrl = createString("profileImgUrl");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath vatNumber = createString("vatNumber");

    public final StringPath vatNumberVarifyYn = createString("vatNumberVarifyYn");

    public QProfileCompany(String variable) {
        this(ProfileCompany.class, forVariable(variable), INITS);
    }

    public QProfileCompany(Path<? extends ProfileCompany> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProfileCompany(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProfileCompany(PathMetadata metadata, PathInits inits) {
        this(ProfileCompany.class, metadata, inits);
    }

    public QProfileCompany(Class<? extends ProfileCompany> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.profileCompanyContact = inits.isInitialized("profileCompanyContact") ? new QProfileCompanyContact(forProperty("profileCompanyContact")) : null;
    }

}

