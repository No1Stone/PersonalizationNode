package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminRoleCode is a Querydsl query type for AdminRoleCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminRoleCode extends EntityPathBase<AdminRoleCode> {

    private static final long serialVersionUID = 239304797L;

    public static final QAdminRoleCode adminRoleCode = new QAdminRoleCode("adminRoleCode");

    public final StringPath code = createString("code");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final NumberPath<Long> roleCodeSeq = createNumber("roleCodeSeq", Long.class);

    public final StringPath useYn = createString("useYn");

    public final StringPath val = createString("val");

    public QAdminRoleCode(String variable) {
        super(AdminRoleCode.class, forVariable(variable));
    }

    public QAdminRoleCode(Path<? extends AdminRoleCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminRoleCode(PathMetadata metadata) {
        super(AdminRoleCode.class, metadata);
    }

}

