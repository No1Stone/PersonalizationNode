package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminCommonDetailCode is a Querydsl query type for AdminCommonDetailCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminCommonDetailCode extends EntityPathBase<AdminCommonDetailCode> {

    private static final long serialVersionUID = 1493374211L;

    public static final QAdminCommonDetailCode adminCommonDetailCode = new QAdminCommonDetailCode("adminCommonDetailCode");

    public final NumberPath<Long> commonDetailCodeSeq = createNumber("commonDetailCodeSeq", Long.class);

    public final StringPath dcode = createString("dcode");

    public final StringPath description = createString("description");

    public final NumberPath<Integer> hit = createNumber("hit", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath pcode = createString("pcode");

    public final StringPath popularYn = createString("popularYn");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final NumberPath<Integer> sortIndex = createNumber("sortIndex", Integer.class);

    public final StringPath useYn = createString("useYn");

    public final StringPath val = createString("val");

    public QAdminCommonDetailCode(String variable) {
        super(AdminCommonDetailCode.class, forVariable(variable));
    }

    public QAdminCommonDetailCode(Path<? extends AdminCommonDetailCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminCommonDetailCode(PathMetadata metadata) {
        super(AdminCommonDetailCode.class, metadata);
    }

}

