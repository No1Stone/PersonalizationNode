package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileTimezone is a Querydsl query type for ProfileTimezone
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileTimezone extends EntityPathBase<ProfileTimezone> {

    private static final long serialVersionUID = -92054186L;

    public static final QProfileTimezone profileTimezone = new QProfileTimezone("profileTimezone");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> profileTimezoneBoxNum = createNumber("profileTimezoneBoxNum", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath setTimeAutoYn = createString("setTimeAutoYn");

    public final NumberPath<Long> timezoneSeq = createNumber("timezoneSeq", Long.class);

    public QProfileTimezone(String variable) {
        super(ProfileTimezone.class, forVariable(variable));
    }

    public QProfileTimezone(Path<? extends ProfileTimezone> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileTimezone(PathMetadata metadata) {
        super(ProfileTimezone.class, metadata);
    }

}

