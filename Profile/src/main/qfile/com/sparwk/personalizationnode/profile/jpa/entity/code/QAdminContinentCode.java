package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminContinentCode is a Querydsl query type for AdminContinentCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminContinentCode extends EntityPathBase<AdminContinentCode> {

    private static final long serialVersionUID = -736836313L;

    public static final QAdminContinentCode adminContinentCode = new QAdminContinentCode("adminContinentCode");

    public final StringPath code = createString("code");

    public final StringPath continent = createString("continent");

    public final StringPath continentCd = createString("continentCd");

    public final NumberPath<Long> continentSeq = createNumber("continentSeq", Long.class);

    public final StringPath description = createString("description");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath useYn = createString("useYn");

    public QAdminContinentCode(String variable) {
        super(AdminContinentCode.class, forVariable(variable));
    }

    public QAdminContinentCode(Path<? extends AdminContinentCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminContinentCode(PathMetadata metadata) {
        super(AdminContinentCode.class, metadata);
    }

}

