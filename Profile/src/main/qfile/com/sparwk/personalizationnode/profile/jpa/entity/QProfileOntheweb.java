package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileOntheweb is a Querydsl query type for ProfileOntheweb
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileOntheweb extends EntityPathBase<ProfileOntheweb> {

    private static final long serialVersionUID = 128936255L;

    public static final QProfileOntheweb profileOntheweb = new QProfileOntheweb("profileOntheweb");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath snsTypeCd = createString("snsTypeCd");

    public final StringPath snsUrl = createString("snsUrl");

    public final StringPath useYn = createString("useYn");

    public QProfileOntheweb(String variable) {
        super(ProfileOntheweb.class, forVariable(variable));
    }

    public QProfileOntheweb(Path<? extends ProfileOntheweb> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileOntheweb(PathMetadata metadata) {
        super(ProfileOntheweb.class, metadata);
    }

}

