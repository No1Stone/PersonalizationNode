package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminCurrencyCode is a Querydsl query type for AdminCurrencyCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminCurrencyCode extends EntityPathBase<AdminCurrencyCode> {

    private static final long serialVersionUID = 659467640L;

    public static final QAdminCurrencyCode adminCurrencyCode = new QAdminCurrencyCode("adminCurrencyCode");

    public final StringPath country = createString("country");

    public final StringPath currency = createString("currency");

    public final StringPath currencyCd = createString("currencyCd");

    public final NumberPath<Long> currencySeq = createNumber("currencySeq", Long.class);

    public final StringPath isoCode = createString("isoCode");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath symbol = createString("symbol");

    public final StringPath useYn = createString("useYn");

    public QAdminCurrencyCode(String variable) {
        super(AdminCurrencyCode.class, forVariable(variable));
    }

    public QAdminCurrencyCode(Path<? extends AdminCurrencyCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminCurrencyCode(PathMetadata metadata) {
        super(AdminCurrencyCode.class, metadata);
    }

}

