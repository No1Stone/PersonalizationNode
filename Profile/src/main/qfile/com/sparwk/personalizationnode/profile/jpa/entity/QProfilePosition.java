package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfilePosition is a Querydsl query type for ProfilePosition
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfilePosition extends EntityPathBase<ProfilePosition> {

    private static final long serialVersionUID = -1562988922L;

    public static final QProfilePosition profilePosition = new QProfilePosition("profilePosition");

    public final StringPath anrYn = createString("anrYn");

    public final StringPath artistYn = createString("artistYn");

    public final NumberPath<Long> companyProfileId = createNumber("companyProfileId", Long.class);

    public final StringPath companyVerifyYn = createString("companyVerifyYn");

    public final StringPath creatorYn = createString("creatorYn");

    public final StringPath deptRoleInfo = createString("deptRoleInfo");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath primaryYn = createString("primaryYn");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> profilePositionSeq = createNumber("profilePositionSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QProfilePosition(String variable) {
        super(ProfilePosition.class, forVariable(variable));
    }

    public QProfilePosition(Path<? extends ProfilePosition> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfilePosition(PathMetadata metadata) {
        super(ProfilePosition.class, metadata);
    }

}

