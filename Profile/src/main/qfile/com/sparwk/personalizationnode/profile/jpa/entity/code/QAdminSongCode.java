package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminSongCode is a Querydsl query type for AdminSongCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminSongCode extends EntityPathBase<AdminSongCode> {

    private static final long serialVersionUID = 2041220476L;

    public static final QAdminSongCode adminSongCode = new QAdminSongCode("adminSongCode");

    public final StringPath code = createString("code");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final NumberPath<Long> songCodeSeq = createNumber("songCodeSeq", Long.class);

    public final StringPath useYn = createString("useYn");

    public final StringPath val = createString("val");

    public QAdminSongCode(String variable) {
        super(AdminSongCode.class, forVariable(variable));
    }

    public QAdminSongCode(Path<? extends AdminSongCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminSongCode(PathMetadata metadata) {
        super(AdminSongCode.class, metadata);
    }

}

