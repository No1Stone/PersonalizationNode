package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileLanguage is a Querydsl query type for ProfileLanguage
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileLanguage extends EntityPathBase<ProfileLanguage> {

    private static final long serialVersionUID = 370583733L;

    public static final QProfileLanguage profileLanguage = new QProfileLanguage("profileLanguage");

    public final StringPath languageCd = createString("languageCd");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public QProfileLanguage(String variable) {
        super(ProfileLanguage.class, forVariable(variable));
    }

    public QProfileLanguage(Path<? extends ProfileLanguage> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileLanguage(PathMetadata metadata) {
        super(ProfileLanguage.class, metadata);
    }

}

