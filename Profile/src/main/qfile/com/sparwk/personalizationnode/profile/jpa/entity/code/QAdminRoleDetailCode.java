package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminRoleDetailCode is a Querydsl query type for AdminRoleDetailCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminRoleDetailCode extends EntityPathBase<AdminRoleDetailCode> {

    private static final long serialVersionUID = -224597938L;

    public static final QAdminRoleDetailCode adminRoleDetailCode = new QAdminRoleDetailCode("adminRoleDetailCode");

    public final StringPath abbeviation = createString("abbeviation");

    public final StringPath creditRoleYn = createString("creditRoleYn");

    public final StringPath customCode = createString("customCode");

    public final StringPath dcode = createString("dcode");

    public final StringPath description = createString("description");

    public final StringPath format = createString("format");

    public final NumberPath<Long> hit = createNumber("hit", Long.class);

    public final StringPath matchingRoleYn = createString("matchingRoleYn");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath pcode = createString("pcode");

    public final StringPath popularYn = createString("popularYn");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath role = createString("role");

    public final NumberPath<Long> roleDetailCodeSeq = createNumber("roleDetailCodeSeq", Long.class);

    public final StringPath splitSheet_roleYn = createString("splitSheet_roleYn");

    public final StringPath useYn = createString("useYn");

    public QAdminRoleDetailCode(String variable) {
        super(AdminRoleDetailCode.class, forVariable(variable));
    }

    public QAdminRoleDetailCode(Path<? extends AdminRoleDetailCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminRoleDetailCode(PathMetadata metadata) {
        super(AdminRoleDetailCode.class, metadata);
    }

}

