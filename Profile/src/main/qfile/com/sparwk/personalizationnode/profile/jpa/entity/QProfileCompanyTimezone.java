package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompanyTimezone is a Querydsl query type for ProfileCompanyTimezone
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompanyTimezone extends EntityPathBase<ProfileCompanyTimezone> {

    private static final long serialVersionUID = -2082032743L;

    public static final QProfileCompanyTimezone profileCompanyTimezone = new QProfileCompanyTimezone("profileCompanyTimezone");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> profileTimezoneBoxNum = createNumber("profileTimezoneBoxNum", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath setTimeAutoYn = createString("setTimeAutoYn");

    public final NumberPath<Long> timezoneSeq = createNumber("timezoneSeq", Long.class);

    public QProfileCompanyTimezone(String variable) {
        super(ProfileCompanyTimezone.class, forVariable(variable));
    }

    public QProfileCompanyTimezone(Path<? extends ProfileCompanyTimezone> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompanyTimezone(PathMetadata metadata) {
        super(ProfileCompanyTimezone.class, metadata);
    }

}

