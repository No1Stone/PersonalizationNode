package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminAnrCode is a Querydsl query type for AdminAnrCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminAnrCode extends EntityPathBase<AdminAnrCode> {

    private static final long serialVersionUID = -417884840L;

    public static final QAdminAnrCode adminAnrCode = new QAdminAnrCode("adminAnrCode");

    public final StringPath anrCd = createString("anrCd");

    public final StringPath anrGroupCd = createString("anrGroupCd");

    public final NumberPath<Long> anrSeq = createNumber("anrSeq", Long.class);

    public final StringPath anrServiceName = createString("anrServiceName");

    public final StringPath description = createString("description");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath useYn = createString("useYn");

    public QAdminAnrCode(String variable) {
        super(AdminAnrCode.class, forVariable(variable));
    }

    public QAdminAnrCode(Path<? extends AdminAnrCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminAnrCode(PathMetadata metadata) {
        super(AdminAnrCode.class, metadata);
    }

}

