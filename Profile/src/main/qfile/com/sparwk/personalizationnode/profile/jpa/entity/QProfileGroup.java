package com.sparwk.personalizationnode.profile.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProfileGroup is a Querydsl query type for ProfileGroup
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileGroup extends EntityPathBase<ProfileGroup> {

    private static final long serialVersionUID = 538125890L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProfileGroup profileGroup = new QProfileGroup("profileGroup");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath headline = createString("headline");

    public final StringPath mattermostId = createString("mattermostId");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath overView = createString("overView");

    public final StringPath profileBgdImgUrl = createString("profileBgdImgUrl");

    public final QProfileGroupContact profileGroupContact;

    public final StringPath profileGroupName = createString("profileGroupName");

    public final ListPath<ProfileGroupOntehweb, QProfileGroupOntehweb> profileGroupOntehweb = this.<ProfileGroupOntehweb, QProfileGroupOntehweb>createList("profileGroupOntehweb", ProfileGroupOntehweb.class, QProfileGroupOntehweb.class, PathInits.DIRECT2);

    public final ListPath<ProfileGroupTimezone, QProfileGroupTimezone> profileGroupTimezone = this.<ProfileGroupTimezone, QProfileGroupTimezone>createList("profileGroupTimezone", ProfileGroupTimezone.class, QProfileGroupTimezone.class, PathInits.DIRECT2);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImgUrl = createString("profileImgUrl");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath webSite = createString("webSite");

    public QProfileGroup(String variable) {
        this(ProfileGroup.class, forVariable(variable), INITS);
    }

    public QProfileGroup(Path<? extends ProfileGroup> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProfileGroup(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProfileGroup(PathMetadata metadata, PathInits inits) {
        this(ProfileGroup.class, metadata, inits);
    }

    public QProfileGroup(Class<? extends ProfileGroup> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.profileGroupContact = inits.isInitialized("profileGroupContact") ? new QProfileGroupContact(forProperty("profileGroupContact")) : null;
    }

}

