package com.sparwk.personalizationnode.profile.jpa.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminLanguageCode is a Querydsl query type for AdminLanguageCode
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminLanguageCode extends EntityPathBase<AdminLanguageCode> {

    private static final long serialVersionUID = 1857312063L;

    public static final QAdminLanguageCode adminLanguageCode = new QAdminLanguageCode("adminLanguageCode");

    public final StringPath iso6391 = createString("iso6391");

    public final StringPath iso6392b = createString("iso6392b");

    public final StringPath iso6392t = createString("iso6392t");

    public final StringPath iso6393 = createString("iso6393");

    public final StringPath language = createString("language");

    public final StringPath languageCd = createString("languageCd");

    public final StringPath languageFamily = createString("languageFamily");

    public final NumberPath<Long> languageSeq = createNumber("languageSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath nativeLanguage = createString("nativeLanguage");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath useYn = createString("useYn");

    public QAdminLanguageCode(String variable) {
        super(AdminLanguageCode.class, forVariable(variable));
    }

    public QAdminLanguageCode(Path<? extends AdminLanguageCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminLanguageCode(PathMetadata metadata) {
        super(AdminLanguageCode.class, metadata);
    }

}

