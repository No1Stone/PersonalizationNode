package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.profilecreate;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileLanguageRequest {

    @Schema(description = "언어 코드")
    private String languageCd;//ok

}
