package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyTimezone;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyTimezone.dto.ProfileCompanyTimezoneRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyTimezone.dto.ProfileCompanyTimezoneRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyTimezoneBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileTimezoneBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyTimezoneRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileTimezoneRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileCompanyTimezoneService {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfileCompanyTimezoneRepository profileCompanyTimezoneRepository;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyTimezoneService.class);
    @Autowired
    private HttpServletRequest httpServletRequest;

    public Response ProfileCompanyTimezoneSaveResponseService(ProfileCompanyTimezoneRequestList dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO - {}", userInfoDTO.getAccountId());
        logger.info("userInfoDTO - {}", userInfoDTO.getLastUseProfileId());
        int resultCount = 0;
        logger.info("asdasdasdasdsadasd - {}",dto.getProfileCompanyTimezoneRequestList().toString());
        for (ProfileCompanyTimezoneRequest e : dto.getProfileCompanyTimezoneRequestList()) {
            logger.info(" - {}",e.getSetTimeAutoYn());
            logger.info(" - {}",e.getTimezoneSeq());
            ProfileCompanyTimezoneBaseDTO setBaseDTO = ProfileCompanyTimezoneBaseDTO.builder()
                    .profileId(e.getProfileId())
                    .profileTimezoneBoxNum(e.getProfileTimezoneBoxNum())
                    .timezoneSeq(e.getTimezoneSeq())
                    .setTimeAutoYn(e.getSetTimeAutoYn())
                    .modUsr(userInfoDTO.getAccountId())
                    .modDt(LocalDateTime.now())
                    .regUsr(userInfoDTO.getAccountId())
                    .regDt(LocalDateTime.now())
                    .build();
            logger.info("aaaaaaa  -{}",e.toString());
            profileServiceRepository.ProfileCompanyTimezoneSaveService(setBaseDTO);
            resultCount++;
        }
        Response res = new Response();
        res.setResult(resultCount);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response ProfileCompanyTimezoneSelectResponseService(Long profileId) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO---{}", userInfoDTO.getAccountId());
        List<ProfileCompanyTimezoneBaseDTO> onthewebBaseDTOList =
                profileCompanyTimezoneRepository.findByProfileId(profileId)
                        .stream().map(e -> modelMapper.map(e, ProfileCompanyTimezoneBaseDTO.class)).collect(Collectors.toList());
        Response res = new Response();
        res.setResult(onthewebBaseDTOList);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
