package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto;

import com.sparwk.personalizationnode.profile.jpa.entity.*;
import com.sparwk.personalizationnode.profile.jpa.entity.account.AccountCompanyDetail;
import com.sparwk.personalizationnode.profile.jpa.entity.account.AccountCompanyLocation;
import com.sparwk.personalizationnode.profile.jpa.entity.account.AccountCompanyType;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyResponse {

    private Long profileId;
    private Long accntId;
    private String profileCompanyName;
    private String mattermostId;
    private String profileImgUrl;
    private String profileBgdImgUrl;
    private String headline;
    private String bio;
    private String comInfoOverview;
    private String comInfoWebsite;
    private String comInfoPhone;
    private String comInfoEmail;
    private String comInfoFound;
    private String comInfoCountryCd;
    private String comInfoAddress;
    private String comInfoLat;
    private String comInfoLon;
    private String comInfoLocation;

    private String ipiNumber;
    private String ipiNumberVarifyYn;
    private String vatNumber;
    private String vatNumberVarifyYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
    private String companyEmail;
    private String contactEmail;
    private String countryCd;
    private String phoneCountryCd;
    //    private List<ProfileCompanyPartner> profileCompanyPartner;
//    private List<ProfileCompanyRoster> profileCompanyRoster;
//    private List<ProfileCompanyStudio> profileCompanyStudio;
    private List<ProfileCompanyOntheweb> profileCompanyOntheweb;
    //    private List<ProfileCompanyTimezone> profileCompanyTimezone;
    private List<AccountCompanyType> accountCompanyType;
    private AccountCompanyDetail accountCompanyDetail;
    private List<AccountCompanyLocation> accountCompanyLocation;
    private ProfileCompanyContact profileCompanyContact;

}
