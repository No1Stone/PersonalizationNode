package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGroupContactDescriptonUpdateRequest {
    private Long profileId ;
    private String profileContactDescription ;


}
