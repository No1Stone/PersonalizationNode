package com.sparwk.personalizationnode.profile.biz.v1.ProfileGender;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileGender.dto.ProfileGenderDto;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGender.dto.ProfileGenderDtoList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileGenderBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileGenderRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileGenderService {

    private Logger logger = LoggerFactory.getLogger(ProfileGenderService.class);

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfileGenderRepository profileGenderRepository;
    @Autowired
    private HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;


    public Response ProfileGenderSave(ProfileGenderDto dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        List<ProfileGenderBaseDTO> resultGender = new ArrayList<>();

        String[] genders = dto.getGenderCd().trim().split(",");
        for (String e : genders) {
            dto.setGenderCd(e);
            logger.info("value check - {}", dto.toString());
            ProfileGenderBaseDTO baseDTO = modelMapper.map(dto, ProfileGenderBaseDTO.class);
            baseDTO.setProfileId(userInfoDTO.getLastUseProfileId());

            ProfileGenderBaseDTO result = profileServiceRepository
                    .ProfileGenderSaveService(baseDTO);
            resultGender.add(result);
            res.setResult(result);
        }


        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGenderSelect(Long profileId) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        List<ProfileGenderBaseDTO> result = profileGenderRepository.findByProfileId(userInfoDTO.getLastUseProfileId())
                .stream().map(e -> modelMapper.map(e, ProfileGenderBaseDTO.class))
                .collect(Collectors.toList());
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGenderDelete(ProfileGenderDto dto) {

        profileGenderRepository.deleteByProfileIdAndGenderCd(dto.getProfileId(), dto.getGenderCd());

        Response res = new Response();
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGenderSaveListService(ProfileGenderDtoList dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        //이놈은 기존데이터를 재사용할 필요가 없음
        //그냥 지웟다가 싹다다시입력
        List<ProfileGenderBaseDTO> delList = profileGenderRepository.findByProfileId(userInfoDTO.getLastUseProfileId())
                .stream().map(e -> modelMapper.map(e, ProfileGenderBaseDTO.class)).collect(Collectors.toList());
        //기존다삭제
        for (ProfileGenderBaseDTO f : delList) {
            if (f.getProfileId() == null) {
                res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }
            profileGenderRepository.deleteByProfileIdAndGenderCd(f.getProfileId(), f.getGenderCd());
        }
        // 신규 다입력
        for (ProfileGenderDto f : dto.getProfileGenderDtoList()) {
            ProfileGenderBaseDTO baseDTO = ProfileGenderBaseDTO.builder()
                    .profileId(userInfoDTO.getLastUseProfileId())
                    .genderCd(f.getGenderCd())
                    .build();
            profileServiceRepository.ProfileGenderSaveService(baseDTO);
        }
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
