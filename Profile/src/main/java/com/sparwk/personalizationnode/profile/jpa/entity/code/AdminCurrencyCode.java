package com.sparwk.personalizationnode.profile.jpa.entity.code;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_currency_code")
public class AdminCurrencyCode {

    @Id
    @Column(name = "currency_seq", nullable = true)
    private Long currencySeq;
    @Column(name = "currency", nullable = true)
    private String currency;
    @Column(name = "iso_code", nullable = true)
    private String isoCode;
    @Column(name = "country", nullable = true)
    private String country;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "symbol", nullable = true)
    private String symbol;
    @Column(name = "currency_cd", nullable = true)
    private String currencyCd;

    @Builder
    AdminCurrencyCode(
            Long currencySeq,
            String currency,
            String isoCode,
            String country,
            String useYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt,
            String symbol,
            String currencyCd
    ) {
        this.currencySeq = currencySeq;
        this.currency = currency;
        this.isoCode = isoCode;
        this.country = country;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
        this.symbol = symbol;
        this.currencyCd = currencyCd;

    }
}
