package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel;

import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileGenderId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGenderResponse {
    private String genderCd;
    private Long profileId;

//    @Builder
//    ProfileGenderResponse(
//            Long profileId,
//            String genderCd
//    ) {
//        this.profileId = profileId;
//        this.genderCd = genderCd;
//    }

}
