package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CompanyProfileSearchRequest {

    private int sliceIndex;
    private int size;
    private String companyName;


}
