package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCareerBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.QProfileCareer;
import com.sparwk.personalizationnode.profile.jpa.entity.QProfileCompany;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class ProfileCompanyRepositoryDynamicImpl implements ProfileCompanyRepositoryDynamic {

    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyRepositoryDynamicImpl.class);
    private QProfileCompany qProfileCompany = QProfileCompany.profileCompany;

    private ProfileCompanyRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long ProfileCompanyRepositoryDynamicUpdate(ProfileCompanyBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProfileCompany);

        if (entity.getAccntId() != null) {
            jpaUpdateClause.set(qProfileCompany.accntId, entity.getAccntId());
        }
        if (entity.getMattermostId() == null || entity.getMattermostId().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.mattermostId, entity.getMattermostId());
        }
        if (entity.getProfileImgUrl() == null || entity.getProfileImgUrl().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.profileImgUrl, entity.getProfileImgUrl());
        }
        if (entity.getProfileBgdImgUrl() == null || entity.getProfileBgdImgUrl().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.profileBgdImgUrl, entity.getProfileBgdImgUrl());
        }
        if (entity.getProfileCompanyName() == null || entity.getProfileCompanyName().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.profileCompanyName, entity.getProfileCompanyName());
        }
        if (entity.getHeadline() == null || entity.getHeadline().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.headline, entity.getHeadline());
        }
        if (entity.getBio() == null || entity.getBio().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.bio, entity.getBio());
        }
        if (entity.getComInfoOverview() == null || entity.getComInfoOverview().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.comInfoOverview, entity.getComInfoOverview());
        }
        if (entity.getComInfoWebsite() == null || entity.getComInfoWebsite().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.comInfoWebsite, entity.getComInfoWebsite());
        }
        if (entity.getComInfoPhone() == null || entity.getComInfoPhone().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.comInfoPhone, entity.getComInfoPhone());
        }
        if (entity.getComInfoEmail() == null || entity.getComInfoEmail().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.comInfoEmail, entity.getComInfoEmail());
        }
        if (entity.getComInfoFound() == null || entity.getComInfoFound().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.comInfoFound, entity.getComInfoFound());
        }
        if (entity.getComInfoCountryCd() == null || entity.getComInfoCountryCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.comInfoCountryCd, entity.getComInfoCountryCd());
        }
        if (entity.getComInfoAddress() == null || entity.getComInfoAddress().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.comInfoAddress, entity.getComInfoAddress());
        }
        if (entity.getComInfoLat() == null || entity.getComInfoLat().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.comInfoLat, entity.getComInfoLat());
        }
        if (entity.getComInfoLon() == null || entity.getComInfoLon().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.comInfoLon, entity.getComInfoLon());
        }


        if (entity.getIpiNumber() == null || entity.getIpiNumber().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.ipiNumber, entity.getIpiNumber());
        }


        if (entity.getIpiNumberVarifyYn() == null || entity.getIpiNumberVarifyYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.ipiNumberVarifyYn, entity.getIpiNumberVarifyYn());
        }
        if (entity.getVatNumber() == null || entity.getVatNumber().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.vatNumber, entity.getVatNumber());
        }
        if (entity.getVatNumberVarifyYn() == null || entity.getVatNumberVarifyYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.vatNumberVarifyYn, entity.getVatNumberVarifyYn());
        }

        if (entity.getComInfoLocation() == null || entity.getComInfoLocation().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompany.comInfoLocation, entity.getComInfoLocation());
        }

        if (entity.getRegUsr() != null) {
            jpaUpdateClause.set(qProfileCompany.regUsr, entity.getRegUsr());
        }
        if (entity.getRegDt() != null) {
            jpaUpdateClause.set(qProfileCompany.regDt, entity.getRegDt());
        }
        if (entity.getModUsr() != null) {
            jpaUpdateClause.set(qProfileCompany.modUsr, entity.getModUsr());
        }
        if (entity.getModDt() != null) {
            jpaUpdateClause.set(qProfileCompany.modDt, entity.getModDt());
        }
            jpaUpdateClause.where(qProfileCompany.profileId.eq(entity.getProfileId()));
        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;

    }
}
