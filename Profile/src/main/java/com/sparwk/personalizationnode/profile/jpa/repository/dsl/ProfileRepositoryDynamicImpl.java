package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.QProfile;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class ProfileRepositoryDynamicImpl implements ProfileRepositoryDynamic {

    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(ProfileRepositoryDynamicImpl.class);
    private QProfile qProfile = QProfile.profile;

    private ProfileRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long ProfileRepositoryDynamicUpdate(ProfileBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProfile);

        if (entity.getAccntId() != null) {
            jpaUpdateClause.set(qProfile.accntId, entity.getAccntId());
        }
        if (entity.getMattermostId() == null || entity.getMattermostId().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.mattermostId, entity.getMattermostId());
        }
        if (entity.getFullName() == null || entity.getFullName().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.fullName, entity.getFullName());
        }
        if (entity.getStageNameYn() == null || entity.getStageNameYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.stageNameYn, entity.getStageNameYn());
        }
        if (entity.getBthYear() == null || entity.getBthYear().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.bthYear, entity.getBthYear());
        }
        if (entity.getBthMonth() == null || entity.getBthMonth().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.bthMonth, entity.getBthMonth());
        }
        if (entity.getBthDay() == null || entity.getBthDay().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.bthDay, entity.getBthDay());
        }
        if (entity.getHireMeYn() == null || entity.getHireMeYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.hireMeYn, entity.getHireMeYn());
        }
        if (entity.getBio() == null || entity.getBio().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.bio, entity.getBio());
        }
        if (entity.getHeadline() == null || entity.getHeadline().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.headline, entity.getHeadline());
        }
        if (entity.getCurrentCityCountryCd() == null || entity.getCurrentCityCountryCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.currentCityCountryCd, entity.getCurrentCityCountryCd());
        }
        if (entity.getCurrentCityNm() == null || entity.getCurrentCityNm().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.currentCityNm, entity.getCurrentCityNm());
        }
        if (entity.getHomeTownCountryCd() == null || entity.getHomeTownCountryCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.homeTownCountryCd, entity.getHomeTownCountryCd());
        }
        if (entity.getHomeTownNm() == null || entity.getHomeTownNm().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.homeTownNm, entity.getHomeTownNm());
        }
        if (entity.getIpiInfo() == null || entity.getIpiInfo().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.ipiInfo, entity.getIpiInfo());
        }
        if (entity.getCaeInfo() == null || entity.getCaeInfo().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.caeInfo, entity.getCaeInfo());
        }
        if (entity.getIsniInfo() == null || entity.getIsniInfo().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.isniInfo, entity.getIsniInfo());
        }


        if (entity.getIpnInfo() == null || entity.getIpnInfo().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.ipnInfo, entity.getIpnInfo());
        }


        if (entity.getNroTypeCd() == null || entity.getNroTypeCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.nroTypeCd, entity.getNroTypeCd());
        }
        if (entity.getNroInfo() == null || entity.getNroInfo().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.nroInfo, entity.getNroInfo());
        }
        if (entity.getProfileImgUrl() == null || entity.getProfileImgUrl().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.profileImgUrl, entity.getProfileImgUrl());
        }
        if (entity.getProfileBgdImgUrl() == null || entity.getProfileBgdImgUrl().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.profileBgdImgUrl, entity.getProfileBgdImgUrl());
        }
        if (entity.getVerifyYn() == null || entity.getVerifyYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.verifyYn, entity.getVerifyYn());
        }
        if (entity.getUseYn() == null || entity.getUseYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.useYn, entity.getUseYn());
        }
        if (entity.getAccomplishmentsInfo() == null || entity.getAccomplishmentsInfo().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfile.accomplishmentsInfo, entity.getAccomplishmentsInfo());
        }



        if(entity.getRegUsr() != null) {jpaUpdateClause.set(qProfile.regUsr, entity.getRegUsr());}
        if(entity.getRegDt() != null) {jpaUpdateClause.set(qProfile.regDt, entity.getRegDt());}
        if(entity.getModUsr() != null) {jpaUpdateClause.set(qProfile.modUsr, entity.getModUsr());}
        if(entity.getModDt() != null) {jpaUpdateClause.set(qProfile.modDt, entity.getModDt());}

        jpaUpdateClause.where(qProfile.profileId.eq(entity.getProfileId()));

        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
