package com.sparwk.personalizationnode.profile.jpa.entity;


import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileCompanyStudioId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_company_studio")
@DynamicUpdate
@IdClass(ProfileCompanyStudioId.class)
public class ProfileCompanyStudio {

    @Id
    @GeneratedValue(generator = "tb_profile_company_studio_seq")
    @Column(name = "studio_id", nullable = true)
    private Long studioId;
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "owner_id", nullable = true)
    private Long ownerId;
    @Column(name = "skip_yn", nullable = true)
    private String skipYn;
    @Column(name = "studio_name", nullable = true)
    private String studioName;
    @Column(name = "business_location_cd", nullable = true)
    private String businessLocationCd;
    @Column(name = "post_cd", nullable = true)
    private String postCd;
    @Column(name = "region", nullable = true)
    private String region;
    @Column(name = "city", nullable = true)
    private String city;
    @Column(name = "addr1", nullable = true)
    private String addr1;
    @Column(name = "addr2", nullable = true)
    private String addr2;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProfileCompanyStudio(
            Long studioId,
            Long profileId,
            Long ownerId,
            String skipYn,
            String studioName,
            String businessLocationCd,
            String postCd,
            String region,
            String city,
            String addr1,
            String addr2,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.studioId = studioId;
        this.profileId = profileId;
        this.ownerId = ownerId;
        this.skipYn = skipYn;
        this.studioName = studioName;
        this.businessLocationCd = businessLocationCd;
        this.postCd = postCd;
        this.region = region;
        this.city = city;
        this.addr1 = addr1;
        this.addr2 = addr2;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }


}
