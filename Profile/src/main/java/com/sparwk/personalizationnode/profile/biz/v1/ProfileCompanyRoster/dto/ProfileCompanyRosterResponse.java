package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.dto;

import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel.ProfilePositionResponse;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel.ProfileResponse;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfileCompanyRosterResponse {

    private Long rosterSeq;
    private Long profileId;
    private Long profilePositionSeq;
    private String rosterStatus;
    private String joiningStartDt;
    private String joiningEndDt;
    private String joiningEndYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
    private ProfileResponse profileResponse;
    private ProfilePositionResponse profilePositionResponse;
    private String userEmail;
    private String userPhone;

}
