package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyOntheweb.dto;


import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyOnthewebRequestList {
    private List<ProfileCompanyOnthewebRequest> profileCompanyOnthewebRequestList;
}
