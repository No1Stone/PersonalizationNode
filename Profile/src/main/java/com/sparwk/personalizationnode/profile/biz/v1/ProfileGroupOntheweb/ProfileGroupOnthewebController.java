package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupOntheweb;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupOntheweb.dto.ProfileGroupOntehwebRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/V1/group/ontheweb")
@CrossOrigin("*")
@Api(tags = "Profile Server Group Ontheweb API")
@RequiredArgsConstructor
public class ProfileGroupOnthewebController {

    private final Logger logger = LoggerFactory.getLogger(ProfileGroupOnthewebController.class);
    private final ProfileGroupOnthewebService profileGroupOnthewebService;

    @ApiOperation(
            value = "그룹 온더웹 조회",
            notes = "그룹 프로필 조회"
    )
    @GetMapping(path = "/info/{profileId}")
    public Response ProfileGroupOnthewebSelectController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileGroupOnthewebService.ProfileGroupOnthewebSelectService(profileId);
        return res;
    }

    @ApiOperation(
            value = "그룹 온더웹 저장",
            notes = "그룹 온더웹 저장"
    )
    @PostMapping(path = "/info/list")
    public Response ProfileGroupOnthewebSaveController(@RequestBody ProfileGroupOntehwebRequestList dto) {
        Response res = profileGroupOnthewebService.ProfileGroupOnthewebSaveService(dto);
        return res;
    }


}
