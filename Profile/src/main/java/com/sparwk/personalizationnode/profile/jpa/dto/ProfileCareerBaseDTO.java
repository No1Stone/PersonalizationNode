package com.sparwk.personalizationnode.profile.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCareerBaseDTO {
    private Long profileId;
    private String careerOrganizationNm;
    private String deptRoleNm;
    private String locationCityCountryCd;
    private String locationCityNm;
    private String presentYn;
    private Long careerSeq;
    private String filePath;
    private Long profileCareerSeq;
    private String careerStartDt;
    private String careerEndDt;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

}
