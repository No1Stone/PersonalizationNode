package com.sparwk.personalizationnode.profile.biz.v1.ProfileTimezone;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileOntheweb.dto.ProfileOnthewebRqeustList;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileTimezone.dto.ProfileTimezoneRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/timezone")
@CrossOrigin("*")
@Api(tags = "Profile Server TimeZone API")
public class ProfileTimezoneContoller {

    @Autowired
    private ProfileTimezoneService profileTimezoneService;

    private final Logger logger = LoggerFactory.getLogger(ProfileTimezoneContoller.class);
    @ApiOperation(
            value = "프로필 WEB URL 입력",
            notes = "web URL 입력"
    )
    @PostMapping(path = "/info")
    public Response ProfileTimezoneSaveController(@Valid @RequestBody ProfileTimezoneRequestList dto) {
        logger.info("------{}",dto);
        Response res = profileTimezoneService.ProfileTimezoneSaveResponseService(dto);
        return res;
    }

    @ApiOperation(
            value = "프로필 언어 정보 검색",
            notes = "언어 정보 검색"
    )
    @GetMapping(path = "/info/{profileId}")
    public Response ProfileTimezoneSelectController(@PathVariable(name = "profileId") Long profileId) {
        Response res = profileTimezoneService.ProfileTimezoneSelectResponseService(profileId);
        return res;
    }

}
