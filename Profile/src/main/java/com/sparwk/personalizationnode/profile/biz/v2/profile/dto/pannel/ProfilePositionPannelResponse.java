package com.sparwk.personalizationnode.profile.biz.v2.profile.dto.pannel;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfilePositionPannelResponse {
    private Long profilePositionSeq;
    private Long profileId;
    private Long companyProfileId;
    private String profileCompanyImgUrl;
    private String profileCompanyName;
    private String companyVerifyYn;
    private String artistYn;
    private String anrYn;
    private String creatorYn;
    private String deptRoleInfo;
    private String primaryYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
