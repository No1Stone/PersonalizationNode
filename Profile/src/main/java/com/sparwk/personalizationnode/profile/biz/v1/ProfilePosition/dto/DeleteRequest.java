package com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto;


import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DeleteRequest {

    private Long profileId;
    private Long profilePositionSeq;

}
