package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.sparwk.personalizationnode.profile.jpa.dto.ProfileInterestMetaBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface ProfileInterestMetaRepositoryDynamic {
    @Transactional
    Long ProfileInterestMetaRepositoryDynamicUpdate(ProfileInterestMetaBaseDTO entity);
    }
