package com.sparwk.personalizationnode.profile.biz.v2.profile.dto.panneldto;

import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCareerBaseDTO;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCareerCodeName extends ProfileCareerBaseDTO {
    private String locationCityCountryCdName;

}
