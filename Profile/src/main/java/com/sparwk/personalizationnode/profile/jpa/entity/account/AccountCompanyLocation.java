package com.sparwk.personalizationnode.profile.jpa.entity.account;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_company_location")
@DynamicUpdate
public class AccountCompanyLocation {
    @Id
    @Column(name = "accnt_id")
    private Long accntId;
    @Column(name = "location_cd", nullable = true, length = 9)
    private String locationCd;

    @Builder
    AccountCompanyLocation
            (
                    Long accntId,
                    String locationCd
            ) {
        this.accntId = accntId;
        this.locationCd = locationCd;
    }

}
