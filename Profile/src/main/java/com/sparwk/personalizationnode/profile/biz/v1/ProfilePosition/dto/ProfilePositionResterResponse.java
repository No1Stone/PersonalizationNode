package com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePositionResterResponse {

    private Long profilePositionSeq;
    private Long profileId;
    private String profileImgUrl;
    private String fullName;
    private String accntEmail;
    private String phoneNumber;
    private String headline;
    private Long companyProfileId;
    private String companyVerifyYn;
    private String artistYn;
    private String anrYn;
    private String creatorYn;
    private String deptRoleInfo;
    private String primaryYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

}
