package com.sparwk.personalizationnode.profile.biz.chat;

import com.sparwk.personalizationnode.profile.jpa.entity.Profile;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Mattermost {

    private Long sparwkProfileId;
    private String imgUrl;
    private MmUser mmUser;

    public Mattermost ofMmProfile(Profile profile, String email, String firstName, String lastName){
    this.sparwkProfileId = profile.getProfileId();
    this.imgUrl = profile.getProfileImgUrl();
    this.mmUser.setEmail(email);
    this.mmUser.setFirstname(firstName);
    this.mmUser.setLastname(lastName);
    return this;
    }
    public Mattermost ofMmProfile(ProfileCompany profile, String email, String firstName, String lastName){
        this.sparwkProfileId = profile.getProfileId();
        this.imgUrl = profile.getProfileImgUrl();
        this.mmUser.setEmail(email);
        this.mmUser.setFirstname(firstName);
        this.mmUser.setLastname(lastName);
        return this;
    }

}
