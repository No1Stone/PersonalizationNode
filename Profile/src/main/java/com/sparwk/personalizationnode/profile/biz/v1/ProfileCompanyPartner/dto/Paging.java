package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Paging {

    private int size;
    private int page;
    private int totalSize;
    private int totalPage;
    private Object content;

}
