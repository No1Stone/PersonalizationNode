package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfileCompanyRosterRequestList {

    private List<ProfileCompanyRosterRequest> profileCompanyRosterRequestList;
}
