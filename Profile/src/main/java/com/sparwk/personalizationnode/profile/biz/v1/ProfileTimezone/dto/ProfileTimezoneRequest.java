package com.sparwk.personalizationnode.profile.biz.v1.ProfileTimezone.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileTimezoneRequest {
    @Schema(description = "프로필 아이디")
    private Long profileId;
    private Long profileTimezoneBoxNum;
    private Long timezoneSeq;
    private String setTimeAutoYn;
}
