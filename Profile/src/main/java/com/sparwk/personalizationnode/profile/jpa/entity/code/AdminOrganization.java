package com.sparwk.personalizationnode.profile.jpa.entity.code;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_organization")
public class AdminOrganization {
    @Id
    @Column(name = "org_cd ", nullable = true)
    private Long orgCd;
    @Column(name = "org_type ", nullable = true)
    private String orgType;
    @Column(name = "org_name ", nullable = true)
    private String orgName;
    @Column(name = "country_cd ", nullable = true)
    private String countryCd;
    @Column(name = "description ", nullable = true)
    private String description;
    @Column(name = "use_yn ", nullable = true)
    private String useYn;
    @Column(name = "reg_usr ", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt ", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr ", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    AdminOrganization(
            Long orgCd,
            String orgType,
            String orgName,
            String countryCd,
            String description,
            String useYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.orgCd = orgCd;
        this.orgType = orgType;
        this.orgName = orgName;
        this.countryCd = countryCd;
        this.description = description;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }


}
