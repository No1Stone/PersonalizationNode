package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfilePosition;
import com.sparwk.personalizationnode.profile.jpa.repository.dsl.ProfilePositionRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ProfilePositionRepository extends JpaRepository<ProfilePosition, String> , ProfilePositionRepositoryDynamic {
    Optional<ProfilePosition> findByProfilePositionSeq(Long seq);

    List<ProfilePosition> findByProfileIdOrderByProfilePositionSeqDesc(Long profileId);
    List<ProfilePosition> findByProfileId(Long profileId);
    List<ProfilePosition> findByProfileIdAndCompanyVerifyYn(Long profileId,String yn);
    List<ProfilePosition> findByProfileIdAndCompanyProfileIdIsNull(Long profileId);
    ProfilePosition findByProfileIdAndPrimaryYn(Long profileId, String primaryYn);


    @Transactional
    void deleteByProfilePositionSeq(Long seq);



    boolean existsByProfilePositionSeq(Long seq);
    boolean existsByProfileId(Long profileId);
    boolean existsByProfileIdAndAndCompanyVerifyYn(Long profileId, String comVeri);
    boolean existsByProfileIdAndPrimaryYn(Long profileId, String primatyYn);
    boolean existsByProfileIdAndPrimaryYnAndCompanyVerifyYn(Long profileId, String primaYn, String comverify);


    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_position set " +
                    "primary_yn = :primaryYn " +
                    "where " +
                    "profile_position_seq = :positionSeq"
            ,nativeQuery = true
    )
    int UpdateProfilePositionPrimatyYnWhereSeq(
            @Param("positionSeq") Long positionSeq,
            @Param("primaryYn") String primaryYn)
            ;


    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_position set " +
                    "company_verify_yn = :yn " +
                    "where " +
                    "profile_id = :profileId " +
                    "and " +
                    "profile_position_seq = :positionSeq"
            ,nativeQuery = true
    )
    int UpdateProfilePositionYn(
            @Param("yn") String yn,
            @Param("profileId") Long profileId,
            @Param("positionSeq") Long positionSeq)
            ;


}
