package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminSongCode;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface AdminSongCodeRepository extends JpaRepository<AdminSongCode, Long> {

    Optional<AdminSongCode> findByCode(String cd);
    boolean existsByCode(String cd);

}
