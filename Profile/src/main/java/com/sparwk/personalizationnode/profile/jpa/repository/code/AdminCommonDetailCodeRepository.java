package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminCommonDetailCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminCommonDetailCodeRepository extends JpaRepository<AdminCommonDetailCode, Long> {

    Optional<AdminCommonDetailCode> findByDcode(String cd);
    boolean existsByDcode(String cd);

}
