package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGroupContactPhoneUpdateRequest {
    private Long profileId;
    private String contctPhoneNumber;
    private String verifyPhoneYn;
    private String countryCd;
}
