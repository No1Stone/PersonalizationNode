package com.sparwk.personalizationnode.profile.biz.v1.ProfileEducation;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileEducation.dto.ProfileEducationDeleteRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileEducation.dto.ProfileEducationDto;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileEducation.dto.ProfileEducationDtoList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCareerBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileEducationBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileEducationRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileEducationService {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfileEducationRepository profileEducationRepository;
    private final MessageSourceAccessor messageSourceAccessor;
    @Autowired
    private HttpServletRequest httpServletRequest;
    private final Logger logger = LoggerFactory.getLogger(ProfileEducationService.class);


    public Response ProfileEducationSave(ProfileEducationDto dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        Response res = new Response();
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        if (dto.getProfileId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }



        int educ = profileEducationRepository.countByProfileIdAndEduSeq(dto.getProfileId(), dto.getProfileEduSeq());

        if (educ > 0) {
            ProfileEducationBaseDTO profileEducationBaseDTO = modelMapper.map(dto, ProfileEducationBaseDTO.class);
            profileEducationBaseDTO.setModUsr(userInfoDTO.getAccountId());
            profileEducationBaseDTO.setModDt(LocalDateTime.now());
            long result = profileServiceRepository.ProfileEducationDynamicUpdateService(profileEducationBaseDTO);
            res.setResult(result);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } else {
            ProfileEducationBaseDTO profileEducationBaseDTO = modelMapper.map(dto, ProfileEducationBaseDTO.class);
            profileEducationBaseDTO.setModUsr(userInfoDTO.getAccountId());
            profileEducationBaseDTO.setRegUsr(userInfoDTO.getAccountId());
            profileEducationBaseDTO.setModDt(LocalDateTime.now());
            profileEducationBaseDTO.setRegDt(LocalDateTime.now());

            ProfileEducationBaseDTO result = profileServiceRepository
                    .ProfileEducationSaveService(profileEducationBaseDTO);
            res.setResult(result);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


    }

    public Response ProfileEducationSelect(Long ProfileId) {
        List<ProfileEducationBaseDTO> result = profileEducationRepository.findByProfileId(ProfileId)
                .stream().map(e -> modelMapper.map(e, ProfileEducationBaseDTO.class))
                .collect(Collectors.toList());
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response ProfileEducationDynamicUpdate(ProfileEducationDto dto) {
        long result = profileServiceRepository
                .ProfileEducationDynamicUpdateService(modelMapper.map(dto, ProfileEducationBaseDTO.class));
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileEducationListSave(ProfileEducationDtoList dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        Response res = new Response();
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        if (dto.getProfileEducationDtoList().get(0).getProfileId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        //현재 database 입력값
        List<ProfileEducationBaseDTO> beforCareer = profileServiceRepository
                .ProfileEducationSelectService(userInfoDTO.getLastUseProfileId());
        logger.info("현재 database 입력값 -{}", beforCareer);

        List<Long> befor = new ArrayList<>();
        //비교요청후 처리할 값 유저가 준 데이터
        for (ProfileEducationDto f : dto.getProfileEducationDtoList()) {
            logger.info("=====시퀀스널체크=============");
            if (f.getEduSeq() != null) {
                befor.add(f.getEduSeq());
            }
            //시퀀스가 없는값은 신규로 저장시킴
            logger.info("=====for=============");
//            if (f.getFilePath() == null
//                    || f.getFilePath().equals(null)
//                    || f.getFilePath().equals("N")
//                    || f.getFilePath().isEmpty()) {

            ProfileEducationBaseDTO baseDTO = modelMapper.map(f, ProfileEducationBaseDTO.class);
            baseDTO.setRegUsr(userInfoDTO.getAccountId());
            baseDTO.setModUsr(userInfoDTO.getAccountId());
            baseDTO.setRegDt(LocalDateTime.now());
            baseDTO.setModDt(LocalDateTime.now());
            ProfileEducationBaseDTO result = profileServiceRepository.ProfileEducationSaveService(baseDTO);
            befor.add(result.getEduSeq());
            logger.info("=====IF=============");
//            }
        }
        //입력이 완료된 데이터를 불러온다.
        List<ProfileEducationBaseDTO> afterEducation = profileServiceRepository
                .ProfileEducationSelectService(userInfoDTO.getLastUseProfileId());
        // 델리트값 구하기 / 입력이 완료된 데이터에서 프론트가 요청한 값을 뺀다
        logger.info("입력이 완료된 데이터를 불러온다. -{}", afterEducation);

        List<Long> after = new ArrayList<>();
        for (ProfileEducationBaseDTO e : afterEducation) {
            after.add(e.getEduSeq());
            logger.info("--------{}", e);
        }
        logger.info("-----{}", after);
        logger.info("-----{}", befor);

        after.removeAll(befor);
        for (Long e : after) {
            profileEducationRepository.deleteByProfileIdAndEduSeq(userInfoDTO.getLastUseProfileId(), e);
        }
        logger.info("지울 리스트 목록 투 스트링  ----------{}", after);
        //지운데이터를 조회해서 보내준다
        List<ProfileEducationBaseDTO> result = profileServiceRepository
                .ProfileEducationSelectService(userInfoDTO.getLastUseProfileId());
        logger.info("   //입력된 값들을 비교해서 지울 리스트. -{}", result);
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileEducationDeleteService(ProfileEducationDeleteRequest dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        if (dto.getProfileId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        profileEducationRepository.deleteByProfileIdAndEduSeq(userInfoDTO.getLastUseProfileId(), dto.getEduSeq());

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
