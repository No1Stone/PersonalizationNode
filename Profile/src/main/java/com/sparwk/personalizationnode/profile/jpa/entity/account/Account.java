package com.sparwk.personalizationnode.profile.jpa.entity.account;


import com.sparwk.personalizationnode.profile.jpa.entity.Profile;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompany;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account")
@DynamicUpdate
//@TableGenerator(
//        name = "tb_account_seq_generator",
//        table = "tb_account_sequences",
//        pkColumnValue = "tb_account_seq",
//        allocationSize = 1,
//        initialValue = 1000000
//)
@Where(clause = "use_yn = 'Y'")
public class Account {
    @Id
//    @GeneratedValue(
//            strategy = GenerationType.TABLE,
//            generator = "tb_account_seq_generator")
    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "accnt_repository_id", nullable = true)
    private Long accntRepositoryId;

    @Column(name = "accnt_type_cd", nullable = true, length = 9)
    private String accntTypeCd;

    @Column(name = "accnt_email", nullable = true, length = 100, unique = true)
    private String accntEmail;

    @Column(name = "accnt_pass", nullable = true, length = 200)
    private String accntPass;

    @Column(name = "country_cd", nullable = true, length = 9)
    private String countryCd;

    @Column(name = "phone_number", nullable = true, length = 20)
    private String phoneNumber;

    @Column(name = "accnt_login_active_yn", nullable = true, length = 1)
    private String accntLoginActiveYn;

    @Column(name = "last_use_profileId")
    private Long lastUseProfileId;

    @Column(name = "verify_yn", nullable = true, length = 1)
    private String verifyYn;

    @Column(name = "use_yn", nullable = true, length = 1)
    private String useYn;

    @Column(name = "verify_phone_yn", nullable = true, length = 1)
    private String verifyPhoneYn;

    @Column(name = "receive_marketing_info_yn ",  nullable = true) private String receiveMarketingInfoYn ;
    @Column(name = "provide_info_tothird_yn ",  nullable = true) private String provideInfoTothirdYn ;
    @Column(name = "personal_info_collection_yn ",  nullable = true) private String personalInfoCollectionYn ;
    @Column(name = "provide_info_tosparwk_yn ",  nullable = true) private String provideInfoTosparwkYn ;
    @Column(name = "transfer_info_toabroad_yn ",  nullable = true) private String transferInfoToabroadYn ;


    @Column(name = "reg_usr", updatable = false)
    //first registrant
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    Account(
            Long accntId,
            Long accntRepositoryId,
            String accntTypeCd,
            String accntEmail,
            String accntPass,
            String countryCd,
            String phoneNumber,
            String accntLoginActiveYn,
            String verifyYn,
            String useYn,
            String verifyPhoneYn,
            String receiveMarketingInfoYn ,
            String provideInfoTothirdYn ,
            String personalInfoCollectionYn ,
            String provideInfoTosparwkYn ,
            String transferInfoToabroadYn ,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt

    ) {
        this.accntId = accntId;
        this.accntRepositoryId = accntRepositoryId;
        this.accntTypeCd = accntTypeCd;
        this.accntEmail = accntEmail;
        this.accntPass = accntPass;
        this.countryCd = countryCd;
        this.phoneNumber = phoneNumber;
        this.accntLoginActiveYn = accntLoginActiveYn;
        this.verifyYn = verifyYn;
        this.useYn = useYn;
        this.verifyPhoneYn = verifyPhoneYn;
        this.receiveMarketingInfoYn =receiveMarketingInfoYn ;
        this.provideInfoTothirdYn =provideInfoTothirdYn ;
        this.personalInfoCollectionYn =personalInfoCollectionYn ;
        this.provideInfoTosparwkYn =provideInfoTosparwkYn ;
        this.transferInfoToabroadYn =transferInfoToabroadYn ;

        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;

    }

//
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Profile.class)
    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
    private List<Profile> profile;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProfileCompany.class)
    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
    private List<ProfileCompany> profileCompany;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyDetail.class)
    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
    private AccountCompanyDetail accountCompanyDetail;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyLocation.class)
    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
    private List<AccountCompanyLocation> accountCompanyLocation;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = AccountCompanyType.class)
    @JoinColumn(name = "accnt_id", referencedColumnName = "accnt_id")
    private List<AccountCompanyType> accountCompanyType;




}
