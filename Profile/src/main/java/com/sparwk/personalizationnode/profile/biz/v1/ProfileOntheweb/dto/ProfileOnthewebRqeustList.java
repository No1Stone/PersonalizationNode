package com.sparwk.personalizationnode.profile.biz.v1.ProfileOntheweb.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileOnthewebRqeustList {

    private List<ProfileOnthewebRequest> profileOnthewebRequestList;

}
