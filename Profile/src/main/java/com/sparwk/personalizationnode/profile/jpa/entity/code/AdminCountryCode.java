package com.sparwk.personalizationnode.profile.jpa.entity.code;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_country_code")
public class AdminCountryCode {
    @Id
    @Column(name = "country_seq ", nullable = true)
    private Long countrySeq;
    @Column(name = "country ", nullable = true)
    private String country;
    @Column(name = "iso2 ", nullable = true)
    private String iso2;
    @Column(name = "iso3 ", nullable = true)
    private String iso3;
    @Column(name = "continent_code ", nullable = true)
    private String continentCode;
    @Column(name = "nmr ", nullable = true)
    private String nmr;
    @Column(name = "dial ", nullable = true)
    private String dial;
    @Column(name = "description ", nullable = true)
    private String description;
    @Column(name = "use_yn ", nullable = true)
    private String useYn;
    @Column(name = "reg_usr ", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt ", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr ", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt ", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "country_cd", nullable = true)
    private String countryCd;

    @Builder
    AdminCountryCode(Long countrySeq,
                     String country,
                     String iso2,
                     String iso3,
                     String continentCode,
                     String nmr,
                     String dial,
                     String description,
                     String useYn,
                     Long regUsr,
                     LocalDateTime regDt,
                     Long modUsr,
                     LocalDateTime modDt,
                     String countryCd
    ) {
        this.countrySeq = countrySeq;
        this.country = country;
        this.iso2 = iso2;
        this.iso3 = iso3;
        this.continentCode = continentCode;
        this.nmr = nmr;
        this.dial = dial;
        this.description = description;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
        this.countryCd = countryCd;
    }
}
