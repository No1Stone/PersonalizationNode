package com.sparwk.personalizationnode.profile.biz.v1.profile.dto;

import lombok.Data;

@Data
public class ProfileArray {

    private String profileArray;
}
