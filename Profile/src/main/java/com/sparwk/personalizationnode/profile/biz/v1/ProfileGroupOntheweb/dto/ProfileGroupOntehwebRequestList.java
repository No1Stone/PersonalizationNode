package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupOntheweb.dto;


import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGroupOntehwebRequestList {

    private List<ProfileGroupOntehwebRequest> profileGroupOntehwebRequest;

}
