package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroup;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroup.dto.update.*;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileGroupBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileGroupRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@RequiredArgsConstructor
public class ProfileGroupService {

    private final Logger logger = LoggerFactory.getLogger(ProfileGroupService.class);
    private final ModelMapper modelMapper;
    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final ProfileGroupRepository profileGroupRepository;


    public Response ProfileGroupHeadLineUpdateService(HeadLineUpdateRequest dto) {
        Response res = new Response();

        int pg = profileGroupRepository.UpdateProfileGroupHeadLine(dto.getProfileId(), dto.getHeadline());

        logger.info("pg - {}",pg);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGroupImgUpdateService(ImgUpdateRequest dto) {
        Response res = new Response();

        int pg = profileGroupRepository.UpdateProfileGroupImg(dto.getProfileId(), dto.getProfileImgUrl());
        logger.info("pg - {}",pg);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGroupNameUpdateService(ProfileGroupNameUpdateRequest dto) {
        Response res = new Response();

        int pg = profileGroupRepository.UpdateProfileGroupName(dto.getProfileId(), dto.getProfileGroupName());
        logger.info("pg - {}",pg);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGroupWebsiteUpdateService(WebSiteUpdateRequest dto) {
        Response res = new Response();

        int pg = profileGroupRepository.UpdateProfileGroupWebsite(dto.getProfileId(), dto.getWebSite());
        logger.info("pg - {}",pg);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGroupOverviewUpdateService(OverViewUpdateRequest dto) {
        Response res = new Response();

        int pg = profileGroupRepository.UpdateProfileGroupOverView(dto.getProfileId(), dto.getOverView());
        logger.info("pg - {}",pg);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGroupBannerUpdateService(BannerUpdateRequest dto) {
        Response res = new Response();

        int pg = profileGroupRepository.UpdateProfileGroupBanner(dto.getProfileId(), dto.getProfileBgdImgUrl());
        logger.info("pg - {}",pg);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGroupMattermostUpdateService(MattermostIdUpdateRequest dto) {
        Response res = new Response();

        int pg = profileGroupRepository.UpdateProfileGroupMattermostId(dto.getProfileId(), dto.getMattermostId());
        logger.info("pg - {}",pg);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGroupSelectService(Long profileId) {
        Response res = new Response();
        res.setResult(profileGroupRepository
                .findById(profileId).map(e -> modelMapper.map(e, ProfileGroupBaseDTO.class)).get());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
