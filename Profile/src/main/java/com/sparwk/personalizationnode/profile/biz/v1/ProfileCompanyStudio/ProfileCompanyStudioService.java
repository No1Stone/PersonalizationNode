package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyStudio;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.ProfileCompanyRosterService;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyStudio.dto.ProfileCompanyStudioDeleteRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyStudio.dto.ProfileCompanyStudioRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyStudio.dto.ProfileCompanyStudioRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyRosterBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyStudioBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyRosterRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyStudioRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileCompanyStudioService {

    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyStudioService.class);
    @Autowired
    private ProfileCompanyStudioRepository profileCompanyStudioRepository;

    private final MessageSourceAccessor messageSourceAccessor;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private HttpServletRequest httpServletRequest;

    public Response ProfileCompanyStudioSaveService(ProfileCompanyStudioRequest dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        ProfileCompanyStudioBaseDTO baseDTO = modelMapper.map(dto, ProfileCompanyStudioBaseDTO.class);
        baseDTO.setModDt(LocalDateTime.now());
        baseDTO.setRegDt(LocalDateTime.now());
        baseDTO.setRegUsr(userInfoDTO.getAccountId());
        baseDTO.setModUsr(userInfoDTO.getAccountId());
        ProfileCompanyStudioBaseDTO result = profileServiceRepository.ProfileCompanyStudioSaveService(baseDTO);


        Response res = new Response();
        res.setResult(profileCompanyStudioRepository.findByStudioId(result.getStudioId()));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilecompanyStudioSelectService(Long profileId) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        List<ProfileCompanyStudioBaseDTO> result = profileCompanyStudioRepository
                .findByProfileId(profileId).stream().map(
                        e -> modelMapper.map(e, ProfileCompanyStudioBaseDTO.class)).collect(Collectors.toList());
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileCompanyStudioListSaveService(ProfileCompanyStudioRequestList dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        for(ProfileCompanyStudioRequest e: dto.getProfileCompanyStudioRequestList()){
            ProfileCompanyStudioBaseDTO baseDTO = modelMapper.map(e, ProfileCompanyStudioBaseDTO.class);
            baseDTO.setModDt(LocalDateTime.now());
            baseDTO.setRegDt(LocalDateTime.now());
            baseDTO.setRegUsr(userInfoDTO.getAccountId());
            baseDTO.setModUsr(userInfoDTO.getAccountId());
        }

        Response res = new Response();
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileCompanyStudioDeleteService(ProfileCompanyStudioDeleteRequest dto) {
        Response res = new Response();
        profileCompanyStudioRepository.deleteByProfileIdAndStudioId(dto.getProfileId(), dto.getStudioId());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
