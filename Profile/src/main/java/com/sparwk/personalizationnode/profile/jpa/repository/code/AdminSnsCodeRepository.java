package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminSnsCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminSnsCodeRepository extends JpaRepository<AdminSnsCode, Long> {

    Optional<AdminSnsCode> findBySnsCd(String Cd);
    boolean existsBySnsCd(String cd);

}
