package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.profilecreate;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.persistence.Column;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountPassportRequest {
    @Schema(description = "여권 firstName")
    @Size(max = 50)
    private String passportFirstName;
    @Schema(description = "여권 midleName")
    @Size(max = 50)
    private String passportMiddleName;
    @Schema(description = "여권 lastName")
    @Size(max = 50)
    private String passportLastName;
    @Schema(description = "여권 스캔파일 저장 url")
    @Size(max = 200)
    private String passportImgFileUrl;
    @Schema(description = "여권국가코드")
    private String countryCd;
    @Size(max = 1)
    private String personalInfoCollectionYn;

    private String passportVerifyYn;

}
