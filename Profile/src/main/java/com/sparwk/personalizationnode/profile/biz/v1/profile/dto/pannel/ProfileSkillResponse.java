package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel;


import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileSkillResponse {

    private String skillTypeCd;
    private Long profileId;
    private String skillDetailTypeCd;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

//    @Builder
//    ProfileSkillResponse(Long profileId,
//                         String skillTypeCd,
//                         String skillDetailTypeCd,
//                         Long regUsr,
//                         LocalDateTime regDt,
//                         Long modUsr,
//                         LocalDateTime modDt
//    ){
//        this.profileId=profileId;
//        this.skillTypeCd=skillTypeCd;
//        this.skillDetailTypeCd=skillDetailTypeCd;
//        this.regUsr = regUsr;
//        this.regDt = regDt;
//        this.modUsr = modUsr;
//        this.modDt = modDt;
//    }

}
