package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel;

import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileOnthewebId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileOnthewebResponse {

    private Long profileId;
    private String snsTypeCd;
    private String snsUrl;
    private String useYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

//    @Builder
//    ProfileOnthewebResponse(
//            Long profileId,
//            Long snsCd,
//            String snsUrl,
//            String useYn,
//            Long regUsr,
//            LocalDateTime regDt,
//            Long modUsr,
//            LocalDateTime modDt
//    ) {
//        this.profileId = profileId;
//        this.snsCd = snsCd;
//        this.snsUrl = snsUrl;
//        this.useYn = useYn;
//        this.regUsr = regUsr;
//        this.regDt = regDt;
//        this.modUsr = modUsr;
//        this.modDt = modDt;
//    }

}
