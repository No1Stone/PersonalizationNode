package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGroupContactImgUpdateRequest {
    private Long profileId ;
    private String profileContactImgUrl ;

}
