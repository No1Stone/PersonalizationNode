package com.sparwk.personalizationnode.profile.biz.v2.profile.dto.panneldto;

import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel.ProfileAnrPitchingResponse;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.*;
import com.sparwk.personalizationnode.profile.jpa.entity.code.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProfilePannelResponseV2 extends ProfileBaseDTO {

    private String currentCityCountryCdName;
    private String homeTownCountryCdName;
    private List<ProfileCareerCodeName> profileCareerCodeList;
    private List<ProfileEducationCodeName> ProfileEducationCodeList;
    private List<ProfileGenderCodeName> ProfileGenderCodeList;
    private List<ProfileInterestMetaCodeName> ProfileInterestMetaCodeList;
    private List<ProfileLanguageCodeName> ProfileLanguageCodeList;
    private List<ProfileOnthewebCodeName> ProfileOnthewebCodeList;
    private List<ProfilePositionCodeName> ProfilePositionCodeList;
    private List<ProfileSkillCodeName> ProfileSkillCodeList;
    private List<ProfileTimezoneCodeName> ProfileTimezoneCodeList;
    private List<ProfileUseInfoCodeName> ProfileUseInfoCodeList;
    private ProfileCompanyBaseDTO profileCompanyBaseDTO;
    private String pitchingYn;



}
