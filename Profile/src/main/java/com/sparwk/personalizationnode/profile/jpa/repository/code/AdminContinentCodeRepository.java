package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminContinentCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminContinentCodeRepository extends JpaRepository<AdminContinentCode,Long> {

    Optional<AdminContinentCode> findByContinentCd(String cd);
    boolean existsByContinentCd(String cd);

}
