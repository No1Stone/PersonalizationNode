package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupTimezone.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGroupTimezoneRequestList {

    private List<ProfileGroupTimezoneRequest> profileGroupTimezoneRequest;

}
