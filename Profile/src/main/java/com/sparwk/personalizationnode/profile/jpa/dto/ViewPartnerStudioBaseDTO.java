package com.sparwk.personalizationnode.profile.jpa.dto;


import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ViewPartnerStudioBaseDTO {

    private String type;
    private Long profileId;
    private Long mappingProfileId;
    private String name;
    private String companyType;
    private String locationCd;
    private String locationCdName;
    private String postCd;
    private String region;
    private String city;
    private String addr1;
    private String addr2;
    private LocalDateTime modDt;


}
