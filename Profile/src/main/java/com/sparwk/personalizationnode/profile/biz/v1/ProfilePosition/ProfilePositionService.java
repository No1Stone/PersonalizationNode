package com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition;

import com.google.gson.Gson;
import com.sparwk.personalizationnode.profile.biz.chat.MmCompany;
import com.sparwk.personalizationnode.profile.biz.chat.UpdateMattermost;
import com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto.*;
import com.sparwk.personalizationnode.profile.config.common.RestCall;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyRosterBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfilePositionBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompany;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompanyRoster;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfilePosition;
import com.sparwk.personalizationnode.profile.jpa.entity.account.Account;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyRosterRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfilePositionRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.account.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfilePositionService {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfilePositionRepository profilePositionRepository;
    @Autowired
    private ProfileCompanyRosterRepository profileCompanyRosterRepository;
    @Autowired
    private RestTemplate restTemplate;
    private final MessageSourceAccessor messageSourceAccessor;
    @Autowired
    private HttpServletRequest httpServletRequest;
    @Autowired
    private ProfileCompanyRepository profileCompanyRepository;
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Value("${sparwk.node.project.projectip}")
    private String projectIp;

    @Value("${sparwk.node.personalization.accountip}")
    private String accountIp;

    @Value("${sparwk.node.personalization.authip}")
    private String authIp;

    @Value("${sparwk.node.personalization.profileip}")
    private String profileIp;
    @Value("${sparwk.node.communication.chat}")
    private String chat;
    @Value("${sparwk.node.schema}")
    private String schema;

    private final Logger logger = LoggerFactory.getLogger(ProfilePositionService.class);

    public Response ProfilePositionSave(ProfilePositionDto dto) {
        logger.info("=========-{}", dto.toString());
        Response res = new Response();
        List<ProfilePositionDto> ppd = new ArrayList<>();
        ppd.add(dto);
        res.setResult(ProfilePositionListSaveAutoVerifyService(ProfilePositionRequestDto.builder().profilePositionList(ppd).build()));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilePositionSelect(Long profileId) {
        List<ProfilePositionResponse> result = profilePositionRepository.findByProfileId(profileId)
                .stream().map(e -> modelMapper.map(e, ProfilePositionResponse.class))
                .collect(Collectors.toList());
        for (ProfilePositionResponse e : result) {
            e.setCompanyVerifyYn("N");
        }

        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilePositionDynamicUpdate(ProfilePositionDto dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        ProfilePositionBaseDTO baseDTO = modelMapper.map(dto, ProfilePositionBaseDTO.class);

        baseDTO.setProfileId(userInfoDTO.getLastUseProfileId());
        baseDTO.setModDt(LocalDateTime.now());
        baseDTO.setModUsr(userInfoDTO.getAccountId());
        long result = profileServiceRepository.ProfilePositionDynamicUpdateService(baseDTO);

        if (dto.getCompanyProfileId() == null) {
        } else {
            ProfileCompanyRosterBaseDTO pcrBasedto = ProfileCompanyRosterBaseDTO
                    .builder()
                    .profileId(dto.getCompanyProfileId())
                    .profilePositionSeq(dto.getProfilePositionSeq())
                    .rosterStatus("RSS000001")
                    .build();
            profileCompanyRosterRepository.save(modelMapper.map(pcrBasedto, ProfileCompanyRoster.class));
            profilePositionRepository.UpdateProfilePositionYn("P", baseDTO.getProfileId(), baseDTO.getProfilePositionSeq());

        }

        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilePositionListSave(ProfilePositionRequestDto profilePositionRequestDto) {
        for (ProfilePositionDto ppd : profilePositionRequestDto.getProfilePositionList()) {
            if (ppd.getCompanyProfileId() == null) {
            } else {
                ProfileCompanyRosterBaseDTO pcrBasedto = ProfileCompanyRosterBaseDTO
                        .builder()
                        .profileId(ppd.getCompanyProfileId())
                        .profilePositionSeq(ppd.getProfilePositionSeq())
                        .rosterStatus("RSS000001")
                        .build();
                profileCompanyRosterRepository.save(modelMapper.map(pcrBasedto, ProfileCompanyRoster.class));
                profilePositionRepository.UpdateProfilePositionYn("P", ppd.getProfileId(), ppd.getProfilePositionSeq());

            }
            profileServiceRepository.ProfilePositionSaveService(modelMapper.map(ppd, ProfilePositionBaseDTO.class));
        }
        Response res = new Response();
        res.setResult(true);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilePositionListSaveAutoVerifyService(ProfilePositionRequestDto profilePositionRequestDto) {
        Response res = new Response();
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        //1. 커런트포지션을 선택 하지 않은경우 -> 인디 크리에이터 인디 아티스트 생성
        // 이 경우는 회원가입할때만 있는 케이스
        ////이거없애자
        /*
        if (!profilePositionRepository.existsByProfileId(userInfoDTO.getLastUseProfileId())) {
            if (profilePositionRequestDto.getProfilePositionList() == null || profilePositionRequestDto.getProfilePositionList().isEmpty()) {
                logger.info("커런트포지션을 선택 하지 않은경우");
                ProfilePositionBaseDTO profilePositionBaseDTO = ProfilePositionBaseDTO.builder()
                        .profileId(userInfoDTO.getLastUseProfileId())
                        .artistYn("Y")
                        .anrYn("N")
                        .creatorYn("Y")
                        .regUsr(userInfoDTO.getAccountId())
                        .regDt(LocalDateTime.now())
                        .modUsr(userInfoDTO.getAccountId())
                        .modDt(LocalDateTime.now())
                        .build();
                res.setResultCd(ResultCodeConst.SUCCESS.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }
        }
        */


        //2. 커런트 포지션을 선택하는경우 크리에이터 아티스트 는 자기가 그냥 회사소속이라고 등록할수있음
        // 윤부장님이 우선 이렇게진행하자고함.

        // 그외 케이스는 커런트 포지션을 선택하는 경우 기존 입력된값이랑 비교해서 처리해야함
        // ANR 회사 승인을 받은경우 삭제불가능
        // 크리에이터 아티스트는 유저가 자체신청 한것이므로 삭제해줘야함.
        // 로스터에있는 포지션인경우 삭제시 에러남

        //====================클리어
        // 그리고 나머지도 삭제해줘야하는데 기본 인디는 삭제하면안되네?
        // 그럼 토큰생성시 유저인포를.. 변경해줘야함. 포지션이 없어도 기본값으로 유저이게 생성
        //====================클리어

        //지우면안될리스트
        List<Long> newAddList = new ArrayList<>();
        //1. 신규입력한 시퀀스
        //2. 컴퍼니승인받은 ANR
        //3. 넘긴 시퀀스
        //4. 여기에서 디비를 조회해서 남은것을 지운다.

        for (ProfilePositionDto e : profilePositionRequestDto.getProfilePositionList()) {
            logger.info("eeeeeeeeeeeeeeeeeeeeeeeee - {}", e);
            if (e.getProfilePositionSeq() == null || e.getProfilePositionSeq().equals("")) {
                //2. 커런트 포지션을 선택하는경우 크리에이터 아티스트 는 자기가 그냥 회사소속이라고 등록할수있음
                // 윤부장님이 우선 이렇게진행하자고함.

                ProfilePosition ppSave = modelMapper.map(e, ProfilePosition.class);
                ppSave.setModDt(LocalDateTime.now());
                ppSave.setModUsr(userInfoDTO.getAccountId());
                ppSave.setRegDt(LocalDateTime.now());
                ppSave.setRegUsr(userInfoDTO.getAccountId());
                ProfilePosition ppResult = profilePositionRepository.save(ppSave);
                newAddList.add(ppResult.getProfilePositionSeq());
                //3. ANR이 껴있는 커런트 포지션인 경우 회사승인을 받음 -> 로스터신청


                if(e.getCompanyProfileId() != null) {
                    ProfileCompany pc = profileCompanyRepository
                            .findById(e.getCompanyProfileId()).orElse(null);

                    String chatURL = "https://" + chat + "/V1/company/add/roaster";
                    logger.info("chatCreateURL - -{}", chatURL);
                    MmCompany umm = new MmCompany();
                    umm.setCompanyId(pc.getMattermostId());
                    umm.setProfileId(profileRepository.findByProfileId(e.getProfileId())
                            .orElse(null).getMattermostId());
                    umm.setCompanySparwkId(pc.getProfileId());
                    logger.info("model data - {}", new Gson().toJson(umm));
                    String mmRes = RestCall.postRes(chatURL, new Gson().toJson(umm));
                    logger.info("mMRes - {}", mmRes);
                }
                //크리에이터 아티스트만신청했는지
                //ANR을 껴서 신청했는지 구분해야함
                //ANR껴있으면 무조껀 로스터등록
                if (e.getAnrYn().equals("Y") && e.getCompanyProfileId() != null) {
                    ProfileCompanyRoster pcr = ProfileCompanyRoster
                            .builder()
                            .profileId(e.getCompanyProfileId())
                            .profilePositionSeq(ppResult.getProfilePositionSeq())
                            .rosterStatus("RSS000001")
                            .regUsr(userInfoDTO.getAccountId())
                            .regDt(LocalDateTime.now())
                            .modDt(LocalDateTime.now())
                            .modUsr(userInfoDTO.getAccountId())
                            .build();
                    ProfileCompanyRoster pcrResult = profileCompanyRosterRepository.save(pcr);
                    logger.info("pcrResultaaa --------{}", pcrResult);
                    profilePositionRepository.UpdateProfilePositionYn("P", ppResult.getProfileId(), ppResult.getProfilePositionSeq());
                }

                //ANR이 안껴있으면 승인받은걸로 간주함
                if (e.getAnrYn().equals("N")) {
                    profilePositionRepository.UpdateProfilePositionYn("Y", ppResult.getProfileId(), ppResult.getProfilePositionSeq());
                }
            } else {
                newAddList.add(e.getProfilePositionSeq());
            }
        }

        // 다입력하고 지우면안될리스트랑 비교해서 삭제
        // 입력후 지우지 말아야할 리스트 정리
        // Anr신청이들어간놈 == 로스터테이블에 시퀀스가 있는놈
        // 로스터테이블에 시퀀스가 있는놈
        List<Long> afterDb = new ArrayList<>();
        afterDb = profilePositionRepository.findByProfileId(userInfoDTO.getLastUseProfileId())
                .stream().map(e -> modelMapper.map(e.getProfilePositionSeq(), Long.class))
                .collect(Collectors.toList());

        //입력이 다된 리스트중 로스터에 데이터가 있는건 삭제하면 안됨 에러남
        List<Long> noDeleteList = profileCompanyRosterRepository.findByProfilePositionSeqIn(afterDb)
                .stream().map(e -> modelMapper.map(e.getProfilePositionSeq(), Long.class))
                .collect(Collectors.toList());
        for (Long e : noDeleteList) {
            newAddList.add(e);
        }

        afterDb.removeAll(newAddList);
        logger.info("리무브 - - - - -{}", afterDb);
        //이왜데이터는 유저가 삭제한 데이터임
        for (Long e : afterDb) {
            profilePositionRepository.deleteByProfilePositionSeq(e);
            if(profilePositionRepository.findByProfilePositionSeq(e)
                    .orElse(null).getCompanyProfileId() != null) {
                ProfileCompany pc = profileCompanyRepository
                        .findById(profilePositionRepository.findByProfilePositionSeq(e)
                                .orElse(null).getCompanyProfileId()).orElse(null);
                String chatURL = "https://" + chat + "/V1/company/delete/roaster";
                logger.info("chatCreateURL - -{}", chatURL);
                MmCompany umm = new MmCompany();
                umm.setCompanyId(pc.getMattermostId());
                umm.setProfileId(profileRepository.findByProfileId(
                        profilePositionRepository.findByProfilePositionSeq(e)
                                .orElse(null).getProfileId()
                        )
                        .orElse(null).getMattermostId());
                umm.setCompanySparwkId(pc.getProfileId());
                String mmRes = RestCall.postRes(chatURL, new Gson().toJson(umm));
                logger.info("mMRes - {}", mmRes);
            }
        }
        if(!profilePositionRepository
                .existsByProfileIdAndPrimaryYn(userInfoDTO.getLastUseProfileId(), "Y")){
            if(profilePositionRepository.existsByProfileId(userInfoDTO.getLastUseProfileId())){
                profilePositionRepository.UpdateProfilePositionPrimatyYnWhereSeq(
                        profilePositionRepository
                                .findByProfileIdOrderByProfilePositionSeqDesc(userInfoDTO.getLastUseProfileId())
                                .get(0).getProfilePositionSeq(),"Y");
            }
        }
        profilePositionRepository.flush();


        List<PositionCompanyResponse> result = profilePositionRepository
                .findByProfileIdOrderByProfilePositionSeqDesc(userInfoDTO.getLastUseProfileId())
                .stream().map(e -> modelMapper.map(e, PositionCompanyResponse.class))
                .collect(Collectors.toList());
        result.stream().forEach(e -> {
                    if(e.getCompanyProfileId() != null){
                        ProfileCompany comre = profileCompanyRepository.findById(e.getCompanyProfileId()).orElse(null);
                        e.setProfileCompanyName(comre.getProfileCompanyName());
                        e.setProfileCompanyImgUrl(comre.getProfileImgUrl());

                    };
                }
        );

        res.setResult(result);  res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response AuthtokenResiltResponse() {
        Response res = new Response();
        String result = AuthtokenResilt();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    @Lazy
    public String AuthtokenResilt() {
        HttpHeaders headers = new HttpHeaders();
        String bearertoken = httpServletRequest.getHeader("Authorization");
        logger.info("positionTokenCreate - {}", bearertoken);
        String token = bearertoken.substring(7);
        StringBuffer sb = new StringBuffer();
        sb.append("Bearer ");
        sb.append(token);
        headers.set("Authorization", bearertoken);

        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)
                        .host(authIp)
                        .path("/V1/token/positionGet");
        UriComponents uriComponents = builder.build();
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> resultResponse =
                restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, entity, String.class);

        JSONObject obj = new JSONObject(resultResponse.getBody());
        logger.info("object - {}", obj.toString());
        Gson gson = new Gson();
        Response responseAccount = gson.fromJson(String.valueOf(obj), Response.class);
        logger.info("-------- {}", responseAccount.getResult().toString());

        return responseAccount.getResult().toString();
    }


    public Response ProfilePositionEditService(ProfilePositionEditRequestList dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();

        //현재 database 입력값
        List<ProfilePositionBaseDTO> beforPosition =
                profilePositionRepository.findByProfileIdAndCompanyProfileIdIsNull(userInfoDTO.getLastUseProfileId())
                        .stream().map(e -> modelMapper.map(e, ProfilePositionBaseDTO.class)).collect(Collectors.toList());
//                profileServiceRepository
//                .ProfilePositionSelectService(userInfoDTO.getLastUseProfileId());


        logger.info("현재 database 입력값 -{}", beforPosition);
        List<Long> befor = new ArrayList<>();
        //비교요청후 처리할 값 유저가 준 데이터
        for (ProfilePositionEditRequest f : dto.getProfilePositionEditRequestList()) {
            logger.info("=====시퀀스널체크=============");
            if (f.getProfilePositionSeq() != null) {
                befor.add(f.getProfilePositionSeq());
                //시퀀스가 없는값은 신규로 저장시킴
                logger.info("=====for=============");
            } else {
                if (f.getCompanyVerifyYn() == null || f.getCompanyVerifyYn().equals(null) || f.getCompanyVerifyYn().equals("N")) {
                    ProfilePositionBaseDTO baseDTO = modelMapper.map(f, ProfilePositionBaseDTO.class);
                    baseDTO.setProfileId(userInfoDTO.getLastUseProfileId());
                    baseDTO.setCompanyVerifyYn("N");
                    baseDTO.setRegUsr(userInfoDTO.getAccountId());
                    baseDTO.setModUsr(userInfoDTO.getAccountId());
                    baseDTO.setRegDt(LocalDateTime.now());
                    baseDTO.setModDt(LocalDateTime.now());
                    ProfilePositionBaseDTO result = profileServiceRepository
                            .ProfilePositionSaveService(baseDTO);
                    befor.add(result.getProfilePositionSeq());
                    logger.info("=====IF=============");
                    if (f.getCompanyProfileId() == null) {
                    } else {
                        ProfileCompanyRosterBaseDTO pcrBasedto = ProfileCompanyRosterBaseDTO
                                .builder()
                                .profileId(f.getCompanyProfileId())
                                .profilePositionSeq(result.getProfilePositionSeq())
                                .rosterStatus("RSS000001")
                                .build();
                        pcrBasedto.setRegUsr(userInfoDTO.getAccountId());
                        pcrBasedto.setModUsr(userInfoDTO.getAccountId());
                        pcrBasedto.setRegDt(LocalDateTime.now());
                        pcrBasedto.setModDt(LocalDateTime.now());
                        profileCompanyRosterRepository.save(modelMapper.map(pcrBasedto, ProfileCompanyRoster.class));
                        profilePositionRepository.UpdateProfilePositionYn("P", result.getProfileId(), result.getProfilePositionSeq());
                    }
                }
            }
        }
        //입력이 완료된 데이터를 불러온다.
        List<ProfilePositionBaseDTO> afterPosition = profilePositionRepository
                .findByProfileIdAndCompanyVerifyYn(userInfoDTO.getLastUseProfileId(), "N")
                .stream().map(e -> modelMapper.map(e, ProfilePositionBaseDTO.class)).collect(Collectors.toList());
        // 델리트값 구하기 / 입력이 완료된 데이터에서 프론트가 요청한 값을 뺀다
        logger.info("입력이 완료된 데이터를 불러온다. -{}", afterPosition);

        List<Long> after = new ArrayList<>();
        for (ProfilePositionBaseDTO e : afterPosition) {
            after.add(e.getProfilePositionSeq());
            logger.info("--------{}", e);
        }
        logger.info("-----{}", after);
        logger.info("-----{}", befor);

        after.removeAll(befor);
        for (Long e : after) {
            profilePositionRepository.deleteByProfilePositionSeq(e);
        }

        logger.info("지울 리스트 목록 투 스트링  ----------{}", after);


        //지운데이터를 조회해서 보내준다
        List<ProfilePositionBaseDTO> result = profileServiceRepository
                .ProfilePositionSelectService(userInfoDTO.getLastUseProfileId());
        logger.info("   //입력된 값들을 비교해서 지울 리스트. -{}", result);
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response PubCompanyService(Long profileId) {
        Response res = new Response();
        List<ProfilePositionBaseDTO> companyList = profilePositionRepository.findByProfileId(profileId).stream().map(e ->
                modelMapper.map(e, ProfilePositionBaseDTO.class)).collect(Collectors.toList());

        List<ProfileCompanyBaseDTO> result = new ArrayList<>();
        for (ProfilePositionBaseDTO e : companyList) {
            result.add(profileCompanyRepository.findById(e.getCompanyProfileId()).map(f ->
                    modelMapper.map(f, ProfileCompanyBaseDTO.class)).get());
        }
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilePositionSeqSelectService(Long seq) {
        Response res = new Response();

//        ProfilePositionBaseDTO baseDTO = profilePositionRepository.findByProfilePositionSeq(seq)
//                        .map(e-> modelMapper.map(e,ProfilePositionBaseDTO.class)).get();

        ProfilePositionResterResponse result = profilePositionRepository.findByProfilePositionSeq(seq)
                .map(e -> modelMapper.map(e, ProfilePositionResterResponse.class)).get();
        ProfileBaseDTO profile = profileRepository.findByProfileId(result.getProfileId())
                .map(e -> modelMapper.map(e, ProfileBaseDTO.class)).get();
        Account account = accountRepository.findByAccntId(profile.getAccntId()).get();
        result.setProfileImgUrl(profile.getProfileImgUrl());
        result.setFullName(profile.getFullName());
        result.setHeadline(profile.getHeadline());
        result.setAccntEmail(account.getAccntEmail());
        result.setPhoneNumber(account.getPhoneNumber());
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilePositionDeleteService(DeleteRequest dto) {
        Response res = new Response();

        if (profileCompanyRosterRepository
                .existsByProfilePositionSeqAndRosterStatus
                        (dto.getProfilePositionSeq(), "RSS000002")) {
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        profileCompanyRosterRepository.RosterDeleteProfilePositionSeq(dto.getProfilePositionSeq());
        profilePositionRepository.deleteByProfilePositionSeq(dto.getProfilePositionSeq());

        List<PositionCompanyResponse> result = profilePositionRepository
                .findByProfileIdOrderByProfilePositionSeqDesc(dto.getProfileId())
                .stream().map(e -> modelMapper.map(e, PositionCompanyResponse.class))
                .collect(Collectors.toList());
        result.stream().forEach(e -> {
            if(e.getCompanyProfileId() != null){
                ProfileCompany comre = profileCompanyRepository.findById(e.getCompanyProfileId()).orElse(null);
            e.setProfileCompanyName(comre.getProfileCompanyName());
            e.setProfileCompanyImgUrl(comre.getProfileImgUrl());
            };
        }
        );



        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilePositionAnrApplyYnService(DeleteRequest dto) {
        Response res = new Response();

        res.setResult(profileCompanyRosterRepository
                .findByProfilePositionSeq(dto.getProfilePositionSeq()).getRosterStatus());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}

