package com.sparwk.personalizationnode.profile.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileLanguageId implements Serializable {
    private String languageCd;
    private Long profileId;
}
