package com.sparwk.personalizationnode.profile.jpa.repository;


import com.sparwk.personalizationnode.profile.jpa.entity.ProfileAnrPitching;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface ProfileAnrPitchingRepository extends JpaRepository<ProfileAnrPitching, Long> {
//
//    @Transactional
//    @Modifying
//    @Query(
//            value = "UPDATE tb_profile_anr_pitching set pitching_yn = :pitchingYn " +
//                    "where profile_id = :profileId "
//            ,nativeQuery = true
//    )
//    int UpdateProfileUpdate(@Param("profileId")Long profileId, @Param("pitchingYn") String pitchingYn);

    boolean existsByProfileId(Long profileId);


}
