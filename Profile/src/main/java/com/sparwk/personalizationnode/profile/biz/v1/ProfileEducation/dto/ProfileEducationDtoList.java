package com.sparwk.personalizationnode.profile.biz.v1.ProfileEducation.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileEducationDtoList {

    @Schema(description = "프로필 에듀케이션 리스트")
    List<ProfileEducationDto> ProfileEducationDtoList;

}
