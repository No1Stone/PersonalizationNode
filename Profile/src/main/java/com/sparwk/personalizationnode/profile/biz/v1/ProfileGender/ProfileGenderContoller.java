package com.sparwk.personalizationnode.profile.biz.v1.ProfileGender;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileGender.dto.ProfileGenderDto;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGender.dto.ProfileGenderDtoList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileGenderBaseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/V1/gender")
@CrossOrigin("*")
@Api(tags = "Profile Server Gender API")
public class ProfileGenderContoller {

    @Autowired
    private ProfileGenderService profileGenderService;

    private final Logger logger = LoggerFactory.getLogger(ProfileGenderContoller.class);

    @ApiOperation(
            value = "프로필 젠더 정보 입력",
            notes = "젠더 정보 입력"
    )
    @PostMapping(path = "/info")
    public Response ProfileGenderSaveController(@Valid @RequestBody ProfileGenderDto dto) {
        Response res = profileGenderService.ProfileGenderSave(dto);
        return res;
    }

    @ApiOperation(
            value = "프로필 젠더 정보 검색",
            notes = "젠더 정보 검색"
    )
    @GetMapping(path = "/info/{profileId}")
    public Response ProfileGenderSelect(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileGenderService.ProfileGenderSelect(profileId);
        return res;
    }

    @PostMapping(path = "/delete")
    public Response profileGenderDelete(@Valid @RequestBody ProfileGenderDto dto){
        Response res = profileGenderService.ProfileGenderDelete(dto);
        return res;
    }

    @PostMapping(path = "/info/list")
    public Response profileGenderSaveList(@Valid @RequestBody ProfileGenderDtoList dto){
        Response res = profileGenderService.ProfileGenderSaveListService(dto);
        return res;
    }

}
