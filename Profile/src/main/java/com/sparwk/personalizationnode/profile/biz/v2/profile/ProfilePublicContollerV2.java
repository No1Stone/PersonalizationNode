package com.sparwk.personalizationnode.profile.biz.v2.profile;

import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/V2/profile/public")
@RequiredArgsConstructor
@Api(tags = "Profile Server Profile Public API V2")
public class ProfilePublicContollerV2 {

    private final ProfileServiceV2 profileService;
    private final Logger logger = LoggerFactory.getLogger(ProfilePublicContollerV2.class);
/*
    @GetMapping(path = "/profilePannel/{profileId}")
    public Response ProfilePannelV2SelectController(@PathVariable(name = "profileId")Long profileId){
        Response res = profileService.ProfilePannelV2SelectService(profileId);
        return res;
    }
*/
    @GetMapping(path = "/profilePannelCode/{profileId}")
    public Response ProfilePannelV2SelectCodeController(@PathVariable(name = "profileId")Long profileId){
        Response res = profileService.ProfilePannelV2SelectBeanCodeService(profileId);
        return res;
    }

    @GetMapping(path = "/test111")
    public Response test111Controller(){
        Response res = profileService.test111();
        return res;
    }
}
