package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel;


import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileCareerId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCareerResponse {

    private Long profileId;
    private String careerOrganizationNm;
    private String deptRoleNm;
    private String locationCityCountryCd;
    private String locationCityNm;
    private String presentYn;
    private Long careerSeq;
    private String filePath;
    private LocalDateTime careerStartDt;
    private LocalDateTime careerEndDt;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
//    @Builder
//    ProfileCareerResponse(
//            Long profileId,
//            String careerOrganizationNm,
//            String deptRoleNm,
//            String locationCityCountryCd,
//            String locationCityNm,
//            String presentYn,
//            Long careerSeq,
//            LocalDateTime careerStartDt,
//            LocalDateTime careerEndDt,
//            Long regUsr,
//            LocalDateTime regDt,
//            Long modUsr,
//            LocalDateTime modDt
//    ){
//        this.profileId=profileId;
//        this.careerOrganizationNm=careerOrganizationNm;
//        this.deptRoleNm=deptRoleNm;
//        this.locationCityCountryCd=locationCityCountryCd;
//        this.locationCityNm=locationCityNm;
//        this.presentYn=presentYn;
//        this.careerSeq=careerSeq;
//        this.careerStartDt=careerStartDt;
//        this.careerEndDt=careerEndDt;
//        this.regUsr = regUsr;
//        this.regDt = regDt;
//        this.modUsr = modUsr;
//        this.modDt = modDt;
//
//    }

}
