package com.sparwk.personalizationnode.profile.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileCompanyTimezoneId implements Serializable {
    private Long profileTimezoneBoxNum;
    private Long profileId;
}
