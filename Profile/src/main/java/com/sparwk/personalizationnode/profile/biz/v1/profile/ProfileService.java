package com.sparwk.personalizationnode.profile.biz.v1.profile;

import com.google.gson.Gson;
import com.sparwk.personalizationnode.profile.biz.chat.*;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto.EmailPhoneDto;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGender.ProfileGenderService;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGender.dto.ProfileGenderDto;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileLanguage.ProfileLanguageService;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileLanguage.dto.ProfileLanguageDto;
import com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.ProfilePositionService;
import com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto.ProfilePositionDto;
import com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto.ProfilePositionRequestDto;
import com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto.TokenResult;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileUseInfo.dto.ProfileUseInfoResponse;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.*;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.EmptyUpdate.*;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel.*;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.profilecreate.*;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.rsvp.AccountBaseDTO;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.rsvp.ProjectBaseDTO;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.rsvp.ProjectMembBaseDTO;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.rsvp.RsvpSendFrom;
import com.sparwk.personalizationnode.profile.biz.v2.profile.dto.pannel.ProfilePositionPannelResponse;
import com.sparwk.personalizationnode.profile.config.common.RestCall;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.*;
import com.sparwk.personalizationnode.profile.jpa.entity.*;
import com.sparwk.personalizationnode.profile.jpa.entity.account.Account;
import com.sparwk.personalizationnode.profile.jpa.entity.account.AccountCompanyDetail;
import com.sparwk.personalizationnode.profile.jpa.entity.account.AccountCompanyType;
import com.sparwk.personalizationnode.profile.jpa.repository.*;
import com.sparwk.personalizationnode.profile.jpa.repository.account.AccountCompanyDetailRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.account.AccountRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.project.ProjectMembRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.project.ProjectRepository;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileService {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ProfileGenderService profileGenderService;
    @Autowired
    private ProfileLanguageService profileLanguageService;
    @Autowired
    private ProfilePositionService profilePositionService;
    @Autowired
    private ProfileCompanyRepository profileCompanyRepository;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfileService.class);

    @Autowired
    private ProfileCareerRepository profileCareerRepository;
    @Autowired
    private ProfileEducationRepository profileEducationRepository;
    @Autowired
    private ProfileGenderRepository profileGenderRepository;
    @Autowired
    private ProfileInterestMetaRepository profileInterestMetaRepository;
    @Autowired
    private ProfileLanguageRepository profileLanguageRepository;
    @Autowired
    private ProfilePositionRepository profilePositionRepository;
    @Autowired
    private ProfileSkillRepository profileSkillRepository;
    @Autowired
    private ProfileOnthewebRepository profileOnthewebRepository;
    @Autowired
    private ProfileTimezoneRepository profileTimezoneRepository;
    @Autowired
    private ProfileCompanyPartnerRepository profileCompanyPartnerRepository;
    @Autowired
    private ProfileCompanyRosterRepository profileCompanyRosterRepository;
    @Autowired
    private ProfileCompanyStudioRepository profileCompanyStudioRepository;
    @Autowired
    private ProfileCompanyOnthewebRepository profileCompanyOnthewebRepository;
    @Autowired
    private ProfileCompanyTimezoneRepository profileCompanyTimezoneRepository;
    @Autowired
    private ProfileUseInfoRepository profileUseInfoRepository;
    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private ProjectMembRepository projectMembRepository;
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Value("${sparwk.node.project.projectip}")
    private String projectIp;

    @Value("${sparwk.node.personalization.accountip}")
    private String accountIp;

    @Value("${sparwk.node.personalization.authip}")
    private String authIp;

    @Value("${sparwk.node.personalization.profileip}")
    private String profileIp;

    @Value("${sparwk.node.externalconnect.mailingip}")
    private String mailingIp;

    @Value("${sparwk.node.communication.chat}")
    private String chat;

    @Value("${sparwk.node.schema}")
    private String schema;


    public Response ProfileSaveResponse(ProfileDto dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        Response res = new Response();
        if (dto.getProfileId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        ProfileBaseDTO profileBaseDTO = modelMapper.map(dto, ProfileBaseDTO.class);
        profileBaseDTO.setAccntId(userInfoDTO.getAccountId());
        profileBaseDTO.setModUsr(userInfoDTO.getAccountId());
        profileBaseDTO.setModDt(LocalDateTime.now());

        ProfileBaseDTO profileResult = profileServiceRepository
                .ProfileSaveService(profileBaseDTO);

        res.setResult(profileResult);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /*

    {
  "accomplishmentsInfo": "update",
  "bio": "update",
  "bthDay": "03",
  "bthMonth": "10",
  "bthYear": "1989",
  "caeInfo": "update",
  "currentCityCountryCd": "CNT000158",
  "currentCityNm": "seoul",
  "fullName": "update",
  "headline": "update",
  "hireMeYn": "N",
  "homeTownCountryCd": "CNT000158",
  "homeTownNm": "seoul",
  "ipiInfo": "update",
  "ipnInfo": "update",
  "isniInfo": "update",
  "mattermostId": "1m1qhmk918joqadqigc9o3vq18",
  "nroInfo": "update",
  "nroTypeCd": "update",
  "profileId": 10000000326,
  "profileImgUrl": "https://sparwk-profile.s3-ap-northeast-2.amazonaws.com/10000000304_a.jpg",
  "stageNameYn": "N",
  "useYn": "Y",
  "verifyYn": "Y"
}

     */

    public Response ProfileUpdateResponse(ProfileUpdateRequest dto) {
        Response res = new Response();
        if (dto.getProfileId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        String chatCreateURL = "https://" + chat + "/V1/user/update/profile/image";
        logger.info("chatCreateURL - -{}", chatCreateURL);

        UpdateMattermost umm = new UpdateMattermost();
        umm.setId(dto.getMattermostId());
        umm.setImgUrl(dto.getProfileImgUrl());
        String mmRes = RestCall.postRes(chatCreateURL, new Gson().toJson(umm));
        logger.info("mMRes - {}", mmRes);

        ProfileBaseDTO profileBaseDTO = modelMapper.map(dto, ProfileBaseDTO.class);
        profileBaseDTO.setAccntId(userInfoDTO.getAccountId());
        profileBaseDTO.setModUsr(userInfoDTO.getAccountId());
        profileBaseDTO.setModDt(LocalDateTime.now());
        long result = profileServiceRepository
                .ProfileDynamicUpdateService(profileBaseDTO);
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileAccountSelectList(Long AccountId) {
        List<ProfileBaseDTO> result = profileServiceRepository.ProfileSelectService(AccountId);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

//    public Long newProfileID() {
//        return profileRepository.newProfileIdSeq();
//    }

    @Lazy
    public AccountPassportRequest accountPassportProfileInsert(AccountPassportRequest apd) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", httpServletRequest.getHeader("Authorization"));
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)
                        .host(accountIp)
                        .path("/V1/passport/info");
        UriComponents uriComponents = builder.build();
        HttpEntity<AccountPassportRequest> entity = new HttpEntity<>(apd, headers);
//        ResponseEntity<String> tokenResponse =
//                restTemplate.exchange(uriComponents.toUriString(), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> tokenResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), entity, String.class);
        JSONObject obj = new JSONObject(tokenResponse.getBody());
        logger.info("passport value - {}", obj.toString());
        Gson gson = new Gson();
        TokenResult result = gson.fromJson(obj.toString(), TokenResult.class);
        AccountPassportRequest re = gson.fromJson(obj.toString(), AccountPassportRequest.class);
        return re;
    }

    public Response findByProfileIdSelectOneService(Long profileId) {
        Response res = new Response();
        int count = profileRepository.countByProfileId(profileId);
        if (count == 0) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        Account userAccountSelect = accountRepository.findById(profileRepository.findByProfileId(profileId).get().getAccntId()).get();

        ProfilePannelSelectOneResponse panelResponse = new ProfilePannelSelectOneResponse();
        logger.info("프로필싸이즈 ---------{}", userAccountSelect.getProfile().size());
        logger.info("프로필싸이즈 ---------{}", userAccountSelect.getProfile().get(0).getProfileId());
        logger.info("프로필싸이즈 ---------{}", profileId);
        for (Profile e : userAccountSelect.getProfile()) {
            logger.info("----------{}", e);
            if (e.getProfileId().equals(profileId)) {
                panelResponse = modelMapper.map(e, ProfilePannelSelectOneResponse.class);
                panelResponse.setAccntEmail(userAccountSelect.getAccntEmail());
                panelResponse.setPhoneNumber(userAccountSelect.getPhoneNumber());
                panelResponse.setProfilePosition(e.getProfilePosition());
                panelResponse.setProfileId(e.getProfileId());
                panelResponse.setProfileLanguage(e.getProfileLanguage());
                panelResponse.setFullName(e.getFullName());
                panelResponse.setProfileOntheweb(e.getProfileOntheweb());
            }
        }

//        ProfileBaseDTO result = modelMapper.map(profileServiceRepository.ProfileFindByprofileId(profileId), ProfileBaseDTO.class);
        res.setResult(panelResponse);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    @Autowired
    private AccountCompanyDetailRepository accountCompanyDetailRepository;

    public Response findByProfileIdSelectOneService2(Long profileId) {
        Response res = new Response();
        int count = profileRepository.countByProfileId(profileId);
        if (count == 0) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        Account userAccountSelect = accountRepository.findById(profileRepository.findByProfileId(profileId).get().getAccntId()).get();

        com.sparwk.personalizationnode.profile.biz.v2.profile.dto.pannel.ProfilePannelSelectOneResponse panelResponse = new com.sparwk.personalizationnode.profile.biz.v2.profile.dto.pannel.ProfilePannelSelectOneResponse();
        logger.info("프로필싸이즈 ---------{}", userAccountSelect.getProfile().size());
        logger.info("프로필싸이즈 ---------{}", userAccountSelect.getProfile().get(0).getProfileId());
        logger.info("프로필싸이즈 ---------{}", profileId);
        for (Profile e : userAccountSelect.getProfile()) {
            logger.info("----------여기니3?{}", e);
            if (e.getProfileId().equals(profileId)) {
                panelResponse = modelMapper.map(e, com.sparwk.personalizationnode.profile.biz.v2.profile.dto.pannel.ProfilePannelSelectOneResponse.class);
                panelResponse.setAccntEmail(userAccountSelect.getAccntEmail());
                panelResponse.setPhoneNumber(userAccountSelect.getPhoneNumber());
                logger.info("------------{여기니2?}");
                List<ProfilePositionPannelResponse> pppr = new ArrayList<>();
                for (ProfilePosition f : e.getProfilePosition()) {
                    logger.info("------------{여기니4?}");
                    ProfilePositionPannelResponse ppprBuild = modelMapper.map(f, ProfilePositionPannelResponse.class);
                    if (profileCompanyRepository.existsByProfileId(f.getCompanyProfileId())) {
                        ProfileCompanyBaseDTO baseDTO = profileCompanyRepository.findById(f.getCompanyProfileId())
                                .map(g -> modelMapper.map(g, ProfileCompanyBaseDTO.class)).get();
                        logger.info("------------{여기니1?}");
                        AccountCompanyDetail acResult = accountCompanyDetailRepository.findById(baseDTO.getAccntId()).get();
                        ppprBuild.setProfileCompanyImgUrl(baseDTO.getProfileImgUrl());
//                        ppprBuild.setProfileCompanyName(baseDTO.getProfileCompanyName());
                        ppprBuild.setProfileCompanyName(acResult.getCompanyName());
                    }
                    pppr.add(ppprBuild);
                }

                panelResponse.setProfilePositionPannelResponse(pppr);
                panelResponse.setProfileId(e.getProfileId());
                panelResponse.setProfileLanguage(e.getProfileLanguage());
                panelResponse.setFullName(e.getFullName());
                panelResponse.setProfileOntheweb(e.getProfileOntheweb());
            }
        }

//        ProfileBaseDTO result = modelMapper.map(profileServiceRepository.ProfileFindByprofileId(profileId), ProfileBaseDTO.class);
        res.setResult(panelResponse);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileProfileInfoSelectService(Long profileId) {
        Response res = new Response();
        int count = profileRepository.countByProfileId(profileId);
        if (count == 0) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        ProfileBaseDTO result = modelMapper.map(profileServiceRepository.ProfileFindByprofileId(profileId), ProfileBaseDTO.class);
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProjectMemberProfileService(String profileList) {
        Response res = new Response();
        String[] profileIdString = profileList.split(",");
        List<ProfileBaseDTO> profileSelectList = new ArrayList<>();
        List<ProjectMemberServerRequest> ProjectMemberServerRequest = new ArrayList<>();

//        for(String e : profileIdString){
//            profileSelectList.add(profileServiceRepository.ProfileFindByprofileId(Long.parseLong(e)));
//        }

        for (String e : profileIdString) {
            ProjectMemberServerRequest requestResult =
                    modelMapper.map(profileServiceRepository.ProfileFindByprofileId(Long.parseLong(e)), ProjectMemberServerRequest.class);
            requestResult.setAccountServerRequest(AccountServerRequestRestTemplate(requestResult.getAccntId()));
            ProjectMemberServerRequest.add(requestResult);
        }

        res.setResult(ProjectMemberServerRequest);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    @Async
    public void ProjectCowriterServerInsert(ProjectMemberAccountResponse res, HttpServletRequest req) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", req.getHeader("Authorization"));
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)
                        .host(projectIp)
                        .path("v1/project-song/cowriter");
        UriComponents uriComponents = builder.build();
        HttpEntity<ProjectMemberAccountResponse> entity = new HttpEntity<>(res, headers);
//        ResponseEntity<String> tokenResponse =
//                restTemplate.exchange(uriComponents.toUriString(), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> tokenResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), entity, String.class);
        JSONObject obj = new JSONObject(tokenResponse.getBody());
    }


    @Lazy
    public AccountServerRequest AccountServerRequestRestTemplate(Long accId) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("AccountId", accId.toString());
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)
                        .host(accountIp)
                        .path("/V1/account/info")
                        .path("/" + accId.toString());
        UriComponents uriComponents = builder.build();
        logger.info("ProjectMemberEmailPhoneSelect - {}", uriComponents.toString());
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response =
                restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, entity, String.class);
        logger.info("Response tostring = {}", response);
        JSONArray ar = new JSONArray(response.getBody());
        logger.info("result toString ar - {}", ar.toString());
//        JSONObject obj = new JSONObject(response.getBody());
//        logger.info("result toString obj - {}",obj.toString());

        Gson gson = new Gson();
        AccountServerRequest result = gson.fromJson(ar.toString(), AccountServerRequest.class);
        logger.info("result - {}", result.toString());
//        result = modelMapper.map(obj, TokenResult.class);
        return result;
    }


    @Lazy
    public Response AccountLastUseProfileIdTemplate(AccountResponse res) {
        HttpHeaders headers = new HttpHeaders();
        String bearertoken = httpServletRequest.getHeader("Authorization");
        logger.info("token", bearertoken);
        String token = bearertoken.substring(7);
        StringBuffer sb = new StringBuffer();
        sb.append("Bearer ");
        sb.append(token);
        headers.set("Authorization", sb.toString());
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)
                        .host(accountIp)
                        .path("V1/account/info/update");
        UriComponents uriComponents = builder.build();
        logger.info("accountLast profile tostring - {}", uriComponents.toString());
        HttpEntity<AccountResponse> entity = new HttpEntity<>(res, headers);
//        ResponseEntity<String> tokenResponse =
//                restTemplate.exchange(uriComponents.toUriString(), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> resultResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), entity, String.class);
        JSONObject obj = new JSONObject(resultResponse.getBody());
        logger.info("object - {}", obj.toString());

        Gson gson = new Gson();
        Response responseAccount = gson.fromJson(String.valueOf(obj), Response.class);
        logger.info("--------ACCRespoonse {}", responseAccount.getResultCd());
        return responseAccount;
    }

    /*
    {
      "accountPassportDto": {
        "countryCd": "CNT000146",
        "passportFirstName": "testfirst",
        "passportImgFileUrl": "testFile",
        "passportLastName": "testLastName",
        "passportMiddleName": "testMifleName",
        "passportVerifyYn": "N",
        "personalInfoCollectionYn": "Y"
      },
      "profileGenderRequestList": [
        {
          "genderCd": "GND000071"
        }
      ],
      "profileInterestMetaRequestList": [
        {
          "detailTypeCd": "DEX000019",
          "kindTypeCd": "DEX",
          "profileId": 0
        }
      ],
      "profileLanguageRequestList": [
        {
          "languageCd": "LAN000006"
        }
      ],
      "profilePositionRequestList": [
        {
          "anrYn": "N",
          "artistYn": "Y",
          "companyProfileId": ,
          "creatorYn": "N",
          "deptRoleInfo": "Dept",
          "primaryYn": "N"
        }
      ],
      "profileRequest": {
        "bthDay": "03",
        "bthMonth": "10",
        "bthYear": "1989",
        "currentCityCountryCd": "CNT000158",
        "currentCityNm": "seoul",
        "fullName": "jangwonseok",
        "headline": "haederLine",
        "homeTownCountryCd": "CNT000158",
        "homeTownNm": "seoul",
        "stageNameYn": "N"
      }
    }
     */
    public Response ProfileIntergrateService(RequestProfileCreate dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        logger.info("inter - {}", dto);

        ProfileBaseDTO profileresult = new ProfileBaseDTO();

        if (dto.getProfileRequest() == null) {
            res.setResultCd(ResultCodeConst.PROFILE_REQUEST_NULL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } else {
            ProfileBaseDTO profileInsert = modelMapper.map(dto.getProfileRequest(), ProfileBaseDTO.class);
            profileInsert.setAccntId(userInfoDTO.getAccountId());
            profileInsert.setUseYn("Y");
            profileInsert.setVerifyYn("N");
            profileInsert.setModUsr(userInfoDTO.getAccountId());
            profileInsert.setModDt(LocalDateTime.now());
            profileInsert.setRegUsr(userInfoDTO.getAccountId());
            profileInsert.setRegDt(LocalDateTime.now());
            profileresult = profileServiceRepository
                    .ProfileSaveService(profileInsert);
            userInfoDTO.setLastUseProfileId(profileresult.getProfileId());
            for (ProfileGenderRequest e : dto.getProfileGenderRequestList()) {
                ProfileGenderDto profileGenderDto = ProfileGenderDto.builder()
                        .profileId(profileresult.getProfileId())
                        .genderCd(e.getGenderCd())
                        .build();
                ProfileGenderBaseDTO genderRes = profileServiceRepository
                        .ProfileGenderSaveService(modelMapper.map(profileGenderDto, ProfileGenderBaseDTO.class));
            }
            for (ProfileLanguageRequest e : dto.getProfileLanguageRequestList()) {
                ProfileLanguageDto profileLanguageDto = ProfileLanguageDto.builder()
                        .profileId(profileresult.getProfileId())
                        .languageCd(e.getLanguageCd())
                        .build();
                ProfileLanguageBaseDTO langRes = profileServiceRepository
                        .ProfileLanguageSaveService(modelMapper.map(profileLanguageDto, ProfileLanguageBaseDTO.class));
            }
            for (ProfileInterestMetaRequest f : dto.getProfileInterestMetaRequestList()) {
                ProfileInterestMetaBaseDTO metaBaseDTO = modelMapper.map(f, ProfileInterestMetaBaseDTO.class);
                metaBaseDTO.setProfileId(profileresult.getProfileId());
                metaBaseDTO.setRegDt(LocalDateTime.now());
                metaBaseDTO.setModDt(LocalDateTime.now());
                metaBaseDTO.setRegUsr(userInfoDTO.getAccountId());
                metaBaseDTO.setModUsr(userInfoDTO.getAccountId());
                ProfileInterestMetaBaseDTO result = profileServiceRepository.ProfileInterestMetaSaveService(metaBaseDTO);
            }

            if (dto.getProfilePositionRequestList() != null || dto.getProfilePositionRequestList().isEmpty()) {
                List<ProfilePositionDto> profilePositionRequestDtoList = dto.getProfilePositionRequestList().stream().map(e ->
                                modelMapper.map(e, ProfilePositionDto.class))
                        .collect(Collectors.toList());
                profilePositionRequestDtoList.stream().forEach(e -> e.setProfileId(userInfoDTO.getLastUseProfileId()));
                profilePositionService.ProfilePositionListSaveAutoVerifyService(ProfilePositionRequestDto.builder()
                        .profilePositionList(profilePositionRequestDtoList)
                        .build());
            }


        }


        logger.info("profile Result - {}", profileresult);
        AccountResponse accLastEntity = AccountResponse.builder()
                .accntId(userInfoDTO.getAccountId())
                .lastUseProfileId(profileresult.getProfileId())
                .countryCd(dto.getProfileRequest().getCurrentCityCountryCd())
                .build();
        logger.info("accLastEntity - {}", new Gson().toJson(accLastEntity));

        Account account = accountRepository.findById(userInfoDTO.getAccountId()).orElse(null);
        Profile profileSe = profileRepository.findById(userInfoDTO.getLastUseProfileId()).orElse(null);

        Mattermost mm = new Mattermost();
        MmUser mu = new MmUser();
        mu.setEmail(account.getAccntEmail());
        mu.setFirstname(profileSe.getFullName());
        mm.setSparwkProfileId(userInfoDTO.getLastUseProfileId());
        mm.setImgUrl(profileSe.getProfileImgUrl());
        mm.setMmUser(mu);
        //로컬버전은 http 라서 고정스트링으로 설정
        String chatCreateURL = "https://" + chat + "/V1/user/create";
        logger.info("chatCreateURL - -{}", chatCreateURL);
        logger.info("mm - -{}", new Gson().toJson(mm));
        logger.info("mu - -{}", new Gson().toJson(mu));

        String mmRes = RestCall.postRes(chatCreateURL, new Gson().toJson(mm));
        String getData = new JSONObject(mmRes).getJSONObject("data").toString();
//        String getId =  new JSONObject(getData).getJSONArray("id").toString();
        CreateMmRes cm = new Gson().fromJson(getData, CreateMmRes.class);

        logger.info("getDaa = {}", getData);
        logger.info("getId = {}", cm.getId());
        profileRepository.UpdateProfileMattermostId(userInfoDTO.getLastUseProfileId(), cm.getId());

        logger.info("set accRes Response", accLastEntity.getAccntId());
        res = AccountLastUseProfileIdTemplate(accLastEntity);
        if (res.getResultCd().toString().equals("0000")) {
            logger.info("라스트유즈 프로필 업데이트 코드확인 _{}", res.getResultCd());
            logger.info("패쓰포트 값확인----------{}", dto.getAccountPassportDto());
            if (dto.getAccountPassportDto().getPassportImgFileUrl() == null
                    || dto.getAccountPassportDto().getPassportImgFileUrl().equals(null)
                    || dto.getAccountPassportDto().getPassportImgFileUrl().equals("")
                    || dto.getAccountPassportDto().getPassportImgFileUrl().isEmpty()
            ) {
            } else {
                dto.getAccountPassportDto().setPassportVerifyYn("N");
                accountPassportProfileInsert(dto.getAccountPassportDto());
            }
        } else {
            return res;
        }
        return res;
    }

    public Response ProfileIntegrateCreateResponse(RequestProfileCreate dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();

        if (dto.getProfileRequest().getFullName() == null || dto.getProfileRequest().getFullName().isEmpty()) {
            res.setResultCd(ResultCodeConst.PROFILE_NAME_NOT_FOUNE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        if (dto.getProfileRequest().getBthYear() == null || dto.getProfileRequest().getBthYear().isEmpty()) {
            res.setResultCd(ResultCodeConst.PROFILE_BIRTH_NOT_FOUND.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        if (dto.getProfileRequest().getHeadline() == null || dto.getProfileRequest().getHeadline().isEmpty()) {
            res.setResultCd(ResultCodeConst.PROFILE_HAEDLINE_NOT_FOUND.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        if (dto.getProfileInterestMetaRequestList() == null
                || dto.getProfileInterestMetaRequestList().isEmpty()
        )
//                || dto.getProfileInterestMetaRequestList().get(0).getKindTypeCd() == null
//                || dto.getProfileInterestMetaRequestList().get(0).getKindTypeCd().isEmpty()
        {
            res.setResultCd(ResultCodeConst.PROFILE_META_REQUEST_LIST_NULL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        if (dto.getProfileGenderRequestList() == null
                || dto.getProfileGenderRequestList().isEmpty()
//                || dto.getProfileGenderRequestList().get(0).getGenderCd() == null
//                || dto.getProfileGenderRequestList().get(0).getGenderCd().isEmpty()
        ) {
            res.setResultCd(ResultCodeConst.PROFILE_GENDER_REQUEST_NULL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        if (dto.getProfileLanguageRequestList() == null
                || dto.getProfileGenderRequestList().isEmpty()
//                || dto.getProfileLanguageRequestList().get(0).getLanguageCd() == null
//                || dto.getProfileLanguageRequestList().get(0).getLanguageCd().isEmpty()
        ) {
            res.setResultCd(ResultCodeConst.PROFILE_LANGUAGE_REQUEST_NULL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        res = ProfileIntergrateService(dto);

        logger.info("리스폰스!----{}", res.getResultCd());

        if (res.getResultCd().toString().equals("0000") || res.getResultCd().equals(0000)
        ) {
            //여기 위에 라스트프로필 업데이트 필요
            String re = profilePositionService.AuthtokenResilt();
            if (re == null || re.isEmpty()) {
                res.setResultCd(ResultCodeConst.NOT_FOUND_TOKEN.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            } else {
                res.setResult(re);
                res.setResultCd(ResultCodeConst.SUCCESS.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }
        } else {
            return res;
        }
    }

    @Transactional
    public Response ProfilePannelSelectService(Long profileId) {
        Response res = new Response();
        List<ProfilePositionResponse> positionList = profilePositionRepository.findByProfileId(profileId).stream().map(f ->
                modelMapper.map(f, ProfilePositionResponse.class)).collect(Collectors.toList());


        List<ProfileCompanyResponse> companyList = profileCompanyRepository.findById(profileId).stream().map(f ->
                modelMapper.map(f, ProfileCompanyResponse.class)).collect(Collectors.toList());

        ProfileResponse profile = profileRepository.findByProfileId(profileId).map(f ->
                modelMapper.map(f, ProfileResponse.class)).get();

        List<ProfileCareerResponse> career = profileCareerRepository.findByProfileId(profileId).stream().map(f ->
                modelMapper.map(f, ProfileCareerResponse.class)).collect(Collectors.toList());

        List<ProfileEducationResponse> edu = profileEducationRepository.findByProfileId(profileId).stream().map(f ->
                modelMapper.map(f, ProfileEducationResponse.class)).collect(Collectors.toList());

        List<ProfileGenderResponse> gender = profileGenderRepository.findByProfileId(profileId).stream().map(f ->
                modelMapper.map(f, ProfileGenderResponse.class)).collect(Collectors.toList());

        List<ProfileInterestMetaResponse> meta = profileInterestMetaRepository.findByProfileId(profileId).stream().map(f ->
                modelMapper.map(f, ProfileInterestMetaResponse.class)).collect(Collectors.toList());

        List<ProfileLanguageResponse> lang = profileLanguageRepository.findByProfileId(profileId).stream().map(f ->
                modelMapper.map(f, ProfileLanguageResponse.class)).collect(Collectors.toList());

        List<ProfileOnthewebResponse> onthe = profileOnthewebRepository.findByProfileId(profileId).stream().map(f ->
                modelMapper.map(f, ProfileOnthewebResponse.class)).collect(Collectors.toList());

        List<ProfileSkillResponse> skillList = profileSkillRepository.findByProfileId(profileId).stream().map(f ->
                modelMapper.map(f, ProfileSkillResponse.class)).collect(Collectors.toList());

        List<ProfileTimezoneResponse> time = profileTimezoneRepository.findByProfileId(profileId).stream().map(f ->
                modelMapper.map(f, ProfileTimezoneResponse.class)).collect(Collectors.toList());
        List<ProfileUseInfoResponse> useInfo = profileUseInfoRepository.findByLastUseProfileId(profileId).stream().map(f ->
                modelMapper.map(f, ProfileUseInfoResponse.class)).collect(Collectors.toList());
        ProfilePanelResponse panel = ProfilePanelResponse.builder()
                .profilePosition(positionList)
                .profileCareer(career)
                .profileCompany(companyList)
                .profileEducation(edu)
                .profileGender(gender)
                .profileInterestMeta(meta)
                .profileLanguage(lang)
                .profileOntheweb(onthe)
                .profileResponse(profile)
                .profileSkill(skillList)
                .profileTimezone(time)
                .profileUseInfo(useInfo)
                .build();
        res.setResult(panel);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    @Transactional
    public Response ProfilePannelSelectListService(ProfileArray pa) {
        Response res = new Response();

        String[] profileArray = pa.getProfileArray().split(",");
        List<ProfilePanelResponse> profilePanelResponsesList = new ArrayList<>();

        for (String e : profileArray) {
            List<ProfilePositionResponse> positionList = profilePositionRepository.findByProfileId(Long.parseLong(e)).stream().map(f ->
                    modelMapper.map(f, ProfilePositionResponse.class)).collect(Collectors.toList());
            List<ProfileCompanyResponse> companyList = profileCompanyRepository.findById(Long.parseLong(e)).stream().map(f ->
                    modelMapper.map(f, ProfileCompanyResponse.class)).collect(Collectors.toList());
            ProfileResponse profile = profileRepository.findByProfileId(Long.parseLong(e)).map(f ->
                    modelMapper.map(f, ProfileResponse.class)).get();
            List<ProfileCareerResponse> career = profileCareerRepository.findByProfileId(Long.parseLong(e)).stream().map(f ->
                    modelMapper.map(f, ProfileCareerResponse.class)).collect(Collectors.toList());
            List<ProfileEducationResponse> edu = profileEducationRepository.findByProfileId(Long.parseLong(e)).stream().map(f ->
                    modelMapper.map(f, ProfileEducationResponse.class)).collect(Collectors.toList());
            List<ProfileGenderResponse> gender = profileGenderRepository.findByProfileId(Long.parseLong(e)).stream().map(f ->
                    modelMapper.map(f, ProfileGenderResponse.class)).collect(Collectors.toList());
            List<ProfileInterestMetaResponse> meta = profileInterestMetaRepository.findByProfileId(Long.parseLong(e)).stream().map(f ->
                    modelMapper.map(f, ProfileInterestMetaResponse.class)).collect(Collectors.toList());
            List<ProfileLanguageResponse> lang = profileLanguageRepository.findByProfileId(Long.parseLong(e)).stream().map(f ->
                    modelMapper.map(f, ProfileLanguageResponse.class)).collect(Collectors.toList());
            List<ProfileOnthewebResponse> onthe = profileOnthewebRepository.findByProfileId(Long.parseLong(e)).stream().map(f ->
                    modelMapper.map(f, ProfileOnthewebResponse.class)).collect(Collectors.toList());
            List<ProfileSkillResponse> skillList = profileSkillRepository.findByProfileId(Long.parseLong(e)).stream().map(f ->
                    modelMapper.map(f, ProfileSkillResponse.class)).collect(Collectors.toList());
            List<ProfileTimezoneResponse> time = profileTimezoneRepository.findByProfileId(Long.parseLong(e)).stream().map(f ->
                    modelMapper.map(f, ProfileTimezoneResponse.class)).collect(Collectors.toList());

            List<ProfileUseInfoResponse> useInfo = profileUseInfoRepository.findByLastUseProfileId(Long.parseLong(e)).stream().map(f ->
                    modelMapper.map(f, ProfileUseInfoResponse.class)).collect(Collectors.toList());

            ProfilePanelResponse panel = ProfilePanelResponse.builder()
                    .profilePosition(positionList)
                    .profileCareer(career)
                    .profileCompany(companyList)
                    .profileEducation(edu)
                    .profileGender(gender)
                    .profileInterestMeta(meta)
                    .profileLanguage(lang)
                    .profileOntheweb(onthe)
                    .profileResponse(profile)
                    .profileSkill(skillList)
                    .profileTimezone(time)
                    .profileUseInfo(useInfo)
                    .build();
            profilePanelResponsesList.add(panel);
        }

        res.setResult(profilePanelResponsesList);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response EmialGetService(Long profileId) {
        ProfileBaseDTO accountIdFind = profileServiceRepository.ProfileFindByprofileId(profileId);

        AccountResponse accLastEntity = AccountResponse.builder()
                .accntId(accountIdFind.getAccntId())
                .build();
        HttpHeaders headers = new HttpHeaders();
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)
                        .host(accountIp)
                        .path("/V1/signUp/getEmail")
                        .path("/" + accountIdFind.getAccntId().toString());
        UriComponents uriComponents = builder.build();
        logger.info("ProjectMemberEmailPhoneSelect - {}", uriComponents.toString());
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response =
                restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, entity, String.class);
        logger.info("---------------{}", response.toString());
        logger.info("---------------{}", response.getBody().toString());
        Response res = new Response();
        res.setResult(response.getBody().toString());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response PhoneGetService(Long profileId) {
        ProfileBaseDTO accountIdFind = profileServiceRepository.ProfileFindByprofileId(profileId);

        AccountResponse accLastEntity = AccountResponse.builder()
                .accntId(accountIdFind.getAccntId())
                .build();
        HttpHeaders headers = new HttpHeaders();
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)
                        .host(accountIp)
                        .path("/V1/signUp/getPhone")
                        .path("/" + accountIdFind.getAccntId().toString());
        UriComponents uriComponents = builder.build();
        logger.info("ProjectMemberEmailPhoneSelect - {}", uriComponents.toString());
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response =
                restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, entity, String.class);
        logger.info("---------------{}", response.toString());
        logger.info("---------------{}", response.getBody().toString());
        Response res = new Response();
        res.setResult(response.getBody().toString());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response EmailPhoneGetService(Long profileId) {
        ProfileBaseDTO accountIdFind = profileServiceRepository.ProfileFindByprofileId(profileId);

        AccountResponse accLastEntity = AccountResponse.builder()
                .accntId(accountIdFind.getAccntId())
                .build();
        HttpHeaders headers = new HttpHeaders();
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)
                        .host(accountIp)
                        .path("/V1/signUp/getPhoneEmail")
                        .path("/" + accountIdFind.getAccntId().toString());
        UriComponents uriComponents = builder.build();
        logger.info("ProjectMemberEmailPhoneSelect - {}", uriComponents.toString());
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response =
                restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, entity, String.class);
        logger.info("---------------{}", response.toString());
        logger.info("---------------{}", response.getBody().toString());
        Gson gson = new Gson();
        EmailPhoneDto dto = gson.fromJson(response.getBody(), EmailPhoneDto.class);
        Response res = new Response();
        res.setResult(dto);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public <T> void POSTBasicTemplate(String schema, String host, String path, T entity) {

        String bearertoken = httpServletRequest.getHeader("Authorization");
//        logger.info("token", bearertoken);
//        String token = bearertoken.substring(7);
//        StringBuffer sb = new StringBuffer();
//        sb.append("Bearer ");
//        sb.append(token);
//        headers.set("Authorization", sb.toString());
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", bearertoken);
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)// http & https
                        .host(host) //localhost:8080
                        .path(path); //V1/profile/info
        UriComponents uriComponents = builder.build();
        logger.info("URI COMPONENT tostring - {}", uriComponents.toString());
        HttpEntity<T> responsEntity = new HttpEntity<>(entity, headers);
        ResponseEntity<String> resultResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), responsEntity, String.class);
        JSONObject obj = new JSONObject(resultResponse.getBody());
        logger.info("object - {}", obj.toString());
        Gson gson = new Gson();
        Response response = gson.fromJson(String.valueOf(obj), Response.class);
        logger.info("--------Respoonse {}", response.getResultCd());
    }

    public <T> void GETBasicTemplate(String schema, String host, String pathURL, String pathParam) {
        String bearertoken = httpServletRequest.getHeader("Authorization");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", bearertoken);
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)
                        .host(host)
                        .path(pathURL)
                        .path("/" + pathParam);
        UriComponents uriComponents = builder.build();
        logger.info("ProjectMemberEmailPhoneSelect - {}", uriComponents.toString());
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response =
                restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET, entity, String.class);
        logger.info("---------------{}", response.toString());
        logger.info("---------------{}", response.getBody().toString());
        Response res = new Response();
        res.setResult(response.getBody().toString());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
    }


    public Response NewPannel(Long profileId) {
        Optional<ProfilePanelResponse> profile = Optional.ofNullable(profileRepository.findByProfileId(profileId).map(
                e -> modelMapper.map(e, ProfilePanelResponse.class)).get());
//        ProfileResponse profileOne = profileRepository.findByProfileId(profileId).map(
//                e -> modelMapper.map(e, ProfileResponse.class))
//                .stream().sorted(Comparator.comparing(ProfileResponse::getRegDt));
//        profile.get().setProfileResponse(profileOne);
        Response res = new Response();
        res.setResult(profile);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public void test111(String pass) {


    }

    public Response RSVPprivateInviteService(Long projectId, Long rsvpProfileId) {
        Response res = new Response();
        logger.info("========={}", projectId);
        Response createForm = RSVPCreate(projectId, rsvpProfileId);
        RsvpSendFrom form = (RsvpSendFrom) createForm.getResult();
        logger.info("----------{}", form);

        String bearertoken = httpServletRequest.getHeader("Authorization");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", bearertoken);
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)// http & https
                        .host(mailingIp) //localhost:8080
                        .path("/V1/mail/RSVP/private/invite"); //V1/profile/info
        UriComponents uriComponents = builder.build();
        logger.info("URI COMPONENT tostring - {}", uriComponents.toString());
        HttpEntity<RsvpSendFrom> responsEntity = new HttpEntity<>(form, headers);
        ResponseEntity<String> resultResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), responsEntity, String.class);
        res.setResult(form);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response RSVPprivateApplyService(Long projectId, Long rsvpProfileId) {
        Response res = new Response();
        logger.info("========={}", projectId);
        Response createForm = RSVPCreate(projectId, rsvpProfileId);
        RsvpSendFrom form = (RsvpSendFrom) createForm.getResult();
        logger.info("----------{}", form);

        String bearertoken = httpServletRequest.getHeader("Authorization");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", bearertoken);
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)// http & https
                        .host(mailingIp) //localhost:8080
                        .path("/V1/mail/RSVP/private/apply"); //V1/profile/info
        UriComponents uriComponents = builder.build();
        logger.info("URI COMPONENT tostring - {}", uriComponents.toString());
        HttpEntity<RsvpSendFrom> responsEntity = new HttpEntity<>(form, headers);
        ResponseEntity<String> resultResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), responsEntity, String.class);
        res.setResult(form);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response RSVPpublicInviteService(Long projectId, Long rsvpProfileId) {
        Response res = new Response();
        logger.info("========={}", projectId);
        Response createForm = RSVPCreate(projectId, rsvpProfileId);
        RsvpSendFrom form = (RsvpSendFrom) createForm.getResult();
        logger.info("----------{}", form);

        String bearertoken = httpServletRequest.getHeader("Authorization");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", bearertoken);
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)// http & https
                        .host(mailingIp) //localhost:8080
                        .path("/V1/mail/RSVP/public/invite"); //V1/profile/info
        UriComponents uriComponents = builder.build();
        logger.info("URI COMPONENT tostring - {}", uriComponents.toString());
        HttpEntity<RsvpSendFrom> responsEntity = new HttpEntity<>(form, headers);
        ResponseEntity<String> resultResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), responsEntity, String.class);
        res.setResult(form);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response RSVPpublicApplyService(Long projectId, Long rsvpProfileId) {
        Response res = new Response();
        logger.info("========={}", projectId);
        Response createForm = RSVPCreate(projectId, rsvpProfileId);
        RsvpSendFrom form = (RsvpSendFrom) createForm.getResult();
        logger.info("----------{}", form);

        String bearertoken = httpServletRequest.getHeader("Authorization");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", bearertoken);
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)// http & https
                        .host(mailingIp) //localhost:8080
                        .path("/V1/mail/RSVP/public/apply"); //V1/profile/info
        UriComponents uriComponents = builder.build();
        logger.info("URI COMPONENT tostring - {}", uriComponents.toString());
        HttpEntity<RsvpSendFrom> responsEntity = new HttpEntity<>(form, headers);
        ResponseEntity<String> resultResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), responsEntity, String.class);
        res.setResult(form);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    private Response RSVPCreate(Long projectId, Long rsvpProfileId) {
        Response res = new Response();
        int projectCount = projectRepository.countByProjId(projectId);
        int rsvpProfileCount = profileRepository.countByProfileId(rsvpProfileId);
        if (projectCount == 0) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        if (rsvpProfileCount == 0) {
            res.setResultCd(ResultCodeConst.NOT_FOUNT_INVITATION_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        ProjectBaseDTO projectBaseDTO = projectRepository.findById(projectId)
                .map(e -> modelMapper.map(e, ProjectBaseDTO.class)).get();
        int ownerProfileCount = profileRepository.countByProfileId(projectBaseDTO.getProjOwner());
        if (ownerProfileCount == 0) {
            res.setResultCd(ResultCodeConst.NOT_FOUNT_OWNER_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        ProfileBaseDTO profileBaseDTO = profileRepository.findByProfileId(projectBaseDTO.getProjOwner())
                .map(e -> modelMapper.map(e, ProfileBaseDTO.class)).get();
        List<String> memberName = new ArrayList<>();
        for (ProjectMembBaseDTO e : projectBaseDTO.getProjectMemb()) {
            memberName.add(profileRepository.findByProfileId(e.getProfileId())
                    .map(f -> modelMapper.map(f, ProfileBaseDTO.class).getFullName()).get());
        }
//        if(memberName.size() == 0){
//            memberName = "";
//        }
        int ownerEmailCount = accountRepository.countByAccntId(profileBaseDTO.getAccntId());
        if (ownerEmailCount == 0) {
            res.setResultCd(ResultCodeConst.NOT_FOUNT_OWNER_EMAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        Long rsvpAcc = profileRepository.findByProfileId(rsvpProfileId)
                .map(e -> modelMapper.map(e, ProfileBaseDTO.class)).get().getAccntId();
        int rsvpEmailCount = accountRepository.countByAccntId(rsvpAcc);
        if (rsvpEmailCount == 0) {
            res.setResultCd(ResultCodeConst.NOT_FOUNT_INVITATION_EMAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        String onwEmail = accountRepository.findById(profileBaseDTO.getAccntId()).map(e ->
                modelMapper.map(e, AccountBaseDTO.class).getAccntEmail()).get();
        String receEmail = accountRepository.findById(rsvpAcc).map(e ->
                modelMapper.map(e, AccountBaseDTO.class).getAccntEmail()).get();
        logger.info("=======전체- {}", projectBaseDTO);
        logger.info("=======멈버수- {}", projectBaseDTO.getProjectMemb().size());
        logger.info("=======프로젝트비밀번호- {}", projectBaseDTO.getProjPwd());
        logger.info("=======프로젝트타이틀- {}", projectBaseDTO.getProjInvtTitle());
        logger.info("=======프로젝트설명- {}", projectBaseDTO.getProjDesc());
        logger.info("=======프로젝트부가설명- {}", projectBaseDTO.getProjWhatFor());
        logger.info("=======프로젝트오너아이디- {}", projectBaseDTO.getProjOwner());
        logger.info("=======오너이름- {}", profileBaseDTO.getFullName());
        logger.info("=======오너 헤드라인- {}", profileBaseDTO.getHeadline());
        logger.info("=======오너 프로필사진- {}", profileBaseDTO.getProfileImgUrl());
        logger.info("=======오너이메일 - {}", onwEmail);
        logger.info("=======받는사람이메일 - {}", receEmail);
        RsvpSendFrom form = RsvpSendFrom.builder()
                .projId(projectId)
                .rsvpProfileId(rsvpProfileId)
                .SenderEmail(onwEmail)
                .receiverEmail(receEmail)
                .memberCount(projectBaseDTO.getProjectMemb().size())
                .projectPassword(projectBaseDTO.getProjPwd())
                .projectTitle(projectBaseDTO.getProjInvtTitle())
                .projectDesctip(projectBaseDTO.getProjInvtDesc())
                .projectSubDesctip(projectBaseDTO.getProjWhatFor())
                .ownerName(profileBaseDTO.getFullName())
                .ownerHeadLine(profileBaseDTO.getHeadline())
                .ownerProfileImg(profileBaseDTO.getProfileImgUrl())
                .memberName(memberName.toString())
                .build();
        res.setResult(form);
        logger.info("=======폼 출력 - {}", form);
        return res;
    }

    public Response ProfileUpdateCaeService(ProfileCaeUpdateDto dto) {
        Response res = new Response();

        Profile profile = profileRepository.findByProfileId(dto.getProfileId()).get();
        profile.setCaeInfo(dto.getCaeInfo());
        profileRepository.save(profile);
        res.setResult(profileRepository.findByProfileId(dto.getProfileId()).map(e -> modelMapper.map(e, ProfileBaseDTO.class)).get());

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileUpdateIpiService(ProfileIpiUpdateDto dto) {
        Response res = new Response();

        Profile profile = profileRepository.findByProfileId(dto.getProfileId()).get();
        profile.setIpiInfo(dto.getIpiInfo());
        profile.setVerifyYn("N");
        profileRepository.save(profile);
        res.setResult(profileRepository.findByProfileId(dto.getProfileId()).map(e -> modelMapper.map(e, ProfileBaseDTO.class)).get());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileUpdateIpnService(ProfileIpnUpdateDto dto) {
        Response res = new Response();
        ProfileBaseDTO profileBaseDTO = profileRepository.findByProfileId(dto.getProfileId()).map(e ->
                modelMapper.map(e, ProfileBaseDTO.class)).get();
        Profile profile = profileRepository.findByProfileId(dto.getProfileId()).get();
        profile.setIpnInfo(dto.getIpnInfo());
        profileRepository.save(profile);
        res.setResult(profileRepository.findByProfileId(dto.getProfileId()).map(e -> modelMapper.map(e, ProfileBaseDTO.class)).get());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileUpdateIsniService(ProfileIsniUpdateDto dto) {
        Response res = new Response();

        Profile profile = profileRepository.findByProfileId(dto.getProfileId()).get();
        profile.setIsniInfo(dto.getIsniInfo());
        profileRepository.save(profile);
        res.setResult(profileRepository.findByProfileId(dto.getProfileId()).map(e -> modelMapper.map(e, ProfileBaseDTO.class)).get());

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileUpdateNroService(ProfileNroUpdateDto dto) {
        Response res = new Response();

        Profile profile = profileRepository.findByProfileId(dto.getProfileId()).get();
        profile.setNroInfo(dto.getNroInfo());
        profile.setNroTypeCd(dto.getNroTypeCd());
        profileRepository.save(profile);

        res.setResult(profileRepository.findByProfileId(dto.getProfileId()).map(e -> modelMapper.map(e, ProfileBaseDTO.class)).get());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response profileTest2(Long profileId) {
        Response res = new Response();
        res.setResult(profileRepository.findByProfileId(profileId).orElse(null));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public void testmmCon(Long profileId) {
        logger.info("select mm = {}",
                profileRepository.SelectMmInfo(profileId));
    }

    public String mmGetService(Long profileId) {
        String mmId = "";
        if (profileRepository.existsById(profileId)) {
            mmId = profileRepository.findById(profileId).orElse(null).getMattermostId();
        }
        if (profileCompanyRepository.existsById(profileId)) {
            mmId = profileCompanyRepository.findById(profileId).orElse(null).getMattermostId();
        }
        logger.info("mm id - - -{}",mmId);
        return mmId;
    }

    public String mmComidGetService(Long profileId) {
        String mmId = "0";

        if (profilePositionRepository
                .existsByProfileIdAndPrimaryYnAndCompanyVerifyYn
                        (profileId, "Y", "Y")) {
            ProfilePosition pp = profilePositionRepository
                    .findByProfileIdAndPrimaryYn(profileId, "Y");
            ProfileCompany pc = profileCompanyRepository.findById(pp.getCompanyProfileId()).orElse(null);
            mmId = pc.getProfileId().toString();
        }
        logger.info("mmcomid - - -{}",mmId);
        return mmId;
    }
}
