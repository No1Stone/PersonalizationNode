package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileGender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProfileGenderRepository extends JpaRepository<ProfileGender, String> {

    Long deleteByGenderCdAndProfileId(String genderCd, Long profileId);
    List<ProfileGender> findByProfileId(Long profileId);
    @Transactional
    void deleteByProfileIdAndGenderCd(Long profileId, String genderCd);

}
