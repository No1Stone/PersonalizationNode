package com.sparwk.personalizationnode.profile.jpa.entity.code;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_anr_code")
public class AdminAnrCode {

    @Id
    @Column(name = "anr_seq", nullable = true)
    private Long anrSeq;
    @Column(name = "anr_service_name", nullable = true)
    private String anrServiceName;
    @Column(name = "anr_group_cd", nullable = true)
    private String anrGroupCd;
    @Column(name = "anr_cd", nullable = true)
    private String anrCd;
    @Column(name = "description", nullable = true)
    private String description;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;


    @Builder
    AdminAnrCode(
            Long anrSeq,
            String anrServiceName,
            String anrGroupCd,
            String anrCd,
            String description,
            String useYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.anrSeq = anrSeq;
        this.anrServiceName = anrServiceName;
        this.anrGroupCd = anrGroupCd;
        this.anrCd = anrCd;
        this.description = description;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }


}
