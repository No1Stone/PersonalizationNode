package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileInterestMeta;
import com.sparwk.personalizationnode.profile.jpa.repository.dsl.ProfileInterestMetaRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProfileInterestMetaRepository extends JpaRepository<ProfileInterestMeta, String>, ProfileInterestMetaRepositoryDynamic {

    List<ProfileInterestMeta> findByProfileId(Long profileId);

    @Transactional
    void deleteByProfileIdAndDetailTypeCdAndKindTypeCd(Long profileId, String detail, String kind);

}
