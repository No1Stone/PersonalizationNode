package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.EmptyUpdate;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileNroUpdateDto {
    @Schema(description = "프로필 아이디")
    private Long profileId;
    private String nroTypeCd;
    private String nroInfo;

}
