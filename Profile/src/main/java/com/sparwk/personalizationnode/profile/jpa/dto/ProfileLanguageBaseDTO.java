package com.sparwk.personalizationnode.profile.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileLanguageBaseDTO {

    private Long profileId;
    private String languageCd;

}
