package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompany;
import com.sparwk.personalizationnode.profile.jpa.repository.dsl.ProfileCompanyRepositoryDynamic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ProfileCompanyRepository extends JpaRepository<ProfileCompany,Long>, ProfileCompanyRepositoryDynamic {

    @Override
    Optional<ProfileCompany> findById(Long aLong);

    @Override
    Page<ProfileCompany> findAll(Pageable pageable);

    Page<ProfileCompany> findByAccntIdIn(List<Long> accid, Pageable pageable);
    Page<ProfileCompany> findByAccntIdInOrderByProfileCompanyNameAsc(List<Long> accid, Pageable pageable);

    Page<ProfileCompany> findAllByAccntIdInAndProfileCompanyNameLike(List<Long> accid, String comname, Pageable pageable);

    List<ProfileCompany> findAllByOrderByProfileCompanyNameAsc();

    boolean existsByProfileId(Long profileId);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_company set mattermost_id = :mertterId where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileCompanyMattermostId(@Param("profileId")Long profileId, @Param("mertterId") String mertterId);


}
