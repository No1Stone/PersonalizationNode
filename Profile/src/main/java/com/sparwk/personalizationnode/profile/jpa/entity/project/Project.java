package com.sparwk.personalizationnode.profile.jpa.entity.project;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompany;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_project")
public class Project {

    @Id
    @Column(name = "proj_id", nullable = true)
    private Long projId;
    @Column(name = "proj_owner", nullable = true)
    private Long projOwner;
    @Column(name = "proj_aval_yn", nullable = true)
    private String projAvalYn;
    @Column(name = "gender_cd", nullable = true)
    private String genderCd;
    @Column(name = "proj_indiv_comp_type", nullable = true)
    private String projIndivCompType;
    @Column(name = "proj_publ_yn", nullable = true)
    private String projPublYn;
    @Column(name = "proj_pwd", nullable = true)
    private String projPwd;
    @Column(name = "proj_invt_title", nullable = true)
    private String projInvtTitle;
    @Column(name = "proj_invt_desc", nullable = true)
    private String projInvtDesc;
    @Column(name = "proj_what_for", nullable = true)
    private String projWhatFor;
    @Column(name = "pitch_proj_type_cd", nullable = true)
    private String pitchProjTypeCd;
    @Column(name = "proj_work_locat", nullable = true)
    private String projWorkLocat;
    @Column(name = "proj_title", nullable = true)
    private String projTitle;
    @Column(name = "proj_desc", nullable = true)
    private String projDesc;
    @Column(name = "proj_ddl_dt", nullable = true)
    private LocalDate projDdlDt;
    @Column(name = "proj_cond_cd", nullable = true)
    private String projCondCd;
    @Column(name = "min_val", nullable = true)
    private long minVal;
    @Column(name = "max_val", nullable = true)
    private long maxVal;
    @Column(name = "recruit_yn", nullable = true)
    private String recruitYn;
    @Column(name = "avatar_file_url", nullable = true)
    private String avatarFileUrl;
    @Column(name = "leave_yn", nullable = true)
    private String leaveYn;
    @Column(name = "complete_yn", nullable = true)
    private String completeYn;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    Project(
            long projId,
            long projOwner,
            String projAvalYn,
            String projIndivCompType,
            String projPublYn,
            String projPwd,
            String projInvtTitle,
            String projInvtDesc,
            String projWhatFor,
            String pitchProjTypeCd,
            String projWorkLocat,
            String projTitle,
            String projDesc,
            LocalDate projDdlDt,
            String projCondCd,
            long minVal,
            long maxVal,
            String recruitYn,
            String avatarFileUrl,
            String leaveYn,
            String completeYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.projId = projId;
        this.projOwner = projOwner;
        this.projAvalYn = projAvalYn;
        this.projIndivCompType = projIndivCompType;
        this.projPublYn = projPublYn;
        this.projPwd = projPwd;
        this.projInvtTitle = projInvtTitle;
        this.projInvtDesc = projInvtDesc;
        this.projWhatFor = projWhatFor;
        this.pitchProjTypeCd = pitchProjTypeCd;
        this.projWorkLocat = projWorkLocat;
        this.projTitle = projTitle;
        this.projDesc = projDesc;
        this.projDdlDt = projDdlDt;
        this.projCondCd = projCondCd;
        this.minVal = minVal;
        this.maxVal = maxVal;
        this.recruitYn = recruitYn;
        this.avatarFileUrl = avatarFileUrl;
        this.leaveYn = leaveYn;
        this.completeYn = completeYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProjectMemb.class)
    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id")
    private List<ProjectMemb> projectMemb;

}
