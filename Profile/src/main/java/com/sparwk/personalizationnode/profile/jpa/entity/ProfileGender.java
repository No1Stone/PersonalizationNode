package com.sparwk.personalizationnode.profile.jpa.entity;

import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileGenderId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_gender")
@DynamicUpdate
@IdClass(ProfileGenderId.class)
public class ProfileGender {
    @Id
    @Column(name = "gender_cd", length = 9)
    private String genderCd;

    @Id
    @Column(name = "profile_id")
    private Long profileId;

    @Builder
    ProfileGender(
            Long profileId,
            String genderCd
    ) {
        this.profileId = profileId;
        this.genderCd = genderCd;
    }

}
