package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileInterestMetaBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.QProfileInterestMeta;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class ProfileInterestMetaRepositoryDynamicImpl implements ProfileInterestMetaRepositoryDynamic {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(ProfileInterestMetaRepositoryDynamicImpl.class);
    private QProfileInterestMeta qProfileInterestMeta = QProfileInterestMeta.profileInterestMeta;

    private ProfileInterestMetaRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long ProfileInterestMetaRepositoryDynamicUpdate(ProfileInterestMetaBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProfileInterestMeta);


        if (entity.getKindTypeCd() == null || entity.getKindTypeCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileInterestMeta.kindTypeCd, entity.getKindTypeCd());
        }
        if (entity.getDetailTypeCd() == null || entity.getDetailTypeCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileInterestMeta.detailTypeCd, entity.getDetailTypeCd());
        }
        if(entity.getRegUsr() != null) {jpaUpdateClause.set(qProfileInterestMeta.regUsr, entity.getRegUsr());}
        if(entity.getRegDt() != null) {jpaUpdateClause.set(qProfileInterestMeta.regDt, entity.getRegDt());}
        if(entity.getModUsr() != null) {jpaUpdateClause.set(qProfileInterestMeta.modUsr, entity.getModUsr());}
        if(entity.getModDt() != null) {jpaUpdateClause.set(qProfileInterestMeta.modDt, entity.getModDt());}

        jpaUpdateClause.where(qProfileInterestMeta.profileId.eq(entity.getProfileId()));


        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
