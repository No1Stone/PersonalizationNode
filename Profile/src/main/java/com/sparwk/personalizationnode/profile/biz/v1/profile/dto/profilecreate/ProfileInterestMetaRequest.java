package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.profilecreate;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileInterestMetaRequest {
    @Schema(description = "종류 코드")
    private String kindTypeCd;
    @Schema(description = "세부 코드")
    private String detailTypeCd;
}
