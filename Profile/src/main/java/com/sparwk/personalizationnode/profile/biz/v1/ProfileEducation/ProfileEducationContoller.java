package com.sparwk.personalizationnode.profile.biz.v1.ProfileEducation;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileEducation.dto.ProfileEducationDeleteRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileEducation.dto.ProfileEducationDto;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileEducation.dto.ProfileEducationDtoList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/education")
@CrossOrigin("*")
@Api(tags = "Profile Server Education API")
public class ProfileEducationContoller {

    @Autowired
    private ProfileEducationService profileEducationService;

    private final Logger logger = LoggerFactory.getLogger(ProfileEducationContoller.class);
    @ApiOperation(
            value = "프로필 교육 정보 입력",
            notes = "교육 정보 입력"
    )
    @PostMapping(path = "/info")
    public Response ProfileEducationSaveController(@Valid @RequestBody ProfileEducationDto dto) {

        Response res = profileEducationService.ProfileEducationSave(dto);
        return res;
    }

    @ApiOperation(
            value = "프로필 교육 정보 검색",
            notes = "교육 정보 검색"
    )
    @GetMapping(path = "/info/{profileId}")
    public Response ProfileEducationSelect(@PathVariable(name = "profileId")Long profileId) {
        var res = profileEducationService.ProfileEducationSelect(profileId);
        return res;
    }
    @ApiOperation(
            value = "프로필 교육 정보 업데이트",
            notes = "교육 정보 검색"
    )
    @PostMapping(path = "/info/update")
    public Response ProfileEducationDynamicUpdateController(@Valid @RequestBody ProfileEducationDto dto) {
        var res = profileEducationService.ProfileEducationDynamicUpdate(dto);
        return res;
    }

    @ApiOperation(
            value = "회사정보 리스트",
            notes = "회사정보 입력"
    )
    @PostMapping(path = "/info/list")
    public Response ProfileEducationSaveListController(
            @Valid @RequestBody ProfileEducationDtoList dto) {

        logger.info("controller init data = ----------{}",dto);
        Response res = profileEducationService.ProfileEducationListSave(dto);
        return res;
    }

    @PostMapping(path = "/delete")
    public Response ProfileEducationDeleteController(
            @Valid @RequestBody ProfileEducationDeleteRequest dto){
        Response res = profileEducationService.ProfileEducationDeleteService(dto);
        return res;
    }



}
