package com.sparwk.personalizationnode.profile.biz.chat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MmUser {

    private String email;
    private String firstname;
    private String lastname;

}
