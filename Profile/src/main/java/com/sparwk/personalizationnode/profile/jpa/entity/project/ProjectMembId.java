package com.sparwk.personalizationnode.profile.jpa.entity.project;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectMembId implements Serializable {
    private Long projId;
    private Long profileId;
}
