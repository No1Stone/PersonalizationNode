package com.sparwk.personalizationnode.profile.biz.v1.ProfileUseInfo.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileUseInfoResponse {

    private Long lastUseProfileId;
    private LocalDateTime LastUseDt;

}
