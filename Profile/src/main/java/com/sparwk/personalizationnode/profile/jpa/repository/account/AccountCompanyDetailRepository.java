package com.sparwk.personalizationnode.profile.jpa.repository.account;

import com.sparwk.personalizationnode.profile.jpa.entity.account.AccountCompanyDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface AccountCompanyDetailRepository extends JpaRepository<AccountCompanyDetail, Long> {

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_account_company_detail " +
                    "set company_name = :companyName " +
                    "where accnt_id = :accntId"
            ,nativeQuery = true
    )
    int UpdateProfileCompanyName(@Param("accntId")Long accntId, @Param("companyName") String companyName);


}
