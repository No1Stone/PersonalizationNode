package com.sparwk.personalizationnode.profile.jpa;

import com.sparwk.personalizationnode.profile.jpa.dto.*;
import com.sparwk.personalizationnode.profile.jpa.entity.*;
import com.sparwk.personalizationnode.profile.jpa.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ProfileServiceRepository {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private ProfileCareerRepository profileCareerRepository;
    @Autowired
    private ProfileEducationRepository profileEducationRepository;
    @Autowired
    private ProfileGenderRepository profileGenderRepository;
    @Autowired
    private ProfileInterestMetaRepository profileInterestMetaRepository;
    @Autowired
    private ProfileLanguageRepository profileLanguageRepository;
    @Autowired
    private ProfilePositionRepository profilePositionRepository;
    @Autowired
    private ProfileSkillRepository profileSkillRepository;
    @Autowired
    private ProfileOnthewebRepository profileOnthewebRepository;
    @Autowired
    private ProfileTimezoneRepository profileTimezoneRepository;
    @Autowired
    private ProfileCompanyPartnerRepository profileCompanyPartnerRepository;
    @Autowired
    private ProfileCompanyRepository profileCompanyRepository;
    @Autowired
    private ProfileCompanyRosterRepository profileCompanyRosterRepository;
    @Autowired
    private ProfileCompanyStudioRepository profileCompanyStudioRepository;
    @Autowired
    private ProfileCompanyOnthewebRepository profileCompanyOnthewebRepository;
    @Autowired
    private ProfileCompanyTimezoneRepository profileCompanyTimezoneRepository;
    @Autowired
    private ProfileUseInfoRepository profileUseInfoRepository;


    public void testSave(ProfileGenderBaseDTO dto) {
        ProfileGender a = modelMapper.map(dto, ProfileGender.class);
        profileGenderRepository.save(a);

    }

    public List<Profile> selectTest(Long id) {
        return profileRepository.findById(id)
                .stream().map(e -> modelMapper.map(e, Profile.class))
                .collect(Collectors.toList());
    }

    public long profilesaveTest(Profile entity) {
        profileRepository.save(entity);
        long result = 1L;
        return result;
    }

    public List<ProfileBaseDTO> ProfileSelectService(Long profileId) {
        return profileRepository
                .findById(profileId).stream().map(e -> modelMapper.map(e, ProfileBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<ProfileCareerBaseDTO> ProfileCareerSelectService(Long profileId) {
        return profileCareerRepository
                .findByProfileId(profileId).stream().map(e -> modelMapper.map(e, ProfileCareerBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<ProfileEducationBaseDTO> ProfileEducationSelectService(Long profileId) {
        return profileEducationRepository
                .findByProfileId(profileId).stream().map(e -> modelMapper.map(e, ProfileEducationBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<ProfileGenderBaseDTO> ProfileGenderSelectService(Long profileId) {
        return profileGenderRepository
                .findByProfileId(profileId).stream().map(e -> modelMapper.map(e, ProfileGenderBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<ProfileInterestMetaBaseDTO> ProfileInterestMetaSelectService(Long profileId) {
        return profileInterestMetaRepository
                .findByProfileId(profileId).stream().map(e -> modelMapper.map(e, ProfileInterestMetaBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<ProfileLanguageBaseDTO> ProfileLanguageSelectService(Long profileId) {
        return profileLanguageRepository
                .findByProfileId(profileId).stream().map(e -> modelMapper.map(e, ProfileLanguageBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<ProfilePositionBaseDTO> ProfilePositionSelectService(Long profileId) {
        return profilePositionRepository
                .findByProfileId(profileId).stream().map(e -> modelMapper.map(e, ProfilePositionBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<ProfileSkillBaseDTO> ProfileSkillSelectService(Long profileId) {
        return profileSkillRepository
                .findByProfileId(profileId).stream().map(e -> modelMapper.map(e, ProfileSkillBaseDTO.class))
                .collect(Collectors.toList());
    }


    public ProfileBaseDTO ProfileSaveService(ProfileBaseDTO DTO) {
        Profile entity = modelMapper.map(DTO, Profile.class);
        profileRepository.saveAndFlush(entity);
        ProfileBaseDTO result = modelMapper.map(entity, ProfileBaseDTO.class);
        return result;
    }

    public ProfileCareerBaseDTO ProfileCareerSaveService(ProfileCareerBaseDTO DTO) {
        ProfileCareer entity = modelMapper.map(DTO, ProfileCareer.class);
        profileCareerRepository.save(entity);
        ProfileCareerBaseDTO result = modelMapper.map(entity, ProfileCareerBaseDTO.class);
        return result;
    }

    @Transactional
    public ProfileEducationBaseDTO ProfileEducationSaveService(ProfileEducationBaseDTO DTO) {
        ProfileEducation entity = modelMapper.map(DTO, ProfileEducation.class);
        profileEducationRepository.save(entity);
        ProfileEducationBaseDTO result = modelMapper.map(entity, ProfileEducationBaseDTO.class);
        return result;
    }

    public ProfileGenderBaseDTO ProfileGenderSaveService(ProfileGenderBaseDTO DTO) {
        ProfileGender entity = modelMapper.map(DTO, ProfileGender.class);
        profileGenderRepository.save(entity);
        ProfileGenderBaseDTO result = modelMapper.map(entity, ProfileGenderBaseDTO.class);
        return result;
    }

    public ProfileInterestMetaBaseDTO ProfileInterestMetaSaveService(ProfileInterestMetaBaseDTO DTO) {
        ProfileInterestMeta entity = modelMapper.map(DTO, ProfileInterestMeta.class);
        profileInterestMetaRepository.save(entity);
        ProfileInterestMetaBaseDTO result = modelMapper.map(entity, ProfileInterestMetaBaseDTO.class);
        return result;
    }

    public ProfileLanguageBaseDTO ProfileLanguageSaveService(ProfileLanguageBaseDTO DTO) {
        ProfileLanguage entity = modelMapper.map(DTO, ProfileLanguage.class);
        profileLanguageRepository.save(entity);
        ProfileLanguageBaseDTO result = modelMapper.map(entity, ProfileLanguageBaseDTO.class);
        return result;
    }

    public ProfilePositionBaseDTO ProfilePositionSaveService(ProfilePositionBaseDTO DTO) {
        ProfilePosition entity = modelMapper.map(DTO, ProfilePosition.class);
        profilePositionRepository.save(entity);
        ProfilePositionBaseDTO result = modelMapper.map(entity, ProfilePositionBaseDTO.class);
        return result;
    }

    public ProfileSkillBaseDTO ProfileSkillSaveService(ProfileSkillBaseDTO DTO) {
        ProfileSkill entity = modelMapper.map(DTO, ProfileSkill.class);
        profileSkillRepository.save(entity);
        ProfileSkillBaseDTO result = modelMapper.map(entity, ProfileSkillBaseDTO.class);
        return result;
    }

    public Long ProfileDynamicUpdateService(ProfileBaseDTO entity) {
        long result = profileRepository.ProfileRepositoryDynamicUpdate(entity);
        return result;
    }

    public Long ProfileCareerDynamicUpdateService(ProfileCareerBaseDTO entity) {
        long result = profileCareerRepository.ProfileCareerRepositoryDynamicUpdate(entity);
        return result;
    }

    public Long ProfileEducationDynamicUpdateService(ProfileEducationBaseDTO entity) {
        long result = profileEducationRepository.ProfileEducationRepositoryDynamicUpdate(entity);
        return result;
    }

    public Long ProfileInterestMetaDynamicUpdateService(ProfileInterestMetaBaseDTO entity) {
        long result = profileInterestMetaRepository.ProfileInterestMetaRepositoryDynamicUpdate(entity);
        return result;
    }

    public Long ProfilePositionDynamicUpdateService(ProfilePositionBaseDTO entity) {
        long result = profilePositionRepository.ProfilePositionRepositoryDynamicUpdate(entity);
        return result;
    }

    public ProfileBaseDTO ProfileFindByprofileId(Long profileId) {
        ProfileBaseDTO result = profileRepository.findByProfileId(profileId)
                .map(e -> modelMapper.map(e, ProfileBaseDTO.class)).get();
        return result;
    }

    public ProfileOnthewebBaseDTO ProfileOnthewebSaveService(ProfileOnthewebBaseDTO DTO) {
        ProfileOntheweb entity = modelMapper.map(DTO, ProfileOntheweb.class);
        profileOnthewebRepository.save(entity);
        ProfileOnthewebBaseDTO result = modelMapper.map(entity, ProfileOnthewebBaseDTO.class);
        return result;
    }

    public ProfileTimezoneBaseDTO ProfileTimezoneSaveService(ProfileTimezoneBaseDTO DTO) {
        ProfileTimezone entity = modelMapper.map(DTO, ProfileTimezone.class);
        profileTimezoneRepository.save(entity);
        ProfileTimezoneBaseDTO result = modelMapper.map(entity, ProfileTimezoneBaseDTO.class);
        return result;
    }

    public ProfileUseInfoBaseDTO ProfileUseInfoSaveService(ProfileUseInfoBaseDTO DTO){
        ProfileUseInfo entity = modelMapper.map(DTO, ProfileUseInfo.class);
        profileUseInfoRepository.save(entity);
        ProfileUseInfoBaseDTO result = modelMapper.map(entity, ProfileUseInfoBaseDTO.class);
        return result;
    }


    public List<ProfileOnthewebBaseDTO> ProfileOnthewebSelectService(Long profileId) {
        return profileOnthewebRepository.findById(profileId).stream()
                .map(e -> modelMapper.map(e, ProfileOnthewebBaseDTO.class)).collect(Collectors.toList());
    }

    public List<ProfileTimezoneBaseDTO> ProfileTimezoneSelectService(Long profileId) {
        return profileTimezoneRepository.findById(profileId).stream()
                .map(e -> modelMapper.map(e, ProfileTimezoneBaseDTO.class)).collect(Collectors.toList());
    }

    public ProfileCompanyBaseDTO ProfileCompanySaveService(ProfileCompanyBaseDTO DTO) {
        ProfileCompany entity = modelMapper.map(DTO, ProfileCompany.class);
        profileCompanyRepository.save(entity);
        ProfileCompanyBaseDTO result = modelMapper.map(entity, ProfileCompanyBaseDTO.class);
        return result;
    }

    public ProfileCompanyPartnerBaseDTO ProfileCompanyPartnerSaveService(ProfileCompanyPartnerBaseDTO DTO) {
        ProfileCompanyPartner entity = modelMapper.map(DTO, ProfileCompanyPartner.class);
        profileCompanyPartnerRepository.save(entity);
        ProfileCompanyPartnerBaseDTO result = modelMapper.map(entity, ProfileCompanyPartnerBaseDTO.class);
        return result;
    }

    public ProfileCompanyRosterBaseDTO ProfileCompanyRosterSaveService(ProfileCompanyRosterBaseDTO DTO) {
        ProfileCompanyRoster entity = modelMapper.map(DTO, ProfileCompanyRoster.class);
        profileCompanyRosterRepository.save(entity);
        ProfileCompanyRosterBaseDTO result = modelMapper.map(entity, ProfileCompanyRosterBaseDTO.class);
        return result;
    }

    public ProfileCompanyStudioBaseDTO ProfileCompanyStudioSaveService(ProfileCompanyStudioBaseDTO DTO) {
        ProfileCompanyStudio entity = modelMapper.map(DTO, ProfileCompanyStudio.class);
        profileCompanyStudioRepository.save(entity);
        ProfileCompanyStudioBaseDTO result = modelMapper.map(entity, ProfileCompanyStudioBaseDTO.class);
        return result;
    }


    public List<ProfileCompanyBaseDTO> ProfileCompanySelectService(Long profileId) {
        return profileCompanyRepository.findById(profileId).stream()
                .map(e -> modelMapper.map(e, ProfileCompanyBaseDTO.class)).collect(Collectors.toList());
    }

    public List<ProfileCompanyPartnerBaseDTO> ProfileCompanyPartnerSelectService(Long profileId) {
        return profileCompanyPartnerRepository.findById(profileId).stream()
                .map(e -> modelMapper.map(e, ProfileCompanyPartnerBaseDTO.class)).collect(Collectors.toList());
    }

    public List<ProfileCompanyRosterBaseDTO> ProfileCompanyRosterSelectService(Long profileId) {
        return profileCompanyRosterRepository.findById(profileId).stream()
                .map(e -> modelMapper.map(e, ProfileCompanyRosterBaseDTO.class)).collect(Collectors.toList());
    }

    public List<ProfileCompanyStudioBaseDTO> ProfileCompanyStudioSelectService(Long profileId) {
        return profileCompanyStudioRepository.findById(profileId).stream()
                .map(e -> modelMapper.map(e, ProfileCompanyStudioBaseDTO.class)).collect(Collectors.toList());
    }








    public ProfileCompanyOnthewebBaseDTO ProfileCompanyOnthewebSaveService(ProfileCompanyOnthewebBaseDTO DTO) {
        ProfileCompanyOntheweb entity = modelMapper.map(DTO, ProfileCompanyOntheweb.class);
        profileCompanyOnthewebRepository.save(entity);
        ProfileCompanyOnthewebBaseDTO result = modelMapper.map(entity, ProfileCompanyOnthewebBaseDTO.class);
        return result;
    }

    public ProfileCompanyTimezoneBaseDTO ProfileCompanyTimezoneSaveService(ProfileCompanyTimezoneBaseDTO DTO) {
        ProfileCompanyTimezone entity = modelMapper.map(DTO, ProfileCompanyTimezone.class);
        profileCompanyTimezoneRepository.save(entity);
        ProfileCompanyTimezoneBaseDTO result = modelMapper.map(entity, ProfileCompanyTimezoneBaseDTO.class);
        return result;
    }

    public List<ProfileCompanyOnthewebBaseDTO> ProfileCompanyOnthewebBaseDTOSelectService(Long profileId) {
        return profileCompanyOnthewebRepository.findById(profileId).stream()
                .map(e -> modelMapper.map(e, ProfileCompanyOnthewebBaseDTO.class)).collect(Collectors.toList());
    }

    public List<ProfileCompanyTimezoneBaseDTO> ProfileCompanyTimezoneSelectService(Long profileId) {
        return profileCompanyTimezoneRepository.findById(profileId).stream()
                .map(e -> modelMapper.map(e, ProfileCompanyTimezoneBaseDTO.class)).collect(Collectors.toList());
    }

    public List<ProfileUseInfoBaseDTO> ProfileUseInfoSelectService(Long profileId){
        return profileUseInfoRepository.findByLastUseProfileId(profileId)
                .stream().map(e -> modelMapper.map(e, ProfileUseInfoBaseDTO.class)).collect(Collectors.toList());
    }
}
