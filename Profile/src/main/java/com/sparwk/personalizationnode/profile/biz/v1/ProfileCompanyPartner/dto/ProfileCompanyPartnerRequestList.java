package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto;

import lombok.*;

import java.util.List;

@Getter@Setter@Builder
@AllArgsConstructor@NoArgsConstructor
public class ProfileCompanyPartnerRequestList {

    private List<ProfileCompanyPartnerRequest> profileCompanyPartnerRequestList;
}
