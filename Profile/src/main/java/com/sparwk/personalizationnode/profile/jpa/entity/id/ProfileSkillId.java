package com.sparwk.personalizationnode.profile.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileSkillId implements Serializable {
    private Long profileId;
    private String skillTypeCd;
    private String skillDetailTypeCd;
}
