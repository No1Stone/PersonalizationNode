package com.sparwk.personalizationnode.profile.biz.v1.ProfileGender.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGenderDto {
    @Schema(description = "프로필 아이디")
    private Long profileId; //ok
    @Schema(description = "프로필 젠더 코드")
    private String genderCd;//ok
}
