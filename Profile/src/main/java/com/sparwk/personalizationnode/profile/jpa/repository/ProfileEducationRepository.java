package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileEducation;
import com.sparwk.personalizationnode.profile.jpa.repository.dsl.ProfileEducationRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProfileEducationRepository extends JpaRepository<ProfileEducation, String>, ProfileEducationRepositoryDynamic {

    List<ProfileEducation> findByProfileId(Long profileId);

    int countByProfileIdAndEduSeq(Long profileId, Long eduSeq);

    @Transactional
    void deleteByProfileIdAndEduSeq(Long profileId, Long eduSeq);

}
