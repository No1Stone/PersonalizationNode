package com.sparwk.personalizationnode.profile.jpa.entity;

import com.sparwk.personalizationnode.profile.jpa.entity.id.ViewPartnerStudioId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "view_partner_studio_uinon")
@DynamicUpdate
@IdClass(ViewPartnerStudioId.class)
public class ViewPartnerStudio {

    @Column(name = "type", nullable = true)
    private String type;
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Id
    @Column(name = "mapping_profile_id", nullable = true)
    private Long mappingProfileId;
    @Column(name = "name", nullable = true)
    private String name;
    @Column(name = "company_type", nullable = true)
    private String companyType;
    @Column(name = "location_cd", nullable = true)
    private String locationCd;
    @Column(name = "location_cd_name", nullable = true)
    private String locationCdName;
    @Column(name = "post_cd", nullable = true)
    private String postCd;
    @Column(name = "region", nullable = true)
    private String region;
    @Column(name = "city", nullable = true)
    private String city;
    @Column(name = "addr1", nullable = true)
    private String addr1;
    @Column(name = "addr2", nullable = true)
    private String addr2;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ViewPartnerStudio(
            String type,
            Long profileId,
            String name,
            String companyType,
            String locationCd,
            String locationCdName,
            String postCd,
            String region,
            String city,
            String addr1,
            String addr2,
            LocalDateTime modDt
    ) {
        this.type = type;
        this.profileId = profileId;
        this.name = name;
        this.companyType = companyType;
        this.locationCd = locationCd;
        this.locationCdName = locationCdName;
        this.postCd = postCd;
        this.region = region;
        this.city = city;
        this.addr1 = addr1;
        this.addr2 = addr2;
        this.modDt = modDt;
    }
/*

CREATE OR REPLACE VIEW sparwk01.view_partner_studio_uinon
 AS
 SELECT 'studio'::text AS type,
    aa.profile_id,
    aa.studio_id AS mapping_profile_id,
    aa.studio_name AS name,
    'studio'::text AS company_type,
    aa.business_location_cd AS location_cd,
    ( SELECT tb_admin_country_code.country
           FROM tb_admin_country_code
          WHERE tb_admin_country_code.country_cd = aa.business_location_cd) AS location_cd_name,
    aa.post_cd,
    aa.region,
    aa.city,
    aa.addr1,
    aa.addr2,
    aa.mod_dt
   FROM tb_profile_company_studio aa
UNION ALL
 SELECT 'partner'::text AS type,
    bb.profile_id,
    bb.partner_id AS mapping_profile_id,
    dd.company_name AS name,
    ( SELECT string_agg(gg.val::text, ','::text) AS string_agg
           FROM tb_account_company_type ff
             LEFT JOIN tb_admin_common_detail_code gg ON ff.company_cd::text = gg.dcode::text AND ff.company_cd::text <> 'COP000000'::text
          WHERE ff.accnt_id = cc.accnt_id) AS company_type,
    ee.location_cd,
    ( SELECT tb_admin_country_code.country
           FROM tb_admin_country_code
          WHERE tb_admin_country_code.country_cd = ee.location_cd::bpchar) AS location_cd_name,
    dd.post_cd,
    dd.region,
    dd.city,
    dd.addr1,
    dd.addr2,
    bb.mod_dt
   FROM tb_profile_company_partner bb
     LEFT JOIN tb_profile_company cc ON bb.partner_id = cc.profile_id
     LEFT JOIN tb_account_company_detail dd ON cc.accnt_id = dd.accnt_id
     LEFT JOIN tb_account_company_location ee ON cc.accnt_id = ee.accnt_id;
 */

}
