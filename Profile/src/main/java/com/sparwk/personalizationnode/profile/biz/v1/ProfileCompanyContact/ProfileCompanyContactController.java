package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyContact;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyContact.dto.ProfileCompanyContactNameRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyContact.dto.ProfileCompanyContactRequest;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/profile/company")
@CrossOrigin("*")
@Api(tags = "Profile Server Profile Company Contact API")
@RequiredArgsConstructor
public class ProfileCompanyContactController {

    private final ProfileCompanyContactService profileCompanyContactService;

    @PostMapping(path = "/contact")
    public Response ProfileCompanyContactSaveController(
            @Valid @RequestBody ProfileCompanyContactRequest dto){
        Response res = profileCompanyContactService.ProfileCompanyContactSaveService(dto);
        return res;
    }

    @PostMapping(path = "/contact/update")
    public Response ProfileCompanyContactUpdateController(
            @Valid @RequestBody ProfileCompanyContactRequest dto){
        Response res = profileCompanyContactService.ProfileCompanyContactUpdateService(dto);
        return res;
    }

    @PostMapping(path = "/contact/name/update")
    public Response ProfileCompanyContactNameUpdateController(
            @Valid @RequestBody ProfileCompanyContactNameRequest dto){
        Response res = profileCompanyContactService.ProfileCompanyContactNameUpdateService(dto);
        return res;
    }

    @GetMapping(path = "/contact/{profileId}")
    public Response ProfileCompanyContactSaveController(@PathVariable(name = "profileId")Long profileId){
        Response res = profileCompanyContactService.ProfileCompanyContactSelectService(profileId);
        return res;
    }



}
