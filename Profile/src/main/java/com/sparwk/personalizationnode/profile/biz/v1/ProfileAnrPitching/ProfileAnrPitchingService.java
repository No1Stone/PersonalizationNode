package com.sparwk.personalizationnode.profile.biz.v1.ProfileAnrPitching;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileAnrPitching.dto.AnrPitchingRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCareer.ProfileCareerService;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileAnrPitching;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileAnrPitchingRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

@Service
@RequiredArgsConstructor
public class ProfileAnrPitchingService {

    private final ModelMapper modelMapper;
    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfileCareerService.class);

    private final ProfileAnrPitchingRepository profileAnrPitchingRepository;

    @Transactional
    public Response ProfileAnrPitchingSaveService(AnrPitchingRequest dto) {
        Response res = new Response();
        ProfileAnrPitching result = null;
//        if(profileAnrPitchingRepository.existsByProfileId(dto.getProfileId())) {
//            profileAnrPitchingRepository.UpdateProfileUpdate(dto.getProfileId(), dto.getPitchingYn());
//        }
//        else {
            result = profileAnrPitchingRepository.save(ProfileAnrPitching.builder()
                    .profileId(dto.getProfileId())
                    .pitchingYn(dto.getPitchingYn())
                    .build());
//        }

        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileAnrPitchingSelectService(AnrPitchingRequest dto) {
        Response res = new Response();
//        res.setResult(profileAnrPitchingRepository.findById(dto.getProfileId()));
        res.setResult(profileAnrPitchingRepository.findAll());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


}
