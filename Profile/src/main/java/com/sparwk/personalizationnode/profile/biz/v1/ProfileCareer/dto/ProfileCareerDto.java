package com.sparwk.personalizationnode.profile.biz.v1.ProfileCareer.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCareerDto {
    @Schema(description = "프로필 아이디")
    private Long profileId;
    @Schema(description = "데이터베이스 고유 번호")
    private Long careerSeq;
    @Schema(description = "교육기관")
    private String careerOrganizationNm;
    @Schema(description = "부서 및 열확")
    private String deptRoleNm;
    @Schema(description = "교육기관 이 위차한 국가 이름")
    private String locationCityCountryCd;
    @Schema(description = "교육기관 위치 도시명")
    private String locationCityNm;
    @Schema(description = "현재 진행 여부")
    private String presentYn;
    @Schema(description = "프론트에서 보여줄 순서용")
    private Long profileCareerSeq;
    @Schema(description = "교육 순번 / 프론트 화면에서 등록하거나 변경한 순서")
    private Long ProfileCareerSeq;
    private String filePath;
    @Schema(description = "교육시작일")
    private String careerStartDt;
    @Schema(description = "교육 종료일")
    private String careerEndDt;

}
