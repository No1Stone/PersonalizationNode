package com.sparwk.personalizationnode.profile.biz.v1.profile.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyDetailServerRequest {
    @Schema(description = "사용자 계정번호")
    private Long accntId;

    @Schema(description = "회사코드")
    @Size(max = 9)
    private String companyCd;

    @Schema(description = "우편번호")
    @Size(max = 20)
    private String postCd;

    @Schema(description = "지역")
    @Size(max = 50)
    private String region;

    @Schema(description = "도시")
    @Size(max = 20)
    private String city;

    @Schema(description = "주소1")
    @Size(max = 200)
    private String addr1;

    @Schema(description = "주소2")
    @Size(max = 200)
    private String addr2;

    @Schema(description = "국가코드")
    @Size(max = 9)
    private String countryCd;

    @Size(max = 20)
    @Schema(description = "핸드폰번호")
    private String phoneNum;

    @Schema(description = "담당자 firstName")
    @Size(max = 50)
    private String contctFirstName;

    @Schema(description = "담당자 midleName")
    @Size(max = 50)
    private String contctMidleName;

    @Schema(description = "담당자 lastName")
    @Size(max = 50)
    private String contctLastName;

    @Schema(description = "담당자 email")
    @Size(max = 100)
    private String contctEmail;

    @Schema(description = "회사 사업자 등록번호 파일 주소")
    @Size(max = 200)
    private String companyLicenseFileUrl;


}
