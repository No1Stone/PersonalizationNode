package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.enumdata;

public enum RosterStatunEnum {

    //신청
    APPLY("RSS000001","APPLY"),
    //소속 신청완료
    AGREEMENT("RSS000002","AGREEMENT"),
    //종료
    END("RSS000003","END");

    private String value;
    private String description;

    RosterStatunEnum(String value, String description){
        this.value = value;
        this.description = description;
    }

    public String getValue(){
        return value;
    }
}
