package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyStudio.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyStudioDeleteRequest {
    private Long profileId;
    private Long studioId;
}
