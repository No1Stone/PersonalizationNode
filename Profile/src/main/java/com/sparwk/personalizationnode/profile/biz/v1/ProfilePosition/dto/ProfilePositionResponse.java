package com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.persistence.Column;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePositionResponse {

    @Schema(description = "시퀀스 순서")
    private Long profilePositionSeq;
    @Schema(description = "프로필 아이디")
    private Long profileId;
    @Schema(description = "회사 코드")
    private Long companyProfileId;
    @Schema(description = "회사 승인여부")
    private String companyVerifyYn;
    @Schema(description = "A&R 여부")
    private String anrYn;
    @Schema(description = "아티스트 여부")
    private String artistYn;
    @Schema(description = "크리에이터 여부")
    private String creatorYn;
    @Schema(description = "부서및 권한")
    private String deptRoleInfo;
    @Schema(description = "주 소속 회사 여부")
    private String primaryYn;


}
