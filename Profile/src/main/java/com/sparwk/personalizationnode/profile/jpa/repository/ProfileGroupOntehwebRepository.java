package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileGroupOntehweb;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProfileGroupOntehwebRepository extends JpaRepository<ProfileGroupOntehweb, Long> {

    boolean existsByProfileId(Long profileId);
    int countByProfileId(Long profileId);
    List<ProfileGroupOntehweb> findByProfileId(Long profileId);
    @Transactional
    void  deleteByProfileIdAndSnsTypeCd(Long profileId, String cd);

}
