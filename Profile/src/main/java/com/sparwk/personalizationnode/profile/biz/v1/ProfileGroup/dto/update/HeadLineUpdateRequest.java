package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroup.dto.update;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HeadLineUpdateRequest {

    private Long profileId;
    private String headline;


}
