package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGroupContactNameUpdateRequest {
    private Long profileId;
    private String contctFirstName;
    private String contctMidleName;
    private String contctLastName;


}
