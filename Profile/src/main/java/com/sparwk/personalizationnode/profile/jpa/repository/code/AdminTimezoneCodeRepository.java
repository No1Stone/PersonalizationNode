package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminTimezoneCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminTimezoneCodeRepository extends JpaRepository<AdminTimezoneCode, Long> {

    Optional<AdminTimezoneCode> findByTimezoneSeq(Long seq);
    boolean existsByTimezoneSeq(Long seq);
}
