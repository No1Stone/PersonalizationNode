package com.sparwk.personalizationnode.profile.biz.v1.ProfileAnrPitching;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileAnrPitching.dto.AnrPitchingRequest;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/anr")
@CrossOrigin("*")
@Api(tags = "Profile Server Anr API")
public class ProfileAnrPitchingController {


    private final Logger logger = LoggerFactory.getLogger(ProfileAnrPitchingController.class);
    @Autowired
    private ProfileAnrPitchingService profileAnrPitchingService;



    @ApiOperation(
            value = "anr YN 저장 및 업데이트",
            notes = "anr YN 저장 및 업데이트"
    )
    @PostMapping(path = "/pitching")
    public Response ProfileAnrPitchingSaveController(@Valid @RequestBody AnrPitchingRequest dto) {
        Response res = profileAnrPitchingService.ProfileAnrPitchingSaveService(dto);
        return res;
    }

    @ApiOperation(
            value = "anr YN 저장 및 업데이트",
            notes = "anr YN 저장 및 업데이트"
    )
    @PostMapping(path = "/select")
    public Response ProfileAnrPitchingSelectController(@Valid @RequestBody AnrPitchingRequest dto) {
        Response res = profileAnrPitchingService.ProfileAnrPitchingSelectService(dto);
        return res;
    }
}
