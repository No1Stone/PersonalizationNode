package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.profilecreate;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGenderRequest {

    @Schema(description = "프로필 젠더 코드")
    private String genderCd;//ok
}
