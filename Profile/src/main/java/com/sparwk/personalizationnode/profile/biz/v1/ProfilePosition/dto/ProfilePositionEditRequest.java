package com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePositionEditRequest {
    @Schema(description = "프로필 아이디")
    private Long profileId;
    private Long profilePositionSeq;
    private Long companyProfileId;
    private String companyVerifyYn;
    private String artistYn;
    private String anrYn;
    private String creatorYn;
    private String deptRoleInfo;
    private String primaryYn;

}
