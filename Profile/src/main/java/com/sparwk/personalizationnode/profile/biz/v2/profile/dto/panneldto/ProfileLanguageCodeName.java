package com.sparwk.personalizationnode.profile.biz.v2.profile.dto.panneldto;

import com.sparwk.personalizationnode.profile.jpa.dto.ProfileLanguageBaseDTO;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileLanguageCodeName extends ProfileLanguageBaseDTO {

    private String languageCdName;

}
