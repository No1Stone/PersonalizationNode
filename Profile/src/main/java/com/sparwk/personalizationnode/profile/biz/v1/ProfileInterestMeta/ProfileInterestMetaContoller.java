package com.sparwk.personalizationnode.profile.biz.v1.ProfileInterestMeta;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileInterestMeta.dto.ProfileInterestMetaRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileInterestMeta.dto.ProfileInterestMetaRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/interest")
@CrossOrigin("*")
@Api(tags = "Profile Server InterestMeta API")
public class ProfileInterestMetaContoller {

    @Autowired
    private ProfileInterestMetaService profileInterestMetaService;

    private final Logger logger = LoggerFactory.getLogger(ProfileInterestMetaContoller.class);

    @ApiOperation(
            value = "프로필 관심 정보 입력",
            notes = "악기 정보 입력"
    )
    @PostMapping(path = "/meta")
    public Response ProfileInterestMetaSaveController(@Valid @RequestBody ProfileInterestMetaRequestList dto) {
        logger.info("",dto);
        Response res = profileInterestMetaService.ProfileInterestMetaSave(dto);
        return res;
    }

    @ApiOperation(
            value = "프로필 관심 정보 검색",
            notes = "악기 정보 검색"
    )
    @GetMapping(path = "/meta/{profileId}")
    public Response ProfileInterestMetaSelect(@PathVariable(name = "profileId")Long profileId ) {
        Response res = profileInterestMetaService.ProfileInterestMetaSelect(profileId);
        return res;
    }

    @ApiOperation(
            value = "프로필 관심 정보 업데이트",
            notes = "악기 정보 업데이트"
    )
    @PostMapping(path = "/meta/update")
    public Response ProfileInterestMetaDynamicUpdateController(@Valid @RequestBody ProfileInterestMetaRequest dto) {

        Response res = profileInterestMetaService.ProfileInterestMetaDynamicUpdate(dto);
        return res;
    }

}
