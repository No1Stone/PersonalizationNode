package com.sparwk.personalizationnode.profile.biz.v1.ProfileUseInfo;

import com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.ProfilePositionService;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileTimezone.dto.ProfileTimezoneRequestList;
import com.sparwk.personalizationnode.profile.biz.v1.profile.ProfileService;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.profilecreate.AccountResponse;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileUseInfoBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileUseInfo;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileUseInfoRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileUseInfoService {

    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfileUseInfoRepository profileUseInfoRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileService profileService;
    @Autowired
    private ProfilePositionService profilePositionService;
    @Autowired
    private HttpServletRequest httpServletRequest;

    private final MessageSourceAccessor messageSourceAccessor;
    private Logger logger = LoggerFactory.getLogger(ProfileUseInfoService.class);

    public Response ProfileUseInfoSaveResponseService(Long changeProfileId) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        String token;
        ProfileUseInfoBaseDTO baseDTO = ProfileUseInfoBaseDTO.builder()
                .accntId(userInfoDTO.getAccountId())
                .lastUseProfileId(userInfoDTO.getLastUseProfileId())
                .LastUseDt(LocalDateTime.now())
                .build();
        ProfileUseInfoBaseDTO result = profileServiceRepository.ProfileUseInfoSaveService(baseDTO);

        AccountResponse ar = AccountResponse
                .builder()
                .accntId(userInfoDTO.getAccountId())
                .lastUseProfileId(changeProfileId)
                .build();
        Response accRes = profileService.AccountLastUseProfileIdTemplate(ar);
        if(accRes.getResultCd().equals("0000")){
            token = profilePositionService.AuthtokenResilt();
            logger.info(token);
            res.setResult(token);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        return res;
    }

    public Response ProfileProfileUseInfoSelectResponseService(Long profileId) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");

        List<ProfileUseInfoBaseDTO> result = profileUseInfoRepository.findByLastUseProfileId(profileId)
                .stream().map(e -> modelMapper.map(e, ProfileUseInfoBaseDTO.class)).collect(Collectors.toList());
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
