package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class IpiUpdateRequest {
    private Long profileId;
    private String ipiNumber;

}
