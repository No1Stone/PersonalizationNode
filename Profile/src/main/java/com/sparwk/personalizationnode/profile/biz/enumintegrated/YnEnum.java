package com.sparwk.personalizationnode.profile.biz.enumintegrated;

public enum YnEnum {

    Y("Y"),
    N("N")

    ;
    private String value;

    YnEnum(
            String value
    ){
        this.value = value;
    }

    public String getName(){
        return name();
    }

    public String getValue(){
        return value;
    }

}
