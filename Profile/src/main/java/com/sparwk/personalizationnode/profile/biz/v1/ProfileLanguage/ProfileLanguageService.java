package com.sparwk.personalizationnode.profile.biz.v1.ProfileLanguage;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileLanguage.dto.ProfileLanguageDto;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileLanguageBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileLanguage;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileLanguageRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileLanguageService {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfileLanguageRepository profileLanguageRepository;
    @Autowired
    private HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfileLanguageService.class);

    public Response ProfileLanguageSave(ProfileLanguageDto dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        if (dto.getProfileId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        List<ProfileLanguageBaseDTO> langResult = new ArrayList<>();
        ProfileLanguageBaseDTO result = null;
        String[] languages = dto.getLanguageCd().trim().split(",");
        if(profileLanguageRepository.existsByProfileId(dto.getProfileId())) {
            List<ProfileLanguage> delList = profileLanguageRepository.findByProfileId(dto.getProfileId());
            for(ProfileLanguage e : delList){
                profileLanguageRepository.deleteByProfileIdAndAndLanguageCd(e.getProfileId(), e.getLanguageCd());
            }
            for (String e : languages) {
                ProfileLanguageBaseDTO baseDTO = modelMapper.map(dto, ProfileLanguageBaseDTO.class);
                baseDTO.setLanguageCd(e);
                logger.info("value check - {}", dto.toString());
                result = profileServiceRepository.ProfileLanguageSaveService(baseDTO);
                langResult.add(result);
            }
        }
        else {
            for (String e : languages) {
                ProfileLanguageBaseDTO baseDTO = modelMapper.map(dto, ProfileLanguageBaseDTO.class);
                baseDTO.setLanguageCd(e);
                logger.info("value check - {}", dto.toString());
                result = profileServiceRepository.ProfileLanguageSaveService(baseDTO);
                langResult.add(result);
            }
        }
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileLanguageSelect(Long profileId) {
        Response res = new Response();
        List<ProfileLanguageBaseDTO> result = profileLanguageRepository.findByProfileId(profileId)
                .stream().map(e -> modelMapper.map(e, ProfileLanguageBaseDTO.class))
                .collect(Collectors.toList());
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


}
