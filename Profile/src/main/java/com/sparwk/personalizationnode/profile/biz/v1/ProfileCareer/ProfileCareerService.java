package com.sparwk.personalizationnode.profile.biz.v1.ProfileCareer;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCareer.dto.ProfileCareerDeleteRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCareer.dto.ProfileCareerDto;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCareer.dto.ProfileCareerDtoList;
import com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto.ProfilePositionEditRequest;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCareerBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfilePositionBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCareer;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCareerRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileCareerService {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfileCareerRepository profileCareerRepository;
    @Autowired
    private HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfileCareerService.class);

    public Response ProfileCareerSave(ProfileCareerDto dto){
        Response res = new Response();
        if (dto.getProfileId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        ProfileCareerBaseDTO baseDTO = modelMapper.map(dto, ProfileCareerBaseDTO.class);
        baseDTO.setProfileId(userInfoDTO.getLastUseProfileId());
        baseDTO.setModDt(LocalDateTime.now());
        baseDTO.setRegDt(LocalDateTime.now());
        baseDTO.setModUsr(userInfoDTO.getAccountId());
        baseDTO.setRegUsr(userInfoDTO.getAccountId());;

        ProfileCareerBaseDTO result = profileServiceRepository
                .ProfileCareerSaveService(baseDTO);
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileCareerSelect(Long profileId){
        List<ProfileCareerBaseDTO> result = profileCareerRepository.findByProfileId(profileId)
                .stream().map(e -> modelMapper.map(e, ProfileCareerBaseDTO.class))
                .collect(Collectors.toList());
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileDynamicUpdate(ProfileCareerDto dto){
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        ProfileCareerBaseDTO baseDTO = modelMapper.map(dto, ProfileCareerBaseDTO.class);
        baseDTO.setProfileId(userInfoDTO.getLastUseProfileId());
        baseDTO.setModDt(LocalDateTime.now());
        baseDTO.setModUsr(userInfoDTO.getAccountId());
        long result = profileServiceRepository.ProfileCareerDynamicUpdateService(baseDTO);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
    public Response ProfileCareerSaveListService(ProfileCareerDtoList dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();

        //현재 database 입력값
        List<ProfileCareerBaseDTO> beforCareer = profileServiceRepository
                .ProfileCareerSelectService(userInfoDTO.getLastUseProfileId());
        List<Long> befor = new ArrayList<>();
        //비교요청후 처리할 값 유저가 준 데이터
        for (ProfileCareerDto f : dto.getProfileCareerDtoList()) {
            if(f.getCareerSeq()!=null){
                befor.add(f.getCareerSeq());
            }
            if (f.getProfileId() == null) {
                res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }
                ProfileCareerBaseDTO baseDTO = modelMapper.map(f, ProfileCareerBaseDTO.class);
                baseDTO.setRegUsr(userInfoDTO.getAccountId());
                baseDTO.setModUsr(userInfoDTO.getAccountId());
                baseDTO.setRegDt(LocalDateTime.now());
                baseDTO.setModDt(LocalDateTime.now());
                ProfileCareerBaseDTO result = profileServiceRepository.ProfileCareerSaveService(baseDTO);
                befor.add(result.getCareerSeq());
//            }
        }
        //입력이 완료된 데이터를 불러온다.
        List<ProfileCareerBaseDTO> afterCareer = profileServiceRepository.ProfileCareerSelectService(userInfoDTO.getLastUseProfileId());
        // 델리트값 구하기 / 입력이 완료된 데이터에서 프론트가 요청한 값을 뺀다
        List<Long> after = new ArrayList<>();
        for(ProfileCareerBaseDTO e : afterCareer) {
            after.add(e.getCareerSeq());
        }
        after.removeAll(befor);
        for(Long e: after){
            profileCareerRepository.deleteByProfileIdAndCareerSeq(userInfoDTO.getLastUseProfileId(), e);
        }
        //지운데이터를 조회해서 보내준다
        List<ProfileCareerBaseDTO> result = profileServiceRepository
                .ProfileCareerSelectService(userInfoDTO.getLastUseProfileId());
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileCareerDeleteService(ProfileCareerDeleteRequest dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        profileCareerRepository.deleteByProfileIdAndCareerSeq(dto.getProfileId(), dto.getCareerSeq());
        Response res = new Response();
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
