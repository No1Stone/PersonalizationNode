package com.sparwk.personalizationnode.profile.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileUseInfoBaseDTO {
    private Long accntId;
    private Long lastUseProfileId;
    private LocalDateTime LastUseDt;

}
