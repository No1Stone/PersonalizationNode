package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface ProfileGroupRepository extends JpaRepository<ProfileGroup, Long> {
    @Transactional
    @Modifying
    @Query(
           value = "UPDATE tb_profile_group set headline = :headline where profile_id = :profileId "
            ,nativeQuery = true
            )
    int UpdateProfileGroupHeadLine(@Param("profileId")Long profileId,@Param("headline") String headline);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_group set profile_group_name = :name where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileGroupName(@Param("profileId")Long profileId,@Param("name") String name);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_group set profile_img_url = :img where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileGroupImg(@Param("profileId")Long profileId,@Param("img") String img);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_group set web_site = :website where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileGroupWebsite(@Param("profileId")Long profileId,@Param("website") String website);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_group set over_view = :overview where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileGroupOverView(@Param("profileId")Long profileId,@Param("overview") String overview);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_group set profile_bgd_img_url = :banner where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileGroupBanner(@Param("profileId")Long profileId,@Param("banner") String banner);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_group set mattermost_id = :mertterId where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileGroupMattermostId(@Param("profileId")Long profileId,@Param("mertterId") String mertterId);



}
