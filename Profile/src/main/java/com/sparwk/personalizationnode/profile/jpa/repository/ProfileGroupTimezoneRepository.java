package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileGroupTimezone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProfileGroupTimezoneRepository extends JpaRepository<ProfileGroupTimezone, Long> {

    List<ProfileGroupTimezone> findByProfileId(Long profileId);
    boolean existsByProfileId(Long profileId);
    @Transactional
    void deleteByProfileIdAndProfileTimezoneBoxNum(Long profileId, Long num);

}
