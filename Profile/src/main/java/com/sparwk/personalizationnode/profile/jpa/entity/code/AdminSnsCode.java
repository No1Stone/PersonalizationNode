package com.sparwk.personalizationnode.profile.jpa.entity.code;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_sns_code")
public class AdminSnsCode {

    @Id
    @Column(name = "sns_seq ", nullable = true)
    private Long snsSeq;
    @Column(name = "sns_name ", nullable = true)
    private String snsName;
    @Column(name = "sns_cd ", nullable = true)
    private String snsCd;
    @Column(name = "sns_icon_url ", nullable = true)
    private String snsIconUrl;
    @Column(name = "use_yn ", nullable = true)
    private String useYn;
    @Column(name = "reg_usr ", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt ", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr ", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    AdminSnsCode(
            Long snsSeq,
            String snsName,
            String snsCd,
            String snsIconUrl,
            String useYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.snsSeq = snsSeq;
        this.snsName = snsName;
        this.snsCd = snsCd;
        this.snsIconUrl = snsIconUrl;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }
}
