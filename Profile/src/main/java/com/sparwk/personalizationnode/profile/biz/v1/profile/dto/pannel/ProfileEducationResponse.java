package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel;


import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileEducationId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileEducationResponse {

    private String eduOrganizationNm;
    private Long profileId;
    private String eduCourseNm;
    private String locationCityCountryCd;
    private String locationCityNm;
    private String presentYn;
    private Long eduSeq;
    private String filePath;
    private Long profileEduSeq;
    private String eduStartDt;
    private String eduEndDt;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
//    @Builder
//    ProfileEducationResponse(
//            Long profileId,
//            String eduOrganizationNm,
//            String eduCourseNm,
//            String locationCityCountryCd,
//            String locationCityNm,
//            String presentYn,
//            Long eduSeq,
//            Long profileEduSeq,
//            String eduStartDt,
//            String eduEndDt,
//            Long regUsr,
//            LocalDateTime regDt,
//            Long modUsr,
//            LocalDateTime modDt
//
//    ) {
//        this.profileId = profileId;
//        this.eduOrganizationNm = eduOrganizationNm;
//        this.eduCourseNm = eduCourseNm;
//        this.locationCityCountryCd = locationCityCountryCd;
//        this.locationCityNm = locationCityNm;
//        this.presentYn = presentYn;
//        this.eduSeq = eduSeq;
//        this.profileEduSeq = profileEduSeq;
//        this.eduStartDt = eduStartDt;
//        this.eduEndDt = eduEndDt;
//        this.regUsr = regUsr;
//        this.regDt = regDt;
//        this.modUsr = modUsr;
//        this.modDt = modDt;
//
//    }

}
