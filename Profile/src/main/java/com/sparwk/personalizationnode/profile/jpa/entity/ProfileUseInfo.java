package com.sparwk.personalizationnode.profile.jpa.entity;

import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileUseInfoId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_use_info")
@DynamicUpdate
@IdClass(ProfileUseInfoId.class)
public class ProfileUseInfo {

    @Id
    @Column(name = "accnt_id", nullable = true)
    private Long accntId;
    @Id
    @Column(name = "last_use_profile_id", nullable = true)
    private Long lastUseProfileId;
    @Column(name = "last_use_dt", nullable = true)
    private LocalDateTime LastUseDt;


    @Builder
    ProfileUseInfo(
            Long accntId,
            Long lastUseProfileId,
            LocalDateTime LastUseDt
    ) {
        this.accntId = accntId;
        this.lastUseProfileId = lastUseProfileId;
        this.LastUseDt = LastUseDt;
    }


}
