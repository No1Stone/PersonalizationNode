package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.profilecreate;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileRequest {
    @Schema(description = "프로필 활동명 풀네임")
    private String fullName;//ok
    @Schema(description = "스테이지 이름 사용여부")
    private String stageNameYn; //ok
    @Schema(description = "생년")
    private String bthYear;//ok
    @Schema(description = "월")
    private String bthMonth;//ok
    @Schema(description = "일")
    private String bthDay;//ok
    @Schema(description = "현재위치 국가")
    private String currentCityCountryCd;//ok
    @Schema(description = "현재위치 도시")
    private String currentCityNm;//ok
    @Schema(description = "고향국가코드")
    private String homeTownCountryCd;//ok
    @Schema(description = "고향 도시 이름")
    private String homeTownNm;//ok
    @Schema(description = "헤드라인")
    private String headline;//ok

}
