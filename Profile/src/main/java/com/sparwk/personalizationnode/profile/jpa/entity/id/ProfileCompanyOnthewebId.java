package com.sparwk.personalizationnode.profile.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileCompanyOnthewebId implements Serializable {
    private Long profileId;
    private String snsTypeCd;
}
