package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany;

import com.google.gson.Gson;
import com.sparwk.personalizationnode.profile.biz.chat.UpdateMattermost;
import com.sparwk.personalizationnode.profile.biz.enumintegrated.YnEnum;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto.*;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto.Paging;
import com.sparwk.personalizationnode.profile.biz.v1.profile.ProfileService;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel.ProfilePannelSelectOneResponse;
import com.sparwk.personalizationnode.profile.config.common.RestCall;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompany;
import com.sparwk.personalizationnode.profile.jpa.entity.account.Account;
import com.sparwk.personalizationnode.profile.jpa.entity.account.AccountCompanyDetail;
import com.sparwk.personalizationnode.profile.jpa.entity.account.AccountCompanyType;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyPartnerRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.account.AccountCompanyDetailRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.account.AccountCompanyTypeRespository;
import com.sparwk.personalizationnode.profile.jpa.repository.account.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileCompanyService {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private ProfileCompanyRepository profileCompanyRepository;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private HttpServletRequest httpServletRequest;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private AccountCompanyTypeRespository accountCompanyTypeRespository;
    @Autowired
    private AccountCompanyDetailRepository accountCompanyDetailRepository;
    @Value("${sparwk.node.communication.chat}")
    private String chat;

    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfileService.class);

    public Response ProfileCompanySaveService(ProfileCompanyRequest dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();

        ProfileCompanyBaseDTO profileCompanyBaseDTO = modelMapper.map(dto, ProfileCompanyBaseDTO.class);
        profileCompanyBaseDTO.setAccntId(userInfoDTO.getAccountId());
        profileCompanyBaseDTO.setRegUsr(userInfoDTO.getAccountId());
        profileCompanyBaseDTO.setModUsr(userInfoDTO.getAccountId());
        profileCompanyBaseDTO.setRegDt(LocalDateTime.now());
        profileCompanyBaseDTO.setModDt(LocalDateTime.now());
        ProfileCompanyBaseDTO profileCompanyResult = profileServiceRepository.ProfileCompanySaveService(profileCompanyBaseDTO);
        accountCompanyDetailRepository.flush();
        profileCompanyRepository.flush();
        accountCompanyDetailRepository.UpdateProfileCompanyName
                (profileCompanyBaseDTO.getAccntId(), profileCompanyBaseDTO.getProfileCompanyName());

        res.setResult(profileCompanyResult);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilecompanySelectService(Long profileId) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        Response res = new Response();
        if (!profileCompanyRepository.existsByProfileId(profileId)) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        ProfileCompanyBaseDTO companyBaseDTO = profileCompanyRepository
                .findById(profileId)
                .map(e -> modelMapper.map(e, ProfileCompanyBaseDTO.class)).get();
        res.setResult(companyBaseDTO);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilecompanyUpdateService(ProfileCompanyRequest dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        Response res = new Response();
        if (dto.getProfileId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        logger.info("---------dto{}", dto);
        logger.info("---------use{}", userInfoDTO);

        if (!dto.getProfileId().equals(userInfoDTO.getLastUseProfileId())) {
            res.setResultCd(ResultCodeConst.PROFILE_ID_REQUEST_TOKEN_MISMATCH.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Long result = profileCompanyRepository
                .ProfileCompanyRepositoryDynamicUpdate(modelMapper.map(dto, ProfileCompanyBaseDTO.class));

        String chatURL = "https://"+chat+"/V1/user/update/profile/image";
        logger.info("chatCreateURL - -{}",chatURL);
        UpdateMattermost umm = new UpdateMattermost();
        umm.setId(dto.getMattermostId());
        umm.setImgUrl(dto.getProfileImgUrl());
        String mmRes = RestCall.postRes(chatURL, new Gson().toJson(umm));
        logger.info("mMRes - {}",mmRes);

//        res.setResult();
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response FindByCompanyListpageGet(int page) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(page, 200, Sort.by(Sort.Direction.DESC, "profileId"));
        logger.info("=======================");
        Page<ProfileCompanyBaseDTO> profileCompany = profileCompanyRepository.findAll(pr).map(e ->
                modelMapper.map(e, ProfileCompanyBaseDTO.class));
        res.setResult(profileCompany);
        return res;
    }

    public Response FindByCompanyListSlaceGetAll(CompanyProfileSearchRequest dto) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getSliceIndex(), dto.getSize());
        logger.info("=======================");
//        Page<ProfileCompanyBaseDTO> profileCompany = accountCompanyTypeRespository.findAll(pr).map(e ->
//                modelMapper.map(e, ProfileCompanyBaseDTO.class));
//        Slice<ProfileCompanyBaseDTO> profileCompany = accountCompanyTypeRespository.findAll(pr).map(e ->
//                modelMapper.map(e, ProfileCompanyBaseDTO.class));
        //나중에 승인 된 어카운트타입만 검색하는 로직 추가
        List<Long> acc = accountCompanyTypeRespository.findAllByCompanyLicenseVerifyYn("Y").stream().map(e ->
                modelMapper.map(e.getAccntId(), Long.class)).collect(Collectors.toList());
        Slice<ProfileCompanyBaseDTO> profileCompanyList = null;

        logger.info("accsize = --------{}", acc.size());
        if (dto.getCompanyName() == null || dto.getCompanyName().equals("") || dto.getCompanyName().isEmpty()) {
            profileCompanyList = profileCompanyRepository.findByAccntIdIn(acc, pr)
                    .map(e -> modelMapper.map(e, ProfileCompanyBaseDTO.class));
        } else {
            profileCompanyList = profileCompanyRepository
                    .findAllByAccntIdInAndProfileCompanyNameLike(acc, "%" + dto.getCompanyName() + "%", pr)
                    .map(e -> modelMapper.map(e, ProfileCompanyBaseDTO.class));
        }

        List<Object> conten = new ArrayList<>();
        for (ProfileCompanyBaseDTO e : profileCompanyList.getContent()) {
            Account userAccountSelect = accountRepository.findById(profileCompanyRepository
                    .findById(e.getProfileId()).get().getAccntId()).get();
            ProfileCompanyResponse companyResponse = new ProfileCompanyResponse();
            for (ProfileCompany f : userAccountSelect.getProfileCompany()) {
                logger.info("에러 여기서 나니?");
                companyResponse = modelMapper.map(f, ProfileCompanyResponse.class);
                if (userAccountSelect.getCountryCd() == null) {
                } else {
                    companyResponse.setCountryCd(userAccountSelect.getCountryCd());
                }
                if (userAccountSelect.getAccntEmail() == null) {
                } else {
                    companyResponse.setCompanyEmail(userAccountSelect.getAccntEmail());
                }
                if (f.getComInfoEmail() == null) {
                } else {
                    companyResponse.setContactEmail(f.getComInfoEmail());
                }
                if (userAccountSelect.getCountryCd() == null) {
                } else {
                    companyResponse.setPhoneCountryCd(userAccountSelect.getCountryCd());
                }
                if (f.getProfileId() == null) {
                } else {
                    companyResponse.setProfileId(f.getProfileId());
                }
                if (f.getProfileCompanyOntheweb() == null) {
                } else {
                    companyResponse.setProfileCompanyOntheweb(f.getProfileCompanyOntheweb());
                }
                if (userAccountSelect.getAccountCompanyType() == null) {
                } else {
                    companyResponse.setAccountCompanyType(userAccountSelect.getAccountCompanyType());
                }
                if (userAccountSelect.getAccountCompanyDetail() == null) {
                } else {
                    companyResponse.setAccountCompanyDetail(userAccountSelect.getAccountCompanyDetail());
                }
                if (userAccountSelect.getAccountCompanyLocation() == null) {
                } else {
                    companyResponse.setAccountCompanyLocation(userAccountSelect.getAccountCompanyLocation());
                }
                conten.add(companyResponse);
            }
        }
        Paging paging = new Paging();
        paging.setContent(conten);
        paging.setPage(profileCompanyList.getNumber());
        paging.setSize(profileCompanyList.getSize());
        paging.setTotalPage(profileCompanyList.getNumber());
        paging.setTotalSize(profileCompanyList.getNumberOfElements());
        res.setResult(paging);
//        res.setResult(accountCompanyTypeRespository.findAll(pr));
//        res.setResult(profileCompany);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response FindByCompanyTypeListSlaceGetAll(int slice, String type) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(slice, 50);
        logger.info("=======================");
//        Page<ProfileCompanyBaseDTO> profileCompany = accountCompanyTypeRespository.findAll(pr).map(e ->
//                modelMapper.map(e, ProfileCompanyBaseDTO.class));
//        Slice<ProfileCompanyBaseDTO> profileCompany = accountCompanyTypeRespository.findByCompanyCd(type,pr).map(e ->
//                modelMapper.map(e, ProfileCompanyBaseDTO.class));
//        res.setResult(profileCompany);
        List<Long> acc = accountCompanyTypeRespository.findAllByCompanyLicenseVerifyYnAndCompanyCd("Y", type).stream().map(
                e -> modelMapper.map(e.getAccntId(), Long.class)).collect(Collectors.toList());

        List<Long> accUse = accountRepository.findByAccntIdInAndUseYn(acc, YnEnum.Y.getValue()).stream().map(e -> modelMapper.map(e.getAccntId(), Long.class))
                .collect(Collectors.toList());

        Slice<ProfileCompanyBaseDTO> profileCompanyList = profileCompanyRepository.findByAccntIdInOrderByProfileCompanyNameAsc(accUse, pr)
                .map(e -> modelMapper.map(e, ProfileCompanyBaseDTO.class));
//        res.setResult(accountCompanyTypeRespository.findByCompanyCd(type,pr));
        res.setResult(profileCompanyList);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response FindByCompanyListSlaceGetAllTest(CompanyProfileSearchRequest dto) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getSliceIndex(), dto.getSize());
        logger.info("=======================");
//        Page<ProfileCompanyBaseDTO> profileCompany = accountCompanyTypeRespository.findAll(pr).map(e ->
//                modelMapper.map(e, ProfileCompanyBaseDTO.class));
//        Slice<ProfileCompanyBaseDTO> profileCompany = accountCompanyTypeRespository.findAll(pr).map(e ->
//                modelMapper.map(e, ProfileCompanyBaseDTO.class));
        //나중에 승인 된 어카운트타입만 검색하는 로직 추가
        List<Long> acc = accountCompanyTypeRespository.findAllByCompanyLicenseVerifyYn("Y").stream().map(e ->
                modelMapper.map(e.getAccntId(), Long.class)).collect(Collectors.toList());
        Slice<ProfileCompanyBaseDTO> profileCompanyList = null;
        Slice<ProfileCompanyResponse> company = null;

        logger.info("accsize = --------{}", acc.size());
        if (dto.getCompanyName() == null || dto.getCompanyName().equals("") || dto.getCompanyName().isEmpty()) {
            profileCompanyList = profileCompanyRepository.findByAccntIdIn(acc, pr)
                    .map(e -> modelMapper.map(e, ProfileCompanyBaseDTO.class));
//            company = profileCompanyRepository.findByAccntIdIn(acc,pr)
//                    .map(e -> modelMapper.map(e, ProfileCompanyBaseDTO.class));
            logger.info("--------date check 확인1 -{}");
            for (ProfileCompanyBaseDTO e : profileCompanyList.getContent()) {
                logger.info("--------date check 확인2 -{}", e);
            }

        } else {
            profileCompanyList = profileCompanyRepository
                    .findAllByAccntIdInAndProfileCompanyNameLike(acc, "%" + dto.getCompanyName() + "%", pr)
                    .map(e -> modelMapper.map(e, ProfileCompanyBaseDTO.class));
        }

//        res.setResult(accountCompanyTypeRespository.findAll(pr));
//        res.setResult(profileCompany);
        res.setResult(company);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilecompanyConnectSelectService(Long profileId) {
        Response res = new Response();
        Account userAccountSelect = accountRepository.findById(profileCompanyRepository.findById(profileId).get().getAccntId()).get();
//        List<AccountCompanyType> accType = accountCompanyTypeRespository.findByAccntId(userAccountSelect.getAccntId());
//        for(AccountCompanyType e: accType){
//        logger.info("컴퍼니타입하나씩--------------{}",e.getCompanyCd());
//        }
        ProfileCompanyResponse companyResponse = new ProfileCompanyResponse();
        for (ProfileCompany e : userAccountSelect.getProfileCompany()) {
            if (e.getProfileId().equals(profileId)) {
                companyResponse = modelMapper.map(e, ProfileCompanyResponse.class);
                companyResponse.setCountryCd(userAccountSelect.getCountryCd());
                companyResponse.setProfileCompanyName(userAccountSelect.getAccountCompanyDetail().getCompanyName());
                companyResponse.setCompanyEmail(userAccountSelect.getAccntEmail());
                companyResponse.setContactEmail(e.getComInfoEmail());
                companyResponse.setPhoneCountryCd(userAccountSelect.getCountryCd());
                companyResponse.setProfileId(e.getProfileId());
                companyResponse.setProfileCompanyOntheweb(e.getProfileCompanyOntheweb());
                companyResponse.setAccountCompanyType(userAccountSelect.getAccountCompanyType());
                companyResponse.setAccountCompanyDetail(userAccountSelect.getAccountCompanyDetail());
                companyResponse.setAccountCompanyLocation(userAccountSelect.getAccountCompanyLocation());
            }
        }
        res.setResult(companyResponse);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    @Autowired
    private ProfileCompanyPartnerRepository profileCompanyPartnerRepository;

    public Response ProfilePartnerSelectService(Long profileId) {
        Response res = new Response();

        List<ProfileCompanyBaseDTO> pc = profileCompanyPartnerRepository.findByProfileIdAndRelationStatusOrderByModDtDesc(profileId, "RES000003")
                .stream().map(e -> modelMapper.map(e, ProfileCompanyBaseDTO.class)).collect(Collectors.toList());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileCompanyVatUpdateService(VatUpdateRequest dto) {
        Response res = new Response();


        ProfileCompany pcBaseDTO = profileCompanyRepository.findById(dto.getProfileId()).get();

        if (dto.getVatNumber() == null || dto.getVatNumber().isEmpty()) {
            pcBaseDTO.setVatNumber("");
            pcBaseDTO.setIpiNumberVarifyYn("N");
        } else {
            pcBaseDTO.setVatNumber(dto.getVatNumber());
            pcBaseDTO.setIpiNumberVarifyYn("N");
        }
        profileCompanyRepository.save(pcBaseDTO);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileCompanyIpiUpdateService(IpiUpdateRequest dto) {
        Response res = new Response();
        logger.info("------------{}", dto);

        ProfileCompany pcBaseDTO = profileCompanyRepository.findById(dto.getProfileId()).get();
//                .map(e -> modelMapper.map(e, ProfileCompanyBaseDTO.class)).get();
        logger.info("-아이피아이 검색결과-----------{}", pcBaseDTO);
        if (dto.getIpiNumber() == null || dto.getIpiNumber().isEmpty()) {
            pcBaseDTO.setIpiNumber("");
            pcBaseDTO.setVatNumberVarifyYn("N");
        } else {
            pcBaseDTO.setIpiNumber(dto.getIpiNumber());
            pcBaseDTO.setVatNumberVarifyYn("N");
        }
//        profileCompanyRepository.save(modelMapper.map(pcBaseDTO,  ProfileCompany.class));
        profileCompanyRepository.save(pcBaseDTO);


        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


}
