package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyContact.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyContactNameRequest {

    @Schema(description = "CompanyProfileId")
    private Long profileId;
    private String contctFirstName;
    private String contctMidleName;
    private String contctLastName;

}
