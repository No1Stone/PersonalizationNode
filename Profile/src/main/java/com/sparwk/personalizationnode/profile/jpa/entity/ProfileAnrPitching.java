package com.sparwk.personalizationnode.profile.jpa.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_anr_pitching")
public class ProfileAnrPitching {

    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "pitching_yn", nullable = true)
    private String pitchingYn;

    @Builder
    ProfileAnrPitching
            (Long profileId,
             String pitchingYn){
        this.profileId = profileId;
        this.pitchingYn = pitchingYn;
    }


}
