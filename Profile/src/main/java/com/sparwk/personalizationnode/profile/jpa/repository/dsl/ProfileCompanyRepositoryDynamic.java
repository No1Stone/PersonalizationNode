package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface ProfileCompanyRepositoryDynamic {
    @Transactional
    Long ProfileCompanyRepositoryDynamicUpdate(ProfileCompanyBaseDTO DTO);
}
