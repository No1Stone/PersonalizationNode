package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel;

import com.sparwk.personalizationnode.profile.jpa.entity.*;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileResponse {

    private Long profileId;
    private Long accntId;
    private String mattermostId;
    private String fullName;
    private String stageNameYn;
    private String bthYear;
    private String bthMonth;
    private String bthDay;
    private String hireMeYn;
    private String bio;
    private String headline;
    private String currentCityCountryCd;
    private String currentCityNm;
    private String homeTownCountryCd;
    private String homeTownNm;
    private String ipiInfo;
    private String caeInfo;
    private String isniInfo;
    private String ipnInfo;
    private String nroTypeCd;
    private String nroInfo;
    private String profileImgUrl;
    private String profileBgdImgUrl;
    private String verifyYn;
    private String useYn;
    private String accomplishmentsInfo;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
//
//    @Builder
//    ProfileResponse(
//            Long profileId,
//            Long accntId,
//            String mattermostId,
//            String fullName,
//            String stageNameYn,
//            String bthYear,
//            String bthMonth,
//            String bthDay,
//            String hireMeYn,
//            String bio,
//            String headline,
//            String currentCityCountryCd,
//            String currentCityNm,
//            String homeTownCountryCd,
//            String homeTownNm,
//            String ipiInfo,
//            String caeInfo,
//            String isniInfo,
//            String ipnInfo,
//            String nroTypeCd,
//            String nroInfo,
//            String profileImgUrl,
//            String profileBgdImgUrl,
//            String verifyYn,
//            String useYn,
//            String accomplishmentsInfo,
//            Long regUsr,
//            LocalDateTime regDt,
//            Long modUsr,
//            LocalDateTime modDt
//
//    ) {
//        this.profileId = profileId;
//        this.accntId = accntId;
//        this.mattermostId = mattermostId;
//        this.fullName = fullName;
//        this.stageNameYn = stageNameYn;
//        this.bthYear = bthYear;
//        this.bthMonth = bthMonth;
//        this.bthDay = bthDay;
//        this.hireMeYn = hireMeYn;
//        this.bio = bio;
//        this.headline = headline;
//        this.currentCityCountryCd = currentCityCountryCd;
//        this.currentCityNm = currentCityNm;
//        this.homeTownCountryCd = homeTownCountryCd;
//        this.homeTownNm = homeTownNm;
//        this.ipiInfo = ipiInfo;
//        this.caeInfo = caeInfo;
//        this.isniInfo = isniInfo;
//        this.ipnInfo = ipnInfo;
//        this.nroTypeCd = nroTypeCd;
//        this.nroInfo = nroInfo;
//        this.profileImgUrl = profileImgUrl;
//        this.profileBgdImgUrl = profileBgdImgUrl;
//        this.verifyYn = verifyYn;
//        this.useYn = useYn;
//        this.accomplishmentsInfo = accomplishmentsInfo;
//        this.regUsr = regUsr;
//        this.regDt = regDt;
//        this.modUsr = modUsr;
//        this.modDt = modDt;
//    }

}
