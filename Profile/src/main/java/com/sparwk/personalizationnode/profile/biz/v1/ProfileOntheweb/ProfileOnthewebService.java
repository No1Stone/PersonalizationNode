package com.sparwk.personalizationnode.profile.biz.v1.ProfileOntheweb;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileOntheweb.dto.ProfileOnthewebRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileOntheweb.dto.ProfileOnthewebRqeustList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileOnthewebBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileLanguageRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileOnthewebRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.CoderResult;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileOnthewebService {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfileOnthewebRepository profileOnthewebRepository;
    @Autowired
    private HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfileOnthewebService.class);

    public Response ProfileOnthewebSaveResponseService(ProfileOnthewebRqeustList dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        int resultCount = 0;
        for (ProfileOnthewebRequest e : dto.getProfileOnthewebRequestList()) {
            if (e.getProfileId() == null) {
                res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }

            ProfileOnthewebBaseDTO setBaseDTO = modelMapper.map(e, ProfileOnthewebBaseDTO.class);
            setBaseDTO.setModUsr(userInfoDTO.getAccountId());
            setBaseDTO.setModDt(LocalDateTime.now());
            setBaseDTO.setRegUsr(userInfoDTO.getAccountId());
            setBaseDTO.setRegDt(LocalDateTime.now());
            profileServiceRepository.ProfileOnthewebSaveService(setBaseDTO);
            resultCount++;
        }
        res.setResult(resultCount);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response ProfileOnthewebSelectResponseService(Long profileId) {
        List<ProfileOnthewebBaseDTO> onthewebBaseDTOList =
                profileOnthewebRepository.findByProfileId(profileId)
                        .stream().map(e -> modelMapper.map(e, ProfileOnthewebBaseDTO.class)).collect(Collectors.toList());
        Response res = new Response();
        res.setResult(onthewebBaseDTOList);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
