package com.sparwk.personalizationnode.profile.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyRosterBaseDTO {

    private Long rosterSeq;
    private Long profileId;
    private Long profilePositionSeq;
    private String rosterStatus;
    private String joiningStartDt;
    private String joiningEndDt;
    private String joiningEndYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;


}
