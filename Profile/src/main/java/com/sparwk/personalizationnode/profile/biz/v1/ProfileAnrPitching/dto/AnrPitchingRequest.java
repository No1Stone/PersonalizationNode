package com.sparwk.personalizationnode.profile.biz.v1.ProfileAnrPitching.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AnrPitchingRequest {
    private Long profileId;
    private String pitchingYn;
}
