package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminCountryCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminCountryCodeRepository extends JpaRepository<AdminCountryCode, Long> {

    Optional<AdminCountryCode> findByCountryCd(String cd);
    boolean existsByCountryCd(String cd);

}
