package com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePositionEditRequestList {

    private List<ProfilePositionEditRequest> profilePositionEditRequestList;


}
