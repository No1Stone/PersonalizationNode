package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompanyContact;
import com.sparwk.personalizationnode.profile.jpa.repository.dsl.ProfileCompanyContactRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileCompanyContactRepository extends JpaRepository<ProfileCompanyContact, Long>, ProfileCompanyContactRepositoryDynamic {

    
}
