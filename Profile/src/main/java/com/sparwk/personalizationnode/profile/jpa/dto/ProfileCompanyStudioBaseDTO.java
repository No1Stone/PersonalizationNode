package com.sparwk.personalizationnode.profile.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyStudioBaseDTO {
    private Long studioId;
    private Long profileId;
    private Long ownerId;
    private String skipYn;
    private String studioName;
    private String businessLocationCd;
    private String postCd;
    private String region;
    private String city;
    private String addr1;
    private String addr2;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
