package com.sparwk.personalizationnode.profile.biz.chat;

import lombok.Data;

@Data
public class CreateMmRes {

    private String firstname;
    private Long deleteat;
    private String timezone;
    private String roles;
    private String mfasecret;
    private String locale;
    private String remoteid;
    private String password;
    private String notifyprops;
    private String nickname;
    private String id;
    private String allowmarketing;
    private String email;
    private Long updateat;
    private String failedattempts;
    private String mfaactive;
    private Long lastpasswordupdate;
    private Long createat;
    private String lastname;
    private String props;
    private String authservice;
    private String authdata;
    private Long lastpictureupdate;
    private String position;
    private String username;
    private String emailverified;


}
