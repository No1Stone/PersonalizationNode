package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompanyPartner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProfileCompanyPartnerRepository extends JpaRepository<ProfileCompanyPartner,Long> {

    List<ProfileCompanyPartner> findByProfileId(Long profileId);

    List<ProfileCompanyPartner> findByProfileIdAndRelationStatusOrderByModDtDesc(Long profileId, String codeString);

    List<ProfileCompanyPartner> findByProfileIdOrderByModDtDesc(Long profileId);

    List<ProfileCompanyPartner> findByProfileIdAndRelationStatus(Long profileId, String relationCd);

    ProfileCompanyPartner findByProfileIdAndPartnerId(Long profileId, Long partnerId);

    boolean existsByProfileIdAndPartnerId(Long profileId, Long partnerId);

}
