package com.sparwk.personalizationnode.profile.biz.v2.profile.dto.panneldto;

import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyBaseDTO;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePositionCodeName {
    private Long profilePositionSeq;
    private Long profileId;
    private Long companyProfileId;
    private String companyVerifyYn;
    private String artistYn;
    private String anrYn;
    private String creatorYn;
    private String deptRoleInfo;
    private String primaryYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
    private ProfileCompanyBaseDTO profileCompanyBaseDTO;


}
