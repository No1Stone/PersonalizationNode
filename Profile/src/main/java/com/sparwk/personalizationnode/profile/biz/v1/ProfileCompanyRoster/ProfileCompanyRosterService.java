package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster;

import com.google.gson.Gson;
import com.sparwk.personalizationnode.profile.biz.enumintegrated.YnEnum;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.dto.ProfileCompanyRosterRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.dto.ProfileCompanyRosterRequestList;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.dto.ProfileCompanyRosterResponse;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.dto.ReceiveNotification;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.enumdata.RosterStatunEnum;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel.ProfilePositionResponse;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel.ProfileResponse;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.rsvp.AccountBaseDTO;
import com.sparwk.personalizationnode.profile.config.common.RestCall;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyRosterBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfilePositionBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompanyRoster;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfilePosition;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyRosterRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfilePositionRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.account.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileCompanyRosterService {
    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyRosterService.class);
    @Autowired
    private ProfileCompanyRosterRepository profileCompanyRosterRepository;
    private final MessageSourceAccessor messageSourceAccessor;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfilePositionRepository profilePositionRepository;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private HttpServletRequest httpServletRequest;

    public Response ProfileCompanyRosterSaveService(ProfileCompanyRosterRequest dto) {

        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        logger.info("ProfileCompanyRoster DTO - - - - - - - -{}", dto);
        ProfileCompanyRosterBaseDTO baseDTO = modelMapper.map(dto, ProfileCompanyRosterBaseDTO.class);
        baseDTO.setModDt(LocalDateTime.now());
        baseDTO.setRegDt(LocalDateTime.now());
        baseDTO.setRegUsr(userInfoDTO.getAccountId());
        baseDTO.setModUsr(userInfoDTO.getAccountId());

        //유저의 프로필 포지션 상태가
        // ex select * from tb_profile_position where profile_id = ? and companyverifyyn = 'Y'  ;
        // String primari = 'Y'
//        profilePositionRepository.existsByProfileIdAndAndCompanyVerifyYn();

        ProfileCompanyRosterBaseDTO result = profileServiceRepository.ProfileCompanyRosterSaveService(baseDTO);


        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilecompanyRosterSelectService(Long profileId) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        List<ProfileCompanyRosterResponse> pcrrList = new ArrayList<>();

        if (profileCompanyRosterRepository.existsByProfileId(profileId)) {

            List<ProfileCompanyRosterBaseDTO> pcrrBaseList = profileCompanyRosterRepository
                    .findByProfileIdOrderByModDtDesc(profileId)
                    .stream().map(e -> modelMapper.map(e, ProfileCompanyRosterBaseDTO.class)).collect(Collectors.toList());


            for (ProfileCompanyRosterBaseDTO e : pcrrBaseList) {
                ProfileCompanyRosterResponse pcrrAdd = modelMapper.map(e, ProfileCompanyRosterResponse.class);

                if (!profilePositionRepository.existsByProfilePositionSeq(pcrrAdd.getProfilePositionSeq())) {
                    res.setResultCd(ResultCodeConst.NOT_FOUNT_PROFILE_POSITION.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                }
                ProfilePositionBaseDTO position = profilePositionRepository.findByProfilePositionSeq(e.getProfilePositionSeq())
                        .map(f -> modelMapper.map(f, ProfilePositionBaseDTO.class)).get();

                if (!profileRepository.existsById(pcrrAdd.getProfileId())) {
                    res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                }
                ProfileResponse pr = profileRepository.findByProfileId(position.getProfileId()).map(g ->
                        modelMapper.map(g, ProfileResponse.class)).get();
                pcrrAdd.setProfileResponse(pr);
                if (!accountRepository.existsById(pr.getAccntId())) {
                    res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                }
                pcrrAdd.setProfilePositionResponse(modelMapper.map(position, ProfilePositionResponse.class));
                AccountBaseDTO ab = accountRepository
                        .findByAccntId(pr.getAccntId()).map(h -> modelMapper.map(h, AccountBaseDTO.class)).get();
                pcrrAdd.setUserEmail(ab.getAccntEmail());
                pcrrAdd.setUserPhone(ab.getPhoneNumber());
                pcrrList.add(pcrrAdd);
            }
            res.setResult(pcrrList);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;

        } else {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
    }

    public Response ProfileCompanyRosterListSaveService(ProfileCompanyRosterRequestList dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        logger.info("dto tostring.... {}",dto);
        for (ProfileCompanyRosterRequest e : dto.getProfileCompanyRosterRequestList()) {
            logger.info("roster ---{}", e);
            ProfileCompanyRosterBaseDTO baseDTO = modelMapper.map(e, ProfileCompanyRosterBaseDTO.class);
            baseDTO.setModDt(LocalDateTime.now());
            baseDTO.setRegDt(LocalDateTime.now());
            baseDTO.setRegUsr(userInfoDTO.getAccountId());
            baseDTO.setModUsr(userInfoDTO.getAccountId());
            baseDTO.setJoiningEndYn(YnEnum.N.getValue());
            logger.info("yyMMdd===test=={}????");
            DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            if (e.getJoiningStartDt() == null
                    || e.getJoiningStartDt().isEmpty()
                    || e.getJoiningStartDt().isBlank()
                    || e.getJoiningStartDt().equals("")) {
                baseDTO.setJoiningStartDt(sdf.format(new Date()));
            }

            if (e.getJoiningEndDt() == null
                    || e.getJoiningEndDt().isEmpty()
                    || e.getJoiningEndDt().isBlank()
                    || e.getJoiningEndDt().equals("")) {
                baseDTO.setJoiningEndDt("99991231");
            }
            ProfileCompanyRoster result = profileCompanyRosterRepository.save(modelMapper.map(baseDTO, ProfileCompanyRoster.class));

            if (result.getRosterStatus().equals(RosterStatunEnum.AGREEMENT.getValue())) {
                logger.info("================================로스터걸림");
                profileServiceRepository.ProfilePositionDynamicUpdateService(ProfilePositionBaseDTO.builder()
                        .profilePositionSeq(result.getProfilePositionSeq())
                        .companyVerifyYn(YnEnum.Y.getValue()).build());
                NotiTestCode002(profilePositionRepository
                        .findByProfilePositionSeq(result.getProfilePositionSeq()).get().getProfileId(), e.getProfileId());
            }
        }

        Response res = new Response();
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    //회사 로스터 승인 프로필
    public void NotiTestCode002(Long userProfileId, Long companyProfileId) {
//        String URL = "http://localhost:8080/requestNotification";
        String URL = "https://communication.sparwkdev.com/notification/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //Noti 002 = 승인된 회사 로스터 프로필아이디
        users.add(String.valueOf(userProfileId));

        //메세지 또는 프로필 이미지세팅용 profileId
        key.put("profileCompanyId", String.valueOf(companyProfileId));

        rn.setNotiCode("NOTI_002");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }
}
