package com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto;

import com.sparwk.personalizationnode.profile.jpa.dto.ProfilePositionBaseDTO;
import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PositionCompanyResponse extends ProfilePositionBaseDTO {

    private String profileCompanyName;
    private String profileCompanyImgUrl;

}
