package com.sparwk.personalizationnode.profile.config.filter.token;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TokenDTO {
    private Long accountId;
    private String accntTypeCd;
    private List<Long> ProfileList;
    private Object permission;

}
