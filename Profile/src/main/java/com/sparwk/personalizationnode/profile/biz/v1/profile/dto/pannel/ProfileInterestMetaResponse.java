package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel;

import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileInterestMetaId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileInterestMetaResponse {

    private String kindTypeCd;
    private Long profileId;
    private String detailTypeCd;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

//    @Builder
//    ProfileInterestMetaResponse(
//
//            String kindTypeCd,
//            Long profileId,
//            String detailTypeCd,
//            Long regUsr,
//            LocalDateTime regDt,
//            Long modUsr,
//            LocalDateTime modDt
//
//    ) {
//
//        this.kindTypeCd = kindTypeCd;
//        this.profileId = profileId;
//        this.detailTypeCd = detailTypeCd;
//        this.regUsr = regUsr;
//        this.regDt = regDt;
//        this.modUsr = modUsr;
//        this.modDt = modDt;
//
//    }


}
