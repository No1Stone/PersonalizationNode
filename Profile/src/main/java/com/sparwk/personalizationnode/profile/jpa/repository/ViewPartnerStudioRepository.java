package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ViewPartnerStudio;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ViewPartnerStudioRepository extends JpaRepository<ViewPartnerStudio, Long> {

    List<ViewPartnerStudio> findAllByProfileIdIn(List<Long> profileId);

    List<ViewPartnerStudio> findByProfileId(Long aLong);
    List<ViewPartnerStudio> findByMappingProfileId(Long aLong);

    Page<ViewPartnerStudio> findByProfileIdOrderByModDtDesc(Long profileId, Pageable pageable);


}
