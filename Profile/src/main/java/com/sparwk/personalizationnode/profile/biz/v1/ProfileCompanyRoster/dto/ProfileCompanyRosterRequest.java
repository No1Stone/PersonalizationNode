package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyRosterRequest {

    private Long profileId;
    private Long patnerId;
    private Long rosterSeq;
    private Long profilePositionSeq;
    private String rosterStatus;
    private String joiningStartDt;
    private String joiningEndDt;
    private String joiningEndYn;
    @Schema(description = "artist, creator, ant, group")
    private String posotionType;


}
