package com.sparwk.personalizationnode.profile.jpa.entity.code;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_language_code")
public class AdminLanguageCode {

    @Id
    @Column(name = "language_seq ", nullable = true)
    private Long languageSeq;
    @Column(name = "language ", nullable = true)
    private String language;
    @Column(name = "language_family ", nullable = true)
    private String languageFamily;
    @Column(name = "native_language ", nullable = true)
    private String nativeLanguage;
    @Column(name = "iso639_1 ", nullable = true)
    private String iso6391;
    @Column(name = "iso639_2t ", nullable = true)
    private String iso6392t;
    @Column(name = "iso639_2b ", nullable = true)
    private String iso6392b;
    @Column(name = "iso639_3 ", nullable = true)
    private String iso6393;
    @Column(name = "use_yn ", nullable = true)
    private String useYn;
    @Column(name = "reg_usr ", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt ", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt ", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "language_cd", nullable = true)
    private String languageCd;

    @Builder
    AdminLanguageCode(
            Long languageSeq,
            String language,
            String languageFamily,
            String nativeLanguage,
            String iso6391,
            String iso6392t,
            String iso6392b,
            String iso6393,
            String useYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt,
            String languageCd
    ) {
        this.languageSeq = languageSeq;
        this.language = language;
        this.languageFamily = languageFamily;
        this.nativeLanguage = nativeLanguage;
        this.iso6391 = iso6391;
        this.iso6392t = iso6392t;
        this.iso6392b = iso6392b;
        this.iso6393 = iso6393;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
        this.languageCd = languageCd;

    }

}
