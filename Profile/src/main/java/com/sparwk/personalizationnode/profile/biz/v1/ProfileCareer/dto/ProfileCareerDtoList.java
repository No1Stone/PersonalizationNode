package com.sparwk.personalizationnode.profile.biz.v1.ProfileCareer.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCareerDtoList {

    private List<ProfileCareerDto> ProfileCareerDtoList;

}
