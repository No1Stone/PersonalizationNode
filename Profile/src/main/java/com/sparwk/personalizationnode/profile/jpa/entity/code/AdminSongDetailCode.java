package com.sparwk.personalizationnode.profile.jpa.entity.code;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_song_detail_code")
public class AdminSongDetailCode {

    @Id
    @Column(name = "song_detail_code_seq", nullable = true)
    private Long songDetailCodeSeq;
    @Column(name = "pcode", nullable = true)
    private String pcode;
    @Column(name = "dcode", nullable = true)
    private String dcode;
    @Column(name = "format", nullable = true)
    private String format;
    @Column(name = "val", nullable = true)
    private String val;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "description", nullable = true)
    private String description;
    @Column(name = "sort_index", nullable = true)
    private int sortIndex;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "popular_yn", nullable = true)
    private String popularYn;
    @Column(name = "hit", nullable = true)
    private Long hit;


    @Builder
    AdminSongDetailCode(
            Long songDetailCodeSeq,
            String pcode,
            String dcode,
            String format,
            String val,
            String useYn,
            String description,
            int sortIndex,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt,
            String popularYn,
            Long hit
    ) {
        this.songDetailCodeSeq = songDetailCodeSeq;
        this.pcode = pcode;
        this.dcode = dcode;
        this.format = format;
        this.val = val;
        this.useYn = useYn;
        this.description = description;
        this.sortIndex = sortIndex;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
        this.popularYn = popularYn;
        this.hit = hit;
    }
}
