package com.sparwk.personalizationnode.profile.jpa.repository.account;

import com.sparwk.personalizationnode.profile.jpa.entity.account.AccountCompanyLocation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountCompanyLocationRepository extends JpaRepository<AccountCompanyLocation, Long> {
}
