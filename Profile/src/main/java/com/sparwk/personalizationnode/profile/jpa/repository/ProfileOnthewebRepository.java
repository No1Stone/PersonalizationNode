package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileOntheweb;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileOnthewebRepository extends JpaRepository<ProfileOntheweb, Long> {

    List<ProfileOntheweb> findByProfileId(Long profileId);

}
