package com.sparwk.personalizationnode.profile.biz.v1.ProfileSkill;

import com.sparwk.personalizationnode.profile.biz.v1.CustomException;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileSkill.dto.ProfileSkillDto;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileSkillBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileSkillRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileSkillService {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfileSkillRepository profileSkillRepository;
    @Autowired
    private HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfileSkillService.class);

    public Response ProfileSkillSave(ProfileSkillDto dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        if (dto.getProfileId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
//        List<ProfileSkillBaseDTO> langSkill = new ArrayList<>();
//        ProfileSkillBaseDTO result = null;
//        String[] Skills = dto.getSkillTypeCd().trim().split(",");
//        String[] SkillsDetail = dto.getSkillDetailTypeCd().trim().split(",");
//        if(Skills.length == SkillsDetail.length){
//            for(int i = 0 ; i < Skills.length ; i++){
//                ProfileSkillBaseDTO baseDTO = modelMapper.map(dto, ProfileSkillBaseDTO.class);
//                baseDTO.setProfileId(userInfoDTO.getLastUseProfileId());
//                baseDTO.setSkillDetailTypeCd(SkillsDetail[i]);
//                baseDTO.setSkillTypeCd(Skills[i]);
//                baseDTO.setModDt(LocalDateTime.now());
//                baseDTO.setRegDt(LocalDateTime.now());
//                baseDTO.setModUsr(userInfoDTO.getAccountId());
//                baseDTO.setRegUsr(userInfoDTO.getAccountId());
//                result = profileServiceRepository.ProfileSkillSaveService(baseDTO);
//                langSkill.add(result);
//            }
//        }
//        else {
//            throw new CustomException("skill, skilldetail 갯수가 일치하지 않음", HttpStatus.BAD_REQUEST);
//        }
        ProfileSkillBaseDTO baseDTO = modelMapper.map(dto, ProfileSkillBaseDTO.class);
        baseDTO.setModDt(LocalDateTime.now());
//                baseDTO.setRegDt(LocalDateTime.now());
        baseDTO.setModUsr(userInfoDTO.getAccountId());
//                baseDTO.setRegUsr(userInfoDTO.getAccountId());
        ProfileSkillBaseDTO result = profileServiceRepository.ProfileSkillSaveService(baseDTO);
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileSkillSelect(Long profileId) {
        List<ProfileSkillBaseDTO> result = profileSkillRepository.findByProfileId(profileId)
                .stream().map(e -> modelMapper.map(e, ProfileSkillBaseDTO.class))
                .collect(Collectors.toList());
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    @Transactional
    public Response profileSkillDelete(ProfileSkillDto dto) {
        Response res = new Response();
        if (dto.getProfileId() == null) {
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        profileSkillRepository
                .deleteByProfileIdAndSkillTypeCdAndSkillDetailTypeCd(
                        dto.getProfileId(), dto.getSkillTypeCd(), dto.getSkillDetailTypeCd());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
