package com.sparwk.personalizationnode.profile.biz.v1.profile.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountServerRequest {

    @Schema(description = "계정 아이디")
    private Long accntId;

    @Schema(description = "멀티 계정 그룹핑 아이디")
    private Long accntRepositoryId;

    @Schema(description = "유저 타입 사람/회사/그룹/단체")
    @Size(max = 9, message = "AccounttyCd max lenth = 9")
    private String accntTypeCd;

    @Schema(description = "사용자 이메일")
    @Size(max = 100, message = "Account email NotNull")
    private String accntEmail;

    @Schema(description = "사용자 비밀번호")
    @Size(max = 200, message = "encoding")
    private String accntPass;

    @Schema(description = "사용자 국가 코드")
    @Size(max = 9, message = "AccounttyCd max lenth = 9")
    private String countryCd;

    @Size(max = 20, message = "")
    @Schema(description = "사용자 핸드폰 번호")
    private String phoneNumber;

    @Size(max = 1, message = "y or n")
    @Schema(description = "접속이메일사용여부 default y")
    private String accntLoginActiveYn;

    @Schema(description = "마지막으로 이용한 프로필 아이디")
    private Long lastUseProfileId;

    @Size(max = 1, message = "y or n")
    @Schema(description = "이메일 인증 여부 default n")
    private String verifyYn;

    @Size(max = 1, message = "y or n")
    @Schema(description = "이메일 인증 여부 default y")
    private String useYn;


    @Size(max = 1, message = "y or n")
    @Schema(description = "개인정보 수집동의여부")
    private String personalInfoCollectionYn;

    @Size(max = 1, message = "y or n")
    @Schema(description = "마케팅 수신 동의여부")
    private String sparwkMarketingYn;
}
