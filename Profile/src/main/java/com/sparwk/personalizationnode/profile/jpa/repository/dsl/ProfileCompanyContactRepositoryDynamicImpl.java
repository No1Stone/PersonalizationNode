package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyContactBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.QProfileCompanyContact;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class ProfileCompanyContactRepositoryDynamicImpl implements ProfileCompanyContactRepositoryDynamic {

    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyContactRepositoryDynamicImpl.class);
    private QProfileCompanyContact qProfileCompanyContact = QProfileCompanyContact.profileCompanyContact;

    private ProfileCompanyContactRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }


    @Override
    public Long ProfileCompanyContactRepositoryDynamicUpdate(ProfileCompanyContactBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProfileCompanyContact);


        if (entity.getProfileContactImgUrl() == null || entity.getProfileContactImgUrl().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompanyContact.profileContactImgUrl, entity.getProfileContactImgUrl());
        }
        if (entity.getProfileContactDescription() == null || entity.getProfileContactDescription().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompanyContact.profileContactDescription, entity.getProfileContactDescription());
        }


        if (entity.getContctFirstName() == null || entity.getContctFirstName().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompanyContact.contctFirstName, entity.getContctFirstName());
        }
        if (entity.getContctMidleName() == null || entity.getContctMidleName().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompanyContact.contctMidleName, entity.getContctMidleName());
        }
        if (entity.getContctLastName() == null || entity.getContctLastName().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompanyContact.contctLastName, entity.getContctLastName());
        }


        if (entity.getContctEmail() == null || entity.getContctEmail().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompanyContact.contctEmail, entity.getContctEmail());
        }
        if (entity.getContctPhoneNumber() == null || entity.getContctPhoneNumber().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompanyContact.contctPhoneNumber, entity.getContctPhoneNumber());
        }

        if (entity.getCountryCd() == null || entity.getCountryCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompanyContact.countryCd, entity.getCountryCd());
        }

        if (entity.getVerifyPhoneYn() == null || entity.getVerifyPhoneYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCompanyContact.verifyPhoneYn, entity.getVerifyPhoneYn());
        }
        if (entity.getRegUsr() != null) {
            jpaUpdateClause.set(qProfileCompanyContact.regUsr, entity.getRegUsr());
        }
        if (entity.getRegDt() != null) {
            jpaUpdateClause.set(qProfileCompanyContact.regDt, entity.getRegDt());
        }
        if (entity.getModUsr() != null) {
            jpaUpdateClause.set(qProfileCompanyContact.modUsr, entity.getModUsr());
        }
        if (entity.getModDt() != null) {
            jpaUpdateClause.set(qProfileCompanyContact.modDt, entity.getModDt());
        }

        jpaUpdateClause.where(qProfileCompanyContact.profileId.eq(entity.getProfileId()));

        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
