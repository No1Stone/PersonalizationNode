package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupTimezone;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupTimezone.dto.ProfileGroupTimezoneRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/V1/group/timezone")
@CrossOrigin("*")
@Api(tags = "Profile Server Group timezone API")
@RequiredArgsConstructor
public class ProfileGroupTimezoneController {

    private final Logger logger = LoggerFactory.getLogger(ProfileGroupTimezoneController.class);
    private final ProfileGroupTimezoneService profileGroupTimezoneService;

    @ApiOperation(
            value = "그룹 온더웹 조회",
            notes = "그룹 프로필 조회"
    )
    @GetMapping(path = "/info/{profileId}")
    public Response ProfileGroupTimezoneSelectController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileGroupTimezoneService.ProfileGroupTimezoneSelectService(profileId);
        return res;
    }

    @ApiOperation(
            value = "그룹 온더웹 조회",
            notes = "그룹 프로필 조회"
    )
    @PostMapping(path = "/info/list")
    public Response ProfileGroupTimezoneSaveListController(@RequestBody ProfileGroupTimezoneRequestList dto) {
        Response res = profileGroupTimezoneService.ProfileGroupTimezoneSaveListService(dto);
        return res;
    }

}
