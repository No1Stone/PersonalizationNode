package com.sparwk.personalizationnode.profile.jpa.entity.code;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_role_detail_code")
public class AdminRoleDetailCode {

    @Id
    @Column(name = "role_detail_code_seq ", nullable = true)
    private Long roleDetailCodeSeq;
    @Column(name = "pcode ", nullable = true)
    private String pcode;
    @Column(name = "dcode ", nullable = true)
    private String dcode;
    @Column(name = "format ", nullable = true)
    private String format;
    @Column(name = "role ", nullable = true)
    private String role;
    @Column(name = "use_yn ", nullable = true)
    private String useYn;
    @Column(name = "description ", nullable = true)
    private String description;
    @Column(name = "reg_usr ", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt ", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr ", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt ", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "popular_yn ", nullable = true)
    private String popularYn;
    @Column(name = "hit ", nullable = true)
    private Long hit;
    @Column(name = "matching_role_yn ", nullable = true)
    private String matchingRoleYn;
    @Column(name = "credit_role_yn ", nullable = true)
    private String creditRoleYn;
    @Column(name = "split_sheet_role_yn ", nullable = true)
    private String splitSheet_roleYn;
    @Column(name = "abbeviation ", nullable = true)
    private String abbeviation;
    @Column(name = "custom_code", nullable = true)
    private String customCode;





    @Builder
    AdminRoleDetailCode(Long roleDetailCodeSeq,
                        String pcode,
                        String dcode,
                        String format,
                        String role,
                        String useYn,
                        String description,
                        Long regUsr,
                        LocalDateTime regDt,
                        Long modUsr,
                        LocalDateTime modDt,
                        String popularYn,
                        Long hit,
                        String matchingRoleYn,
                        String creditRoleYn,
                        String splitSheet_roleYn,
                        String abbeviation,
                        String customCode
    ) {
        this.roleDetailCodeSeq = roleDetailCodeSeq;
        this.pcode = pcode;
        this.dcode = dcode;
        this.format = format;
        this.role = role;
        this.useYn = useYn;
        this.description = description;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
        this.popularYn = popularYn;
        this.hit = hit;
        this.matchingRoleYn = matchingRoleYn;
        this.creditRoleYn = creditRoleYn;
        this.splitSheet_roleYn = splitSheet_roleYn;
        this.abbeviation = abbeviation;
        this.customCode = customCode;
    }

}
