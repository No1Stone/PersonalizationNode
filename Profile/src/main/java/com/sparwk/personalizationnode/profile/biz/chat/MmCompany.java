package com.sparwk.personalizationnode.profile.biz.chat;

import lombok.Data;

@Data
public class MmCompany {

    //metterMost_id
    private String companyId;
    //Sparwk Id
    private Long companySparwkId;
    //추가되는사람 metterMost_Id
    private String profileId;

}
