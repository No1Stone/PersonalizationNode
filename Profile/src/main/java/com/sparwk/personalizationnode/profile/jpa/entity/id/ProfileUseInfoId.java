package com.sparwk.personalizationnode.profile.jpa.entity.id;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class ProfileUseInfoId implements Serializable {

    private Long accntId;
    private Long lastUseProfileId;

}
