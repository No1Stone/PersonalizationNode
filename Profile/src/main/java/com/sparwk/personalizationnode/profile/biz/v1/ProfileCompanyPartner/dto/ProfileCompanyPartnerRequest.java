package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyPartnerRequest {
    private Long profileId;
    private Long partnerId;
    private String relationType;
    private String relationStatus;
}
