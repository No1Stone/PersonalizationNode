package com.sparwk.personalizationnode.profile.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileCompanyStudioId implements Serializable {
    private Long studioId;
    private Long profileId;
}
