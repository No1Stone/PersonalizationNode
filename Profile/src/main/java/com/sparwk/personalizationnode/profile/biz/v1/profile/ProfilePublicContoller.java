package com.sparwk.personalizationnode.profile.biz.v1.profile;

import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.ProfileArray;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.ProfileDto;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.ProjectMemberRequest;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.profilecreate.RequestProfileCreate;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/profile/public")
@CrossOrigin("*")
@Api(tags = "Profile Server Profile Public API V1")
public class ProfilePublicContoller {

    @Autowired
    private ProfileService profileService;
    private final Logger logger = LoggerFactory.getLogger(ProfilePublicContoller.class);


    @ApiOperation(
            value = "토큰이 필요없는 프로필 정보검색",
            notes = "프로필 정보검색"
    )
    @GetMapping(path = "/profileSelectall/{accountId}")
        public Response ProfileIdPathVariableSelectAll(@PathVariable(name = "accountId")Long accountId){
            Response res = profileService.ProfileAccountSelectList(accountId);
            return res;
        }
    @ApiOperation(
            value = "토큰이 필요없는 프로필 정보검색",
            notes = "프로필 정보검색"
    )
    @GetMapping(path = "/profileSelectone/{profileId}")
    public Response ProfileIdPathVariableSelectOne(@PathVariable(name = "profileId")Long profileId){
        Response res = profileService.findByProfileIdSelectOneService(profileId);
        return res;
    }

    @ApiOperation(
            value = "토큰이 필요없는 프로필 정보검색",
            notes = "프로필 정보검색"
    )
    @GetMapping(path = "/profileSelectone2/{profileId}")
    public Response ProfileIdPathVariableSelectOne2(@PathVariable(name = "profileId")Long profileId){
        Response res = profileService.findByProfileIdSelectOneService2(profileId);
        return res;
    }

    @ApiOperation(
            value = "토큰이 필요없는 프로필 정보검색",
            notes = "프로필 정보검색"
    )
    @GetMapping(path = "/profilePannel/{profileId}")
    public Response ProfilePannelSelectController(@PathVariable(name = "profileId")Long profileId){
        Response res = profileService.ProfilePannelSelectService(profileId);
        return res;
    }

    @ApiOperation(
            value = "토큰이 필요없는 프로필 정보검색",
            notes = "프로필 정보검색"
    )
    @PostMapping(path = "/profilePannel")
    public Response ProfilePannelSelectListController(@RequestBody ProfileArray pa){
        Response res = profileService.ProfilePannelSelectListService(pa);
        return res;
    }

    @GetMapping(path = "/email/{profileId}")
    public Response ProfileEmailSelect(@PathVariable(name = "profileId")Long profileId){
    Response res = profileService.EmialGetService(profileId);
    return res;
    }

    @GetMapping(path = "/phone/{profileId}")
    public Response ProfilePhoneSelect(@PathVariable(name = "profileId")Long profileId){
        Response res = profileService.PhoneGetService(profileId);
        return res;
    }

    @GetMapping(path = "/emailphone/{profileId}")
    public Response ProfileEmailPhoneSelect(@PathVariable(name = "profileId")Long profileId){
        Response res = profileService.EmailPhoneGetService(profileId);
        return res;
    }


    @GetMapping(path = "/test1/{profileId}")
    public Response profileTest(@PathVariable (name = "profileId") Long profileId){
        Response res = profileService.NewPannel(profileId);
        return res;
    }

    @GetMapping(path = "/test2/{profileId}")
    public Response profileTest2(@PathVariable (name = "profileId") Long profileId){
        Response res = profileService.profileTest2(profileId);
        return res;
    }

    @GetMapping(path = "/testmm/{profileId}")
    public void testmmCon(@PathVariable(name = "profileId")Long profileId){
        profileService.testmmCon(profileId);
    }

    @GetMapping(path = "/mmGet/{profileId}")
    public String mmGet(@PathVariable(name = "profileId")Long profileId){
        return profileService.mmGetService(profileId);
    }
    @GetMapping(path = "/mmComGet/{profileId}")
    public String mmComidGet(@PathVariable(name = "profileId")Long profileId){
        return profileService.mmComidGetService(profileId);
    }

}
