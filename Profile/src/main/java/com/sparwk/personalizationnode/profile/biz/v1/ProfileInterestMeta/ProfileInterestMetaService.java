package com.sparwk.personalizationnode.profile.biz.v1.ProfileInterestMeta;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileInterestMeta.dto.ProfileInterestMetaRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileInterestMeta.dto.ProfileInterestMetaRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileInterestMetaBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileInterestMeta;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileInterestMetaRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileInterestMetaService {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfileInterestMetaRepository profileInterestMetaRepository;
    @Autowired
    private HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfileInterestMetaService.class);

    public Response ProfileInterestMetaSave(ProfileInterestMetaRequestList dto){
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
//        List<ProfileInterestMetaBaseDTO> interResult = new ArrayList<>();

        List<ProfileInterestMeta> deleteList = profileInterestMetaRepository.findByProfileId(userInfoDTO.getLastUseProfileId());

        for(ProfileInterestMeta e :  deleteList) {
            profileInterestMetaRepository.deleteByProfileIdAndDetailTypeCdAndKindTypeCd(e.getProfileId(), e.getDetailTypeCd(), e.getKindTypeCd());
        }

        for(ProfileInterestMetaRequest f : dto.getProfileInterestMetaRequestList()){
            logger.info("---{}",f);
            ProfileInterestMetaBaseDTO baseDTO = modelMapper.map(f , ProfileInterestMetaBaseDTO.class);
            baseDTO.setProfileId(userInfoDTO.getLastUseProfileId());
            baseDTO.setRegDt(LocalDateTime.now());
            baseDTO.setModDt(LocalDateTime.now());
            baseDTO.setRegUsr(userInfoDTO.getAccountId());
            baseDTO.setModUsr(userInfoDTO.getAccountId());
//            ProfileInterestMetaBaseDTO result = profileServiceRepository.ProfileInterestMetaSaveService(baseDTO);
//            interResult.add(result);
            ProfileInterestMeta save = profileInterestMetaRepository.save(modelMapper.map(baseDTO, ProfileInterestMeta.class));
        }

        res.setResult(profileInterestMetaRepository.findByProfileId(userInfoDTO.getLastUseProfileId()));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response ProfileInterestMetaSelect(Long profileId){
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        Response res = new Response();
        List<ProfileInterestMetaBaseDTO> result = profileInterestMetaRepository.findByProfileId(profileId)
            .stream().map(e -> modelMapper.map(e, ProfileInterestMetaBaseDTO.class))
            .collect(Collectors.toList());
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileInterestMetaDynamicUpdate(ProfileInterestMetaRequest dto){
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        ProfileInterestMetaBaseDTO baseDTO = modelMapper.map(dto, ProfileInterestMetaBaseDTO.class);
        baseDTO.setProfileId(dto.getProfileId());
        baseDTO.setModUsr(userInfoDTO.getAccountId());
        baseDTO.setModDt(LocalDateTime.now());
        long result = profileServiceRepository.ProfileInterestMetaDynamicUpdateService(baseDTO);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
