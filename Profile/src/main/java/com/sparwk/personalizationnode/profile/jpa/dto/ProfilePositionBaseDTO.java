package com.sparwk.personalizationnode.profile.jpa.dto;

import lombok.*;

import javax.persistence.Column;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePositionBaseDTO {
    private Long profilePositionSeq;
    private Long profileId;
    private Long companyProfileId;
    private String companyVerifyYn;
    private String artistYn;
    private String anrYn;
    private String creatorYn;
    private String deptRoleInfo;
    private String primaryYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;


}
