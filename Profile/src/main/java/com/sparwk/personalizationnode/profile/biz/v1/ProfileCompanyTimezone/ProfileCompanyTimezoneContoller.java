package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyTimezone;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyTimezone.dto.ProfileCompanyTimezoneRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/company")
@CrossOrigin("*")
@Api(tags = "Profile Server Profile company TimeZone API")
public class ProfileCompanyTimezoneContoller {

    @Autowired
    private ProfileCompanyTimezoneService profileCompanyTimezoneService;

    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyTimezoneContoller.class);
    @ApiOperation(
            value = "프로필 WEB URL 입력",
            notes = "web URL 입력"
    )
    @PostMapping(path = "/timezone")
    public Response ProfileTimezoneSaveController(@Valid @RequestBody ProfileCompanyTimezoneRequestList dto) {
        Response res = profileCompanyTimezoneService.ProfileCompanyTimezoneSaveResponseService(dto);
        return res;
    }
    @ApiOperation(
            value = "프로필 언어 정보 검색",
            notes = "언어 정보 검색"
    )
    @GetMapping(path = "/timezone/{profileId}")
    public Response ProfileTimezoneSelectController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileCompanyTimezoneService.ProfileCompanyTimezoneSelectResponseService(profileId);
        return res;
    }

}
