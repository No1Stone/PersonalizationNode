package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompany;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompanyTimezone;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileTimezone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileCompanyTimezoneRepository extends JpaRepository<ProfileCompanyTimezone, Long> {

    List<ProfileCompanyTimezone> findByProfileId(Long profileId);
}
