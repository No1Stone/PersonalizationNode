package com.sparwk.personalizationnode.profile.jpa.entity;

import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileGroupOntehwebId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_group_ontheweb")
@DynamicUpdate
@IdClass(ProfileGroupOntehwebId.class)
public class ProfileGroupOntehweb {
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Id
    @Column(name = "sns_type_cd", nullable = true)
    private String snsTypeCd;
    @Column(name = "sns_url", nullable = true)
    private String snsUrl;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;


    @Builder
    ProfileGroupOntehweb(
            Long profileId,
            String snsTypeCd,
            String snsUrl,
            String useYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt

    ) {
        this.profileId = profileId;
        this.snsTypeCd = snsTypeCd;
        this.snsUrl = snsUrl;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }
}
