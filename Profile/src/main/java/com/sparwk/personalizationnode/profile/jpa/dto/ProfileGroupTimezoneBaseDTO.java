package com.sparwk.personalizationnode.profile.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGroupTimezoneBaseDTO {

    private Long profileId;
    private Long profileTimezoneBoxNum;
    private Long timezoneSeq;
    private String setTimeAutoYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

}
