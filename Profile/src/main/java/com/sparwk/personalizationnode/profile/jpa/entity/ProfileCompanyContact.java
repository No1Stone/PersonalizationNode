package com.sparwk.personalizationnode.profile.jpa.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_company_contact")
@DynamicUpdate
public class ProfileCompanyContact {

    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "profile_contact_img_url", nullable = true)
    private String profileContactImgUrl;
    @Column(name = "profile_contact_description", nullable = true)
    private String profileContactDescription;
    @Column(name = "contct_first_name", nullable = true)
    private String contctFirstName;
    @Column(name = "contct_midle_name", nullable = true)
    private String contctMidleName;
    @Column(name = "contct_last_name", nullable = true)
    private String contctLastName;
    @Column(name = "contct_email", nullable = true)
    private String contctEmail;
    @Column(name = "contct_phone_number", nullable = true)
    private String contctPhoneNumber;
    @Column(name = "verify_phone_yn", nullable = true)
    private String verifyPhoneYn;
    @Column(name = "country_cd", nullable = true, length = 9)
    private String countryCd;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProfileCompanyContact(
            Long profileId,
            String profileContactImgUrl,
            String profileContactDescription,
            String contctFirstName,
            String contctMidleName,
            String contctLastName,
            String contctEmail,
            String contctPhoneNumber,
            String verifyPhoneYn,
            String countryCd,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.profileId = profileId;
        this.profileContactImgUrl = profileContactImgUrl;
        this.profileContactDescription = profileContactDescription;
        this.contctFirstName = contctFirstName;
        this.contctMidleName = contctMidleName;
        this.contctLastName = contctLastName;
        this.contctEmail = contctEmail;
        this.contctPhoneNumber = contctPhoneNumber;
        this.verifyPhoneYn = verifyPhoneYn;
        this.countryCd = countryCd;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

}
