package com.sparwk.personalizationnode.profile.biz.v1.profile;

import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.EmptyUpdate.*;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.ProfileDto;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.ProfileUpdateRequest;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.ProjectMemberRequest;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.profilecreate.RequestProfileCreate;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/profile")
@CrossOrigin("*")
@Api(tags = "Profile Server Profile API")
public class ProfileContoller {

    @Autowired
    private ProfileService profileService;

    private final Logger logger = LoggerFactory.getLogger(ProfileContoller.class);

    @ApiOperation(
            value = "프로필 정보입력",
            notes = "회원가입 페이지의 프로필 생성 정보입력"
    )
    @Operation(description = "Login Request", parameters = {
            @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "프로필 생성 컨트롤러")
    })
    @PostMapping(path = "/info")
    public Response ProfileSaveController(@Valid @RequestBody ProfileDto dto) {
        Response res = profileService.ProfileSaveResponse(dto);
        return res;
    }

    @PostMapping(path = "/update")
    public Response ProfileUpdateController(@Valid @RequestBody ProfileUpdateRequest dto
   ) {
        Response res = profileService.ProfileUpdateResponse(dto);
        return res;
    }

    @PostMapping(path = "/update/cae")
    public Response ProfileUpdateCaeController(@Valid @RequestBody ProfileCaeUpdateDto dto
    ) {
        Response res = profileService.ProfileUpdateCaeService(dto);
        return res;
    }

    @PostMapping(path = "/update/ipi")
    public Response ProfileUpdateIpiController(@Valid @RequestBody ProfileIpiUpdateDto dto
    ) {
        Response res = profileService.ProfileUpdateIpiService(dto);
        return res;
    }

    @PostMapping(path = "/update/ipn")
    public Response ProfileUpdateIpnController(@Valid @RequestBody ProfileIpnUpdateDto dto
    ) {
        Response res = profileService.ProfileUpdateIpnService(dto);
        return res;
    }

    @PostMapping(path = "/update/isni")
    public Response ProfileUpdateIsniController(@Valid @RequestBody ProfileIsniUpdateDto dto
    ) {
        Response res = profileService.ProfileUpdateIsniService(dto);
        return res;
    }

    @PostMapping(path = "/update/nro")
    public Response ProfileUpdateNroController(@Valid @RequestBody ProfileNroUpdateDto dto
    ) {
        Response res = profileService.ProfileUpdateNroService(dto);
        return res;
    }



    @ApiOperation(
            value = "프로필 정보검색",
            notes = "프로필 정보검색"
    )
    @GetMapping(path = "/infoAccountId")
    public Response ProfileAccountIdSelectList(HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = profileService.ProfileAccountSelectList(userInfoDTO.getAccountId());
        return res;
    }

    @GetMapping(path = "/infoProfileId/{profileId}")
    public Response ProfileIdSelect(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileService.ProfileProfileInfoSelectService(profileId);
        return res;
    }

    @PostMapping(path = "/integrate/create")
    public Response ProfileIntegrateCreate(
            @Valid @RequestBody RequestProfileCreate dto,
            HttpServletRequest req) {
        logger.info("create List log - {}",dto);
        Response res = profileService.ProfileIntegrateCreateResponse(dto);
        return res;
    }
    @ApiOperation(
            value = "private project 오너가 초대",
            notes = "오너가 사용자 초대하는 메일"
    )
    //오너가 초대
    @GetMapping(path = "/RSVP/private/invite/{projectId}/{rsvpProfileId}")
    public Response RSVPprivateInviteController(@PathVariable(name = "projectId")Long projectId,
                                       @PathVariable(name = "rsvpProfileId") Long rsvpProfileId){
    Response res = profileService.RSVPprivateInviteService(projectId,rsvpProfileId);
    return res;
    }

    @ApiOperation(
            value = "private project 오너가 승인",
            notes = "오너가 승인후 사용자에게 프로젝트 비밀번호 알려줌"
    )
    //오너가 승인해서 비밀번호 알려줌
    @GetMapping(path = "/RSVP/private/apply/{projectId}/{rsvpProfileId}")
    public Response RSVPprivateApplyController(@PathVariable(name = "projectId")Long projectId,
                                       @PathVariable(name = "rsvpProfileId") Long rsvpProfileId){
        Response res = profileService.RSVPprivateApplyService(projectId,rsvpProfileId);
        return res;
    }

    @ApiOperation(
            value = "public project 오너가 초대",
            notes = "오너가 사용자 초대하는 메일"
    )
    @GetMapping(path = "/RSVP/public/invite/{projectId}/{rsvpProfileId}")
    public Response RSVPpublicInviteController(@PathVariable(name = "projectId")Long projectId,
                                         @PathVariable(name = "rsvpProfileId") Long rsvpProfileId){
        Response res = profileService.RSVPpublicInviteService(projectId,rsvpProfileId);
        return res;
    }

    @ApiOperation(
            value = "public project 오너가 승인",
            notes = "오너가 승인"
    )
    //오너가 승인해서 비밀번호 알려줌
    @GetMapping(path = "/RSVP/public/apply/{projectId}/{rsvpProfileId}")
    public Response RSVPpublicApplyController(@PathVariable(name = "projectId")Long projectId,
                                        @PathVariable(name = "rsvpProfileId") Long rsvpProfileId){
        Response res = profileService.RSVPpublicApplyService(projectId,rsvpProfileId);
        return res;
    }


}
