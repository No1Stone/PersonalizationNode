package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompanyStudio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProfileCompanyStudioRepository extends JpaRepository<ProfileCompanyStudio, Long> {

    List<ProfileCompanyStudio> findByProfileId(Long ProfileId);

    List<ProfileCompanyStudio> findByProfileIdOrderByModDtDesc(Long ProfileId);

    ProfileCompanyStudio findByStudioId(Long studio);

    @Transactional
    void deleteByProfileIdAndStudioId(Long profileId, Long studio);
}
