package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.profilecreate;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePositionRequest {



    @Schema(description = "회사 프로필아이디")
    private Long companyProfileId;
    @Schema(description = "A&R 체크 Y & N")
    private String anrYn;
    @Schema(description = "artist 체크 Y & N")
    private String artistYn;
    @Schema(description = "artist 체크 Y & N")
    private String creatorYn;
    @Schema(description = "부서및 권한")
    private String deptRoleInfo;
    @Schema(description = "주 소속 회사 여부")
    private String primaryYn;



}
