package com.sparwk.personalizationnode.profile.biz.v1.ProfileOntheweb.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileOnthewebRequest {
    @Schema(description = "프로필 아이디")
    private Long profileId;
    private String snsTypeCd;
    private String snsUrl;
    private String useYn;
}
