package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupTimezone.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGroupTimezoneRequest {

    private Long profileId;
    private Long profileTimezoneBoxNum;
    private Long timezoneSeq;
    private String setTimeAutoYn;


}
