package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminRoleCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminRoleCodeRepository extends JpaRepository<AdminRoleCode, Long> {

    Optional<AdminRoleCode> findByCode(String cd);
    boolean existsByCode(String cd);

}
