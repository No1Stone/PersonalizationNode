package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyOntheweb.dto;


import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyOnthewebRequest {
    private Long profileId;
    private String snsTypeCd;
    private String snsUrl;
    private String useYn;
}
