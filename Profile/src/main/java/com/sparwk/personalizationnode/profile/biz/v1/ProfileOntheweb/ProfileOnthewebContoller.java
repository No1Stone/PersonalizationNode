package com.sparwk.personalizationnode.profile.biz.v1.ProfileOntheweb;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileLanguage.ProfileLanguageService;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileLanguage.dto.ProfileLanguageDto;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileOntheweb.dto.ProfileOnthewebRqeustList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/ontheweb")
@CrossOrigin("*")
@Api(tags = "Profile Server OntheWeb API")
public class ProfileOnthewebContoller {

    @Autowired
    private ProfileOnthewebService profileOnthewebService;

    private final Logger logger = LoggerFactory.getLogger(ProfileOnthewebContoller.class);
    @ApiOperation(
            value = "프로필 WEB URL 입력",
            notes = "web URL 입력"
    )
    @PostMapping(path = "/info")
    public Response ProfileOnthewebSaveController(@Valid @RequestBody ProfileOnthewebRqeustList dto) {
        logger.info("ontehweb insert-----{}",dto);
        Response res = profileOnthewebService.ProfileOnthewebSaveResponseService(dto);
        return res;
    }
    @ApiOperation(
            value = "프로필 언어 정보 검색",
            notes = "언어 정보 검색"
    )
    @GetMapping(path = "/info/{profileId}")
    public Response ProfileOnthewebSelectController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileOnthewebService.ProfileOnthewebSelectResponseService(profileId);
        return res;
    }

}
