package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EmailPhoneDto {



    @Schema(description = "사용자 이메일")
    @Size(max = 100, message = "Account email NotNull")
    private String accntEmail;
    @Size(max = 20, message = "")
    @Schema(description = "사용자 핸드폰 번호")
    private String phoneNumber;

}
