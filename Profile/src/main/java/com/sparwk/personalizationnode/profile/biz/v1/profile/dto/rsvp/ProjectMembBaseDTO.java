package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.rsvp;

import lombok.*;

import java.time.LocalDateTime;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProjectMembBaseDTO {
    private long projId;
    private long profileId;
    private LocalDateTime patpSdt;
    private LocalDateTime patpEdt;
    private String activeYn;
    private String confirmYn;
    private String applyStat;
    private String invtStat;
    private LocalDateTime invtDt;
    private String banYn;
    private LocalDateTime banDt;
    private String quitYn;
    private LocalDateTime quitDt;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
