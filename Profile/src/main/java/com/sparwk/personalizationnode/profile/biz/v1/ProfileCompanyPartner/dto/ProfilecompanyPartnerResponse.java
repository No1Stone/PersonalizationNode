package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto.ProfileCompanyResponse;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilecompanyPartnerResponse {
    private Long profileId;
    private Long partnerId;
    private String relationType;
    private String relationStatus;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
    private ProfileCompanyResponse profileCompanyResponse;


}
