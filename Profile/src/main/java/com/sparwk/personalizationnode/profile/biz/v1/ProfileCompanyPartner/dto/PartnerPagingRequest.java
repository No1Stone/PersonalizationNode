package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PartnerPagingRequest {

    private Long profileId;
    private int size;
    private int page;
    private Object detailContents;

}
