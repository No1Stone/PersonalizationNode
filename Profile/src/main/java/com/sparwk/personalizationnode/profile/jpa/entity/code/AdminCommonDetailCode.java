package com.sparwk.personalizationnode.profile.jpa.entity.code;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_common_detail_code")
public class AdminCommonDetailCode {

    @Id
    @Column(name = "common_detail_code_seq", nullable = true)
    private Long commonDetailCodeSeq;
    @Column(name = "pcode", nullable = true)
    private String pcode;
    @Column(name = "dcode", nullable = true)
    private String dcode;
    @Column(name = "val", nullable = true)
    private String val;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "description", nullable = true)
    private String description;
    @Column(name = "sort_index", nullable = true)
    private int sortIndex;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "popular_yn", nullable = true)
    private String popularYn;
    @Column(name = "hit", nullable = true)
    private int hit;

    @Builder
    AdminCommonDetailCode(Long commonDetailCodeSeq,
                          String pcode,
                          String dcode,
                          String val,
                          String useYn,
                          String description,
                          int sortIndex,
                          Long regUsr,
                          LocalDateTime regDt,
                          Long modUsr,
                          LocalDateTime modDt,
                          String popularYn,
                          int hit
    ) {
        this.commonDetailCodeSeq = commonDetailCodeSeq;
        this.pcode = pcode;
        this.dcode = dcode;
        this.val = val;
        this.useYn = useYn;
        this.description = description;
        this.sortIndex = sortIndex;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
        this.popularYn = popularYn;
        this.hit = hit;
    }

}
