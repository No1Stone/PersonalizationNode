package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminSongDetailCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminSongDetailCodeRepository extends JpaRepository<AdminSongDetailCode, Long> {

    Optional<AdminSongDetailCode> findByDcode(String cd);
    boolean existsByDcode(String cd);

}
