package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileGroupContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface ProfileGroupContactRepository extends JpaRepository<ProfileGroupContact, Long> {

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_group_contact set profile_contact_img_url = :img where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileGroupContactImg(@Param("profileId")Long profileId, @Param("img") String img);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_group_contact set profile_contact_description = :descrip where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileGroupContactDescrip(@Param("profileId")Long profileId, @Param("descrip") String descrip);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_group_contact set contct_email = :email where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileGroupContactEmail(@Param("profileId")Long profileId, @Param("email") String email);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_group_contact set " +
                    "contct_first_name = :first, " +
                    "contct_midle_name= :midle, " +
                    "contct_last_name= :last " +
                    "where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileGroupContactName(
            @Param("profileId") Long profileId,
            @Param("first") String first,
            @Param("midle") String midle,
            @Param("last") String last);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_group_contact set " +
                    "contct_phone_number = :number, " +
                    "verify_phone_yn= :yn, " +
                    "country_cd= :cd " +
                    "where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileGroupContactPhone(
            @Param("profileId") Long profileId,
            @Param("number") String number,
            @Param("yn") String yn,
            @Param("cd") String cd);



}
