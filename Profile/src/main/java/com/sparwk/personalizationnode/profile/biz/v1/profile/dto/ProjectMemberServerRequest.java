package com.sparwk.personalizationnode.profile.biz.v1.profile.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectMemberServerRequest {

    @Schema(description = "계정 아이디")
    private Long accntId;
    @Schema(description = "프로필 아이디")
    private Long profileId;
    @Schema(description = "프로필 활동명 풀네임")
    private String fullName;
    @Schema(description = "스테이지 이름 사용여부")
    private String stageNameYn;
    @Schema(description = "생년")
    private String bthYear;
    @Schema(description = "월")
    private String bthMonth;
    @Schema(description = "일")
    private String bthDay;
    @Schema(description = "현재위치 국가")
    private String currentCityCountryCd;
    @Schema(description = "현재위치 도시")
    private String currentCityNm;
    @Schema(description = "고향국가코드")
    private String homeTownCountryCd;
    @Schema(description = "고향 도시 이름")
    private String homeTownNm;
    @Schema(description = "헤드라인")
    private String headline;


    @Schema
    private AccountServerRequest accountServerRequest;
    @Schema
    private AccountCompanyDetailServerRequest accountCompanyDetailServerRequest;
    @Schema
    private List<SongServerRequest> songServerRequest;


}
