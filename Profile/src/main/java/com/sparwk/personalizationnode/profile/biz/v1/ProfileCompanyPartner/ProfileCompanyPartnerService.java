package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto.ProfileCompanyResponse;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto.*;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyPartnerBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyStudioBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ViewPartnerStudioBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompany;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompanyPartner;
import com.sparwk.personalizationnode.profile.jpa.entity.account.Account;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyPartnerRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyStudioRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ViewPartnerStudioRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.account.AccountRepository;
import lombok.RequiredArgsConstructor;
import net.bytebuddy.TypeCache;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileCompanyPartnerService {
    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyPartnerService.class);
    @Autowired
    private ProfileCompanyPartnerRepository profileCompanyPartnerRepository;
    private final MessageSourceAccessor messageSourceAccessor;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private HttpServletRequest httpServletRequest;
    @Autowired
    private ProfileCompanyRepository profileCompanyRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private ViewPartnerStudioRepository viewPartnerStudioRepository;

    public Response ProfileCompanyPartnerSaveService(ProfileCompanyPartnerRequest dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        ProfileCompanyPartnerBaseDTO baseDTO = modelMapper.map(dto, ProfileCompanyPartnerBaseDTO.class);
        baseDTO.setModDt(LocalDateTime.now());
        baseDTO.setRegDt(LocalDateTime.now());
        baseDTO.setRegUsr(userInfoDTO.getAccountId());
        baseDTO.setModUsr(userInfoDTO.getAccountId());
        ProfileCompanyPartnerBaseDTO profileResult = profileServiceRepository.ProfileCompanyPartnerSaveService(baseDTO);
        logger.info("----상태-------{}", profileResult.getRelationStatus());
        String retCode = null;
//        if(dto.getRelationType().equals("RET000001")){
        retCode = "RET000001";
//        }
        if (dto.getRelationType().equals("RET000002")) {
            retCode = "RET000003";
        }
        if (dto.getRelationType().equals("RET000003")) {
            retCode = "RET000002";
        }
        logger.info("상대방에게인써트전");
        if (profileResult.getRelationStatus().equals("RES000001")) {
            logger.info("상대방에게인써트후");
            ProfileCompanyPartnerBaseDTO afteBbaseDTO = ProfileCompanyPartnerBaseDTO.builder()
                    .profileId(dto.getPartnerId())
                    .partnerId(dto.getProfileId())
                    .relationStatus("RES000002")
                    .relationType(retCode)
                    .build();
            afteBbaseDTO.setModDt(LocalDateTime.now());
            afteBbaseDTO.setRegDt(LocalDateTime.now());
            afteBbaseDTO.setRegUsr(userInfoDTO.getAccountId());
            afteBbaseDTO.setModUsr(userInfoDTO.getAccountId());
            ProfileCompanyPartnerBaseDTO partnerResult = profileServiceRepository.ProfileCompanyPartnerSaveService(afteBbaseDTO);
        }

        if (dto.getRelationStatus().equals("RES000003")) {
            ProfileCompanyPartnerBaseDTO afteBbaseDTO = ProfileCompanyPartnerBaseDTO.builder()
                    .profileId(dto.getPartnerId())
                    .partnerId(dto.getProfileId())
                    .relationStatus("RES000003")
                    .relationType(retCode)
                    .build();
            afteBbaseDTO.setModDt(LocalDateTime.now());
            afteBbaseDTO.setRegDt(LocalDateTime.now());
            afteBbaseDTO.setRegUsr(userInfoDTO.getAccountId());
            afteBbaseDTO.setModUsr(userInfoDTO.getAccountId());
            ProfileCompanyPartnerBaseDTO partnerResult = profileServiceRepository.ProfileCompanyPartnerSaveService(afteBbaseDTO);
        }

        if (dto.getRelationStatus().equals("RES000004")) {
            ProfileCompanyPartnerBaseDTO afteBbaseDTO = ProfileCompanyPartnerBaseDTO.builder()
                    .profileId(dto.getPartnerId())
                    .partnerId(dto.getProfileId())
                    .relationStatus("RES000004")
                    .relationType(retCode)
                    .build();
            afteBbaseDTO.setModDt(LocalDateTime.now());
            afteBbaseDTO.setRegDt(LocalDateTime.now());
            afteBbaseDTO.setRegUsr(userInfoDTO.getAccountId());
            afteBbaseDTO.setModUsr(userInfoDTO.getAccountId());
            ProfileCompanyPartnerBaseDTO partnerResult = profileServiceRepository.ProfileCompanyPartnerSaveService(afteBbaseDTO);
        }

        if (dto.getRelationStatus().equals("RES000005")) {
            ProfileCompanyPartnerBaseDTO afteBbaseDTO = ProfileCompanyPartnerBaseDTO.builder()
                    .profileId(dto.getPartnerId())
                    .partnerId(dto.getProfileId())
                    .relationStatus("RES000005")
                    .relationType(retCode)
                    .build();
            afteBbaseDTO.setModDt(LocalDateTime.now());
            afteBbaseDTO.setRegDt(LocalDateTime.now());
            afteBbaseDTO.setRegUsr(userInfoDTO.getAccountId());
            afteBbaseDTO.setModUsr(userInfoDTO.getAccountId());
            ProfileCompanyPartnerBaseDTO partnerResult = profileServiceRepository.ProfileCompanyPartnerSaveService(afteBbaseDTO);
        }


        if (dto.getRelationStatus().equals("RES000006")) {
            ProfileCompanyPartnerBaseDTO afteBbaseDTO = ProfileCompanyPartnerBaseDTO.builder()
                    .profileId(dto.getPartnerId())
                    .partnerId(dto.getProfileId())
                    .relationStatus("RES000006")
                    .relationType(retCode)
                    .build();
            afteBbaseDTO.setModDt(LocalDateTime.now());
            afteBbaseDTO.setRegDt(LocalDateTime.now());
            afteBbaseDTO.setRegUsr(userInfoDTO.getAccountId());
            afteBbaseDTO.setModUsr(userInfoDTO.getAccountId());
            ProfileCompanyPartnerBaseDTO partnerResult = profileServiceRepository.ProfileCompanyPartnerSaveService(afteBbaseDTO);
        }

        Response res = new Response();
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilecompanyPartnerSelectService(Long profileId) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        List<ProfileCompanyPartnerBaseDTO> result = profileCompanyPartnerRepository
                .findByProfileId(profileId).stream().map(
                        e -> modelMapper.map(e, ProfileCompanyPartnerBaseDTO.class)).collect(Collectors.toList());
        List<ProfilecompanyPartnerResponse> pcprListResult = new ArrayList<>();
        for (ProfileCompanyPartnerBaseDTO e : result) {
            ProfilecompanyPartnerResponse pcprAdd = modelMapper.map(e, ProfilecompanyPartnerResponse.class);
            Account userAccountSelect = accountRepository.findById(profileCompanyRepository.findById(e.getPartnerId()).get().getAccntId()).get();
            ProfileCompanyResponse companyResponse = new ProfileCompanyResponse();
            for (ProfileCompany f : userAccountSelect.getProfileCompany()) {
                companyResponse = modelMapper.map(f, ProfileCompanyResponse.class);
                companyResponse.setCountryCd(userAccountSelect.getCountryCd());
                companyResponse.setCompanyEmail(userAccountSelect.getAccntEmail());
                companyResponse.setContactEmail(f.getComInfoEmail());
                companyResponse.setPhoneCountryCd(userAccountSelect.getCountryCd());
                companyResponse.setProfileId(f.getProfileId());
                companyResponse.setProfileCompanyOntheweb(f.getProfileCompanyOntheweb());
                companyResponse.setAccountCompanyType(userAccountSelect.getAccountCompanyType());
                companyResponse.setAccountCompanyDetail(userAccountSelect.getAccountCompanyDetail());
                companyResponse.setAccountCompanyLocation(userAccountSelect.getAccountCompanyLocation());
            }

            pcprAdd.setProfileCompanyResponse(companyResponse);
            pcprListResult.add(pcprAdd);
            ProfileCompanyBaseDTO pcb = profileCompanyRepository
                    .findById(e.getPartnerId()).map(f -> modelMapper.map(f, ProfileCompanyBaseDTO.class)).get();
        }

        res.setResult(pcprListResult);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileCompanyPartnerListSaveService(ProfileCompanyPartnerRequestList dto) {
        Response res = new Response();
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        for (ProfileCompanyPartnerRequest e : dto.getProfileCompanyPartnerRequestList()) {
            ProfileCompanyPartnerBaseDTO baseDTO = modelMapper.map(e, ProfileCompanyPartnerBaseDTO.class);
            baseDTO.setModDt(LocalDateTime.now());
            baseDTO.setRegDt(LocalDateTime.now());
            baseDTO.setRegUsr(userInfoDTO.getAccountId());
            baseDTO.setModUsr(userInfoDTO.getAccountId());
            profileServiceRepository.ProfileCompanyPartnerSaveService(baseDTO);
        }
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response ProfilecompanyPartnerTagetSelectService(PartnerPagingRequest dto) {
        Response res = new Response();
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");

        List<Long> removeProfile = profileCompanyPartnerRepository.findByProfileIdAndRelationStatus(dto.getProfileId(), "RES000001").stream().map(e ->
                modelMapper.map(e.getPartnerId(), Long.class)).collect(Collectors.toList());
        removeProfile.add(dto.getProfileId());
        List<Long> returnProfile = profileCompanyRepository.findAll()
                .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class)).collect(Collectors.toList());

        logger.info("removeProfile-------{}", removeProfile);
        logger.info("removeProfile-------{}", removeProfile.size());
        logger.info("returnProfile-------{}", returnProfile);
        logger.info("returnProfile-------{}", returnProfile.size());
        returnProfile.removeAll(removeProfile);
        logger.info("remove       -------{}", returnProfile);
        logger.info("remove       -------{}", returnProfile.size());
        List<ProfilecompanyPartnerResponse> pcprListResult = new ArrayList<>();
        //시작페이지가 0 크기 20 =  0 ~19
        //시작페이지 1 크기 20 = 20 ~ 39
//            size*page+page ;
        int size = dto.getSize();
        int page = dto.getPage();
        List<Long> contentsList = new ArrayList<>();

        int start = size * page;
        int end = size * page + size;
        if (returnProfile.size() < end) {
            end = returnProfile.size();
        }

        logger.info("{}", size * page);
        logger.info("{}", size * page + size);
        List<ProfileCompanyResponse> companyResponseResult = new ArrayList<>();
        for (int i = size * page; i < end; i++) {
            logger.info("프로필넘버확인       -------{}", returnProfile.get(i));
            logger.info("i       -------{}", i);
            logger.info("i       -------{}", returnProfile.get(i));
            contentsList.add(returnProfile.get(i));
            Account userAccountSelect = accountRepository.findById(profileCompanyRepository
                    .findById(returnProfile.get(i)).get().getAccntId()).get();
            ProfileCompanyResponse companyResponse = new ProfileCompanyResponse();
            for (ProfileCompany f : userAccountSelect.getProfileCompany()) {
                logger.info("에러 여기서 나니?");
                companyResponse = modelMapper.map(f, ProfileCompanyResponse.class);
                if (userAccountSelect.getCountryCd() == null) {
                } else {
                    companyResponse.setCountryCd(userAccountSelect.getCountryCd());
                }
                if (userAccountSelect.getAccntEmail() == null) {
                } else {
                    companyResponse.setCompanyEmail(userAccountSelect.getAccntEmail());
                }
                if (f.getComInfoEmail() == null) {
                } else {
                    companyResponse.setContactEmail(f.getComInfoEmail());
                }
                if (userAccountSelect.getCountryCd() == null) {
                } else {
                    companyResponse.setPhoneCountryCd(userAccountSelect.getCountryCd());
                }
                if (f.getProfileId() == null) {
                } else {
                    companyResponse.setProfileId(f.getProfileId());
                }
                if (f.getProfileCompanyOntheweb() == null) {
                } else {
                    companyResponse.setProfileCompanyOntheweb(f.getProfileCompanyOntheweb());
                }
                if (userAccountSelect.getAccountCompanyType() == null) {
                } else {
                    companyResponse.setAccountCompanyType(userAccountSelect.getAccountCompanyType());
                }
                if (userAccountSelect.getAccountCompanyDetail() == null) {
                } else {
                    companyResponse.setAccountCompanyDetail(userAccountSelect.getAccountCompanyDetail());
                }
                if (userAccountSelect.getAccountCompanyLocation() == null) {
                } else {
                    companyResponse.setAccountCompanyLocation(userAccountSelect.getAccountCompanyLocation());
                }
                companyResponseResult.add(companyResponse);
            }

        }
//        Object pagingContens = pcprListResult;
        Object pagingContens = companyResponseResult;
//        Object pagingContens = contentsList;
        Paging pp = Paging.builder()
                .size(size)
                .page(page)
                .totalSize(returnProfile.size())
                .totalPage(returnProfile.size() / size)
                .content(pagingContens).build();

        res.setResult(pp);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    @Autowired
    private ProfileCompanyStudioRepository profileCompanyStudioRepository;

    public Response ProfileCompanyPartnerStudioSelectService(Long profileId) {
        Response res = new Response();
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        Paging paging = new Paging();

        List<Long> pcpBaseDto = profileCompanyPartnerRepository.findByProfileIdOrderByModDtDesc(profileId)
                .stream().map(e -> modelMapper.map(e.getPartnerId(), Long.class)).collect(Collectors.toList());
        List<Long> pcsBaseDto = profileCompanyStudioRepository.findByProfileIdOrderByModDtDesc(profileId)
                .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class)).collect(Collectors.toList());
        List<Object> obj = new ArrayList<>();

        //페이징 처리를위해 1차적으로 값을 세팅함 / 콘텐츠는 나중에 데이터 량이 많아지면 오래걸릴수도있으니. 적은값먼저 세팅.
        List<PartnerStudioResponse> returnTotal = new ArrayList<>();
        for (Long e : pcpBaseDto) {
            returnTotal.add(
                    PartnerStudioResponse
                            .builder()
                            .returnType("partner")
                            .sortValue(e)
                            .build());
        }
        for (Long e : pcsBaseDto) {
            returnTotal.add(
                    PartnerStudioResponse
                            .builder()
                            .returnType("studio")
                            .sortValue(e)
                            .build());
        }

        //페이징 처리를 하면서 값을 채움
        int size = 0;
        int page = 5;
        logger.info("{}", size * page);
        logger.info("{}", size * page + size);
        int end = size * page + size;
        if (returnTotal.size() < end) {
            end = returnTotal.size();
        }
        for (int i = size * page; i < end; i++) {
            List<ProfileCompanyResponse> companyResponseResult = new ArrayList<>();
            if (returnTotal.get(i).getReturnType().equals("partner")) {
                returnTotal.set(i, PartnerStudioResponse
                        .builder()
                        .returnType("studio")
                        .sortValue(returnTotal.get(i).getSortValue())
//                        .contents()
                        .build());


            }
            if (returnTotal.get(i).getReturnType().equals("studio")) {
                returnTotal.set(i, PartnerStudioResponse
                        .builder()
                        .returnType("studio")
                        .sortValue(returnTotal.get(i).getSortValue())
                        .contents(profileCompanyStudioRepository.findByProfileId(returnTotal.get(i).getSortValue())
                                .stream().map(e -> modelMapper.map(e, ProfileCompanyStudioBaseDTO.class)))
                        .build());
            }
        }
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response test11(Long profileId) {
        Response res = new Response();
        res.setResult(viewPartnerStudioRepository
                .findAllByProfileIdIn(profileCompanyPartnerRepository
                        .findByProfileId(profileId).stream().map(e ->
                                modelMapper.map(e.getPartnerId(), Long.class)).collect(Collectors.toList())));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response test12(Long profileId) {
        Response res = new Response();
        List<Long> aa = profileCompanyPartnerRepository
                .findByProfileId(profileId).stream().map(e ->
                        modelMapper.map(e.getPartnerId(), Long.class)).collect(Collectors.toList());
        List<ViewPartnerStudioBaseDTO> result = new ArrayList<>();
        for (Long e : aa) {
            result.add(viewPartnerStudioRepository.findById(e).map(f -> modelMapper.map(f, ViewPartnerStudioBaseDTO.class)).get());
        }

        res.setResult(viewPartnerStudioRepository.findByProfileId(profileId));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response PartnerStudioSelectService(PartnerPagingRequest dto) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());
        Slice<ViewPartnerStudioBaseDTO> pageResult = viewPartnerStudioRepository
                .findByProfileIdOrderByModDtDesc(dto.getProfileId(), pr).map(e ->
                        modelMapper.map(e, ViewPartnerStudioBaseDTO.class));
        res.setResult(pageResult);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response PartnerStudioSelfPagingSelectService(PartnerPagingRequest dto) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());
        Slice<ViewPartnerStudioBaseDTO> pageResult = viewPartnerStudioRepository
                .findByProfileIdOrderByModDtDesc(dto.getProfileId(), pr).map(e ->
                        modelMapper.map(e, ViewPartnerStudioBaseDTO.class));
        Paging paging = new Paging();
        logger.info("pageResult ------- {}",pageResult.getContent());
        //현재 뷰테이블을 조회할수 있음.
        //이뷰테이블을 페이징처리해서 쏘트해줘야함.
        //페이징처리까지 된거를 가공해서 준다?
        List<Object> conten = new ArrayList<>();
        for (ViewPartnerStudioBaseDTO e : pageResult.getContent()) {
            if (e.getType().equals("studio")) {
                conten.add(e);
            }
            if (e.getType().equals("partner")) {
                Account userAccountSelect = accountRepository.findById(profileCompanyRepository
                        .findById(e.getMappingProfileId()).get().getAccntId()).get();
                ProfileCompanyResponse companyResponse = new ProfileCompanyResponse();
                logger.info("companyResponse === {}", companyResponse);
                for (ProfileCompany f : userAccountSelect.getProfileCompany()) {
                    logger.info("에러 여기서 나니?");
                    companyResponse = modelMapper.map(f, ProfileCompanyResponse.class);
                    if (userAccountSelect.getCountryCd() == null) {
                    } else {
                        companyResponse.setCountryCd(userAccountSelect.getCountryCd());
                    }
                    if (userAccountSelect.getAccntEmail() == null) {
                    } else {
                        companyResponse.setCompanyEmail(userAccountSelect.getAccntEmail());
                    }
                    if (f.getComInfoEmail() == null) {
                    } else {
                        companyResponse.setContactEmail(f.getComInfoEmail());
                    }
                    if (userAccountSelect.getCountryCd() == null) {
                    } else {
                        companyResponse.setPhoneCountryCd(userAccountSelect.getCountryCd());
                    }
                    if (f.getProfileId() == null) {
                    } else {
                        companyResponse.setProfileId(f.getProfileId());
                    }
                    if (f.getProfileCompanyOntheweb() == null) {
                    } else {
                        companyResponse.setProfileCompanyOntheweb(f.getProfileCompanyOntheweb());
                    }
                    if (userAccountSelect.getAccountCompanyType() == null) {
                    } else {
                        companyResponse.setAccountCompanyType(userAccountSelect.getAccountCompanyType());
                    }
                    if (userAccountSelect.getAccountCompanyDetail() == null) {
                    } else {
                        companyResponse.setAccountCompanyDetail(userAccountSelect.getAccountCompanyDetail());
                    }
                    if (userAccountSelect.getAccountCompanyLocation() == null) {
                    } else {
                        companyResponse.setAccountCompanyLocation(userAccountSelect.getAccountCompanyLocation());
                    }

                    ProfileCompanyPartner typeget = profileCompanyPartnerRepository.findByProfileIdAndPartnerId(dto.getProfileId(), f.getProfileId());
                    conten.add(PartnerStudioPagingResponse
                            .builder()
                            .type("partner")
                            .relationType(typeget.getRelationType())
                            .relationStatus(typeget.getRelationStatus())
                            .profileCompanyResponse(companyResponse)
                            .build());
                }
            }
        }
        paging.setContent(conten);
        paging.setPage(pageResult.getNumber());
        paging.setSize(pageResult.getSize());
        paging.setTotalPage(pageResult.getNumber());
        paging.setTotalSize(pageResult.getNumberOfElements());
        res.setResult(paging);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response StreamTestService(Long profileId) {
        Response res = new Response();

        var test = profileCompanyPartnerRepository
                .findByProfileId(profileId).stream().filter(
                        e -> e.getRelationStatus().equals("RES000003")
                )
                ;
//
//        var test2 = test.forEach(e -> accountRepository.findById(
//                profileCompanyRepository.findById(
//                e.getPartnerId())).stream().map(f -> modelMapper.map(f, ProfileCompanyResponse.class))
//                .collect(Collectors.toList())
//        );



        ProfileCompanyResponse aa = modelMapper.map(profileCompanyRepository
                .findById(profileId), ProfileCompanyResponse.class);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}


