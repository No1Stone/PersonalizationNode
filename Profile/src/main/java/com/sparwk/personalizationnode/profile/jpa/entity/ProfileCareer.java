package com.sparwk.personalizationnode.profile.jpa.entity;


import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_career")
@DynamicUpdate
public class ProfileCareer {
    @Column(name = "career_organization_nm", length = 50)
    private String careerOrganizationNm;

    @Column(name = "profile_id")
    private Long profileId;

    @Column(name = "career_end_dt", nullable = true)
    private String careerEndDt;

    @Id
    @GeneratedValue(generator = "tb_profile_career_seq")
    @Column(name = "career_seq", nullable = true)
    private Long careerSeq;
    @Column(name = "career_start_dt", nullable = true)
    private String careerStartDt;
    @Column(name = "dept_role_nm", nullable = true, length = 100)
    private String deptRoleNm;
    @Column(name = "file_path")
    private String filePath;

    @Column(name = "location_city_country_cd", nullable = true, length = 9)
    private String locationCityCountryCd;
    @Column(name = "location_city_nm", nullable = true, length = 50)
    private String locationCityNm;
    @Column(name = "present_yn", nullable = true, length = 1)
    private String presentYn;
    @Column(name = "profile_career_seq", nullable = true)
    private Long profileCareerSeq;


    @Column(name = "reg_usr", updatable = false)
    //first registrant
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    ProfileCareer(
            Long profileId,
            String careerOrganizationNm,
            String deptRoleNm,
            String locationCityCountryCd,
            String locationCityNm,
            String presentYn,
            Long profileCareerSeq,
            Long careerSeq,
            String filePath,
            String careerStartDt,
            String careerEndDt,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.profileId = profileId;
        this.careerOrganizationNm = careerOrganizationNm;
        this.deptRoleNm = deptRoleNm;
        this.locationCityCountryCd = locationCityCountryCd;
        this.locationCityNm = locationCityNm;
        this.presentYn = presentYn;
        this.careerSeq = careerSeq;
        this.filePath = filePath;
        this.profileCareerSeq = profileCareerSeq;
        this.careerStartDt = careerStartDt;
        this.careerEndDt = careerEndDt;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;


    }

}
