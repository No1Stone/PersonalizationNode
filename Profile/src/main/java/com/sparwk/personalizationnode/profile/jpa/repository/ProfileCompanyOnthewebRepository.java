package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompany;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompanyOntheweb;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileOntheweb;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileCompanyOnthewebRepository extends JpaRepository<ProfileCompanyOntheweb, Long> {

    List<ProfileCompanyOntheweb> findByProfileId(Long profileId);

}
