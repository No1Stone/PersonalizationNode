package com.sparwk.personalizationnode.profile.jpa.entity.account.id;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
public class AccountCompanyTypeId implements Serializable {

    private Long accntId;
    private String companyCd;

}
