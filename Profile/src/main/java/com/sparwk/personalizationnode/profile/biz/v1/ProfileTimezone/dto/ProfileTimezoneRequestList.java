package com.sparwk.personalizationnode.profile.biz.v1.ProfileTimezone.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileTimezoneRequestList {
    List<ProfileTimezoneRequest> profileTimezoneRequestList;
}
