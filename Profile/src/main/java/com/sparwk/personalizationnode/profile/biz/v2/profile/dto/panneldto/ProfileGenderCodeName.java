package com.sparwk.personalizationnode.profile.biz.v2.profile.dto.panneldto;


import com.sparwk.personalizationnode.profile.jpa.dto.ProfileGenderBaseDTO;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGenderCodeName extends ProfileGenderBaseDTO {

    private String genderCdName;
}
