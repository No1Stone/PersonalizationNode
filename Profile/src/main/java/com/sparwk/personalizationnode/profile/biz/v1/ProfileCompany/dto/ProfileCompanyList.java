package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto;

import lombok.*;

@Getter@Setter
@NoArgsConstructor@AllArgsConstructor
@Builder
@ToString
public class ProfileCompanyList {

    private int page;
    private int size;
}
