package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCareerBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.QProfileCareer;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class ProfileCareerRepositoryDynamicImpl implements ProfileCareerRepositoryDynamic {

    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(ProfileCareerRepositoryDynamicImpl.class);
    private QProfileCareer qProfileCareer = QProfileCareer.profileCareer;

    private ProfileCareerRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long ProfileCareerRepositoryDynamicUpdate(ProfileCareerBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProfileCareer);

        if (entity.getDeptRoleNm() == null || entity.getDeptRoleNm().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCareer.deptRoleNm, entity.getDeptRoleNm());
        }
        if (entity.getLocationCityCountryCd() == null || entity.getLocationCityCountryCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCareer.locationCityCountryCd, entity.getLocationCityCountryCd());
        }
        if (entity.getLocationCityNm() == null || entity.getLocationCityNm().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCareer.locationCityNm, entity.getLocationCityNm());
        }
        if (entity.getPresentYn() == null || entity.getPresentYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCareer.presentYn, entity.getPresentYn());
        }
        if (entity.getCareerSeq() != null) {
            jpaUpdateClause.set(qProfileCareer.careerSeq, entity.getCareerSeq());
        }
//        문자열 검사로직변경
        if (entity.getCareerStartDt() == null || entity.getCareerStartDt().isEmpty()) {

        }
        else {
            jpaUpdateClause.set(qProfileCareer.careerStartDt, entity.getCareerStartDt());
        }
        if (entity.getCareerEndDt() == null|| entity.getCareerEndDt().isEmpty()) {

        }
        else {
            jpaUpdateClause.set(qProfileCareer.careerEndDt, entity.getCareerEndDt());
        }
        if (entity.getFilePath() == null || entity.getFilePath().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileCareer.filePath, entity.getFilePath());
        }
//        프로필시퀀스도추가
        if(entity.getProfileCareerSeq() != null) {
            jpaUpdateClause.set(qProfileCareer.profileCareerSeq, entity.getProfileCareerSeq());
        }
        if(entity.getRegUsr() != null) {jpaUpdateClause.set(qProfileCareer.regUsr, entity.getRegUsr());}
        if(entity.getRegDt() != null) {jpaUpdateClause.set(qProfileCareer.regDt, entity.getRegDt());}
        if(entity.getModUsr() != null) {jpaUpdateClause.set(qProfileCareer.modUsr, entity.getModUsr());}
        if(entity.getModDt() != null) {jpaUpdateClause.set(qProfileCareer.modDt, entity.getModDt());}



        jpaUpdateClause.where(qProfileCareer.profileId.eq(entity.getProfileId()));
        jpaUpdateClause.where(qProfileCareer.careerOrganizationNm.eq(entity.getCareerOrganizationNm()));


        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
