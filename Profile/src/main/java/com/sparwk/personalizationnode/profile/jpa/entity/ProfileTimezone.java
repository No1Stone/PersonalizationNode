package com.sparwk.personalizationnode.profile.jpa.entity;

import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileTimezoneId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_timezone")
@DynamicUpdate
@IdClass(ProfileTimezoneId.class)
public class ProfileTimezone {
    @Id
    @Column(name = "profile_timezone_box_num")
    private Long profileTimezoneBoxNum;
    @Column(name = "profile_id")
    @Id
    private Long profileId;
    @Column(name = "timezone_seq")
    private Long timezoneSeq;
    @Column(name = "set_time_auto_yn", nullable = true)
    private String setTimeAutoYn;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProfileTimezone(
            Long profileTimezoneBoxNum,
            Long profileId,
            Long timezoneSeq,
            String setTimeAutoYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.profileTimezoneBoxNum = profileTimezoneBoxNum;
        this.profileId = profileId;
        this.timezoneSeq = timezoneSeq;
        this.setTimeAutoYn = setTimeAutoYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

}
