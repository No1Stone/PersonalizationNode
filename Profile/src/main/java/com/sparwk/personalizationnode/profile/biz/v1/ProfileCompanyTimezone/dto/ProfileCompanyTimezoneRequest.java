package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyTimezone.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyTimezoneRequest {

    private Long profileId;
    private Long timezoneSeq;
    private String setTimeAutoYn;
    private Long profileTimezoneBoxNum;
}
