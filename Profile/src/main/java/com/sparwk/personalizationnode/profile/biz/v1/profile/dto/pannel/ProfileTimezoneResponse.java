package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel;

import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileTimezoneId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileTimezoneResponse {
    private Long profileid;
    private Long profileTimezonBoxNum;
    private Long timezoneSeq;
    private String setTimeAutoYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

//    @Builder
//    ProfileTimezoneResponse(
//            Long timezoneSeq,
//            String setTimeAutoYn,
//            Long regUsr,
//            LocalDateTime regDt,
//            Long modUsr,
//            LocalDateTime modDt
//    ) {
//        this.timezoneSeq = timezoneSeq;
//        this.setTimeAutoYn = setTimeAutoYn;
//        this.regUsr = regUsr;
//        this.regDt = regDt;
//        this.modUsr = modUsr;
//        this.modDt = modDt;
//    }

}
