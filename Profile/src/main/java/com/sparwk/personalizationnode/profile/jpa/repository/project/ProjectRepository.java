package com.sparwk.personalizationnode.profile.jpa.repository.project;

import com.sparwk.personalizationnode.profile.jpa.entity.project.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {

    int countByProjId(Long projId);

}
