package com.sparwk.personalizationnode.profile.jpa.entity.project;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_project_memb")
//@IdClass(ProjectMembId.class)
public class ProjectMemb {

    @Id
    @Column(name = "proj_id", nullable = false)
    private Long projId;
    @Column(name = "profile_id", nullable = false)
    private Long profileId;
    @Column(name = "patp_sdt", nullable = true)
    private LocalDateTime patpSdt;
    @Column(name = "patp_edt", nullable = true)
    private LocalDateTime patpEdt;
    @Column(name = "active_yn", nullable = true)
    private String activeYn;
    @Column(name = "confirm_yn", nullable = true)
    private String confirmYn;
    @Column(name = "apply_stat", nullable = true)
    private String applyStat;
    @Column(name = "invt_stat", nullable = true)
    private String invtStat;
    @Column(name = "invt_dt", nullable = true)
    private LocalDateTime invtDt;
    @Column(name = "ban_yn", nullable = true)
    private String banYn;
    @Column(name = "ban_dt", nullable = true)
    private LocalDateTime banDt;
    @Column(name = "quit_yn", nullable = true)
    private String quitYn;
    @Column(name = "quit_dt", nullable = true)
    private LocalDateTime quitDt;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProjectMemb(
            long projId,
            long profileId,
            LocalDateTime patpSdt,
            LocalDateTime patpEdt,
            String activeYn,
            String confirmYn,
            String applyStat,
            String invtStat,
            LocalDateTime invtDt,
            String banYn,
            LocalDateTime banDt,
            String quitYn,
            LocalDateTime quitDt,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.projId = projId;
        this.profileId = profileId;
        this.patpSdt = patpSdt;
        this.patpEdt = patpEdt;
        this.activeYn = activeYn;
        this.confirmYn = confirmYn;
        this.applyStat = applyStat;
        this.invtStat = invtStat;
        this.invtDt = invtDt;
        this.banYn = banYn;
        this.banDt = banDt;
        this.quitYn = quitYn;
        this.quitDt = quitDt;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }
}
