package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminAnrCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminAnrCodeRepository extends JpaRepository<AdminAnrCode, Long> {

    Optional<AdminAnrCode> findByAnrCd(String cd);
    boolean existsByAnrCd(String cd);
}
