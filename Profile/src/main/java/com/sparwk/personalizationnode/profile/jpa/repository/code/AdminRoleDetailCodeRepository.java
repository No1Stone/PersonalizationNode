package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminRoleDetailCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminRoleDetailCodeRepository extends JpaRepository<AdminRoleDetailCode, Long> {

    Optional<AdminRoleDetailCode> findByDcode(String cd);
    boolean existsByDcode(String cd);

}
