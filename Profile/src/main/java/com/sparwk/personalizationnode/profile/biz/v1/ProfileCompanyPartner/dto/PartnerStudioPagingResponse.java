package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto.ProfileCompanyResponse;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PartnerStudioPagingResponse {

    private String type;
    private String relationType;
    private String relationStatus;
    private ProfileCompanyResponse profileCompanyResponse;


}
