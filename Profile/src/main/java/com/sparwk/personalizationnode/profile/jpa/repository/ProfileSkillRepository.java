package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileSkill;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileSkillRepository extends JpaRepository<ProfileSkill, String> {

    List<ProfileSkill> findByProfileId(Long profileId);
    void deleteByProfileIdAndSkillTypeCdAndSkillDetailTypeCd(Long profileId, String stc,String dstc);
}
