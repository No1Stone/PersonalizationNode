package com.sparwk.personalizationnode.profile.biz.v1.ProfileLanguage.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileLanguageDto {
    @Schema(description = "프로필 아이디")
    private Long profileId;//ok
    @Schema(description = "언어 코드")
    private String languageCd;//ok

}
