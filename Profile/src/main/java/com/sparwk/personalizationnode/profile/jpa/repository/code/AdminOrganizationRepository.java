package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminOrganization;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminOrganizationRepository extends JpaRepository<AdminOrganization, Long> {

    Optional<AdminOrganization> findByOrgCd(String cd);
    boolean existsByOrgCd(String cd);

}
