package com.sparwk.personalizationnode.profile.jpa.entity.code;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_continent_code")
public class AdminContinentCode {

    @Id
    @Column(name = "continent_seq", nullable = true)
    private Long continentSeq;
    @Column(name = "continent", nullable = true)
    private String continent;
    @Column(name = "code", nullable = true)
    private String code;
    @Column(name = "description", nullable = true)
    private String description;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "continent_cd", nullable = true)
    private String continentCd;

    @Builder
    AdminContinentCode(
            Long continentSeq,
            String continent,
            String code,
            String description,
            String useYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt,
            String continentCd
    ) {
        this.continentSeq = continentSeq;
        this.continent = continent;
        this.code = code;
        this.description = description;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
        this.continentCd = continentCd;
    }


}
