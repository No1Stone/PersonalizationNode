package com.sparwk.personalizationnode.profile.biz.v1.profile.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileUpdateRequest {
    @Schema(description = "프로필 아이디")
    private Long profileId;
    private String mattermostId;
    private String fullName;
    private String stageNameYn;
    private String bthYear;
    private String bthMonth;
    private String bthDay;
    private String hireMeYn;
    private String bio;
    private String headline;
    private String currentCityCountryCd;
    private String currentCityNm;
    private String homeTownCountryCd;
    private String homeTownNm;
    private String ipiInfo;
    private String caeInfo;
    private String isniInfo;
    private String ipnInfo;
    private String nroTypeCd;
    private String nroInfo;
    private String profileImgUrl;
    private String profileBgdImgUrl;
    private String verifyYn;
    private String useYn;
    private String accomplishmentsInfo;



}
