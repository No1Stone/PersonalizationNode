package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto.IpiUpdateRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto.ProfileCompanyRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto.VatUpdateRequest;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/profile/company")
@CrossOrigin("*")
@Api(tags = "Profile Server Profile Company API")
public class ProfileCompanyController {

    @Autowired
    private ProfileCompanyService profileCompanyService;
    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyController.class);

    @PostMapping(path = "/info")
    public Response ProfileCompanySaveController(@Valid @RequestBody ProfileCompanyRequest dto
    ) {
        Response res = profileCompanyService.ProfileCompanySaveService(dto);
        return res;
    }

    @GetMapping(path = "/info/{profileId}")
    public Response ProfileCompanySelectController(@PathVariable(name = "profileId") Long profileId) {
        Response res = profileCompanyService.ProfilecompanySelectService(profileId);
        return res;
    }

    @GetMapping(path = "/info/connect/{profileId}")
    public Response ProfileCompanyConnectSelectController(@PathVariable(name = "profileId") Long profileId) {
        Response res = profileCompanyService.ProfilecompanyConnectSelectService(profileId);
        return res;
    }

    @GetMapping(path = "/info/partner/{profileId}")
    public Response ProfilePartnerSelectController(@PathVariable(name = "profileId") Long profileId) {
        Response res = profileCompanyService.ProfilePartnerSelectService(profileId);
        return res;
    }

    @PostMapping(path = "/info/update")
    public Response ProfileCompanyUpdateController(@Valid @RequestBody ProfileCompanyRequest dto) {
        Response res = profileCompanyService.ProfilecompanyUpdateService(dto);
        return res;
    }

    @PostMapping(path = "/vatUpdate")
    public Response ProfileCompanyVatUpdateController(@Valid @RequestBody VatUpdateRequest dto) {
        Response res = profileCompanyService.ProfileCompanyVatUpdateService(dto);
        return res;
    }

    @PostMapping(path = "/ipiUpdate")
    public Response ProfileCompanyIpiUpdateController(@Valid @RequestBody IpiUpdateRequest dto) {
        Response res = profileCompanyService.ProfileCompanyIpiUpdateService(dto);
        return res;
    }


}

