package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.sparwk.personalizationnode.profile.jpa.dto.ProfileBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface ProfileRepositoryDynamic {
    @Transactional
    Long ProfileRepositoryDynamicUpdate(ProfileBaseDTO entity);

}
