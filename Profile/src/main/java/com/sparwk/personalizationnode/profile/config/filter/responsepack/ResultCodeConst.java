package com.sparwk.personalizationnode.profile.config.filter.responsepack;

public enum ResultCodeConst {
    //공통처리
    SUCCESS("0000"),
    NOT_FOUND_INFO("0010"),
    NOT_FOUND_TOKEN("3001"),
    NOT_FOUNT_OWNER_EMAIL("3103"),
    NOT_FOUNT_INVITATION_EMAIL("3104"),

    //valid
    NOT_EXIST_PROFILE_ID("1200"),
    NOT_FOUND_PROJECT_ID("5010"),
    NOT_VALID_COMPANY_CD("1100"),
    ACC_SERVER_FAIL("3202"),
    PROFILE_CREATE_FAIL("3203"),
    ACCOUNT_PASSPORT_NULL("3204"),
    PROFILE_REQUEST_NULL("3205"),
    PROFILE_GENDER_REQUEST_NULL("3206"),
    PROFILE_LANGUAGE_REQUEST_NULL("3207"),
    PROFILE_POSITION_REQUEST_LIST_NULL("3208"),
    PROFILE_META_REQUEST_LIST_NULL("3209"),
    PROFILE_NAME_NOT_FOUNE("3210"),
    PROFILE_BIRTH_NOT_FOUND("3211"),
    PROFILE_PHONE_NOT_FOUND("3212"),
    PROFILE_HAEDLINE_NOT_FOUND("3213"),
    PROFILE_ID_REQUEST_TOKEN_MISMATCH("3214"),
    NOT_FOUNT_INVITATION_PROFILE_ID("3215"),
    NOT_FOUNT_OWNER_PROFILE_ID("3216"),
    NOT_FOUNT_PROFILE_POSITION("3217"),




    //Fail
    FAIL("2000"),

    //System Error
    ERROR("9999");
    private String code;

    ResultCodeConst(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
