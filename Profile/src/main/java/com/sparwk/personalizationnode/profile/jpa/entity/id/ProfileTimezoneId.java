package com.sparwk.personalizationnode.profile.jpa.entity.id;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class ProfileTimezoneId implements Serializable {
    private Long profileTimezoneBoxNum;
    private Long profileId;
}
