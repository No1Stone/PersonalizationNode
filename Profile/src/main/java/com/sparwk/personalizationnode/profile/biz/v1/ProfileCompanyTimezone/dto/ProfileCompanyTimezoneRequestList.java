package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyTimezone.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyTimezoneRequestList {
  private List<ProfileCompanyTimezoneRequest> profileCompanyTimezoneRequestList;
}
