package com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.persistence.Column;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePositionDto {
    @Schema(description = "프로필 아이디")
    private Long profileId;
    @Schema(description = "프로필포지션시퀀스 신규생성땐 보내지 않습니다/업데이트 수정시 시퀀스번호가있어야합니다..")
    private Long profilePositionSeq;
    @Schema(description = "회사 코드")
    private Long companyProfileId;
    @Schema(description = "A&R 여부")
    private String anrYn;
    @Schema(description = "아티스트 여부")
    private String artistYn;
    @Schema(description = "아티스트 여부")
    private String creatorYn;
    @Schema(description = "부서및 권한")
    private String deptRoleInfo;
    @Schema(description = "주 소속 회사 여부")
    private String primaryYn;

}
