package com.sparwk.personalizationnode.profile.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyPartnerBaseDTO {
    private Long profileId;
    private Long partnerId;
    private String relationType;
    private String relationStatus;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
