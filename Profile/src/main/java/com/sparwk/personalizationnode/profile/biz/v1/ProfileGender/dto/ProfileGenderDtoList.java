package com.sparwk.personalizationnode.profile.biz.v1.ProfileGender.dto;


import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGenderDtoList {
    private List<ProfileGenderDto> profileGenderDtoList;
}
