package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyStudio.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyStudioRequestList {

    private List<ProfileCompanyStudioRequest> profileCompanyStudioRequestList;

}
