package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompanyRoster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProfileCompanyRosterRepository extends JpaRepository<ProfileCompanyRoster,Long> {

    List<ProfileCompanyRoster> findByProfileId(Long profileId);
//    List<ProfileCompanyRoster> findByProfileIdAndRosterStatusOrderByModDtDesc(Long profileId, String rosterStatus);
    List<ProfileCompanyRoster> findByProfileIdOrderByModDtDesc(Long profileId);
    boolean existsByProfileId(Long profileId);
    boolean existsByProfilePositionSeqAndRosterStatus(Long positionSeq, String code);

    List<ProfileCompanyRoster> findByProfilePositionSeqIn(List<Long> positionSeq);

    @Transactional
    @Modifying
    @Query(
            value = "delete from tb_profile_company_roster " +
                    "where profile_position_seq = :profilePositionSeq "
            ,nativeQuery = true
    )
    void RosterDeleteProfilePositionSeq(@Param("profilePositionSeq") Long profilePositionSeq);

    ProfileCompanyRoster findByProfilePositionSeq(Long profilePositionSeq);
}
