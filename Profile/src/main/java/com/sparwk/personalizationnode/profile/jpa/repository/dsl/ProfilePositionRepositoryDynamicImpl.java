package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfilePositionBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.QProfilePosition;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class ProfilePositionRepositoryDynamicImpl implements ProfilePositionRepositoryDynamic {

    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(ProfilePositionRepositoryDynamicImpl.class);
    private QProfilePosition qProfilePosition = QProfilePosition.profilePosition;

    private ProfilePositionRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long ProfilePositionRepositoryDynamicUpdate(ProfilePositionBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProfilePosition);

        if (entity.getCompanyProfileId() != null) {
            jpaUpdateClause.set(qProfilePosition.companyProfileId, entity.getCompanyProfileId());
        }
        if (entity.getCompanyVerifyYn() == null || entity.getCompanyVerifyYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfilePosition.companyVerifyYn, entity.getCompanyVerifyYn());
        }
        if (entity.getArtistYn() == null || entity.getArtistYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfilePosition.artistYn, entity.getArtistYn());
        }
        if (entity.getAnrYn() == null || entity.getAnrYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfilePosition.anrYn, entity.getAnrYn());
        }
        if (entity.getCreatorYn() == null || entity.getCreatorYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfilePosition.creatorYn, entity.getCreatorYn());
        }
        if (entity.getDeptRoleInfo() == null || entity.getDeptRoleInfo().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfilePosition.deptRoleInfo, entity.getDeptRoleInfo());
        }
        if (entity.getPrimaryYn() == null || entity.getPrimaryYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfilePosition.primaryYn, entity.getPrimaryYn());
        }
        if (entity.getRegUsr() != null) {
            jpaUpdateClause.set(qProfilePosition.regUsr, entity.getRegUsr());
        }
        if (entity.getRegDt() != null) {
            jpaUpdateClause.set(qProfilePosition.regDt, entity.getRegDt());
        }
        if (entity.getModUsr() != null) {
            jpaUpdateClause.set(qProfilePosition.modUsr, entity.getModUsr());
        }
        if (entity.getModDt() != null) {
            jpaUpdateClause.set(qProfilePosition.modDt, entity.getModDt());
        }


        jpaUpdateClause.where(qProfilePosition.profilePositionSeq.eq(entity.getProfilePositionSeq()));
        if (entity.getProfileId() != null) {
            jpaUpdateClause.where(qProfilePosition.profileId.eq(entity.getProfileId()));
        }

        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
