package com.sparwk.personalizationnode.profile.jpa.repository.account;

import com.sparwk.personalizationnode.profile.jpa.entity.account.AccountCompanyType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface AccountCompanyTypeRespository extends JpaRepository <AccountCompanyType, Long>{

    List<AccountCompanyType> findAll();

    List<AccountCompanyType> findAllByCompanyLicenseVerifyYn(String yn);

    List<AccountCompanyType> findAllByCompanyLicenseVerifyYnAndCompanyCd(String yn, String type);

    List<AccountCompanyType> findByCompanyCd(String companyCd);

    List<AccountCompanyType> findByAccntId(Long accId);


}
