package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupOntheweb.dto;


import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGroupOntehwebRequest {

    private Long profileId;
    private String snsTypeCd;
    private String snsUrl;
    private String useYn;

}
