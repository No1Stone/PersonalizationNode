package com.sparwk.personalizationnode.profile.jpa.entity;


import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileSkillId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_skill")
@DynamicUpdate
@IdClass(ProfileSkillId.class)
public class ProfileSkill{

    @Id
    @Column(name = "skill_type_cd", length = 9)
    private String skillTypeCd;

    @Id
    @Column(name = "profile_id")
    private Long profileId;

    @Id
    @Column(name = "skill_detail_type_cd", nullable = true, length = 9)
    private String skillDetailTypeCd;


    @Column(name = "reg_usr", updatable = false)
    //first registrant
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    ProfileSkill(Long profileId,
                 String skillTypeCd,
                 String skillDetailTypeCd,
                 Long regUsr,
                 LocalDateTime regDt,
                 Long modUsr,
                 LocalDateTime modDt
    ){
        this.profileId=profileId;
        this.skillTypeCd=skillTypeCd;
        this.skillDetailTypeCd=skillDetailTypeCd;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

}
