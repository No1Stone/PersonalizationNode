package com.sparwk.personalizationnode.profile.config.filter;
import lombok.RequiredArgsConstructor;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@RequiredArgsConstructor
public class Webconfig implements WebMvcConfigurer {

    private final HandlerInterceptor authInterceptor;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor)
                .addPathPatterns("/V1/profile/**")
                .addPathPatterns("/V1/carerr/**")
                .addPathPatterns("/V1/education/**")
                .addPathPatterns("/V1/gender/**")
                .addPathPatterns("/V1/interest/**")
                .addPathPatterns("/V1/language/**")
                .addPathPatterns("/V1/position/**")
                .addPathPatterns("/V1/skill/**")
                .addPathPatterns("/V1/ontheweb/**")
                .addPathPatterns("/V1/timezone/**")
                .addPathPatterns("/V1/company/**")
                .addPathPatterns("/V1/change/**")
                .addPathPatterns("/V1/group/**")
                .excludePathPatterns("account/swagger-ui/**")
                .excludePathPatterns("/V1/profile/public/**")
                .excludePathPatterns("/V2/profile/public/**")
                .excludePathPatterns("/V1/profile/company/public/**")
        ;
    }

    @Bean
    public RestTemplate restTemplate() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        HttpClient client = HttpClientBuilder.create()
                .setMaxConnTotal(50)
                .setMaxConnPerRoute(20).build();
        factory.setHttpClient(client);
        factory.setConnectTimeout(10000);
        factory.setReadTimeout(5000);
        RestTemplate restTemplate = new RestTemplate(factory);
        return restTemplate;
    }

}