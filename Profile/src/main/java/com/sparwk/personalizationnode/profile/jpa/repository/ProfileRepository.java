package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.biz.chat.SelectMmInfo;
import com.sparwk.personalizationnode.profile.jpa.entity.Profile;
import com.sparwk.personalizationnode.profile.jpa.repository.dsl.ProfileRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface ProfileRepository extends JpaRepository<Profile, Long> , ProfileRepositoryDynamic {

    Optional<Profile> findByAccntIdAndProfileId(Long AccountId, Long ProfileId);
    Optional<Profile> findByProfileId(Long ProfileId);
    int countByProfileId(Long profileId);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile set mattermost_id = :mertterId where profile_id = :profileId "
            ,nativeQuery = true
    )
    int UpdateProfileMattermostId(@Param("profileId")Long profileId, @Param("mertterId") String mertterId);

    @Query(
            value = "SELECT " +
                    "tp.accnt_id AS accountId," +
                    "tp.profile_id AS profileId, " +
                    "tp.mattermost_id AS mattermostId, " +
                    "tp.profile_img_url AS profileImgUrl," +
                    "ta.accnt_email AS email," +
                    "tap.passport_first_name AS firstname," +
                    "tap.passport_last_name AS lastname " +
                    " FROM tb_profile tp " +
                    " left join tb_account ta on ta.accnt_id = tp.accnt_id " +
                    " left join tb_account_passport tap on tap.accnt_id = tp.accnt_id " +
                    " where tp.profile_id = :profileId "
            ,nativeQuery = true
    )
    Object SelectMmInfo(@Param("profileId")Long profileId);


//    @Query(value = "SELECT nextval('tb_profile_id_seq')", nativeQuery = true)
//    Long newProfileIdSeq();

}
