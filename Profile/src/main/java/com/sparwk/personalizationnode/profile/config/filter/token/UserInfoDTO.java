package com.sparwk.personalizationnode.profile.config.filter.token;

import lombok.*;

import java.util.HashMap;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserInfoDTO {
    private Long accountId;
    private String accntTypeCd;
    private Long lastUseProfileId;
    private List<Long> ProfileList;
    private HashMap<String, String> permission;

}
