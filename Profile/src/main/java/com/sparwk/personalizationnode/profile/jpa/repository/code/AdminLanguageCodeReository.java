package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminLanguageCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminLanguageCodeReository extends JpaRepository<AdminLanguageCode, Long> {
    Optional<AdminLanguageCode> findByLanguageCd(String cd);
    boolean existsByLanguageCd(String cd);
}
