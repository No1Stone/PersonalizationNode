package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupOntheweb;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroup.ProfileGroupService;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupOntheweb.dto.ProfileGroupOntehwebRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupOntheweb.dto.ProfileGroupOntehwebRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileGroupContactBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileGroupOntehweb;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileGroupOntehwebRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileGroupOnthewebService {
    private final Logger logger = LoggerFactory.getLogger(ProfileGroupOnthewebService.class);
    private final ModelMapper modelMapper;
    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final ProfileGroupOntehwebRepository profileGroupOntehwebRepository;

    public Response ProfileGroupOnthewebSelectService(Long profileId) {
        Response res = new Response();
        res.setResult(profileGroupOntehwebRepository
                .findByProfileId(profileId).stream().map(e -> modelMapper
                        .map(e, ProfileGroupContactBaseDTO.class)).collect(Collectors.toList()));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response ProfileGroupOnthewebSaveService(ProfileGroupOntehwebRequestList dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        List<String> befo = new ArrayList<>();
        if(profileGroupOntehwebRepository
                .existsByProfileId(dto.getProfileGroupOntehwebRequest().get(0).getProfileId())) {
           befo = profileGroupOntehwebRepository.findByProfileId(dto.getProfileGroupOntehwebRequest().get(0).getProfileId())
                    .stream().map(e -> modelMapper.map(e.getSnsTypeCd(), String.class)).collect(Collectors.toList());
        }
        List<String> after = new ArrayList<>();
        for(ProfileGroupOntehwebRequest e: dto.getProfileGroupOntehwebRequest()){
            logger.info("??여기??");
            after.add(e.getSnsTypeCd());
            ProfileGroupOntehweb save = modelMapper.map(e, ProfileGroupOntehweb.class);
            save.setModUsr(userInfoDTO.getAccountId());
            save.setRegUsr(userInfoDTO.getAccountId());
            save.setModDt(LocalDateTime.now());
            save.setRegDt(LocalDateTime.now());
            profileGroupOntehwebRepository.save(save);
        }
        befo.removeAll(after);
        for(String e: befo){
            profileGroupOntehwebRepository
                    .deleteByProfileIdAndSnsTypeCd(dto.getProfileGroupOntehwebRequest()
                            .get(0).getProfileId(), e);
        }
        res.setResult(profileGroupOntehwebRepository.findByProfileId(dto.getProfileGroupOntehwebRequest()
                .get(0).getProfileId()));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
