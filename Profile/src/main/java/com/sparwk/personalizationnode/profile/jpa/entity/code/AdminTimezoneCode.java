package com.sparwk.personalizationnode.profile.jpa.entity.code;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_timezone_code")
public class AdminTimezoneCode {

    @Id
    @Column(name = "timezone_seq ", nullable = true)
    private Long timezoneSeq;
    @Column(name = "timezone_name ", nullable = true)
    private String timezoneName;
    @Column(name = "continent ", nullable = true)
    private String continent;
    @Column(name = "city ", nullable = true)
    private String city;
    @Column(name = "utc_hour ", nullable = true)
    private String utcHour;
    @Column(name = "diff_time ", nullable = true)
    private Long diffTime;
    @Column(name = "use_yn ", nullable = true)
    private String useYn;
    @Column(name = "reg_usr ", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt ", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr ", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    AdminTimezoneCode(
            Long timezoneSeq,
            String timezoneName,
            String continent,
            String city,
            String utcHour,
            Long diffTime,
            String useYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.timezoneSeq = timezoneSeq;
        this.timezoneName = timezoneName;
        this.continent = continent;
        this.city = city;
        this.utcHour = utcHour;
        this.diffTime = diffTime;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

}
