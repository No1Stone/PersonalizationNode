package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileAnrPitchingResponse {
    private String pitchingYn;
}
