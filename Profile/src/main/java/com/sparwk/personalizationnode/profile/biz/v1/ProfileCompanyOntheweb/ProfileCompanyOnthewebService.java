package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyOntheweb;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyOntheweb.dto.ProfileCompanyOnthewebRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyOntheweb.dto.ProfileCompanyOnthewebRequestList;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto.ProfileCompanyPartnerRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto.ProfileCompanyPartnerRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyOnthewebBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyPartnerBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyOnthewebRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyPartnerRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileCompanyOnthewebService {
    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyOnthewebService.class);
    @Autowired
    private ProfileCompanyOnthewebRepository profileCompanyOnthewebRepository;

    private final MessageSourceAccessor messageSourceAccessor;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private HttpServletRequest httpServletRequest;



    public Response ProfileCompanyOnthewebSaveService(ProfileCompanyOnthewebRequest dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        ProfileCompanyOnthewebBaseDTO baseDTO = modelMapper.map(dto, ProfileCompanyOnthewebBaseDTO.class);
        baseDTO.setModDt(LocalDateTime.now());
        baseDTO.setRegDt(LocalDateTime.now());
        baseDTO.setRegUsr(userInfoDTO.getAccountId());
        baseDTO.setModUsr(userInfoDTO.getAccountId());

        ProfileCompanyOnthewebBaseDTO result = profileServiceRepository.ProfileCompanyOnthewebSaveService(baseDTO);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilecompanyOnthewebSelectService(Long profileId) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        List<ProfileCompanyOnthewebBaseDTO> result = profileCompanyOnthewebRepository
                .findByProfileId(profileId).stream().map(
                        e -> modelMapper.map(e, ProfileCompanyOnthewebBaseDTO.class)).collect(Collectors.toList());
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response ProfileCompanyOnthewebListSaveService(ProfileCompanyOnthewebRequestList dto) {
        Response res = new Response();
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        for(ProfileCompanyOnthewebRequest e: dto.getProfileCompanyOnthewebRequestList()){
            ProfileCompanyOnthewebBaseDTO baseDTO = modelMapper.map(e, ProfileCompanyOnthewebBaseDTO.class);
            baseDTO.setModDt(LocalDateTime.now());
            baseDTO.setRegDt(LocalDateTime.now());
            baseDTO.setRegUsr(userInfoDTO.getAccountId());
            baseDTO.setModUsr(userInfoDTO.getAccountId());
        profileServiceRepository.ProfileCompanyOnthewebSaveService(baseDTO);
        }
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
