package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroup.dto.update.MattermostIdUpdateRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact.dto.ProfileGroupContactEmailUpdateRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact.dto.ProfileGroupContactImgUpdateRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact.dto.ProfileGroupContactNameUpdateRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact.dto.ProfileGroupContactPhoneUpdateRequest;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/group/contact")
@CrossOrigin("*")
@Api(tags = "Profile Server Group API")
@RequiredArgsConstructor
public class ProfileGroupContactController {

    private final Logger logger = LoggerFactory.getLogger(ProfileGroupContactController.class);
    private final ProfileGroupContactService profileGroupContactService;

    @ApiOperation(
            value = "그룹 프로필 조회",
            notes = "그룹 프로필 조회"
    )
    @GetMapping(path = "/info/{profileId}")
    public Response ProfileGroupContactSelectController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileGroupContactService.ProfileGroupContactSelectService(profileId);
        return res;
    }

    @ApiOperation(
            value = "그룹 프로필 배너 수정",
            notes = "그룹 프로필 배너 수정"
    )
    @PostMapping(path = "/update/email")
    public Response ProfileGroupContactEmailUpdateController(@Valid @RequestBody ProfileGroupContactEmailUpdateRequest dto) {
        Response res = profileGroupContactService.ProfileGroupContactEmailUpdateService(dto);
        return res;
    }

    @ApiOperation(
            value = "그룹 프로필 배너 수정",
            notes = "그룹 프로필 배너 수정"
    )
    @PostMapping(path = "/update/img")
    public Response ProfileGroupContactImgUpdateController(@Valid @RequestBody ProfileGroupContactImgUpdateRequest dto) {
        Response res = profileGroupContactService.ProfileGroupContactImgUpdateService(dto);
        return res;
    }

    @ApiOperation(
            value = "그룹 프로필 배너 수정",
            notes = "그룹 프로필 배너 수정"
    )
    @PostMapping(path = "/update/name")
    public Response ProfileGroupContactNameUpdateController(@Valid @RequestBody ProfileGroupContactNameUpdateRequest dto) {
        Response res = profileGroupContactService.ProfileGroupContactNameUpdateService(dto);
        return res;
    }

    @ApiOperation(
            value = "그룹 프로필 배너 수정",
            notes = "그룹 프로필 배너 수정"
    )
    @PostMapping(path = "/update/phone")
    public Response ProfileGroupContactPhoneUpdateController(@Valid @RequestBody ProfileGroupContactPhoneUpdateRequest dto) {
        Response res = profileGroupContactService.ProfileGroupContactPhoneUpdateService(dto);
        return res;
    }

}
