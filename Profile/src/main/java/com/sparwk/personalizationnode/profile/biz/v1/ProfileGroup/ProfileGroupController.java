package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroup;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroup.dto.update.*;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/group")
@CrossOrigin("*")
@Api(tags = "Profile Server Group API")
@RequiredArgsConstructor
public class ProfileGroupController {

    private final Logger logger = LoggerFactory.getLogger(ProfileGroupController.class);
    private final ProfileGroupService profileGroupService;

    @ApiOperation(
            value = "그룹 해더 수정",
            notes = "그룹 해더 수정"
    )
    @PostMapping(path = "/update/headline")
    public Response ProfileGroupHeadLineUpdateController(@Valid @RequestBody HeadLineUpdateRequest dto) {
        Response res = profileGroupService.ProfileGroupHeadLineUpdateService(dto);
        return res;
    }

    @ApiOperation(
            value = "그룹 프로필 이미지 수정",
            notes = "그룹 프로필 이미지 수정"
    )
    @PostMapping(path = "/update/img")
    public Response ProfileGroupImgUpdateController(@Valid @RequestBody ImgUpdateRequest dto) {
        Response res = profileGroupService.ProfileGroupImgUpdateService(dto);
        return res;
    }

    @ApiOperation(
            value = "그룹 프로필 네임 수정",
            notes = "그룹 프로필 네임 수정"
    )
    @PostMapping(path = "/update/groupname")
    public Response ProfileGroupNameUpdateController(@Valid @RequestBody ProfileGroupNameUpdateRequest dto) {
        Response res = profileGroupService.ProfileGroupNameUpdateService(dto);
        return res;
    }

    @ApiOperation(
            value = "그룹 프로필 웹사이트 수정",
            notes = "그룹 프로필 웹사이트 수정"
    )
    @PostMapping(path = "/update/website")
    public Response ProfileGroupWebsiteUpdateController(@Valid @RequestBody WebSiteUpdateRequest dto) {
        Response res = profileGroupService.ProfileGroupWebsiteUpdateService(dto);
        return res;
    }

    @ApiOperation(
            value = "그룹 프로필 오버뷰 수정",
            notes = "그룹 프로필 오버뷰 수정"
    )
    @PostMapping(path = "/update/overview")
    public Response ProfileGroupOverviewUpdateController(@Valid @RequestBody OverViewUpdateRequest dto) {
        Response res = profileGroupService.ProfileGroupOverviewUpdateService(dto);
        return res;
    }

    @ApiOperation(
            value = "그룹 프로필 배너 수정",
            notes = "그룹 프로필 배너 수정"
    )
    @PostMapping(path = "/update/banner")
    public Response ProfileGroupBannerUpdateController(@Valid @RequestBody BannerUpdateRequest dto) {
        Response res = profileGroupService.ProfileGroupBannerUpdateService(dto);
        return res;
    }

    @ApiOperation(
            value = "그룹 프로필 배너 수정",
            notes = "그룹 프로필 배너 수정"
    )
    @PostMapping(path = "/update/mattermost")
    public Response ProfileGroupMattermostUpdateController(@Valid @RequestBody MattermostIdUpdateRequest dto) {
        Response res = profileGroupService.ProfileGroupMattermostUpdateService(dto);
        return res;
    }

    @ApiOperation(
            value = "그룹 프로필 조회",
            notes = "그룹 프로필 조회"
    )
    @GetMapping(path = "/info/{profileId}")
    public Response ProfileGroupSelectController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileGroupService.ProfileGroupSelectService(profileId);
        return res;
    }

    

}
