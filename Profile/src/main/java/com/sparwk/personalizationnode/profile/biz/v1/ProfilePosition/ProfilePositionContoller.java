package com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition;

import com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto.*;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfilePositionBaseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/V1/position")
@CrossOrigin("*")
@Api(tags = "Profile Server Position API")
public class ProfilePositionContoller {

    @Autowired
    private ProfilePositionService profilePositionService;

    private final Logger logger = LoggerFactory.getLogger(ProfilePositionContoller.class);

    @ApiOperation(
            value = "프로필 언어 정보 입력",
            notes = "언어 정보 입력"
    )
    @PostMapping(path = "/info")
    public Response ProfilePositionSaveController(@Valid @RequestBody ProfilePositionDto dto) {
        Response res = profilePositionService.ProfilePositionSave(dto);
        return res;
    }

    @ApiOperation(
            value = "프로필 언어 정보 입력",
            notes = "언어 정보 입력"
    )
    @PostMapping(path = "/edit")
    public Response ProfilePositionEditController(@Valid @RequestBody ProfilePositionEditRequestList dto) {
        Response res = profilePositionService.ProfilePositionEditService(dto);
        return res;
    }

    @ApiOperation(
            value = "회사정보 리스트",
            notes = "회사정보 입력"
    )
    @PostMapping(path = "/lcreate")
    public Response ProfilePositionSaveController(@Valid @RequestBody ProfilePositionRequestDto profilePositionRequestDto) {
//        Response res = profilePositionService.ProfilePositionListSave(profilePositionRequestDto);
        Response res = profilePositionService.ProfilePositionListSaveAutoVerifyService(profilePositionRequestDto);
        return res;
    }

    @PostMapping(path = "/positionTest")
    public Response ProfilePositionListSaveAutoVerifyController(@Valid @RequestBody ProfilePositionRequestDto profilePositionRequestDto) {
        Response res = profilePositionService.ProfilePositionListSaveAutoVerifyService(profilePositionRequestDto);
        return res;
    }

    @GetMapping(path = "/info/{profileId}")
    public Response ProfilePositionSelect(@PathVariable(name = "profileId") Long profileId) {
        Response res = profilePositionService.ProfilePositionSelect(profileId);
        return res;
    }

    @PostMapping(path = "/info/update")
    public Response ProfilePositionDynamicUpdateController(@Valid @RequestBody ProfilePositionDto dto) {
        Response res = profilePositionService.ProfilePositionDynamicUpdate(dto);
        return res;
    }

    @GetMapping(path = "/test1")
    public Response test1(HttpServletRequest req) {
        Response res = profilePositionService.AuthtokenResiltResponse();
        return res;
    }

    @GetMapping(path = "/pubCompany/{profileId}")
    public Response PubCompanyController(@PathVariable(name = "profileId") Long profileId) {
        Response res = profilePositionService.PubCompanyService(profileId);
        return res;
    }

    @GetMapping(path = "/roster/{seq}")
    public Response ProfilePositionSeqSelectController(@PathVariable(name = "seq") Long seq) {
        Response res = profilePositionService.ProfilePositionSeqSelectService(seq);
        return res;
    }

    @PostMapping(path = "/del")
    public Response ProfilePositionDeleteController(@RequestBody DeleteRequest dto){
        Response res = profilePositionService.ProfilePositionDeleteService(dto);
        return res;
    }

    @PostMapping(path = "/anrApply")
    public Response ProfilePositionAnrApplyYnController(@RequestBody DeleteRequest dto){
        Response res = profilePositionService.ProfilePositionAnrApplyYnService(dto);
        return res;
    }




}
