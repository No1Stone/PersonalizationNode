package com.sparwk.personalizationnode.profile.jpa.repository.project;

import com.sparwk.personalizationnode.profile.jpa.entity.project.ProjectMemb;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectMembRepository extends JpaRepository<ProjectMemb, Long> {
}
