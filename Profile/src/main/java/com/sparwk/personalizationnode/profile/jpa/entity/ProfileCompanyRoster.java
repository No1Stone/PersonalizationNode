package com.sparwk.personalizationnode.profile.jpa.entity;

import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileCompanyPartnerId;
import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileCompanyRosterId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_company_roster")
@DynamicUpdate
@IdClass(ProfileCompanyRosterId.class)
public class ProfileCompanyRoster {

    @Id
    @GeneratedValue(generator = "tb_profile_company_roster_seq")
    @Column(name = "roster_seq", nullable = true)
    private Long rosterSeq;
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "profile_position_seq", nullable = true)
    private Long profilePositionSeq;
    @Column(name = "roster_status", nullable = true)
    private String rosterStatus;
    @Column(name = "joining_start_dt", nullable = true)
    private String joiningStartDt;
    @Column(name = "joining_end_dt", nullable = true)
    private String joiningEndDt;
    @Column(name = "joining_end_yn", nullable = true)
    private String joiningEndYn;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProfileCompanyRoster(
            Long rosterSeq,
            Long profileId,
            Long profilePositionSeq,
            String rosterStatus,
            String joiningStartDt,
            String joiningEndDt,
            String joiningEndYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.rosterSeq = rosterSeq;
        this.profileId = profileId;
        this.profilePositionSeq = profilePositionSeq;
        this.rosterStatus = rosterStatus;
        this.joiningStartDt = joiningStartDt;
        this.joiningEndDt = joiningEndDt;
        this.joiningEndYn = joiningEndYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }
}
