package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel;

import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileLanguageId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileLanguageResponse {

    private String languageCd;
    private Long profileId;

//    @Builder
//    ProfileLanguageResponse(
//            Long profileId,
//            String languageCd
//    ) {
//        this.profileId = profileId;
//        this.languageCd = languageCd;
//    }

}
