package com.sparwk.personalizationnode.profile.biz.v2.profile;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileGender.ProfileGenderService;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileLanguage.ProfileLanguageService;
import com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.ProfilePositionService;
import com.sparwk.personalizationnode.profile.biz.v1.profile.ProfileService;
import com.sparwk.personalizationnode.profile.biz.v2.profile.dto.panneldto.*;
import com.sparwk.personalizationnode.profile.config.common.ClientIp;
import com.sparwk.personalizationnode.profile.config.filter.CodeConfig;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.Profile;
import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminTimezoneCode;
import com.sparwk.personalizationnode.profile.jpa.repository.*;
import com.sparwk.personalizationnode.profile.jpa.repository.account.AccountRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.code.*;
import com.sparwk.personalizationnode.profile.jpa.repository.project.ProjectMembRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.project.ProjectRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileServiceV2 {

    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfileService.class);
    private final ModelMapper modelMapper;
    private final RestTemplate restTemplate;
    private final HttpServletRequest httpServletRequest;
    private final ProfileServiceRepository profileServiceRepository;
    private final ProfileRepository profileRepository;
    private final ProfileGenderService profileGenderService;
    private final ProfileLanguageService profileLanguageService;
    private final ProfilePositionService profilePositionService;
    private final ProfileCompanyRepository profileCompanyRepository;
    private final ProfileCareerRepository profileCareerRepository;
    private final ProfileEducationRepository profileEducationRepository;
    private final ProfileGenderRepository profileGenderRepository;
    private final ProfileInterestMetaRepository profileInterestMetaRepository;
    private final ProfileLanguageRepository profileLanguageRepository;
    private final ProfilePositionRepository profilePositionRepository;
    private final ProfileSkillRepository profileSkillRepository;
    private final ProfileOnthewebRepository profileOnthewebRepository;
    private final ProfileTimezoneRepository profileTimezoneRepository;
    private final ProfileCompanyPartnerRepository profileCompanyPartnerRepository;
    private final ProfileCompanyRosterRepository profileCompanyRosterRepository;
    private final ProfileCompanyStudioRepository profileCompanyStudioRepository;
    private final ProfileCompanyOnthewebRepository profileCompanyOnthewebRepository;
    private final ProfileCompanyTimezoneRepository profileCompanyTimezoneRepository;
    private final ProfileUseInfoRepository profileUseInfoRepository;
    private final ProjectMembRepository projectMembRepository;
    private final ProjectRepository projectRepository;
    private final AccountRepository accountRepository;
    private final AdminCommonDetailCodeRepository adminCommonDetailCodeRepository;
    private final AdminCountryCodeRepository adminCountryCodeRepository;
    private final AdminRoleDetailCodeRepository adminRoleDetailCodeRepository;
    private final AdminTimezoneCodeRepository adminTimezoneCodeRepository;
    private final AdminAnrCodeRepository adminAnrCodeRepository;
    private final AdminContinentCodeRepository adminContinentCodeRepository;
    private final AdminCurrencyCodeRepository adminCurrencyCodeRepository;
    private final AdminLanguageCodeReository adminLanguageCodeReository;
    private final AdminOrganizationRepository adminOrganizationRepository;
    private final AdminSnsCodeRepository adminSnsCodeRepository;
    private final AdminRoleCodeRepository adminRoleCodeRepository;
    private final AdminSongCodeRepository adminSongCodeRepository;
    private final AdminSongDetailCodeRepository adminSongDetailCodeRepository;
    private final CodeConfig codeConfig;
    private final ClientIp clientIp;

    @Value("${sparwk.node.project.projectip}")
    private String projectIp;
    @Value("${sparwk.node.personalization.accountip}")
    private String accountIp;
    @Value("${sparwk.node.personalization.authip}")
    private String authIp;
    @Value("${sparwk.node.personalization.profileip}")
    private String profileIp;
    @Value("${sparwk.node.externalconnect.mailingip}")
    private String mailingIp;
    @Value("${sparwk.node.schema}")
    private String schema;


    public Response ProfilePannelV2SelectService(Long profileId) {
        Response res = new Response();

        //area1
        long area1 = System.currentTimeMillis();
        ProfilePannelResponseV2 result = new ProfilePannelResponseV2();
        Profile selectList = profileRepository.findByProfileId(profileId)
                .orElse(null);
        //area2
        long area2 = System.currentTimeMillis();
        result = modelMapper.map(selectList, ProfilePannelResponseV2.class);
        if (result.getCurrentCityCountryCd() != null
        ) {
            result.setCurrentCityCountryCdName(CodeValid(result.getCurrentCityCountryCd()));
        }
        //area3
        long area3 = System.currentTimeMillis();
        if (result.getHomeTownCountryCd() != null
        ) {
            result.setHomeTownCountryCdName(CodeValid(result.getHomeTownCountryCd()));
        }

        //area4
        long area4 = System.currentTimeMillis();
        result.setProfilePositionCodeList(
                selectList.getProfilePosition()
                        .stream().map(e -> modelMapper.map(e, ProfilePositionCodeName.class)).collect(Collectors.toList()));
        //area5
        long area5 = System.currentTimeMillis();
        result.getProfilePositionCodeList().stream().forEach(e -> e
                .setProfileCompanyBaseDTO(
                        profileCompanyRepository.findById(e.getCompanyProfileId())
                                .map(f -> modelMapper.map(f, ProfileCompanyBaseDTO.class))
                                .orElse(null)
                ));
        //area6
        long area6 = System.currentTimeMillis();
        result.setProfileCareerCodeList(selectList.getProfileCareer()
                .stream().map(e -> modelMapper.map(e, ProfileCareerCodeName.class))
                .collect(Collectors.toList())
        );
        //area7
        long area7 = System.currentTimeMillis();
        result.getProfileCareerCodeList()
                .stream().forEach(e -> e.setLocationCityCountryCdName(
                        CodeValid(e.getLocationCityCountryCd())));
        //area8
        long area8 = System.currentTimeMillis();
        result.setProfileEducationCodeList(selectList.getProfileEducation()
                .stream().map(e ->
                        modelMapper.map(e, ProfileEducationCodeName.class))
                .collect(Collectors.toList()));
        //area9
        long area9 = System.currentTimeMillis();
        result.getProfileEducationCodeList()
                .stream().forEach(e -> e.setLocationCityCountryCdName(
                        CodeValid(e.getLocationCityCountryCd())));
        //area10
        long area10 = System.currentTimeMillis();
        result.setProfileGenderCodeList(selectList.getProfileGender()
                .stream().map(e ->
                        modelMapper.map(e, ProfileGenderCodeName.class))
                .collect(Collectors.toList()));
        //area11
        long area11 = System.currentTimeMillis();
        result.getProfileGenderCodeList().stream().forEach(e -> e.setGenderCdName(CodeValid(e.getGenderCd())));

        ///////////////////////////////////////작업중
//        result.getProfileGenderCodeList()
//                        .stream().forEach(e ->
//                        e.setGenderCdName(
//                                adminCommonDetailCodeRepository.)
//                );
        //area12
        long area12 = System.currentTimeMillis();
        result.setProfileInterestMetaCodeList(selectList.getProfileInterestMeta()
                .stream().map(e ->
                        modelMapper.map(e, ProfileInterestMetaCodeName.class))
                .collect(Collectors.toList()));
        //area13
        long area13 = System.currentTimeMillis();
        result.getProfileInterestMetaCodeList().stream().forEach(e -> e.setDetailTypeCdName(CodeValid(e.getDetailTypeCd())));
        //area14
        long area14 = System.currentTimeMillis();
        result.setProfileLanguageCodeList(selectList.getProfileLanguage()
                .stream().map(e ->
                        modelMapper.map(e, ProfileLanguageCodeName.class))
                .collect(Collectors.toList()));
        //area15
        long area15 = System.currentTimeMillis();
        result.getProfileLanguageCodeList().stream().forEach(e -> e.setLanguageCdName(CodeValid(e.getLanguageCd())));
        //area16
        long area16 = System.currentTimeMillis();
        result.setProfileOnthewebCodeList(selectList.getProfileOntheweb()
                .stream().map(e ->
                        modelMapper.map(e, ProfileOnthewebCodeName.class))
                .collect(Collectors.toList()));
        //area17
        long area17 = System.currentTimeMillis();
        result.getProfileOnthewebCodeList().stream().forEach(e ->
                e.setSnsTypeName(CodeValid(e.getSnsTypeCd()))
        );
        //area18
        long area18 = System.currentTimeMillis();
        result.getProfileOnthewebCodeList().stream().forEach(e ->
                e.setSnsTypeIconUrl(AdminSNScodIconURLService(e.getSnsTypeCd()))
        );
        //area19
        long area19 = System.currentTimeMillis();
        result.setProfileSkillCodeList(selectList.getProfileSkill()
                .stream().map(e ->
                        modelMapper.map(e, ProfileSkillCodeName.class))
                .collect(Collectors.toList()));
        //area20
        long area20 = System.currentTimeMillis();
        result.getProfileSkillCodeList().stream().forEach(
                e -> e.setSkillDetailTypeCdName(CodeValid(e.getSkillDetailTypeCd()))
        );
        //area21
        long area21 = System.currentTimeMillis();
        result.setProfileTimezoneCodeList(selectList.getProfileTimezone()
                .stream().map(e ->
                        modelMapper.map(e, ProfileTimezoneCodeName.class))
                .collect(Collectors.toList()));
        //area22
        long area22 = System.currentTimeMillis();
        if (result.getProfileTimezoneCodeList() != null) {
            result.getProfileTimezoneCodeList().stream().forEach(e ->
                    e.setTimezoneSeqName(
                            e.getTimezoneSeq() == 0 ? "0" :
                                    adminTimezoneCodeRepository
                                            .findByTimezoneSeq(e.getTimezoneSeq())
                                            .orElse(null).getTimezoneName()));
        }
        //area23
        long area23 = System.currentTimeMillis();
        result.getProfileSkillCodeList().stream().forEach(e ->
                e.setSkillDetailTypeCdName(CodeValid(e.getSkillDetailTypeCd()))
        );
        //area24
        long area24 = System.currentTimeMillis();
        result.setProfileUseInfoCodeList(selectList.getProfileUseInfo()
                .stream().map(e ->
                        modelMapper.map(e, ProfileUseInfoCodeName.class))
                .collect(Collectors.toList()));
        //area25
        long area25 = System.currentTimeMillis();
//        res.setResult(profileFind);

        logger.info("area1 - {}", (area2 - area1) / 100);
        logger.info("area2 - {}", (area3 - area2) / 100);
        logger.info("area3 - {}", (area4 - area3) / 100);
        logger.info("area4 - {}", (area5 - area4) / 100);
        logger.info("area5 - {}", (area6 - area5) / 100);
        logger.info("area6 - {}", (area7 - area6) / 100);
        logger.info("area7 - {}", (area8 - area7) / 100);
        logger.info("area8 - {}", (area9 - area8) / 100);
        logger.info("area9 - {}", (area10 - area9) / 100);
        logger.info("area10 - {}", (area11 - area10) / 100);
        logger.info("area11 - {}", (area12 - area11) / 100);
        logger.info("area12 - {}", (area13 - area12) / 100);
        logger.info("area13 - {}", (area14 - area13) / 100);
        logger.info("area14 - {}", (area15 - area14) / 100);
        logger.info("area15 - {}", (area16 - area15) / 100);
        logger.info("area16 - {}", (area17 - area16) / 100);
        logger.info("area17 - {}", (area18 - area17) / 100);
        logger.info("area18 - {}", (area19 - area18) / 100);
        logger.info("area19 - {}", (area20 - area19) / 100);
        logger.info("area20 - {}", (area21 - area20) / 100);
        logger.info("area21 - {}", (area22 - area21) / 100);
        logger.info("area22 - {}", (area23 - area22) / 100);
        logger.info("area23 - {}", (area24 - area23) / 100);
        logger.info("area24 - {}", (area25 - area24) / 100);

        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    private String CodeValid(String code) {
        String result = "";
        if (code.length() < 3) {
            return null;
        }
        String switchString = code.substring(0, 3);
        switch (switchString) {
            case "NOT":
            case "TAP":
            case "RYL":
            case "TIL":
            case "ENC":
            case "GOV":
            case "HED":
            case "ACT":
            case "ISO":
            case "APS":
            case "BSL":
            case "COF":
            case "SIC":
            case "GND":
            case "ORG":
            case "MSG":
            case "EMO":
            case "PER":
            case "CMN":
            case "KST":
            case "SND":
            case "TMP":
            case "TST":
            case "BIT":
            case "HSC":
            case "IND":
            case "MED":
            case "PJC":
            case "PJT":
            case "PWL":
            case "SGN":
            case "SST":
            case "SVR":
            case "UPL":
            case "COP":
            case "SAV":
            case "SSF":
            case "SMK":
            case "SSK":
            case "SGK":
            case "VEF":
            case "HSN":
            case "HQA":
            case "PRD":
            case "PPS":
            case "PWM":
            case "PWE":
            case "PWP":
            case "ARG":
            case "RPT":
                if (adminCommonDetailCodeRepository.existsByDcode(code)) {
                    return result = adminCommonDetailCodeRepository.findByDcode(code).orElse(null).getVal();
                }
                break;
            case "ANR":
                if (adminAnrCodeRepository.existsByAnrCd(code)) {
                    return result = adminAnrCodeRepository.findByAnrCd(code).orElse(null).getAnrServiceName();
                }
                break;
            case "COT":
                if (adminContinentCodeRepository.existsByContinentCd(code)) {
                    return result = adminContinentCodeRepository.findByContinentCd(code).orElse(null).getContinent();
                }
                break;

            case "CNT":
                if (adminCountryCodeRepository.existsByCountryCd(code)) {
                    return result = adminCountryCodeRepository.findByCountryCd(code).orElse(null).getCountry();
                }
                break;

            case "CUR":
                if (adminCurrencyCodeRepository.existsByCurrencyCd(code)) {
                    return result = adminCurrencyCodeRepository.findByCurrencyCd(code).orElse(null).getCurrency();
                }
                break;

            case "LAN":
                if (adminLanguageCodeReository.existsByLanguageCd(code)) {
                    return result = adminLanguageCodeReository.findByLanguageCd(code).orElse(null).getLanguage();
                }
                break;
            case "NRO":
            case "PRO":
                if (adminOrganizationRepository.existsByOrgCd(code)) {
                    return result = adminOrganizationRepository.findByOrgCd(code).orElse(null).getOrgName();
                }
                break;
            case "DEX":
                if (adminRoleDetailCodeRepository.existsByDcode(code)) {
                    return result = adminRoleDetailCodeRepository.findByDcode(code).orElse(null).getRole();
                }

                break;
            case "SNS":
                if (adminSnsCodeRepository.existsBySnsCd(code)) {
                    return result = adminSnsCodeRepository.findBySnsCd(code).orElse(null).getSnsName();
                }
                break;
            case "SGG":
            case "SGA":
            case "SGS":
            case "SGT":
            case "SGE":
            case "SGP":
            case "SGM":
            case "SGI":
            case "SGV":
            case "SGC":
                if (adminSongCodeRepository.existsByCode(code)) {
                    return result = adminSongDetailCodeRepository.findByDcode(code).orElse(null).getVal();
                }
                break;
        }
        return result;
    }

    public String AdminSNScodIconURLService(String code) {
        String iconUrl = "";
        if (adminSnsCodeRepository.existsBySnsCd(code)) {
            return iconUrl = adminSnsCodeRepository.findBySnsCd(code).orElse(null).getSnsIconUrl();
        }
        return iconUrl;
    }

    public Response test111() {
        Response res = new Response();
        res.setResult(codeConfig.getCodeList());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response test222() {
        Response res = new Response();
        res.setResult(codeConfig.getSnsIconURLList());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
//    public Response test333() {
//        Response res = new Response();
//        res.setResult(codeConfig.getTimzoneCodeList());
//        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
//        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
//        return res;
//    }


    public Response ProfilePannelV2SelectBeanCodeService(Long profileId) {
        Response res = new Response();

        //area1
        long area1 = System.currentTimeMillis();
        ProfilePannelResponseV2 result = new ProfilePannelResponseV2();
        Profile selectList = profileRepository.findByProfileId(profileId)
                .orElse(null);
        //area2
        long area2 = System.currentTimeMillis();
        logger.info("=======CurrentCity===");
        result = modelMapper.map(selectList, ProfilePannelResponseV2.class);
        if (result.getCurrentCityCountryCd() != null
        ) {
            result.setCurrentCityCountryCdName(codeConfig.getCodeList().get(result.getCurrentCityCountryCd()));
        }
        //area3
        logger.info("=======Hometown===");
        long area3 = System.currentTimeMillis();
        if (result.getHomeTownCountryCd() != null
        ) {
            result.setHomeTownCountryCdName(codeConfig.getCodeList().get(result.getHomeTownCountryCd()));
        }

        if (selectList.getProfilePosition() != null) {
            //area4
            long area4 = System.currentTimeMillis();
            logger.info("=======position===");
            result.setProfilePositionCodeList(
                    selectList.getProfilePosition()
                            .stream().map(e -> modelMapper.map(e, ProfilePositionCodeName.class)).collect(Collectors.toList()));
            //area5
            long area5 = System.currentTimeMillis();
            logger.info("=======position2===");
            ProfileCompanyBaseDTO aa = new ProfileCompanyBaseDTO();

            result.getProfilePositionCodeList().stream().forEach(e ->
                    e.setProfileCompanyBaseDTO(
                            e.getCompanyProfileId() == null ? null :
                            profileCompanyRepository.findById(e.getCompanyProfileId())
                                    .map(f -> modelMapper.map(f, ProfileCompanyBaseDTO.class))
                                    .orElse(null)
                    ));
        }

        if (selectList.getProfileCareer() != null) {
            //area6
            long area6 = System.currentTimeMillis();
            logger.info("=======career===");
            result.setProfileCareerCodeList(selectList.getProfileCareer()
                    .stream().map(e -> modelMapper.map(e, ProfileCareerCodeName.class))
                    .collect(Collectors.toList())
            );
            //area7
            logger.info("=======career2===");
            long area7 = System.currentTimeMillis();
            result.getProfileCareerCodeList()
                    .stream().forEach(e -> e.setLocationCityCountryCdName(
                            codeConfig.getCodeList().get(e.getLocationCityCountryCd())));
        }


        if (selectList.getProfileEducation() != null) {
            //area8
            long area8 = System.currentTimeMillis();
            logger.info("=======edu===");
            result.setProfileEducationCodeList(selectList.getProfileEducation()
                    .stream().map(e ->
                            modelMapper.map(e, ProfileEducationCodeName.class))
                    .collect(Collectors.toList()));
            //area9
            long area9 = System.currentTimeMillis();
            logger.info("=======edu2===");
            result.getProfileEducationCodeList()
                    .stream().forEach(e -> e.setLocationCityCountryCdName(
                            codeConfig.getCodeList().get(e.getLocationCityCountryCd())));
        }

        if (selectList.getProfileGender() != null) {
            //area10
            long area10 = System.currentTimeMillis();
            result.setProfileGenderCodeList(selectList.getProfileGender()
                    .stream().map(e ->
                            modelMapper.map(e, ProfileGenderCodeName.class))
                    .collect(Collectors.toList()));
            //area11
            long area11 = System.currentTimeMillis();
            result.getProfileGenderCodeList().stream().forEach(e -> e.setGenderCdName(
                    codeConfig.getCodeList().get(e.getGenderCd())));
        }
        ///////////////////////////////////////작업중
//        result.getProfileGenderCodeList()
//                        .stream().forEach(e ->
//                        e.setGenderCdName(
//                                adminCommonDetailCodeRepository.)
//                );

        if (selectList.getProfileInterestMeta() != null) {
            //area12
            long area12 = System.currentTimeMillis();
            result.setProfileInterestMetaCodeList(selectList.getProfileInterestMeta()
                    .stream().map(e ->
                            modelMapper.map(e, ProfileInterestMetaCodeName.class))
                    .collect(Collectors.toList()));
            //area13
            long area13 = System.currentTimeMillis();
            result.getProfileInterestMetaCodeList().stream().forEach(e -> e.setDetailTypeCdName(codeConfig.getCodeList().get(e.getDetailTypeCd())));

        }

        if (selectList.getProfileLanguage() != null) {
            //area14
            long area14 = System.currentTimeMillis();
            result.setProfileLanguageCodeList(selectList.getProfileLanguage()
                    .stream().map(e ->
                            modelMapper.map(e, ProfileLanguageCodeName.class))
                    .collect(Collectors.toList()));
            //area15
            long area15 = System.currentTimeMillis();
            result.getProfileLanguageCodeList().stream().forEach(e -> e.setLanguageCdName(
                    codeConfig.getCodeList().get(e.getLanguageCd())));
        }

        if (selectList.getProfileOntheweb() != null) {
            //area16
            long area16 = System.currentTimeMillis();
            result.setProfileOnthewebCodeList(selectList.getProfileOntheweb()
                    .stream().map(e ->
                            modelMapper.map(e, ProfileOnthewebCodeName.class))
                    .collect(Collectors.toList()));
            //area17
            long area17 = System.currentTimeMillis();
            result.getProfileOnthewebCodeList().stream().forEach(e ->
                    e.setSnsTypeName(codeConfig.getCodeList().get(e.getSnsTypeCd()))
            );
            //area18
            long area18 = System.currentTimeMillis();
            result.getProfileOnthewebCodeList().stream().forEach(e ->
                    e.setSnsTypeIconUrl(codeConfig.getSnsIconURLList().get(e.getSnsTypeCd()))
            );
        }

        if (selectList.getProfileSkill() != null) {
            //area19
            long area19 = System.currentTimeMillis();
            result.setProfileSkillCodeList(selectList.getProfileSkill()
                    .stream().map(e ->
                            modelMapper.map(e, ProfileSkillCodeName.class))
                    .collect(Collectors.toList()));
            //area20
            long area20 = System.currentTimeMillis();
            result.getProfileSkillCodeList().stream().forEach(
                    e -> e.setSkillDetailTypeCdName(codeConfig.getCodeList().get(e.getSkillDetailTypeCd()))
            );
        }

        if (selectList.getProfileTimezone() != null) {
            //area21
            long area21 = System.currentTimeMillis();
            result.setProfileTimezoneCodeList(selectList.getProfileTimezone()
                    .stream().map(e ->
                            modelMapper.map(e, ProfileTimezoneCodeName.class))
                    .collect(Collectors.toList()));
            //area22
            long area22 = System.currentTimeMillis();
            if (result.getProfileTimezoneCodeList() != null) {
                result.getProfileTimezoneCodeList().stream().forEach(e ->
                        e.setTimezoneSeqName(
                                e.getTimezoneSeq() == 0 ? "0" :
                                        codeConfig.getCodeList().get(String.valueOf(e.getTimezoneSeq()))));
            }
        }

        if (selectList.getProfileSkill() != null) {
            //area23
            long area23 = System.currentTimeMillis();
            result.getProfileSkillCodeList().stream().forEach(e ->
                    e.setSkillDetailTypeCdName(codeConfig.getCodeList().get(e.getSkillDetailTypeCd()))
            );
        }
        //area24
        if (selectList.getProfileUseInfo() != null) {
            long area24 = System.currentTimeMillis();
            result.setProfileUseInfoCodeList(selectList.getProfileUseInfo()
                    .stream().map(e ->
                            modelMapper.map(e, ProfileUseInfoCodeName.class))
                    .collect(Collectors.toList()));
        }

        if (selectList.getProfileAnrPitching() != null) {
            long area25 = System.currentTimeMillis();
            result.setPitchingYn(
                    selectList.getProfileAnrPitching().getPitchingYn());
        }



        //area25
        long area25 = System.currentTimeMillis();
//        res.setResult(profileFind);
/*
        logger.info("area1 - {}", (area2-area1));
        logger.info("area2 - {}", (area3-area2));
        logger.info("area3 - {}", (area4-area3));
        logger.info("area4 - {}", (area5-area4));
        logger.info("area5 - {}", (area6-area5));
        logger.info("area6 - {}", (area7-area6));
        logger.info("area7 - {}", (area8-area7));
        logger.info("area8 - {}", (area9-area8));
        logger.info("area9 - {}", (area10-area9));
        logger.info("area10 - {}", (area11-area10));
        logger.info("area11 - {}", (area12-area11));
        logger.info("area12 - {}", (area13-area12));
        logger.info("area13 - {}", (area14-area13));
        logger.info("area14 - {}", (area15-area14));
        logger.info("area15 - {}", (area16-area15));
        logger.info("area16 - {}", (area17-area16));
        logger.info("area17 - {}", (area18-area17));
        logger.info("area18 - {}", (area19-area18));
        logger.info("area19 - {}", (area20-area19));
        logger.info("area20 - {}", (area21-area20));
        logger.info("area21 - {}", (area22-area21));
        logger.info("area22 - {}", (area23-area22));
        logger.info("area23 - {}", (area24-area23));
        logger.info("area24 - {}", (area25-area24));
        logger.info("total - {}", (area25-area1));
*/
        logger.info("getIp - {}", clientIp.getClientIp(httpServletRequest));
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;

    }

}
