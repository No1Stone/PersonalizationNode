package com.sparwk.personalizationnode.profile.biz.chat;

import lombok.Data;

@Data
public class UpdateMattermost {

    private String id;
    private String imgUrl;

}
