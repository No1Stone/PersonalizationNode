package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyContact.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyContactRequest {

    @Schema(description = "CompanyProfileId")
    private Long profileId;
    private String profileContactImgUrl;
    private String profileContactDescription;
    private String contctFirstName;
    private String contctMidleName;
    private String contctLastName;
    private String contctEmail;
    private String contctPhoneNumber;
    private String verifyPhoneYn;
    private String countryCd;

}
