package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.sparwk.personalizationnode.profile.jpa.dto.ProfilePositionBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface ProfilePositionRepositoryDynamic {
    @Transactional
    Long ProfilePositionRepositoryDynamicUpdate(ProfilePositionBaseDTO entity);
}
