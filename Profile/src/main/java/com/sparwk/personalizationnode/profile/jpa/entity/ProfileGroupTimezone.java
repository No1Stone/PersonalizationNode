package com.sparwk.personalizationnode.profile.jpa.entity;

import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileGroupTimezoneId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_group_timezone")
@DynamicUpdate
@IdClass(ProfileGroupTimezoneId.class)
public class ProfileGroupTimezone {
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Id
    @Column(name = "profile_timezone_box_num", nullable = true)
    private Long profileTimezoneBoxNum;
    @Column(name = "timezone_seq", nullable = true)
    private Long timezoneSeq;
    @Column(name = "set_time_auto_yn", nullable = true)
    private String setTimeAutoYn;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;


    @Builder
    ProfileGroupTimezone(
            Long profileId,
            Long profileTimezoneBoxNum,
            Long timezoneSeq,
            String setTimeAutoYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.profileId=profileId;
        this.profileTimezoneBoxNum=profileTimezoneBoxNum;
        this.timezoneSeq=timezoneSeq;
        this.setTimeAutoYn=setTimeAutoYn;
        this.regUsr=regUsr;
        this.regDt=regDt;
        this.modUsr=modUsr;
        this.modDt=modDt;
    }

}
