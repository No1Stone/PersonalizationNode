package com.sparwk.personalizationnode.profile.biz.v1.ProfileInterestMeta.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileInterestMetaRequestList {

    List<ProfileInterestMetaRequest> profileInterestMetaRequestList;

}
