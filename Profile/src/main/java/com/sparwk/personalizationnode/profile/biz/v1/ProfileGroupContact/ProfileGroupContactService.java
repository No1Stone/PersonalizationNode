package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroup.ProfileGroupService;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact.dto.ProfileGroupContactEmailUpdateRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact.dto.ProfileGroupContactImgUpdateRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact.dto.ProfileGroupContactNameUpdateRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupContact.dto.ProfileGroupContactPhoneUpdateRequest;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileGroupBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileGroupContactBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileGroupContactRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileGroupRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@RequiredArgsConstructor
public class ProfileGroupContactService {
    private final Logger logger = LoggerFactory.getLogger(ProfileGroupService.class);
    private final ModelMapper modelMapper;
    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final ProfileGroupContactRepository profileGroupContactRepository;

    public Response ProfileGroupContactSelectService(Long profileId) {
        Response res = new Response();
        res.setResult(profileGroupContactRepository
                .findById(profileId).map(e -> modelMapper.map(e, ProfileGroupContactBaseDTO.class)).get());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGroupContactEmailUpdateService(ProfileGroupContactEmailUpdateRequest dto) {
        Response res = new Response();
        profileGroupContactRepository.UpdateProfileGroupContactEmail(dto.getProfileId(),dto.getContctEmail());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGroupContactImgUpdateService(ProfileGroupContactImgUpdateRequest dto) {
        Response res = new Response();
        profileGroupContactRepository.UpdateProfileGroupContactImg(dto.getProfileId(),dto.getProfileContactImgUrl());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGroupContactNameUpdateService(ProfileGroupContactNameUpdateRequest dto) {
        Response res = new Response();
        profileGroupContactRepository.UpdateProfileGroupContactName(
                dto.getProfileId(),dto.getContctFirstName(),dto.getContctMidleName(), dto.getContctLastName()
        );
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGroupContactPhoneUpdateService(ProfileGroupContactPhoneUpdateRequest dto) {
        Response res = new Response();
        profileGroupContactRepository.UpdateProfileGroupContactPhone(
                dto.getProfileId(),dto.getContctPhoneNumber(), dto.getVerifyPhoneYn(),dto.getCountryCd()
        );
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
