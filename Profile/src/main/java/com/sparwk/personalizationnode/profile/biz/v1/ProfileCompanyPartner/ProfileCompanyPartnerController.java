package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto.PartnerPagingRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto.ProfileCompanyPartnerRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto.ProfileCompanyPartnerRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/profile/company")
@CrossOrigin("*")
@Api(tags = "Profile Server Profile company partner API")
public class ProfileCompanyPartnerController {

    @Autowired
    private ProfileCompanyPartnerService profileCompanyPartnerService;
    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyPartnerController.class);

    @PostMapping(path = "/partner")
    public Response ProfileCompanyPartnerSaveController(@Valid @RequestBody ProfileCompanyPartnerRequest dto) {
        Response res = profileCompanyPartnerService.ProfileCompanyPartnerSaveService(dto);
        return res;
    }

    @PostMapping(path = "/partner/list")
    public Response ProfileCompanyPartnerListSaveController(@Valid @RequestBody ProfileCompanyPartnerRequestList dto) {
        Response res = profileCompanyPartnerService.ProfileCompanyPartnerListSaveService(dto);
        return res;
    }

    @GetMapping(path = "/partner/{profileId}")
    public Response ProfileCompanyPartnerSelectController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileCompanyPartnerService.ProfilecompanyPartnerSelectService(profileId);
        return res;
    }

    @GetMapping(path = "/partner/studio/{profileId}")
    public Response ProfileCompanyPartnerStudioSelectController(@PathVariable(name = "profileId")Long profileId){
        Response res = profileCompanyPartnerService.ProfileCompanyPartnerStudioSelectService(profileId);
        return res;
    }

    @PostMapping(path = "/partner/target")
    public Response ProfileCompanyPartnerTagetSelectController(@RequestBody PartnerPagingRequest dto) {
        Response res = profileCompanyPartnerService.ProfilecompanyPartnerTagetSelectService(dto);
        return res;
    }

    @PostMapping(path= "/partner/studio/slice")
    public Response PartnerStudioSelectController(@RequestBody PartnerPagingRequest dto){
        Response res = profileCompanyPartnerService.PartnerStudioSelectService(dto);
        return res;
    }

    @PostMapping(path = "/partner/studio/slice/detail")
    public Response PartnerStudioSliceDetailController(@RequestBody PartnerPagingRequest dto){
        Response res = profileCompanyPartnerService.PartnerStudioSelfPagingSelectService(dto);
        return res;
    }

    @GetMapping(path = "/test1234/{profileId}")
    public Response StreamTest111(@PathVariable(name ="profileId" )Long profileId){

        Response res = profileCompanyPartnerService.StreamTestService(profileId);
        return res;
    }



}
