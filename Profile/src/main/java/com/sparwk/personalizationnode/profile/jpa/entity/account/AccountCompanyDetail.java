package com.sparwk.personalizationnode.profile.jpa.entity.account;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_company_detail")
@DynamicUpdate
public class AccountCompanyDetail {

    @Id
    @Column(name = "accnt_id", nullable = true)
    private Long accntId;
    @Column(name = "company_name", nullable = true)
    private String companyName;
    @Column(name = "post_cd", nullable = true)
    private String postCd;
    @Column(name = "region", nullable = true)
    private String region;
    @Column(name = "city", nullable = true)
    private String city;
    @Column(name = "addr1", nullable = true)
    private String addr1;
    @Column(name = "addr2", nullable = true)
    private String addr2;
    @Column(name = "contct_first_name", nullable = true)
    private String contctFirstName;
    @Column(name = "contct_midle_name", nullable = true)
    private String contctMidleName;
    @Column(name = "contct_last_name", nullable = true)
    private String contctLastName;
    @Column(name = "contct_email", nullable = true)
    private String contctEmail;


    @Column(name = "reg_usr", updatable = false)
//first registrant
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
//@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
//@UpdateTimestamp//dbtime
    private LocalDateTime modDt;


    @Builder
    AccountCompanyDetail(
            Long accntId,
            String companyName,
            String postCd,
            String region,
            String city,
            String addr1,
            String addr2,
            String contctFirstName,
            String contctMidleName,
            String contctLastName,
            String contctEmail,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt

    ) {
        this.accntId=accntId;
        this.companyName=companyName;
        this.postCd=postCd;
        this.region=region;
        this.city=city;
        this.addr1=addr1;
        this.addr2=addr2;
        this.contctFirstName=contctFirstName;
        this.contctMidleName=contctMidleName;
        this.contctLastName=contctLastName;
        this.contctEmail=contctEmail;

        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }


}
