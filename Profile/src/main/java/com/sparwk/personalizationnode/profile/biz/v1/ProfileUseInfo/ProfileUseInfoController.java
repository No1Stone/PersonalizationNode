package com.sparwk.personalizationnode.profile.biz.v1.ProfileUseInfo;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileTimezone.ProfileTimezoneContoller;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileTimezone.dto.ProfileTimezoneRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/change")
@CrossOrigin("*")
@Api(tags = "Profile Server Profile 변경 API")
public class ProfileUseInfoController {

    @Autowired
    private ProfileUseInfoService profileUseInfoService;

    private final Logger logger = LoggerFactory.getLogger(ProfileTimezoneContoller.class);

    @ApiOperation(
            value = "프로필 WEB URL 입력",
            notes = "web URL 입력"
    )
    @PostMapping(path = "/info/{changeProfileId}")
    public Response ProfileUseInfoSaveController(@PathVariable(name = "changeProfileId")Long changeProfileId) {
        Response res = profileUseInfoService.ProfileUseInfoSaveResponseService(changeProfileId);
        return res;
    }
    @ApiOperation(
            value = "프로필 언어 정보 검색",
            notes = "언어 정보 검색"
    )
    @GetMapping(path = "/info/{profileId}")
    public Response ProfileUseInfoeSelectController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileUseInfoService.ProfileProfileUseInfoSelectResponseService(profileId);
        return res;
    }



}
