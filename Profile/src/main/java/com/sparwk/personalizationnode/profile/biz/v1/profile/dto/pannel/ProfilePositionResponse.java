package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePositionResponse {

    private Long profilePositionSeq;
    private Long profileId;
    private Long companyProfileId;
    private String companyVerifyYn;
    private String artistYn;
    private String anrYn;
    private String creatorYn;
    private String deptRoleInfo;
    private String primaryYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

//    @Builder
//    ProfilePositionResponse(
//            Long profilePositionSeq,
//            Long companyProfileId,
//            String artistYn,
//            String anrYn,
//            String deptRoleInfo,
//            String primaryYn,
//            Long regUsr,
//            LocalDateTime regDt,
//            Long modUsr,
//            LocalDateTime modDt
//            ) {
//        this.profilePositionSeq=profilePositionSeq;
//        this.companyProfileId=companyProfileId;
//        this.artistYn=artistYn;
//        this.anrYn=anrYn;
//        this.deptRoleInfo=deptRoleInfo;
//        this.primaryYn=primaryYn;
//        this.regUsr=regUsr;
//        this.regDt=regDt;
//        this.modUsr=modUsr;
//        this.modDt=modDt;
//    }

}
