package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter@Setter@Builder
@AllArgsConstructor@NoArgsConstructor
public class PartnerStudioResponse {

    @Schema(description = "파트너인지 스튜디오인지 변수명은 좀더생각...")
    private String returnType;
    @Schema(description = "Sort 기준값")
    private Long sortValue;
    private Object contents;


}
