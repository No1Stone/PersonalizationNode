package com.sparwk.personalizationnode.profile.jpa.dto;


import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileOnthewebBaseDTO {
    private Long profileId;
    private String snsTypeCd;
    private String snsUrl;
    private String useYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
