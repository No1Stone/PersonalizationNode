package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster;


import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.dto.ProfileCompanyRosterRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.dto.ProfileCompanyRosterRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/profile/company")
@CrossOrigin("*")
@Api(tags = "Profile Server Profile company roster API")
public class ProfileCompanyRosterController {


    @Autowired
    private ProfileCompanyRosterService profileCompanyRosterService;
    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyRosterController.class);



    @PostMapping(path = "/roster")
    public Response ProfileCompanyRosterSaveController(@Valid @RequestBody ProfileCompanyRosterRequest dto) {
        Response res = profileCompanyRosterService.ProfileCompanyRosterSaveService(dto);
        return res;
    }

    @PostMapping(path = "/roster/list")
    public Response ProfileCompanyRosterListSaveController(@Valid @RequestBody ProfileCompanyRosterRequestList dto) {
        logger.info(" - {}",dto);
        Response res = profileCompanyRosterService.ProfileCompanyRosterListSaveService(dto);
        return res;
    }

    @GetMapping(path = "/roster/{profileId}")
    public Response ProfileCompanyRosterSelectController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileCompanyRosterService.ProfilecompanyRosterSelectService(profileId);
        return res;
    }





}
