package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyContact;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyContact.dto.ProfileCompanyContactNameRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyContact.dto.ProfileCompanyContactRequest;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyContactBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCompanyContact;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyContactRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileCompanyRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class ProfileCompanyContactService {

    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyContactService.class);
    private final ProfileCompanyContactRepository profileCompanyContactRepository;
    private final ProfileCompanyRepository profileCompanyRepository;
    private final ModelMapper modelmapper;
    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;

    public Response ProfileCompanyContactSaveService(ProfileCompanyContactRequest dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();

        ProfileCompanyContact pcc = modelmapper.map(dto, ProfileCompanyContact.class);
        pcc.setModDt(LocalDateTime.now());
        pcc.setRegDt(LocalDateTime.now());
        pcc.setModUsr(userInfoDTO.getAccountId());
        pcc.setRegUsr(userInfoDTO.getAccountId());
        ProfileCompanyContact pccSave = profileCompanyContactRepository.save(pcc);

        res.setResult(pccSave);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileCompanyContactSelectService(Long profileId) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        ProfileCompanyContactBaseDTO result = profileCompanyContactRepository.findById(profileId)
                .map(e -> modelmapper.map(e, ProfileCompanyContactBaseDTO.class)).get();
//        res.setResult(profileCompanyRepository.findById(profileId));
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileCompanyContactUpdateService(ProfileCompanyContactRequest dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();

        Long result = profileCompanyContactRepository
                .ProfileCompanyContactRepositoryDynamicUpdate(modelmapper.map(dto, ProfileCompanyContactBaseDTO.class));

        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileCompanyContactNameUpdateService(ProfileCompanyContactNameRequest dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        ProfileCompanyContact pccBaseDTO = profileCompanyContactRepository
                .findById(dto.getProfileId()).get();

        if (dto.getContctFirstName() == null || dto.getContctFirstName().isEmpty()) {
            pccBaseDTO.setContctFirstName("");
        } else {
            pccBaseDTO.setContctFirstName(dto.getContctFirstName());
        }

        if (dto.getContctMidleName() == null || dto.getContctMidleName().isEmpty()) {
            pccBaseDTO.setContctMidleName("");
        } else {
            pccBaseDTO.setContctMidleName(dto.getContctMidleName());
        }

        if (dto.getContctLastName() == null || dto.getContctLastName().isEmpty()) {
            pccBaseDTO.setContctLastName("");
        } else {
            pccBaseDTO.setContctLastName(dto.getContctLastName());
        }

        ProfileCompanyContact result = profileCompanyContactRepository
                .save(pccBaseDTO);


        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
