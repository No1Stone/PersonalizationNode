package com.sparwk.personalizationnode.profile.biz.v2.profile.dto.pannel;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileLanguage;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileOntheweb;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfilePosition;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter@Setter@Builder
@AllArgsConstructor@NoArgsConstructor
public class ProfilePannelSelectOneResponse {
    private Long profileId;
    private Long accntId;
    private String mattermostId;
    private String fullName;
    private String stageNameYn;
    private String bthYear;
    private String bthMonth;
    private String bthDay;
    private String hireMeYn;
    private String bio;
    private String headline;
    private String currentCityCountryCd;
    private String currentCityNm;
    private String homeTownCountryCd;
    private String homeTownNm;
    private String ipiInfo;
    private String caeInfo;
    private String isniInfo;
    private String ipnInfo;
    private String nroTypeCd;
    private String nroInfo;
    private String profileImgUrl;
    private String profileBgdImgUrl;
    private String verifyYn;
    private String useYn;
    private String accomplishmentsInfo;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
    private String accntEmail;
    private String phoneNumber;


    private List<ProfilePositionPannelResponse> profilePositionPannelResponse;
    private List<ProfileLanguage> profileLanguage;
    private List<ProfileOntheweb> profileOntheweb;



}
