package com.sparwk.personalizationnode.profile.jpa.repository.code;

import com.sparwk.personalizationnode.profile.jpa.entity.code.AdminCurrencyCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminCurrencyCodeRepository extends JpaRepository<AdminCurrencyCode,Long> {

    Optional<AdminCurrencyCode> findByCurrencyCd(String cd);
    boolean existsByCurrencyCd(String cd);

}
