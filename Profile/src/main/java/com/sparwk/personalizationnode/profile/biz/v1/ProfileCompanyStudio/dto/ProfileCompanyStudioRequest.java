package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyStudio.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyStudioRequest {
    private Long profileId;
    private Long patnerId;
    private Long studioId;
    private Long ownerId;
    private String skipYn;
    private String studioName;
    private String businessLocationCd;
    private String postCd;
    private String region;
    private String city;
    private String addr1;
    private String addr2;
}
