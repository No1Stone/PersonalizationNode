package com.sparwk.personalizationnode.profile.biz.v1.ProfileTimezone;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileOntheweb.dto.ProfileOnthewebRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileOntheweb.dto.ProfileOnthewebRqeustList;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileTimezone.dto.ProfileTimezoneRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileTimezone.dto.ProfileTimezoneRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.ProfileServiceRepository;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileOnthewebBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileTimezoneBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileOnthewebRepository;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileTimezoneRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileTimezoneService {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ProfileServiceRepository profileServiceRepository;
    @Autowired
    private ProfileTimezoneRepository profileTimezoneRepository;
    @Autowired
    private HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfileTimezoneService.class);

    public Response ProfileTimezoneSaveResponseService(ProfileTimezoneRequestList dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO - {}", userInfoDTO.getAccountId());
        logger.info("userInfoDTO - {}", userInfoDTO.getLastUseProfileId());
        Response res = new Response();
        int resultCount = 0;
        for (ProfileTimezoneRequest e : dto.getProfileTimezoneRequestList()) {
            if (e.getProfileId() == null) {
                res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }
            logger.info(e.getSetTimeAutoYn());
            logger.info(" - {}", e.getTimezoneSeq());
            ProfileTimezoneBaseDTO setBaseDTO = ProfileTimezoneBaseDTO.builder()
                    .profileId(e.getProfileId())
                    .timezoneSeq(e.getTimezoneSeq())
                    .setTimeAutoYn(e.getSetTimeAutoYn())
                    .profileTimezoneBoxNum(e.getProfileTimezoneBoxNum())
                    .modUsr(userInfoDTO.getAccountId())
                    .modDt(LocalDateTime.now())
                    .regUsr(userInfoDTO.getAccountId())
                    .regDt(LocalDateTime.now())
                    .build();
            profileServiceRepository.ProfileTimezoneSaveService(setBaseDTO);
            resultCount++;
        }
        res.setResult(resultCount);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileTimezoneSelectResponseService(Long profileId) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO---{}", userInfoDTO.getAccountId());
        List<ProfileTimezoneBaseDTO> onthewebBaseDTOList =
                profileTimezoneRepository.findByProfileId(profileId)
                        .stream().map(e -> modelMapper.map(e, ProfileTimezoneBaseDTO.class)).collect(Collectors.toList());
        Response res = new Response();
        res.setResult(onthewebBaseDTOList);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
