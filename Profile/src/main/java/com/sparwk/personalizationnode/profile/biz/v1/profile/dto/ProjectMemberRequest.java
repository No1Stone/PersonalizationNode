package com.sparwk.personalizationnode.profile.biz.v1.profile.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectMemberRequest {
    @Schema(description = "멤버 프로필 아이디 리스트 콤마로 해주세요")
    private String profileList;

}
