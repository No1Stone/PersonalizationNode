package com.sparwk.personalizationnode.profile.biz.v2.profile.dto.panneldto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileUseInfoCodeName {
    private Long accntId;
    private Long lastUseProfileId;
    private LocalDateTime LastUseDt;

}
