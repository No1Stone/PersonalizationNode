package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileEducationBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.QProfileEducation;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class ProfileEducationRepositoryDynamicImpl implements ProfileEducationRepositoryDynamic {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(ProfileEducationRepositoryDynamicImpl.class);
    private QProfileEducation qProfileEducation = QProfileEducation.profileEducation;

    private ProfileEducationRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long ProfileEducationRepositoryDynamicUpdate(ProfileEducationBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProfileEducation);

        if (entity.getEduOrganizationNm() == null || entity.getEduOrganizationNm().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileEducation.eduOrganizationNm, entity.getEduOrganizationNm());
        }
        if (entity.getEduCourseNm() == null || entity.getEduCourseNm().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileEducation.eduCourseNm, entity.getEduCourseNm());
        }
        if (entity.getLocationCityCountryCd() == null || entity.getLocationCityCountryCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileEducation.locationCityCountryCd, entity.getLocationCityCountryCd());
        }
        if (entity.getLocationCityNm() == null || entity.getLocationCityNm().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileEducation.locationCityNm, entity.getLocationCityNm());
        }
        if (entity.getPresentYn() == null || entity.getPresentYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileEducation.presentYn, entity.getPresentYn());
        }

        if (entity.getProfileEduSeq() != null) {
            jpaUpdateClause.set(qProfileEducation.profileEduSeq, entity.getProfileEduSeq());
        }

        if (entity.getEduStartDt() == null || entity.getEduStartDt().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileEducation.eduStartDt, entity.getEduStartDt());
        }
        if (entity.getEduEndDt() == null || entity.getEduEndDt().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileEducation.eduEndDt, entity.getEduEndDt());
        }

        if (entity.getFilePath() == null || entity.getFilePath().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileEducation.filePath, entity.getFilePath());
        }


        if (entity.getRegUsr() != null) {
            jpaUpdateClause.set(qProfileEducation.regUsr, entity.getRegUsr());
        }
        if (entity.getRegDt() != null) {
            jpaUpdateClause.set(qProfileEducation.regDt, entity.getRegDt());
        }
        if (entity.getModUsr() != null) {
            jpaUpdateClause.set(qProfileEducation.modUsr, entity.getModUsr());
        }
        if (entity.getModDt() != null) {
            jpaUpdateClause.set(qProfileEducation.modDt, entity.getModDt());
        }


        jpaUpdateClause.where(qProfileEducation.eduSeq.eq(entity.getEduSeq()));
        jpaUpdateClause.where(qProfileEducation.profileId.eq(entity.getProfileId()));

        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
