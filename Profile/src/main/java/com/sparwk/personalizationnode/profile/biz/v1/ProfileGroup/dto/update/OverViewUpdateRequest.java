package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroup.dto.update;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OverViewUpdateRequest {

    private Long profileId;
    private String overView;

}
