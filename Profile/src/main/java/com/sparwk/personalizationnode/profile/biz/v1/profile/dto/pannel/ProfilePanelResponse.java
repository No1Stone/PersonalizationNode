package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.pannel;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileUseInfo.dto.ProfileUseInfoResponse;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileGenderBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Function;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePanelResponse {

    private ProfileResponse profileResponse;
    private List<ProfileGenderResponse> profileGender;
    private List<ProfileInterestMetaResponse> profileInterestMeta;
    private List<ProfileLanguageResponse> profileLanguage;
    private List<ProfilePositionResponse> profilePosition;
    private List<ProfileEducationResponse> profileEducation;
    private List<ProfileCareerResponse> profileCareer;
    private List<ProfileSkillResponse> profileSkill;
    private List<ProfileOnthewebResponse> profileOntheweb;
    private List<ProfileTimezoneResponse> profileTimezone;
    private List<ProfileCompanyResponse> profileCompany;
    private List<ProfileUseInfoResponse> profileUseInfo;

}
