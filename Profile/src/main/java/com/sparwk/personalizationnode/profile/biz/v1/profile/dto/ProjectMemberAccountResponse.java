package com.sparwk.personalizationnode.profile.biz.v1.profile.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectMemberAccountResponse {



    @Schema(description = "사용자 이메일")
    @Size(max = 100, message = "Account email NotNull")
    private String accntEmail;


    @Schema(description = "사용자 국가 코드")
    @Size(max = 9, message = "AccounttyCd max lenth = 9")
    private String countryCd;

    @Size(max = 20, message = "")
    @Schema(description = "사용자 핸드폰 번호")
    private String phoneNumber;

}
