package com.sparwk.personalizationnode.profile.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGroupContactBaseDTO {
    private Long profileId ;
    private String profileContactImgUrl ;
    private String profileContactDescription ;
    private String contctFirstName ;
    private String contctMidleName ;
    private String contctLastName ;
    private String contctEmail ;
    private String contctPhoneNumber;
    private String verifyPhoneYn ;
    private String countryCd;
    private Long regUsr ;
    private LocalDateTime regDt ;
    private Long modUsr ;
    private LocalDateTime modDt  ;

}
