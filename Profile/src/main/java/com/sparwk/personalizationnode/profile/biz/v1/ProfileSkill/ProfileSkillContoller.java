package com.sparwk.personalizationnode.profile.biz.v1.ProfileSkill;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileSkill.dto.ProfileSkillDto;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileSkillBaseDTO;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/V1/skill")
@CrossOrigin("*")
@Api(tags = "Profile Server Skill API")
public class ProfileSkillContoller {

    @Autowired
    private ProfileSkillService profileSkillService;
    private final Logger logger = LoggerFactory.getLogger(ProfileSkillContoller.class);

    @PostMapping(path = "/info")
    public Response ProfileSkillSaveController(@Valid @RequestBody ProfileSkillDto dto) {
        Response res = profileSkillService.ProfileSkillSave(dto);
        return res;
    }

    @GetMapping(path = "/info/{profileId}")
    public Response ProfileSkillSelect(@PathVariable(name = "profileId")Long profileId ) {
        Response res = profileSkillService.ProfileSkillSelect(profileId);
        return res;
    }

    @PostMapping(path = "/delete")
    public Response ProfileSkillDeleteController(@RequestBody ProfileSkillDto dto){
        Response res = profileSkillService.profileSkillDelete(dto);
    return res;
    }

}
