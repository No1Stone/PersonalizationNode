package com.sparwk.personalizationnode.profile.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileEducationBaseDTO {
    private Long profileId;
    private String eduOrganizationNm;
    private String eduCourseNm;
    private String locationCityCountryCd;
    private String locationCityNm;
    private String presentYn;
    private Long eduSeq;
    private String filePath;
    private Long profileEduSeq;
    private String eduStartDt;
    private String eduEndDt;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

}
