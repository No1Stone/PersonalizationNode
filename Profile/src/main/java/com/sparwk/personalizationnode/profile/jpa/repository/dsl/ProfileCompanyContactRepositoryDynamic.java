package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCompanyContactBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface ProfileCompanyContactRepositoryDynamic {
    @Transactional
    Long ProfileCompanyContactRepositoryDynamicUpdate(ProfileCompanyContactBaseDTO entity);

}
