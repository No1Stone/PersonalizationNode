package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.sparwk.personalizationnode.profile.jpa.dto.ProfileEducationBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface ProfileEducationRepositoryDynamic {
    @Transactional
    Long ProfileEducationRepositoryDynamicUpdate(ProfileEducationBaseDTO entity);

}
