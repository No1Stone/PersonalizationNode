package com.sparwk.personalizationnode.profile.biz.v1.ProfileCareer;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCareer.dto.ProfileCareerDeleteRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCareer.dto.ProfileCareerDto;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCareer.dto.ProfileCareerDtoList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/carerr")
@CrossOrigin("*")
@Api(tags = "Profile Server Carerr API")
public class ProfileCareerContoller {

    @Autowired
    private ProfileCareerService profileCareerService;

    private final Logger logger = LoggerFactory.getLogger(ProfileCareerContoller.class);

    @ApiOperation(
            value = "프로필 캐리어 정보 입력",
            notes = "캐리어 정보 입력"
    )
    @PostMapping(path = "/info")
    public Response ProfileCareerSaveController(@Valid @RequestBody ProfileCareerDto dto) {
        Response res = profileCareerService.ProfileCareerSave(dto);
        return res;
    }

    @ApiOperation(
            value = "프로필 캐리어 정보 검색",
            notes = "캐리어 정보 검색"
    )
    @GetMapping(path = "/info/{profileId}")
    public Response ProfileCareerSelect(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileCareerService.ProfileCareerSelect(profileId);
        return res;
    }


    @ApiOperation(
            value = "프로필 캐리어 정보 업데이트",
            notes = "캐리어 정보 업데이트"
    )
    @PostMapping(path = "/info/update")
    public Response ProfileCareerDynamicUpdateController(@Valid @RequestBody ProfileCareerDto dto) {
        Response res = profileCareerService.ProfileDynamicUpdate(dto);
        return res;
    }

    @PostMapping(path = "/info/list")
    public Response profileCareerSaveList(@Valid @RequestBody ProfileCareerDtoList dto){
        Response res = profileCareerService.ProfileCareerSaveListService(dto);
        return res;
    }

    @PostMapping(path = "/delete")
    public Response profileCareerDelete(@Valid @RequestBody ProfileCareerDeleteRequest dto){
        Response res = profileCareerService.ProfileCareerDeleteService(dto);
        return res;
    }
}
