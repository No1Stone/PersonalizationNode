package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.rsvp;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter@Setter@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProjectBaseDTO {
    private long projId;
    private long projOwner;
    private String projAvalYn;
    private String genderCd;
    private String projIndivCompType;
    private String projPublYn;
    private String projPwd;
    private String projInvtTitle;
    private String projInvtDesc;
    private String projWhatFor;
    private String pitchProjTypeCd;
    private String projWorkLocat;
    private String projTitle;
    private String projDesc;
    private LocalDate projDdlDt;
    private String projCondCd;
    private long minVal;
    private long maxVal;
    private String recruitYn;
    private String avatarFileUrl;
    private String leaveYn;
    private String completeYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
    private List<ProjectMembBaseDTO> projectMemb;
}
