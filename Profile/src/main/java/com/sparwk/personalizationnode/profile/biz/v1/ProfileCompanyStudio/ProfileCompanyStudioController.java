package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyStudio;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.ProfileCompanyRosterController;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.ProfileCompanyRosterService;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyRoster.dto.ProfileCompanyRosterRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyStudio.dto.ProfileCompanyStudioDeleteRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyStudio.dto.ProfileCompanyStudioRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyStudio.dto.ProfileCompanyStudioRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/profile/company")
@CrossOrigin("*")
@Api(tags = "Profile Server Profile company studio API")
public class ProfileCompanyStudioController {


    @Autowired
    private ProfileCompanyStudioService ProfileCompanyStudioService;
    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyStudioController.class);

    @PostMapping(path = "/studio")
    public Response ProfileCompanyStudioSaveController(@Valid @RequestBody ProfileCompanyStudioRequest dto) {
        Response res = ProfileCompanyStudioService.ProfileCompanyStudioSaveService(dto);
        return res;
    }

    @PostMapping(path = "/studio/list")
    public Response ProfileCompanyStudioListSaveController(@Valid @RequestBody ProfileCompanyStudioRequestList dto) {
        Response res = ProfileCompanyStudioService.ProfileCompanyStudioListSaveService(dto);
        return res;
    }


    @GetMapping(path = "/studio/{profileId}")
    public Response ProfileCompanyStudioSelectController(@PathVariable(name = "profileId") Long profileId) {
        Response res = ProfileCompanyStudioService.ProfilecompanyStudioSelectService(profileId);
        return res;
    }

    @PostMapping(path = "/studio/delete")
    public Response ProfileCompanyStudioDeleteController(@RequestBody ProfileCompanyStudioDeleteRequest dto){
        Response res = ProfileCompanyStudioService.ProfileCompanyStudioDeleteService(dto);
        return res;
    }

}
