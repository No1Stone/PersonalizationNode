package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto.CompanyProfileSearchRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto.ProfileCompanyList;
import com.sparwk.personalizationnode.profile.biz.v1.profile.ProfileService;
import com.sparwk.personalizationnode.profile.biz.v1.profile.dto.ProfileArray;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/V1/profile/company/public")
@CrossOrigin("*")
@Api(tags = "Profile Server Profile Company Public API")
public class ProfileCompanyPublicContoller {

    @Autowired
    private ProfileCompanyService profileCompanyService;
    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyPublicContoller.class);

//    @GetMapping(path = "/list/{page}")
//    public Response publicCompanyListpage(@PathVariable(name = "page")int page){
//        logger.info("=======================");
//        Response res = profileCompanyService.FindByCompanyListpageGet(page);
//        return res;
//    }


    @PostMapping(path = "/list/slice")
    public Response publicCompanyListslaceController(@RequestBody CompanyProfileSearchRequest dto){
        logger.info("=======================");
        Response res = profileCompanyService.FindByCompanyListSlaceGetAll(dto);
        return res;
    }


    @GetMapping(path = "/list/slice/type/{slice}/{type}")
    public Response publicCompanyListslaceController(@PathVariable(name = "slice")int slice
    ,@PathVariable(name = "type")String type
    ){
        logger.info("=======================");
        Response res = profileCompanyService.FindByCompanyTypeListSlaceGetAll(slice, type);
        return res;
    }



}
