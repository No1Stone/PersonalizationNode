package com.sparwk.personalizationnode.profile.biz.v1.profile.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongServerRequest {
    @Schema(description = "프로필 아이디 /나중에 토큰에서 정보받아오는걸로 수정후 삭제예정")
    private Long profileId;
    @Schema(description = "송 아이디")
    private String songVersionCd;
    @Schema(description = "노래 소유자")
    private Long songOwner;
    @Schema(description = "언어")
    private String langCd;
    @Schema(description = "노래 제목")
    private String songTitle;
    @Schema(description = "노래 부제목")
    private String songSubTitle;
    @Schema(description = "노래 설명")
    private String description;
    @Schema(description = "아바타이미지 사용여부")
    private String avatarImgUseYn;
    @Schema(description = "아바타 이미지 경로")
    private String avatarFileUrl;
    @Schema(description = "노래 이용 상태")
    private String songAvailYn;
    @Schema(description = "노래 진행 상태 코드")
    private String songStatusCd;
    @Schema(description = "노래 상태 변경 일자")
    private LocalDateTime songStatusModDt;



}
