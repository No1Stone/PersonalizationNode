package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.profilecreate;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class RequestProfileCreate {

    @Schema(description = "계정 여권 정보")
    private AccountPassportRequest accountPassportDto;

    @Schema(description = "프로필 기본 정보")
    private ProfileRequest profileRequest;

    @Schema(description = "프로필 회사정보 리스트")
    private List<ProfilePositionRequest> profilePositionRequestList;

    @Schema(description = "관심정보")
    private List<ProfileInterestMetaRequest> profileInterestMetaRequestList;

    @Schema(description = "프로필 성별 정보")
    private List<ProfileGenderRequest> profileGenderRequestList;

    @Schema(description = "프로필 언어 정보")
    private List<ProfileLanguageRequest> profileLanguageRequestList;

}
