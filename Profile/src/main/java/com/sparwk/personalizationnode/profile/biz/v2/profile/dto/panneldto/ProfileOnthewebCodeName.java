package com.sparwk.personalizationnode.profile.biz.v2.profile.dto.panneldto;


import com.sparwk.personalizationnode.profile.jpa.dto.ProfileOnthewebBaseDTO;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileOnthewebCodeName extends ProfileOnthewebBaseDTO {

    private String snsTypeName;
    private String snsTypeIconUrl;

}
