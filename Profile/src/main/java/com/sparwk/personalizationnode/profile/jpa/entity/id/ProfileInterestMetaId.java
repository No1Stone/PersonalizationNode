package com.sparwk.personalizationnode.profile.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileInterestMetaId implements Serializable {
    private String kindTypeCd;
    private Long profileId;
    private String detailTypeCd;
}
