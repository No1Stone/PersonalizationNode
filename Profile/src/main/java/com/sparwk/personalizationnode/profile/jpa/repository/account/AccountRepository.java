package com.sparwk.personalizationnode.profile.jpa.repository.account;

import com.sparwk.personalizationnode.profile.jpa.entity.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    int countByAccntId(Long accId);

    Optional<Account> findByAccntId(Long aLong);

    List<Account> findByAccntIdInAndUseYn(List<Long> acc, String useYn);
}
