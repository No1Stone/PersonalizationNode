package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileTimezone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileTimezoneRepository extends JpaRepository<ProfileTimezone, Long> {

    List<ProfileTimezone> findByProfileId(Long profileId);
}
