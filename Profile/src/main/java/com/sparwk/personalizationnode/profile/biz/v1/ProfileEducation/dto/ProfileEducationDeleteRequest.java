package com.sparwk.personalizationnode.profile.biz.v1.ProfileEducation.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileEducationDeleteRequest {
    @Schema(description = "프로필 아이디")
    private Long profileId;
    private Long eduSeq;

}
