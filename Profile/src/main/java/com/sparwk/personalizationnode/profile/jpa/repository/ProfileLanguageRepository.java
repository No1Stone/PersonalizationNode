package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileLanguage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProfileLanguageRepository extends JpaRepository<ProfileLanguage, String> {

    List<ProfileLanguage> findByProfileId(Long profileId);
    boolean existsByProfileId(Long profileId);
    @Transactional
    void deleteByProfileIdAndAndLanguageCd(Long profileId, String cd);
}
