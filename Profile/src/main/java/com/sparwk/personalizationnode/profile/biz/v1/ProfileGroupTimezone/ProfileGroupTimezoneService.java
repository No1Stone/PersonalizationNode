package com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupTimezone;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupTimezone.dto.ProfileGroupTimezoneRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileGroupTimezone.dto.ProfileGroupTimezoneRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileGroupTimezoneBaseDTO;
import com.sparwk.personalizationnode.profile.jpa.entity.ProfileGroupTimezone;
import com.sparwk.personalizationnode.profile.jpa.repository.ProfileGroupTimezoneRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileGroupTimezoneService {
    private final Logger logger = LoggerFactory.getLogger(ProfileGroupTimezoneService.class);
    private final ModelMapper modelMapper;
    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final ProfileGroupTimezoneRepository profileGroupTimezoneRepository;

    public Response ProfileGroupTimezoneSelectService(Long profileId) {
        Response res = new Response();
        res.setResult(profileGroupTimezoneRepository
                .findByProfileId(profileId).stream().map(e -> modelMapper
                        .map(e, ProfileGroupTimezoneBaseDTO.class)).collect(Collectors.toList()));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileGroupTimezoneSaveListService(ProfileGroupTimezoneRequestList dto) {
        Response res = new Response();
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        List<Long> befo = new ArrayList<>();
        List<Long> after = new ArrayList<>();
        logger.info("==={}",dto);

        if (profileGroupTimezoneRepository.existsByProfileId(dto.getProfileGroupTimezoneRequest().get(0).getProfileId())) {
            befo = profileGroupTimezoneRepository.findByProfileId(dto.getProfileGroupTimezoneRequest().get(0).getProfileId())
                    .stream().map(e -> modelMapper.map(e.getProfileTimezoneBoxNum(), Long.class)).collect(Collectors.toList());
        }

        for (ProfileGroupTimezoneRequest e : dto.getProfileGroupTimezoneRequest()) {
            after.add(e.getProfileTimezoneBoxNum());
            ProfileGroupTimezone save = modelMapper.map(e, ProfileGroupTimezone.class);
            save.setModUsr(userInfoDTO.getAccountId());
            save.setRegUsr(userInfoDTO.getAccountId());
            save.setModDt(LocalDateTime.now());
            save.setRegDt(LocalDateTime.now());
            profileGroupTimezoneRepository.save(save);
        }
        befo.removeAll(after);
        for (Long e : befo) {
            profileGroupTimezoneRepository
                    .deleteByProfileIdAndProfileTimezoneBoxNum(dto.getProfileGroupTimezoneRequest().get(0).getProfileId(), e);
        }
        res.setResult(profileGroupTimezoneRepository
                .findByProfileId(dto.getProfileGroupTimezoneRequest().get(0).getProfileId())
                .stream().map(e -> modelMapper
                        .map(e, ProfileGroupTimezoneBaseDTO.class)).collect(Collectors.toList()));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


}
