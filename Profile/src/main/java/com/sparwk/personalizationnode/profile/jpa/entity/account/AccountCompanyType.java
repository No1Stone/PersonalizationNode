package com.sparwk.personalizationnode.profile.jpa.entity.account;

import com.sparwk.personalizationnode.profile.jpa.entity.account.id.AccountCompanyTypeId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_company_type")
@DynamicUpdate
@IdClass(AccountCompanyTypeId.class)
public class AccountCompanyType {

    @Id
    @Column(name = "accnt_id")
    private Long accntId;
    @Id
    @Column(name = "company_cd")
    private String companyCd;
    @Column(name = "company_license_file_url", nullable = true)
    private String companyLicenseFileUrl;
    @Column(name = "company_license_verify_yn", nullable = true)
    private String companyLicenseVerifyYn;
    @Column(name = "company_license_file_name", nullable = true)
    private String companyLicenseFileName;

    @Builder
    AccountCompanyType
            (
                    Long accntId,
                    String companyCd,
                    String companyLicenseFileUrl,
                    String companyLicenseVerifyYn,
                    String companyLicenseFileName
            ) {
        this.accntId = accntId;
        this.companyCd = companyCd;
        this.companyLicenseFileUrl = companyLicenseFileUrl;
        this.companyLicenseVerifyYn = companyLicenseVerifyYn;
        this.companyLicenseFileName = companyLicenseFileName;
    }


}
