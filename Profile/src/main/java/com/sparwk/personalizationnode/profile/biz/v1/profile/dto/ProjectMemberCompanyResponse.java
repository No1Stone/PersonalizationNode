package com.sparwk.personalizationnode.profile.biz.v1.profile.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectMemberCompanyResponse {

    @Schema(description = "회사코드")
    @Size(max = 9)
    private String companyCd;

    @Schema(description = "국가코드")
    @Size(max = 9)
    private String countryCd;


}
