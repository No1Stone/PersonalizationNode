package com.sparwk.personalizationnode.profile.jpa.repository.dsl;

import com.sparwk.personalizationnode.profile.jpa.dto.ProfileCareerBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface ProfileCareerRepositoryDynamic {
    @Transactional
    Long ProfileCareerRepositoryDynamicUpdate(ProfileCareerBaseDTO entity);
}
