package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyOntheweb;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyOntheweb.dto.ProfileCompanyOnthewebRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyOntheweb.dto.ProfileCompanyOnthewebRequestList;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto.ProfileCompanyPartnerRequest;
import com.sparwk.personalizationnode.profile.biz.v1.ProfileCompanyPartner.dto.ProfileCompanyPartnerRequestList;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/profile/company")
@CrossOrigin("*")
@Api(tags = "Profile Server Profile company On The Web API")
public class ProfileCompanyOnthewebController {

    @Autowired
    private ProfileCompanyOnthewebService profileCompanyOnthewebService;
    private final Logger logger = LoggerFactory.getLogger(ProfileCompanyOnthewebController.class);


    @PostMapping(path = "/ontheweb")
    public Response ProfileCompanyOnthewebSaveController(@Valid @RequestBody ProfileCompanyOnthewebRequest dto) {
        Response res = profileCompanyOnthewebService.ProfileCompanyOnthewebSaveService(dto);
        return res;
    }

    @PostMapping(path = "/ontheweb/list")
    public Response ProfileCompanyOnthewebListSaveController(@Valid @RequestBody ProfileCompanyOnthewebRequestList dto) {
        Response res = profileCompanyOnthewebService.ProfileCompanyOnthewebListSaveService(dto);
        return res;
    }

    @GetMapping(path = "/ontheweb/{profileId}")
    public Response ProfileCompanyOnthewebSelectController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileCompanyOnthewebService.ProfilecompanyOnthewebSelectService(profileId);
        return res;
    }


}
