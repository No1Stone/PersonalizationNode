package com.sparwk.personalizationnode.profile.biz.v1.profile.dto.rsvp;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RsvpSendFrom {
    private Long projId;
    private Long rsvpProfileId;
    private String SenderEmail;
    private String receiverEmail;
    private int memberCount;
    private String projectPassword;
    private String projectTitle;
    private String projectDesctip;
    private String projectSubDesctip;
    private String ownerName;
    private String ownerHeadLine;
    private String ownerProfileImg;
    private String memberName;
}
