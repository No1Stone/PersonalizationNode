package com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TokenResult {

    private String Token;
    private Long AccountId;

}
