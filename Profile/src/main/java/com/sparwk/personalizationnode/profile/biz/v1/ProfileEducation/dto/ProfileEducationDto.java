package com.sparwk.personalizationnode.profile.biz.v1.ProfileEducation.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileEducationDto {
    @Schema(description = "프로필 아이디")
    private Long profileId;
    @Schema(description = "교육기관")
    private String eduOrganizationNm;
    @Schema(description = "교육명")
    private String eduCourseNm;
    @Schema(description = "교육기관이 위치한 나라")
    private String locationCityCountryCd;
    @Schema(description = "교육기관이 위치한 도시")
    private String locationCityNm;
    @Schema(description = "교육기관 수료과목 결과")
    private String presentYn;
    @Schema(description = "교육기관 순번")
    private Long eduSeq;
    @Schema(description = "파일유알엘")
    private String filePath;
    private Long profileEduSeq;
    @Schema(description = "교육시작일")
    private String eduStartDt;
    @Schema(description = "교육종료일")
    private String eduEndDt;
}
