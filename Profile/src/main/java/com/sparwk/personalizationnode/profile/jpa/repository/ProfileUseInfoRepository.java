package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileUseInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileUseInfoRepository extends JpaRepository<ProfileUseInfo, Long> {
    List<ProfileUseInfo> findByLastUseProfileId(Long profileId);
}
