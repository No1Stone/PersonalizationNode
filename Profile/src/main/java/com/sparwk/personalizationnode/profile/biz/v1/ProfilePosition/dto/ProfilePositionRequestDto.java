package com.sparwk.personalizationnode.profile.biz.v1.ProfilePosition.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePositionRequestDto {

    @Schema(description = "프로필 회사정보 리스트")
    private List<ProfilePositionDto> profilePositionList;

}
