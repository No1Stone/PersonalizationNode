package com.sparwk.personalizationnode.profile.biz.v1.ProfileCompany.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyRequest {
    @Schema(description = "프로필 아이디")
    private Long profileId;
    private String mattermostId;
    private String profileCompanyName;
    private String profileImgUrl;
    private String profileBgdImgUrl;
    private String headline;
    private String bio;
    private String comInfoOverview;
    private String comInfoWebsite;
    private String comInfoPhone;
    private String comInfoEmail;
    private String comInfoFound;
    private String comInfoCountryCd;
    private String comInfoAddress;
    private String comInfoLat;
    private String comInfoLon;
    private String ipiNumber;
    private String ipiNumberVarifyYn;
    private String vatNumber;
    private String vatNumberVarifyYn;
    private String comInfoLocation;

}
