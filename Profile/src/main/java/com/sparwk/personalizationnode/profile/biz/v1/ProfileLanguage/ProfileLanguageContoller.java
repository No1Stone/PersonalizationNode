package com.sparwk.personalizationnode.profile.biz.v1.ProfileLanguage;

import com.sparwk.personalizationnode.profile.biz.v1.ProfileLanguage.dto.ProfileLanguageDto;
import com.sparwk.personalizationnode.profile.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.profile.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.profile.jpa.dto.ProfileLanguageBaseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/V1/language")
@CrossOrigin("*")
@Api(tags = "Profile Server Language API")
public class ProfileLanguageContoller {

    @Autowired
    private ProfileLanguageService profileLanguageService;

    private final Logger logger = LoggerFactory.getLogger(ProfileLanguageContoller.class);
    @ApiOperation(
            value = "프로필 언어 정보 입력",
            notes = "언어 정보 입력"
    )
    @PostMapping(path = "/info")
    public Response ProfileLanguageSaveController(@Valid @RequestBody ProfileLanguageDto dto) {
        Response res = profileLanguageService.ProfileLanguageSave(dto);
        return res;
    }
    @ApiOperation(
            value = "프로필 언어 정보 검색",
            notes = "언어 정보 검색"
    )
    @GetMapping(path = "/info/{profileId}")
    public Response ProfileLanguageSelect(@PathVariable(name = "profileId")Long profileId) {
        Response res = profileLanguageService.ProfileLanguageSelect(profileId);
        return res;
    }

}
