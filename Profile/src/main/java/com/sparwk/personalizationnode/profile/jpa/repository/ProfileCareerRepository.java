package com.sparwk.personalizationnode.profile.jpa.repository;

import com.sparwk.personalizationnode.profile.jpa.entity.ProfileCareer;
import com.sparwk.personalizationnode.profile.jpa.repository.dsl.ProfileCareerRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProfileCareerRepository extends JpaRepository<ProfileCareer, String>, ProfileCareerRepositoryDynamic {

    List<ProfileCareer> findByProfileId(Long profileId);
    @Transactional
    void deleteByProfileIdAndCareerSeq(Long profileId, Long careerSeq);
}
