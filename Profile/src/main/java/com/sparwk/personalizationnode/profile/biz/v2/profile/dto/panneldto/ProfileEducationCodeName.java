package com.sparwk.personalizationnode.profile.biz.v2.profile.dto.panneldto;

import com.sparwk.personalizationnode.profile.jpa.dto.ProfileEducationBaseDTO;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileEducationCodeName extends ProfileEducationBaseDTO {

    private String locationCityCountryCdName;

}
