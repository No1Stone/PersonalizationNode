package com.sparwk.personalizationnode.profile.config.filter;

import com.sparwk.personalizationnode.profile.jpa.repository.code.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.stream.Collectors;

@Configuration
@RequiredArgsConstructor
public class CodeConfig {
    private final Logger logger = LoggerFactory.getLogger(CodeConfig.class);
    private final AdminCommonDetailCodeRepository adminCommonDetailCodeRepository;
    private final AdminCountryCodeRepository adminCountryCodeRepository;
    private final AdminRoleDetailCodeRepository adminRoleDetailCodeRepository;
    private final AdminTimezoneCodeRepository adminTimezoneCodeRepository;
    private final AdminAnrCodeRepository adminAnrCodeRepository;
    private final AdminContinentCodeRepository adminContinentCodeRepository;
    private final AdminCurrencyCodeRepository adminCurrencyCodeRepository;
    private final AdminLanguageCodeReository adminLanguageCodeReository;
    private final AdminOrganizationRepository adminOrganizationRepository;
    private final AdminSnsCodeRepository adminSnsCodeRepository;
    private final AdminRoleCodeRepository adminRoleCodeRepository;
    private final AdminSongCodeRepository adminSongCodeRepository;
    private final AdminSongDetailCodeRepository adminSongDetailCodeRepository;
    private HashMap<String, String> preCodeList;
    private HashMap<String, String> preSnsURLCodeList;


    @PostConstruct
    private void CodeList() {
        HashMap<String, String> codeList = new HashMap<>();
        adminCommonDetailCodeRepository.findAll()
                .stream().map(e -> codeList.put(e.getDcode(), e.getVal())).collect(Collectors.toList());
        adminAnrCodeRepository.findAll()
                .stream().map(e -> codeList.put(e.getAnrCd(), e.getAnrServiceName())).collect(Collectors.toList());
        adminContinentCodeRepository.findAll()
                .stream().map(e -> codeList.put(e.getContinentCd(), e.getContinent())).collect(Collectors.toList());
        adminCountryCodeRepository.findAll()
                .stream().map(e -> codeList.put(e.getCountryCd(), e.getCountry())).collect(Collectors.toList());
        adminCurrencyCodeRepository.findAll()
                .stream().map(e -> codeList.put(e.getCurrencyCd(), e.getCurrency())).collect(Collectors.toList());
        adminLanguageCodeReository.findAll()
                .stream().map(e -> codeList.put(e.getLanguageCd(), e.getLanguage())).collect(Collectors.toList());
        adminOrganizationRepository.findAll()
                .stream().map(e -> codeList.put(String.valueOf(e.getOrgCd()), e.getOrgName())).collect(Collectors.toList());
        adminRoleDetailCodeRepository.findAll()
                .stream().map(e -> codeList.put(e.getDcode(), e.getRole())).collect(Collectors.toList());
        adminSnsCodeRepository.findAll()
                .stream().map(e -> codeList.put(e.getSnsCd(), e.getSnsName())).collect(Collectors.toList());
        adminSongDetailCodeRepository.findAll()
                .stream().map(e -> codeList.put(e.getDcode(), e.getVal())).collect(Collectors.toList());
        adminTimezoneCodeRepository.findAll()
                .stream().map(e -> codeList.put(String.valueOf(e.getTimezoneSeq()), e.getTimezoneName())).collect(Collectors.toList());


        this.preCodeList = codeList;
        /*
        int a1 = adminCommonDetailCodeRepository.findAll().size();
        int a2 = adminCommonDetailCodeRepository.findAll().size();
        int a3 = adminAnrCodeRepository.findAll().size();
        int a4 = adminContinentCodeRepository.findAll().size();
        int a5 = adminCountryCodeRepository.findAll().size();
        int a6 = adminCurrencyCodeRepository.findAll().size();
        int a7 = adminLanguageCodeReository.findAll().size();
        int a8 = adminOrganizationRepository.findAll().size();
        int a9 = adminRoleDetailCodeRepository.findAll().size();
        int a10 = adminSnsCodeRepository.findAll().size();
        int a11 = adminSongDetailCodeRepository.findAll().size();
        logger.info(" - {}", a1);
        logger.info(" - {}", a2);
        logger.info(" - {}", a3);
        logger.info(" - {}", a4);
        logger.info(" - {}", a5);
        logger.info(" - {}", a6);
        logger.info(" - {}", a7);
        logger.info(" - {}", a8);
        logger.info(" - {}", a9);
        logger.info(" - {}", a10);
        logger.info(" - {}", a11);
        */

    }

    @PostConstruct
    private void SnsIconURLList() {
        HashMap<String, String> codeList = new HashMap<>();
        adminSnsCodeRepository.findAll()
                .stream().map(e -> codeList.put(e.getSnsCd(), e.getSnsIconUrl())).collect(Collectors.toList());
        this.preSnsURLCodeList = codeList;
    }
//    @PostConstruct
//    private HashMap<Long, String> TimzoneCodeList() {
//        HashMap<Long, String> codeList = new HashMap<>();
//        adminTimezoneCodeRepository.findAll()
//                .stream().map(e -> codeList.put(e.getTimezoneSeq(), e.getTimezoneName())).collect(Collectors.toList());
//        return codeList;
//    }

    @Bean
    public HashMap<String, String> getCodeList() {
        return this.preCodeList;
    }

    @Bean
    public HashMap<String, String> getSnsIconURLList() {
        return this.preSnsURLCodeList;
    }

//    @Bean
//    public HashMap<Long, String> getTimzoneCodeList(){
//        HashMap<Long, String> codeList = new HashMap<>();
//        codeList = TimzoneCodeList();
//        return codeList;
//    }

}
