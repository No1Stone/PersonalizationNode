package com.sparwk.personalizationnode.profile.jpa.entity.code;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_role_code")
public class AdminRoleCode {
    @Id
    @Column(name = "role_code_seq ", nullable = true)
    private Long roleCodeSeq;
    @Column(name = "code ", nullable = true)
    private String code;
    @Column(name = "val ", nullable = true)
    private String val;
    @Column(name = "use_yn ", nullable = true)
    private String useYn;
    @Column(name = "reg_usr ", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt ", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr ", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    AdminRoleCode(
            Long roleCodeSeq,
            String code,
            String val,
            String useYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.roleCodeSeq = roleCodeSeq;
        this.code = code;
        this.val = val;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }


}
