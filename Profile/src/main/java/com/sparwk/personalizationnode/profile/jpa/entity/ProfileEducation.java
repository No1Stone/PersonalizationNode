package com.sparwk.personalizationnode.profile.jpa.entity;


import com.sparwk.personalizationnode.profile.jpa.entity.id.ProfileEducationId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_education")
@DynamicUpdate
//@IdClass(ProfileEducationId.class)
public class ProfileEducation {

    @Column(name = "edu_organization_nm", length = 50)
    private String eduOrganizationNm;
    @Column(name = "profile_id")
    private Long profileId;
    @Column(name = "edu_course_nm", nullable = true, length = 100)
    private String eduCourseNm;
    @Column(name = "file_path")
    private String filePath;
    @Column(name = "location_city_country_cd", nullable = true, length = 9)
    private String locationCityCountryCd;
    @Column(name = "location_city_nm", nullable = true, length = 50)
    private String locationCityNm;
    @Column(name = "present_yn", nullable = true, length = 1)
    private String presentYn;
    @Id
    @GeneratedValue(generator = "tb_profile_education_seq")
    @Column(name = "edu_seq")
    private Long eduSeq;
    @Column(name = "profile_edu_seq")
    private Long profileEduSeq;
    @Column(name = "edu_start_dt", nullable = true)
    private String eduStartDt;
    @Column(name = "edu_end_dt", nullable = true)
    private String eduEndDt;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    ProfileEducation(
            Long profileId,
            String eduOrganizationNm,
            String eduCourseNm,
            String locationCityCountryCd,
            String locationCityNm,
            String presentYn,
            Long eduSeq,
            String filePath,
            Long profileEduSeq,
            String eduStartDt,
            String eduEndDt,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt

    ) {
        this.profileId = profileId;
        this.eduOrganizationNm = eduOrganizationNm;
        this.eduCourseNm = eduCourseNm;
        this.locationCityCountryCd = locationCityCountryCd;
        this.locationCityNm = locationCityNm;
        this.presentYn = presentYn;
        this.eduSeq = eduSeq;
        this.filePath = filePath;
        this.profileEduSeq = profileEduSeq;
        this.eduStartDt = eduStartDt;
        this.eduEndDt = eduEndDt;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;

    }

}
