package com.sparwk.personalizationnode.profile.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileGroupTimezoneId implements Serializable {

    private Long profileId;
    private Long profileTimezoneBoxNum;

}
