# **Development environment configuration**

```language JAVA
FrameWork 
  Spring Boot

Server Description 
  Profile BackEndServer

Server Spec
  Spring Mvc
  Spring Validation
  Spring DevTool
  Spring Web
  Spring Fox boot
  Spring Fox Swagger
  Spring Data JPA
  JDBC
  QueryDSL
  Lombok
  Log4j
  PostGesql
 ```
# **Server Architecture**
```
com.sparwk.personalizationnode.profile.
├── biz
│     ├─ v1
│     │   └─ Function   
│     └─ v1    
│         └─ Function
├── config
│     ├─ common
│     └─ filter    
├── jpa
│     ├─ dto
│     ├─ entity
│     └─ repository
└── qfile: Dabase QueryEntity
```

  
  