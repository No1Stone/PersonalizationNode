package com.sparwk.personalizationnode.auth.jpa.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account")
@Where(clause = "use_yn = 'Y'")
public class Account {

    @Id
    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "accnt_repository_id", nullable = true)
    private Long accntRepositoryId;

    @Column(name = "accnt_type_cd", nullable = true, length = 9)
    private String accntTypeCd;

    @Column(name = "accnt_email", nullable = true, length = 100)
    private String accntEmail;

    @Column(name = "accnt_pass", nullable = true, length = 200)
    private String accntPass;

    @Column(name = "country_cd", nullable = true, length = 9)
    private String countryCd;

    @Column(name = "phone_number", nullable = true, length = 20)
    private String phoneNumber;

    @Column(name = "accnt_login_active_yn", nullable = true, length = 1)
    private String accntLoginActiveYn;

    @Column(name = "last_use_profileId")
    private Long lastUseProfileId;

    @Column(name = "verify_yn", nullable = true, length = 1)
    private String verifyYn;

    @Column(name = "use_yn", nullable = true, length = 1)
    private String useYn;

}
