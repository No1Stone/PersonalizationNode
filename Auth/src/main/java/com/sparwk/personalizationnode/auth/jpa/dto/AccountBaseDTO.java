package com.sparwk.personalizationnode.auth.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountBaseDTO {

    private Long accntId;
    private Long accntRepositoryId;
    private String accntTypeCd;
    private String accntEmail;
    private String accntPass;
    private String countryCd;
    private String phoneNumber;
    private String accntLoginActiveYn;
    private Long lastUseProfileId;
    private String verifyYn;
    private String useYn;

}
