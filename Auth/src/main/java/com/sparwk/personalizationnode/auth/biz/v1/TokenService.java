package com.sparwk.personalizationnode.auth.biz.v1;

import com.google.gson.Gson;
import com.sparwk.personalizationnode.auth.biz.CustomException;
import com.sparwk.personalizationnode.auth.biz.v1.dto.*;
import com.sparwk.personalizationnode.auth.biz.v1.enumdata.AdminLogoutEnum;
import com.sparwk.personalizationnode.auth.biz.v1.enumdata.TokenPermissionEnum;
import com.sparwk.personalizationnode.auth.biz.v1.enumdata.YnEnum;
import com.sparwk.personalizationnode.auth.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.auth.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.auth.jpa.dto.AccountBaseDTO;
import com.sparwk.personalizationnode.auth.jpa.dto.ProfileBaseDTO;
import com.sparwk.personalizationnode.auth.jpa.dto.ProfileCompanyBaseDTO;
import com.sparwk.personalizationnode.auth.jpa.entity.admin.AdminLoginLog;
import com.sparwk.personalizationnode.auth.jpa.repository.*;
import com.sparwk.personalizationnode.auth.jpa.repository.admin.AdminLoginLogRepository;
import com.sparwk.personalizationnode.auth.jpa.repository.admin.AdminPasswordRepository;
import com.sparwk.personalizationnode.auth.jpa.repository.admin.AdminRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.lang.reflect.Field;
import java.security.Key;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TokenService {

    private Logger logger = LoggerFactory.getLogger(TokenService.class);
    @Value("${security.jwt.token.secret-key}")
    private String jwt;
    private final AccountRepository accountRepository;
    private final ProfileRepository profileRepository;
    private final ProfilePositionRepository profilePositionRepository;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;
    private final AccountCompanyTypeRepository accountCompanyTypeRepository;
    private final ProfileCompanyRepository profileCompanyRepository;
    private final ProfileGroupRepository profileGroupRepository;
    private final AccountGroupTypeRepository accountGroupTypeRepository;
    private final MessageSourceAccessor messageSourceAccessor;
    private final SparwkRegex sparwkRegex;
    private final AccountPassportRepository accountPassportRepository;
    private final AdminPasswordRepository adminPasswordRepository;
    private final AdminRepository adminRepository;
    private final HttpServletRequest httpServletRequest;
    private final AdminLoginLogRepository adminLoginLogRepository;

    public String LoginValid(LoginValue entity) {
        String tokenResult = null;
        AccountBaseDTO result = null;
        Gson gson = new Gson();
        //앞에서발리데이션 하면서 조회했지만.
        //다른 공통모듈에서 사용하는로직이라 어쩔수없이 중복.
        result = accountRepository.findByAccntEmail(entity.getEmail())
                .map(e -> modelMapper.map(e, AccountBaseDTO.class)).get();
        //프로필 포지션 정보를 셋팅하는 로직
        UserInfoDTO userInfo = CreateUserInfo(result.getAccntId(), result.getLastUseProfileId());
        String userInfoString = gson.toJson(userInfo);
        tokenResult = createToken(userInfoString);
        return tokenResult;
    }

    public Response LoginValidServiceResponse(LoginValue entity) {
        Response res = new Response();
        AccountBaseDTO result = null;
        Gson gson = new Gson();
        String validResult = null;
        if (!sparwkRegex.emailValidaion(entity.getEmail())) {
            throw new CustomException("Email Pattern mismatch", HttpStatus.valueOf(600));
        }


        if (!accountRepository.existsByAccntEmail(entity.getEmail())) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_EMAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        if (entity.getPassword() == null || entity.getEmail() == null
                || entity.getEmail().isEmpty() || entity.getPassword().isEmpty()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PASSWORD.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            //리절트코드를 뒤늦게 추가해서 토큰생성 공통모듈에 어쩔수없이 이로직 한번더 사용
            logger.info("password - - - - -{}", passwordEncoder.encode(entity.getPassword()));
            result = accountRepository.findByAccntEmail(entity.getEmail())
                    .map(e -> modelMapper.map(e, AccountBaseDTO.class)).get();
        } catch (RuntimeException e) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_EMAIL_PASSWORD.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        if (result.getAccntEmail().isEmpty()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_EMAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        if (!result.getAccntEmail().equals(entity.getEmail())
                && !passwordEncoder.matches(entity.getPassword(), result.getAccntPass())) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_EMAIL_PASSWORD.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        if (result.getAccntEmail().equals(entity.getEmail())
                && passwordEncoder.matches(entity.getPassword(), result.getAccntPass())) {
            validResult = LoginValid(entity);
        }

        if (validResult == null || validResult.isEmpty()) {
            res.setResultCd(ResultCodeConst.FAIL_CREATE_TOKEN.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } else {
            String userinfoString = gson.toJson(CreateUserInfo(result.getAccntId(), result.getLastUseProfileId()));
            res.setResult(createToken(userinfoString));
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
    }

    /*
        public Response tokenUserinfoGetResponse(String token) {
            Response res = new Response();
            res.setResult(tokenUserinfoGet(token));
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


     */
    public UserInfoDTO tokenUserinfoGet(String token) {
        String deToken = getSubject(token);
        Gson gson = new Gson();
        UserInfoDTO userInfoDTO = gson.fromJson(deToken, UserInfoDTO.class);
        return userInfoDTO;
    }

    public UserInfoDTO CreateUserInfo(Long accountId, Long lastProfileId) {
        UserInfoDTO userInfoDTO = new UserInfoDTO();
        userInfoDTO.setAccountId(accountId);
        HashMap<String, String> permi = new HashMap<>();
        List<Long> profileIdList = new ArrayList<>();

        if (profileRepository.existsByAccntId(accountId) || profileCompanyRepository.existsByAccntId(accountId)
                || profileGroupRepository.existsByAccntId(accountId)
        ) {


            if (profileRepository.existsByAccntId(accountId)) {
                logger.info("유저");
                List<ProfileBaseDTO> profileBaseDTO = profileRepository.findByAccntId(accountId).stream()
                        .map(e -> modelMapper.map(e, ProfileBaseDTO.class)).collect(Collectors.toList());
                for (ProfileBaseDTO e : profileBaseDTO) {
                    profileIdList.add(e.getProfileId());
                }
                userInfoDTO.setProfileList(profileIdList);
            }
            if (profileCompanyRepository.existsByAccntId(accountId)) {
                logger.info("회사1");
                List<ProfileCompanyBaseDTO> baseDTO = profileCompanyRepository.findByAccntId(accountId).stream()
                        .map(e -> modelMapper.map(e, ProfileCompanyBaseDTO.class)).collect(Collectors.toList());
                logger.info("회사2");
                for (ProfileCompanyBaseDTO e : baseDTO) {
                    profileIdList.add(e.getProfileId());
                    logger.info("회사3");
                }
                logger.info("회사4");
                userInfoDTO.setProfileList(profileIdList);
            }
            if (profileGroupRepository.existsByAccntId(accountId)) {
                userInfoDTO.setProfileList(profileGroupRepository.findByAccntId(accountId).stream()
                        .map(e -> modelMapper.map(e.getProfileId(), Long.class)).collect(Collectors.toList()));
            }


        } else {
            profileIdList.add(0l);
            userInfoDTO.setProfileList(profileIdList);
        }

        AccountBaseDTO accountBaseDTO = accountRepository.findByAccntId(accountId).map(e ->
                modelMapper.map(e, AccountBaseDTO.class)).get();
        userInfoDTO.setAccntTypeCd(accountBaseDTO.getAccntTypeCd());


        if (accountBaseDTO.getLastUseProfileId() == null) {
            userInfoDTO.setLastUseProfileId(0L);
        } else {
            userInfoDTO.setLastUseProfileId(accountBaseDTO.getLastUseProfileId());
        }

        if (accountBaseDTO.getAccntTypeCd().equals(TokenPermissionEnum.COMPANY.getPermissionTitle())) {
            logger.info("company init ");
            int companyCheck = accountCompanyTypeRepository.countByAccntIdAndCompanyLicenseVerifyYn(accountId, YnEnum.Y.getValue());
            logger.info(" 컴파니 체크  -  {}", companyCheck);
            if (companyCheck > 0) {
                permi.put(TokenPermissionEnum.COMPANY_VERIFY.getPermissionTitle(), YnEnum.Y.getValue());
            } else {
                permi.put(TokenPermissionEnum.COMPANY_VERIFY.getPermissionTitle(), YnEnum.N.getValue());
            }
            userInfoDTO.setPermission(permi);
            return userInfoDTO;
        }

        if (accountBaseDTO.getAccntTypeCd().equals(TokenPermissionEnum.USER.getPermissionTitle())) {

            //어드민 인증
            if (accountPassportRepository.existsByAccntIdAndPassportVerifyYn(accountId, YnEnum.Y.getValue())) {
                permi.put(TokenPermissionEnum.ACCOUNT_YN.getPermissionTitle(), YnEnum.Y.getValue());
            } else {
                permi.put(TokenPermissionEnum.ACCOUNT_YN.getPermissionTitle(), YnEnum.N.getValue());
            }
            if (profileRepository.existsByProfileIdAndVerifyYn(lastProfileId, YnEnum.Y.getValue())) {
                permi.put(TokenPermissionEnum.IPI_YN.getPermissionTitle(), YnEnum.Y.getValue());
            } else {
                permi.put(TokenPermissionEnum.IPI_YN.getPermissionTitle(), YnEnum.N.getValue());
            }

            //포지션
            permi.put(TokenPermissionEnum.ARTIST_YN.getPermissionTitle(), YnEnum.Y.getValue());
            permi.put(TokenPermissionEnum.CREATOR_YN.getPermissionTitle(), YnEnum.Y.getValue());
            if (profilePositionRepository.existsByProfileIdAndAnrYnAndCompanyVerifyYn(lastProfileId, YnEnum.Y.getValue(), YnEnum.Y.getValue())) {
                permi.put(TokenPermissionEnum.ANR_YN.getPermissionTitle(), YnEnum.Y.getValue());
            } else {
                permi.put(TokenPermissionEnum.ANR_YN.getPermissionTitle(), YnEnum.N.getValue());
            }
            userInfoDTO.setPermission(permi);
            return userInfoDTO;
        }

        if (accountBaseDTO.getAccntTypeCd().equals(TokenPermissionEnum.GROUP.getPermissionTitle())) {
            int perCheck = accountGroupTypeRepository.countByAccntIdAndGroupLicenseVarifyYn(accountId, YnEnum.Y.getValue());
            if (perCheck > 0) {
                permi.put(TokenPermissionEnum.GROUP_VERIFY.getPermissionTitle(), YnEnum.Y.getValue());
            } else {
                permi.put(TokenPermissionEnum.GROUP_VERIFY.getPermissionTitle(), YnEnum.N.getValue());
            }
        }
        return userInfoDTO;
    }

    @Async
    public String createToken(String userInfo) {
        //1초 1분 1시간 24시간 7일
        long expTime = 1000 * 60 * 60 * 24 * 7;

        if (expTime <= 0) {
            throw new RuntimeException("Token Time Out");
        }
        SignatureAlgorithm signal = SignatureAlgorithm.HS256;
        byte[] secretKey = DatatypeConverter.parseBase64Binary(jwt);
        Key signKey = new SecretKeySpec(secretKey, signal.getJcaName());
        return Jwts.builder()
                .setSubject(userInfo)
                .signWith(signKey, signal)
                .setExpiration(new Date(System.currentTimeMillis() + expTime))
                .compact()
                ;
    }

    public String getSubject(String token) {
        Claims claim = Jwts.parserBuilder()
                .setSigningKey(DatatypeConverter.parseBase64Binary(jwt))
                .build()
                .parseClaimsJws(token)
                .getBody();
        return claim.getSubject();
    }

    public Response positionTokenGet(Long accId) {
        String result = null;
        AccountBaseDTO accRe = accountRepository.findById(accId)
                .map(e -> modelMapper.map(e, AccountBaseDTO.class)).get();
        logger.info("accRe =========== - {}", accRe);

        LoginValue lv = new LoginValue();
        lv.setEmail(accRe.getAccntEmail());
        lv.setPassword(accRe.getAccntPass());
        result = LoginValid(lv);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response extensionServiceResponse(HttpServletRequest req) {
        UserInfoDTO userInfoDTO = TokenDecoding(req);
        AccountDto accountDto = accountRepository
                .findByAccntId(userInfoDTO.getAccountId()).map(e -> modelMapper.map(e, AccountDto.class)).get();
        UserInfoDTO newUserInfoDTO = CreateUserInfo(userInfoDTO.getAccountId(), accountDto.getLastUseProfileId());
        Gson gson = new Gson();
        String userInfoString = gson.toJson(newUserInfoDTO);
        logger.info("new userinfostring - {}", userInfoString);
        String token = createToken(userInfoString);
        Response res = new Response();
        res.setResult(token);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response adminExtensionServiceResponse(HttpServletRequest req) {
        Response res = new Response();
        UserInfoDTO userInfoDTO = TokenDecoding(req);
        UserInfoDTO adminUserInfo = new UserInfoDTO();
        adminUserInfo.setAccountId(userInfoDTO.getAccountId());
        adminUserInfo.setAccntTypeCd(TokenPermissionEnum.ADMIM.getPermissionTitle());
        HashMap<String, String> permi = new HashMap<>();
        permi.put(TokenPermissionEnum.TYPE.getPermissionTitle(), TokenPermissionEnum.ADMIM.getPermissionTitle());
        permi.put(TokenPermissionEnum.PERMISSION_ASSIGN_SEQ.getPermissionTitle(), String.valueOf(adminRepository
                .findById(userInfoDTO.getAccountId()).orElse(null).getPermissionAssignSeq()));
        adminUserInfo.setPermission(permi);
        Gson gson = new Gson();
        String tokecString = gson.toJson(adminUserInfo);
        logger.info("clear-------------{}", tokecString);
        res.setResult(createToken(tokecString));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public UserInfoDTO TokenDecoding(HttpServletRequest req) {
        Gson gson = new Gson();
        String reqAuthorization = req.getHeader("Authorization");
        logger.info("reqAuthorization - {}", reqAuthorization);
        String token = reqAuthorization.substring(7);
        logger.info("token - {}", token);
        String sub = getSubject(token);
        UserInfoDTO userInfoDTO = gson.fromJson(sub, UserInfoDTO.class);
        logger.info("userInfoDTO - {}", userInfoDTO);
        return userInfoDTO;
    }

    public Response AdminLoginValidServiceResponse(AdminLoginRequest dto) {
        Response res = new Response();
        long adminId = 0L;
        if (adminRepository.existsByAdminEmail(dto.getEmail())) {
            adminId = adminRepository.findByAdminEmail(dto.getEmail()).orElse(null).getAdminId();
        } else {
            res.setResultCd(ResultCodeConst.NOT_FOUND_EMAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        if (!passwordEncoder.matches(dto.getPassword(),
                adminPasswordRepository.findById(adminId)
                        .orElse(null).getAdminPassword())) {
            adminLoginLogRepository.save(AdminLoginLog.builder()
                    .adminId(adminId)
                    .connectResult(AdminLogoutEnum.PASSWORD_FAIL.getTitle())
                    .connectTime(LocalDateTime.now())
                    .connectBrowser(dto.getConnectBrowser())
                    .connectOs(dto.getConnectOs())
                    .connectDevice(dto.getConnectDevice())
                    .connectIp(dto.getConnectIp())
                    .build());
            res.setResultCd(ResultCodeConst.NOT_FOUND_PASSWORD.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        UserInfoDTO adminUserInfo = new UserInfoDTO();
        adminUserInfo.setAccountId(adminId);
        adminUserInfo.setAccntTypeCd(TokenPermissionEnum.ADMIM.getPermissionTitle());
        HashMap<String, String> permi = new HashMap<>();
        permi.put(TokenPermissionEnum.TYPE.getPermissionTitle(), TokenPermissionEnum.ADMIM.getPermissionTitle());
        permi.put(TokenPermissionEnum.PERMISSION_ASSIGN_SEQ.getPermissionTitle(), String.valueOf(adminRepository
                .findByAdminEmail(dto.getEmail()).orElse(null).getPermissionAssignSeq()));
        adminUserInfo.setPermission(permi);
        Gson gson = new Gson();
        String tokecString = gson.toJson(adminUserInfo);

        logger.info("clear-------------{}", tokecString);

        adminLoginLogRepository.save(AdminLoginLog.builder()
                .adminId(adminId)
                .connectResult(AdminLogoutEnum.SUCCESS.getTitle())
                .connectTime(LocalDateTime.now())
                .connectBrowser(dto.getConnectBrowser())
                .connectOs(dto.getConnectOs())
                .connectDevice(dto.getConnectDevice())
                .connectIp(dto.getConnectIp())
                .build());

        res.setResult(createToken(tokecString));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AdminLoginoutServiceResponse(AdminLoginRequest dto) {
        Response res = new Response();

        UserInfoDTO userInfoDTO = TokenDecoding(httpServletRequest);

        adminLoginLogRepository.save(AdminLoginLog.builder()
                .adminId(userInfoDTO.getAccountId())
                .connectResult(AdminLogoutEnum.LOG_OUT.getTitle())
                .connectTime(LocalDateTime.now())
                .connectBrowser(dto.getConnectBrowser())
                .connectOs(dto.getConnectOs())
                .connectDevice(dto.getConnectDevice())
                .connectIp(dto.getConnectIp())
                .build());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
