package com.sparwk.personalizationnode.auth.biz.v1.dto;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.regex.Pattern;

@Service
public class SparwkRegex {
    /*
    private String s;
    private int a;
    private int day;
    private int count;
    private SparwkRegex(String s, int day, int count) {
        this.s = s;
        this.day = day;
        this.count = count;
    }
*/

    public boolean emailValidaion(String s) {
        return Pattern
                .matches("^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$", s);
    }

    public boolean Password8to20Nouper(String s) {
        return Pattern.matches("^.*(?=^.{8,20}$)(?=.*\\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$", s);
    }

    public boolean Password8to20Uper(String s) {
        return Pattern.matches("^.*(?=^.{8,20}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&-_+.=/]).*$", s);
    }

    public boolean PasswordExpireDaysInit(LocalDateTime s, int day) {
        return s.plusDays(day).isAfter(LocalDateTime.now());
    }

    public boolean PasswordExpire90Days(LocalDateTime s) {

        return s.plusDays(90).isAfter(LocalDateTime.now());
    }

    public boolean PasswordExpire180Days(LocalDateTime s) {

        return s.plusDays(180).isAfter(LocalDateTime.now());
    }

    public boolean PasswordExpire270Days(LocalDateTime s) {

        return s.plusDays(270).isAfter(LocalDateTime.now());
    }

    public boolean PasswordExpire365Days(LocalDateTime s) {

        return s.plusDays(365).isAfter(LocalDateTime.now());
    }

    public boolean PasswordCountLock5(int count) {
        if (count < 5) {
            return true;
        } else {
            return false;
        }
    }
}
