package com.sparwk.personalizationnode.auth.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyBaseDTO {

    private Long profileId;
    private Long accntId;
    private String profileCompanyName;
    private String mattermostId;
    private String profileImgUrl;
    private String profileBgdImgUrl;
    private String headline;
    private String bio;
    private String comInfoOverview;
    private String comInfoWebsite;
    private String comInfoPhone;
    private String comInfoEmail;
    private String comInfoFound;
    private String comInfoCountryCd;
    private String comInfoAddress;
    private String comInfoLocation;
    private String comInfoLat;
    private String comInfoLon;
    private String ipiNumber;
    private String ipiNumberVarifyYn;
    private String vatNumber;
    private String vatNumberVarifyYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

}
