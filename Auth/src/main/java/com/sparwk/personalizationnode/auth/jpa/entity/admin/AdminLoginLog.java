package com.sparwk.personalizationnode.auth.jpa.entity.admin;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_login_log")
public class AdminLoginLog {

    @Id
    @GeneratedValue(generator = "tb_admin_login_log_seq")
    @Column(name = "admin_login_log_seq", nullable = true)
    private Long adminLoginLogSeq;
    @Column(name = "connect_time", nullable = true)
    private LocalDateTime connectTime;
    @Column(name = "connect_result", nullable = true)
    private String connectResult;
    @Column(name = "admin_id", nullable = true)
    private Long adminId;
    @Column(name = "connect_browser", nullable = true)
    private String connectBrowser;
    @Column(name = "connect_os", nullable = true)
    private String connectOs;
    @Column(name = "connect_device", nullable = true)
    private String connectDevice;
    @Column(name = "connect_ip", nullable = true)
    private String connectIp;

    @Builder
    AdminLoginLog(
            Long adminLoginLogSeq,
            LocalDateTime connectTime,
            String connectResult,
            Long adminId,
            String connectBrowser,
            String connectOs,
            String connectDevice,
            String connectIp
    ) {
        this.adminLoginLogSeq = adminLoginLogSeq;
        this.connectTime = connectTime;
        this.connectResult = connectResult;
        this.adminId = adminId;
        this.connectBrowser = connectBrowser;
        this.connectOs = connectOs;
        this.connectDevice = connectDevice;
        this.connectIp = connectIp;
    }

}
