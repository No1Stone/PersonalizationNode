package com.sparwk.personalizationnode.auth.biz.v1.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountDto {

    private Long accntId;
    private Long accntRepositoryId;
    private String accntTypeCd;
    private String accntEmail;
    private String accntPass;
    private String countryCd;
    private String phoneNumber;
    private String accntLoginActiveYn;
    private Long lastUseProfileId;
    private String verifyYn;
    private String useYn;

}
