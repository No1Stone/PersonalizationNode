package com.sparwk.personalizationnode.auth.biz.v1.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileDto {

    private Long profileId;
    private Long accntId;
    private String mattermostId;
    private String fullName;
    private String stageNameYn;
    private String bthYear;
    private String bthMonth;
    private String bthDay;
    private String hireMeYn;
    private String bio;
    private String headline;
    private String currentCityCountryCd;
    private String currentCityNm;
    private String homeTownCountryCd;
    private String homeTownNm;
    private String ipiInfo;
    private String caeInfo;
    private String isniInfo;
    private String ipnInfo;
    private String nroTypeCd;
    private String nroInfo;
    private String verifyYn;
    private String useYn;
    private String accomplishmentsInfo;
}
