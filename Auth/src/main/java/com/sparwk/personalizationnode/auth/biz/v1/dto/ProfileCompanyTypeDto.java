package com.sparwk.personalizationnode.auth.biz.v1.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyTypeDto {
    private String companyCd;
    private String companyLicenseVerifyYn;
}
