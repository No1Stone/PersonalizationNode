package com.sparwk.personalizationnode.auth.jpa.repository;

import com.sparwk.personalizationnode.auth.jpa.entity.ProfilePosition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ProfilePositionRepository extends JpaRepository<ProfilePosition, String> {
    List<ProfilePosition> findByProfileId(Long profileId);
    List<ProfilePosition> findByProfileIdAndAnrYnOrArtistYn(Long profileId, String anr, String art);
    int countByProfileIdAndAnrYnOrArtistYn(Long profileId, String anr, String art);
    boolean existsByProfileIdAndAnrYnOrArtistYn(Long profileId, String anr, String art);
    boolean existsByProfileIdAndAnrYnAndCompanyVerifyYn(Long profileId, String anrYn, String comYn);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_profile_position set " +
                    "company_verify_yn = :yn " +
                    "where " +
                    "profile_id = :profileId " +
                    "and " +
                    "profile_position_seq = :positionSeq"
            ,nativeQuery = true
    )
    int UpdateProfilePositionYn(
            @Param("yn") String yn,
            @Param("profileId") Long profileId,
            @Param("positionSeq") Long positionSeq)
            ;
}
