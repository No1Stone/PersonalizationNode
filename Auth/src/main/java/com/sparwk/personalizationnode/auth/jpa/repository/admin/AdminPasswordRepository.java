package com.sparwk.personalizationnode.auth.jpa.repository.admin;

import com.sparwk.personalizationnode.auth.jpa.entity.admin.AdminPassword;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminPasswordRepository extends JpaRepository<AdminPassword, Long> {

    boolean existsByAdminPassword(String paasword);

}
