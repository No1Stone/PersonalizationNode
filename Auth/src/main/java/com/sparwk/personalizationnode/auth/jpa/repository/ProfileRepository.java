package com.sparwk.personalizationnode.auth.jpa.repository;

import com.sparwk.personalizationnode.auth.jpa.entity.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileRepository extends JpaRepository<Profile, Long> {

    List<Profile> findByAccntId(Long accountId);
    int countByAccntId(Long accountId);
    Profile findByAccntIdAndProfileId(Long accountId, Long ProfileId);

    boolean existsByAccntId(Long accountId);
    boolean existsByProfileIdAndVerifyYn(Long profileId, String yn);

}
