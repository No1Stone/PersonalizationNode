package com.sparwk.personalizationnode.auth.biz.v1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginValue{


    @Schema(description = "로그인 요청 이메일")
    private String email;
    @Schema(description = "로그인 요청 패쓰워드")
    private String password;


}
