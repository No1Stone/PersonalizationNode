package com.sparwk.personalizationnode.auth.jpa.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_position")
public class ProfilePosition {

    @Id
    @GeneratedValue(generator = "tb_profile_position_seq")
    @Column(name = "profile_position_seq")
    private Long profilePositionSeq;
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "company_profile_id", nullable = true)
    private Long companyProfileId;
    @Column(name = "company_verify_yn", nullable = true)
    private String companyVerifyYn;
    @Column(name = "artist_yn", nullable = true)
    private String artistYn;
    @Column(name = "anr_yn", nullable = true)
    private String anrYn;
    @Column(name = "creator_yn", nullable = true)
    private String creatorYn;
    @Column(name = "dept_role_info", nullable = true)
    private String deptRoleInfo;
    @Column(name = "primary_yn", nullable = true)
    private String primaryYn;


    @Column(name = "reg_usr", updatable = false)
    //first registrant
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

}
