package com.sparwk.personalizationnode.auth.config.filter;

import lombok.*;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
@Getter@Setter@Builder
@NoArgsConstructor@AllArgsConstructor
public class TokenBodyDTO {
    String sub;
    Long accountId;
    String permissions;
    String createTime;
    String iat;
    String exp;
    String userIp;
    LocalDateTime utc0;
    LocalDateTime utcUser;
}
