package com.sparwk.personalizationnode.auth.biz.v1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.HashMap;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserInfoDTO {
    private Long accountId;
    @Schema(description = "user/ company/ group/ admin")
    private String accntTypeCd;
    private Long lastUseProfileId;
    private List<Long> ProfileList;
    @Schema(description = "permission")
    private HashMap<String, String> permission;

}
