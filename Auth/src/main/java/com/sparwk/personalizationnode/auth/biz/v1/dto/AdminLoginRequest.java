package com.sparwk.personalizationnode.auth.biz.v1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AdminLoginRequest {

    @Schema(description = "로그인 요청 이메일 / 어드민로그아웃땐보내지않습니다.")
    private String email;
    @Schema(description = "로그인 요청 패쓰워드 / 어드민로그아웃땐보내지않습니다.")
    private String password;
    private String connectBrowser;
    private String connectOs;
    private String connectDevice;
    private String connectIp;

}
