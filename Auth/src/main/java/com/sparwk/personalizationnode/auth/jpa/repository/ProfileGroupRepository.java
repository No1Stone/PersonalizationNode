package com.sparwk.personalizationnode.auth.jpa.repository;

import com.sparwk.personalizationnode.auth.jpa.entity.ProfileGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileGroupRepository extends JpaRepository<ProfileGroup,Long> {

    boolean existsByAccntId(Long accountId);
    List<ProfileGroup> findByAccntId(Long accountId);

}
