package com.sparwk.personalizationnode.auth.jpa.repository.admin;

import com.sparwk.personalizationnode.auth.jpa.entity.admin.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminRepository extends JpaRepository<Admin, Long> {

    boolean existsByAdminId(Long adminId);
    boolean existsByAdminEmail(String adminEmail);
    Optional<Admin> findByAdminEmail(String adminEmail);

}
