package com.sparwk.personalizationnode.auth.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountGroupTypeBaseDTO {

    private Long accntId;
    private String groupCd;
    private String groupLicenseFileUrl;
    private String groupLicenseVarifyYn;
    private String groupLicenseFileName;
    private String songDataURL;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

}
