package com.sparwk.personalizationnode.auth.jpa.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_group")
@DynamicUpdate
public class ProfileGroup {
    @Id
    @GeneratedValue(generator = "profile_id_seq")
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "accnt_id", nullable = true)
    private Long accntId;
    @Column(name = "mattermost_id", nullable = true)
    private String mattermostId;
    @Column(name = "profile_group_name", nullable = true)
    private String profileGroupName;
    @Column(name = "profile_img_url", nullable = true)
    private String profileImgUrl;
    @Column(name = "profile_bgd_img_url", nullable = true)
    private String profileBgdImgUrl;
    @Column(name = "headline", nullable = true)
    private String headline;
    @Column(name = "web_site")
    private String webSite;
    @Column(name = "over_view")
    private String overView;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProfileGroup(
            Long profileId,
            Long accntId,
            String mattermostId,
            String profileGroupName,
            String profileImgUrl,
            String profileBgdImgUrl,
            String headline,
            String webSite,
            String overView,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ){
        this.profileId=profileId;
        this.accntId=accntId;
        this.mattermostId=mattermostId;
        this.profileGroupName=profileGroupName;
        this.profileImgUrl=profileImgUrl;
        this.profileBgdImgUrl=profileBgdImgUrl;
        this.headline=headline;
        this.webSite = webSite;
        this.overView = overView;
        this.regUsr=regUsr;
        this.regDt=regDt;
        this.modUsr=modUsr;
        this.modDt=modDt;
    }


}
