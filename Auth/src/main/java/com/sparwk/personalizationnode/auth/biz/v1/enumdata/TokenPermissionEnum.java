package com.sparwk.personalizationnode.auth.biz.v1.enumdata;

public enum TokenPermissionEnum {

    TYPE("type"),
    COMPANY("company"),
    COMPANY_VERIFY("companyVerify"),
    USER("user"),
    USER_VERIFY("userVerify"),
    GROUP("group"),
    GROUP_VERIFY("groupVerify"),
    ADMIM("admin"),
    ACCOUNT_YN("accountYn"),
    IPI_YN("ipiYn"),
    ARTIST_YN("artistYn"),
    CREATOR_YN("creatorYn"),
    ANR_YN("anrYn"),
    PERMISSION_ASSIGN_SEQ("assignSeq")
    ;
    private String permissionTitle;

    TokenPermissionEnum(
            String permissionTitle
    ){
        this.permissionTitle = permissionTitle;
    }

    public String getName(){
        return name();
    }

    public String getPermissionTitle(){
        return permissionTitle;
    }

}
