package com.sparwk.personalizationnode.auth.jpa.entity;

import com.sparwk.personalizationnode.auth.jpa.entity.id.AccountGroupTypeId;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_group_type")
@IdClass(AccountGroupTypeId.class)
public class AccountGroupType {

    @Id
    @Column(name = "accnt_id", nullable = true)
    private Long accntId;
    @Id
    @Column(name = "group_cd", nullable = true)
    private String groupCd;
    @Column(name = "group_license_file_url", nullable = true)
    private String groupLicenseFileUrl;
    @Column(name = "group_license_varify_yn", nullable = true)
    private String groupLicenseVarifyYn;
    @Column(name = "group_license_file_name", nullable = true)
    private String groupLicenseFileName;
    @Column(name = "song_data_url", nullable = true)
    private String songDataURL;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    AccountGroupType(
            Long accntId,
            String groupCd,
            String groupLicenseFileUrl,
            String groupLicenseVarifyYn,
            String groupLicenseFileName,
            String songDataURL,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.accntId = accntId;
        this.groupCd = groupCd;
        this.groupLicenseFileUrl = groupLicenseFileUrl;
        this.groupLicenseVarifyYn = groupLicenseVarifyYn;
        this.groupLicenseFileName = groupLicenseFileName;
        this.songDataURL = songDataURL;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;

    }
}
