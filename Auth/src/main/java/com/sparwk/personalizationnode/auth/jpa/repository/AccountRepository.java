package com.sparwk.personalizationnode.auth.jpa.repository;

import com.sparwk.personalizationnode.auth.jpa.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByAccntEmailAndAccntPass(String email, String pass);
    int countByAccntIdAndLastUseProfileId(Long accountId, Long profileId);
    Optional<Account> findByAccntId(Long accountId);
    int countByAccntEmail(String email);
    Optional<Account> findByAccntEmail(String email);

    boolean existsByAccntEmail(String email);

}
