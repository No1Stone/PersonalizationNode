package com.sparwk.personalizationnode.auth.biz.v1.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TokenResult {

    private String Token;
    private Long AccountId;
}
