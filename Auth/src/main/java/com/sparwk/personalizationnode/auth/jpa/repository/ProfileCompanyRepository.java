package com.sparwk.personalizationnode.auth.jpa.repository;

import com.sparwk.personalizationnode.auth.jpa.entity.ProfileCompany;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ProfileCompanyRepository extends JpaRepository<ProfileCompany,Long> {

    @Override
    Optional<ProfileCompany> findById(Long aLong);

    @Override
    Page<ProfileCompany> findAll(Pageable pageable);

    Page<ProfileCompany> findAllByAccntIdIn(List<Long> accid, Pageable pageable);

    Page<ProfileCompany> findAllByAccntIdInAndProfileCompanyNameLike(List<Long> accid, String comname, Pageable pageable);

    boolean existsByProfileId(Long profileId);

    boolean existsByAccntId(Long accountId);

    List<ProfileCompany> findByAccntId(Long accountId);
}
