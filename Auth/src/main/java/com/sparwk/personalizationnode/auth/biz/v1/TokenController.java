package com.sparwk.personalizationnode.auth.biz.v1;

import com.google.gson.Gson;
import com.sparwk.personalizationnode.auth.biz.v1.dto.AdminLoginRequest;
import com.sparwk.personalizationnode.auth.biz.v1.dto.LoginValue;
import com.sparwk.personalizationnode.auth.biz.v1.dto.UserInfoDTO;
import com.sparwk.personalizationnode.auth.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/token") //http://personalization.sparwkdev.com/auth/V1/token/createToken
@CrossOrigin("*")
@Api(tags = "Token 컨트롤러")
public class TokenController {

    @Autowired
    private TokenService tokenService;

    private final Logger logger = LoggerFactory.getLogger(TokenController.class);

    @ApiOperation(
            value = "로그인 토큰발행"
    )
    @Operation(
            description = "Login Request",
            parameters = {
                    @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "로그인 토큰발행")
            }
    )
    @PostMapping(path = "/login")
    public Response CreateToken(@Valid @RequestBody LoginValue dto) {
        logger.info("dto value - {}", dto.getEmail());
        Response res = tokenService.LoginValidServiceResponse(dto);
        return res;
    }

    @ApiOperation(
            value = "로그인 토큰발행"
    )
    @Operation(
            description = "Login Request",
            parameters = {
                    @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "로그인 토큰발행")
            }
    )
    @PostMapping(path = "/admin/login")
    public Response AdminCreateToken(@Valid @RequestBody AdminLoginRequest dto) {
        logger.info("dto value - {}", dto.getEmail());
        Response res = tokenService.AdminLoginValidServiceResponse(dto);
        return res;
    }

    @ApiOperation(
            value = "로그아웃"
    )
    @Operation(
            description = "Logout Request",
            parameters = {
                    @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "로그인 토큰발행")
            }
    )
    @PostMapping(path = "/admin/logout")
    public Response AdminLogoutController(@Valid @RequestBody AdminLoginRequest dto) {
        Response res = tokenService.AdminLoginoutServiceResponse(dto);
        return res;
    }

    @GetMapping(path = "/admin/extension")
    public Response adminTokenExtension(HttpServletRequest req) {
        Response res = tokenService.adminExtensionServiceResponse(req);
        return res;
    }


/*
    @GetMapping(path = "/userInfo")
    public Response getSubject(HttpServletRequest req) {
        String reqAuthorization =  req.getHeader("Authorization");
        String token = reqAuthorization.substring(7);
        logger.info(" token - {}", token);
        Response res = tokenService.tokenUserinfoGetResponse(token);
        return res;
    }

 */

    @GetMapping(path = "/extension")
    public Response tokenExtension(HttpServletRequest req) {
        Response res = tokenService.extensionServiceResponse(req);
        return res;
    }

    @GetMapping(path = "/positionGet")
    public Response PositionTokenGetController(HttpServletRequest req){
        String token = req.getHeader("Authorization").substring(7);
        Gson gson = new Gson();
        UserInfoDTO userInfoDTO = gson.fromJson(tokenService.getSubject(token), UserInfoDTO.class);
        logger.info("aaaaaaaa - {}", userInfoDTO.getAccountId());
        Response res = tokenService.positionTokenGet(userInfoDTO.getAccountId());
        return res;
    }

}
