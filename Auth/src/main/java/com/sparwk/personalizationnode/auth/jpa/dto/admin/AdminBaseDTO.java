package com.sparwk.personalizationnode.auth.jpa.dto.admin;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AdminBaseDTO {
    private Long adminId;
    private String adminEmail;
    private String description;
    private String useYn;

    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

    private String dial;
    private String phoneNumber;
    private String lockYn;
    private Long permissionAssignSeq;
    private String fullName;

}
