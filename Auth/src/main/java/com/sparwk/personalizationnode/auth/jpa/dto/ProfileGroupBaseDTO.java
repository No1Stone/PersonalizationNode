package com.sparwk.personalizationnode.auth.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGroupBaseDTO {
    private Long profileId;
    private Long accntId;
    private String mattermostId;
    private String groupName;
    private String profileImgUrl;
    private String profileBgdImgUrl;
    private String headline;
    private String webSite;
    private String overView;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
