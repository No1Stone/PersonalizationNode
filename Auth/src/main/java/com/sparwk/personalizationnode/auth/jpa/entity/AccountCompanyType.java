package com.sparwk.personalizationnode.auth.jpa.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_company_type")
@DynamicUpdate
public class AccountCompanyType {

    @Id
    @Column(name = "accnt_id", nullable = true)
    private Long accntId;
    @Column(name = "company_cd", nullable = true)
    private String companyCd;
    @Column(name = "company_license_file_url", nullable = true)
    private String companyLicenseFileUrl;
    @Column(name = "company_license_verify_yn", nullable = true)
    private String companyLicenseVerifyYn;


    @Builder
    AccountCompanyType
            (
                    Long accntId,
                    String companyCd,
                    String companyLicenseFileUrl,
                    String companyLicenseVerifyYn
            ) {
        this.accntId = accntId;
        this.companyCd = companyCd;
        this.companyLicenseFileUrl = companyLicenseFileUrl;
        this.companyLicenseVerifyYn = companyLicenseVerifyYn;
    }


}
