package com.sparwk.personalizationnode.auth.jpa.repository;

import com.sparwk.personalizationnode.auth.jpa.entity.AccountGroupType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountGroupTypeRepository extends JpaRepository<AccountGroupType, Long> {

    int countByAccntIdAndGroupLicenseVarifyYn(Long accountid, String verifyYn);

}
