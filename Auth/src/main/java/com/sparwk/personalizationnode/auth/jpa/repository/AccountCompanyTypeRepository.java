package com.sparwk.personalizationnode.auth.jpa.repository;

import com.sparwk.personalizationnode.auth.jpa.entity.AccountCompanyType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountCompanyTypeRepository extends JpaRepository<AccountCompanyType, Long> {

    int countByAccntIdAndCompanyLicenseVerifyYn(Long accountId, String yn);
    List<AccountCompanyType> findByAccntIdAndCompanyLicenseVerifyYn(Long accountId, String yn);
}
