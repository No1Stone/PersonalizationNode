package com.sparwk.personalizationnode.auth.jpa.repository.admin;

import com.sparwk.personalizationnode.auth.jpa.entity.admin.AdminLoginLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminLoginLogRepository extends JpaRepository<AdminLoginLog, Long> {


}
