package com.sparwk.personalizationnode.auth.jpa.entity.admin;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin_password")
public class AdminPassword {
    @Id
    @Column(name = "admin_id", nullable = true)
    private Long adminId;
    @Column(name = "admin_password", nullable = true)
    private String adminPassword;
    @Column(name = "admin_prev_password", nullable = true)
    private String adminPrevPassword;
    @Column(name = "last_connect", nullable = true)
    private LocalDateTime lastConnect;

    @Builder
    AdminPassword(
            Long adminId,
            String adminPassword,
            String adminPrevPassword,
            LocalDateTime lastConnect
    ) {
        this.adminId = adminId;
        this.adminPassword = adminPassword;
        this.adminPrevPassword = adminPrevPassword;
        this.lastConnect = lastConnect;
    }

}
