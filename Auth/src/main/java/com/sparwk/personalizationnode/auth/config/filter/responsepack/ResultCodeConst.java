package com.sparwk.personalizationnode.auth.config.filter.responsepack;

public enum ResultCodeConst {
    //공통처리
    SUCCESS("0000"),
    NOT_FOUND_INFO("0010"),

    //valid
    NOT_EXIST_PROFILE_ID("3201"),
    FAIL_CREATE_TOKEN("3002"),
    NOT_FOUND_PROJECT_ID("5010"),
    NOT_VALID_COMPANY_CD("1100"),
    NOT_VALID_SONG_ID("3401"),
    NOT_FOUND_EMAIL("3010"),
    NOT_FOUND_PASSWORD("3011"),
    NOT_FOUND_EMAIL_PASSWORD("3012"),


    //Fail
    FAIL("2000"),

    //System Error
    SERVICE_FAIL("9000"),
    ERROR("9999");
    private String code;

    ResultCodeConst(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
