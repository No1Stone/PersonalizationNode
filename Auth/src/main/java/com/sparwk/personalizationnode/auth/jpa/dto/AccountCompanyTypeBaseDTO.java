package com.sparwk.personalizationnode.auth.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyTypeBaseDTO {

    private Long accntId;
    private String companyCd;
    private String companyLicenseFileUrl;
    private String companyLicenseVerifyYn;

}
