package com.sparwk.personalizationnode.auth.biz.v1.enumdata;


public enum AdminLogoutEnum {

    SUCCESS("SUCCESS"),
    PASSWORD_FAIL("PASSWORDFAIL"),
    LOG_OUT("LOGOUT");

    private String title;

    AdminLogoutEnum(String title){
    this.title = title;
    }

    public String getName(){
        return name();
    }

    public String getTitle(){
        return title;
    }

}
