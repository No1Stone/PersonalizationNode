package com.sparwk.personalizationnode.auth.jpa.repository;

import com.sparwk.personalizationnode.auth.jpa.entity.AccountPassport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountPassportRepository extends JpaRepository<AccountPassport, Long> {

    boolean existsByAccntIdAndPassportVerifyYn(Long accountId, String yn);
    AccountPassport findByAccntId(Long accountId);
}
