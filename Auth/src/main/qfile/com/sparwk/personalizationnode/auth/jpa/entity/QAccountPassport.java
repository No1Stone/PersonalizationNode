package com.sparwk.personalizationnode.auth.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountPassport is a Querydsl query type for AccountPassport
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountPassport extends EntityPathBase<AccountPassport> {

    private static final long serialVersionUID = 243533862L;

    public static final QAccountPassport accountPassport = new QAccountPassport("accountPassport");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath countryCd = createString("countryCd");

    public final StringPath passportFirstName = createString("passportFirstName");

    public final StringPath passportImgFileUrl = createString("passportImgFileUrl");

    public final StringPath passportLastName = createString("passportLastName");

    public final StringPath passportMiddleName = createString("passportMiddleName");

    public final StringPath passportVerifyYn = createString("passportVerifyYn");

    public final StringPath personalInfoCollectionYn = createString("personalInfoCollectionYn");

    public QAccountPassport(String variable) {
        super(AccountPassport.class, forVariable(variable));
    }

    public QAccountPassport(Path<? extends AccountPassport> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountPassport(PathMetadata metadata) {
        super(AccountPassport.class, metadata);
    }

}

