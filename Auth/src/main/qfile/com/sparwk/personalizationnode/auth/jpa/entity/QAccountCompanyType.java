package com.sparwk.personalizationnode.auth.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountCompanyType is a Querydsl query type for AccountCompanyType
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountCompanyType extends EntityPathBase<AccountCompanyType> {

    private static final long serialVersionUID = 922539203L;

    public static final QAccountCompanyType accountCompanyType = new QAccountCompanyType("accountCompanyType");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath companyCd = createString("companyCd");

    public final StringPath companyLicenseFileUrl = createString("companyLicenseFileUrl");

    public final StringPath companyLicenseVerifyYn = createString("companyLicenseVerifyYn");

    public QAccountCompanyType(String variable) {
        super(AccountCompanyType.class, forVariable(variable));
    }

    public QAccountCompanyType(Path<? extends AccountCompanyType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountCompanyType(PathMetadata metadata) {
        super(AccountCompanyType.class, metadata);
    }

}

