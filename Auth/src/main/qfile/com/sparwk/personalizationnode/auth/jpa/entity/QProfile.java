package com.sparwk.personalizationnode.auth.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfile is a Querydsl query type for Profile
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfile extends EntityPathBase<Profile> {

    private static final long serialVersionUID = 1008683888L;

    public static final QProfile profile = new QProfile("profile");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath accomplishmentsInfo = createString("accomplishmentsInfo");

    public final StringPath bio = createString("bio");

    public final StringPath bthDay = createString("bthDay");

    public final StringPath bthMonth = createString("bthMonth");

    public final StringPath bthYear = createString("bthYear");

    public final StringPath caeInfo = createString("caeInfo");

    public final StringPath currentCityCountryCd = createString("currentCityCountryCd");

    public final StringPath currentCityNm = createString("currentCityNm");

    public final StringPath fullName = createString("fullName");

    public final StringPath headline = createString("headline");

    public final StringPath hireMeYn = createString("hireMeYn");

    public final StringPath homeTownCountryCd = createString("homeTownCountryCd");

    public final StringPath homeTownNm = createString("homeTownNm");

    public final StringPath ipiInfo = createString("ipiInfo");

    public final StringPath ipnInfo = createString("ipnInfo");

    public final StringPath isniInfo = createString("isniInfo");

    public final StringPath mattermostId = createString("mattermostId");

    public final StringPath nroInfo = createString("nroInfo");

    public final StringPath nroTypeCd = createString("nroTypeCd");

    public final StringPath profileBgdImgUrl = createString("profileBgdImgUrl");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImgUrl = createString("profileImgUrl");

    public final StringPath stageNameYn = createString("stageNameYn");

    public final StringPath useYn = createString("useYn");

    public final StringPath verifyYn = createString("verifyYn");

    public QProfile(String variable) {
        super(Profile.class, forVariable(variable));
    }

    public QProfile(Path<? extends Profile> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfile(PathMetadata metadata) {
        super(Profile.class, metadata);
    }

}

