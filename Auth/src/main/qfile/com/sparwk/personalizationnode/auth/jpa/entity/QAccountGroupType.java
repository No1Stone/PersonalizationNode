package com.sparwk.personalizationnode.auth.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountGroupType is a Querydsl query type for AccountGroupType
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountGroupType extends EntityPathBase<AccountGroupType> {

    private static final long serialVersionUID = -1588746043L;

    public static final QAccountGroupType accountGroupType = new QAccountGroupType("accountGroupType");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath groupCd = createString("groupCd");

    public final StringPath groupLicenseFileName = createString("groupLicenseFileName");

    public final StringPath groupLicenseFileUrl = createString("groupLicenseFileUrl");

    public final StringPath groupLicenseVarifyYn = createString("groupLicenseVarifyYn");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath songDataURL = createString("songDataURL");

    public QAccountGroupType(String variable) {
        super(AccountGroupType.class, forVariable(variable));
    }

    public QAccountGroupType(Path<? extends AccountGroupType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountGroupType(PathMetadata metadata) {
        super(AccountGroupType.class, metadata);
    }

}

