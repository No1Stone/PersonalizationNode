package com.sparwk.personalizationnode.auth.jpa.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminPassword is a Querydsl query type for AdminPassword
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminPassword extends EntityPathBase<AdminPassword> {

    private static final long serialVersionUID = -882449934L;

    public static final QAdminPassword adminPassword1 = new QAdminPassword("adminPassword1");

    public final NumberPath<Long> adminId = createNumber("adminId", Long.class);

    public final StringPath adminPassword = createString("adminPassword");

    public final StringPath adminPrevPassword = createString("adminPrevPassword");

    public final DateTimePath<java.time.LocalDateTime> lastConnect = createDateTime("lastConnect", java.time.LocalDateTime.class);

    public QAdminPassword(String variable) {
        super(AdminPassword.class, forVariable(variable));
    }

    public QAdminPassword(Path<? extends AdminPassword> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminPassword(PathMetadata metadata) {
        super(AdminPassword.class, metadata);
    }

}

