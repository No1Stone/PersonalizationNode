package com.sparwk.personalizationnode.auth.jpa.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminLoginLog is a Querydsl query type for AdminLoginLog
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdminLoginLog extends EntityPathBase<AdminLoginLog> {

    private static final long serialVersionUID = -76693902L;

    public static final QAdminLoginLog adminLoginLog = new QAdminLoginLog("adminLoginLog");

    public final NumberPath<Long> adminId = createNumber("adminId", Long.class);

    public final NumberPath<Long> adminLoginLogSeq = createNumber("adminLoginLogSeq", Long.class);

    public final StringPath connectBrowser = createString("connectBrowser");

    public final StringPath connectDevice = createString("connectDevice");

    public final StringPath connectIp = createString("connectIp");

    public final StringPath connectOs = createString("connectOs");

    public final StringPath connectResult = createString("connectResult");

    public final DateTimePath<java.time.LocalDateTime> connectTime = createDateTime("connectTime", java.time.LocalDateTime.class);

    public QAdminLoginLog(String variable) {
        super(AdminLoginLog.class, forVariable(variable));
    }

    public QAdminLoginLog(Path<? extends AdminLoginLog> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminLoginLog(PathMetadata metadata) {
        super(AdminLoginLog.class, metadata);
    }

}

