package com.sparwk.personalizationnode.account.jpa.entity.profile;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileGroup is a Querydsl query type for ProfileGroup
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileGroup extends EntityPathBase<ProfileGroup> {

    private static final long serialVersionUID = -1782061333L;

    public static final QProfileGroup profileGroup = new QProfileGroup("profileGroup");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath headline = createString("headline");

    public final StringPath mattermostId = createString("mattermostId");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath overView = createString("overView");

    public final StringPath profileBgdImgUrl = createString("profileBgdImgUrl");

    public final StringPath profileGroupName = createString("profileGroupName");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImgUrl = createString("profileImgUrl");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath webSite = createString("webSite");

    public QProfileGroup(String variable) {
        super(ProfileGroup.class, forVariable(variable));
    }

    public QProfileGroup(Path<? extends ProfileGroup> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileGroup(PathMetadata metadata) {
        super(ProfileGroup.class, metadata);
    }

}

