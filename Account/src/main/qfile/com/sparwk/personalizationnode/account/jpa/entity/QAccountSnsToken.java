package com.sparwk.personalizationnode.account.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountSnsToken is a Querydsl query type for AccountSnsToken
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountSnsToken extends EntityPathBase<AccountSnsToken> {

    private static final long serialVersionUID = -1420110274L;

    public static final QAccountSnsToken accountSnsToken = new QAccountSnsToken("accountSnsToken");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath snsToken = createString("snsToken");

    public final StringPath snsTypeCd = createString("snsTypeCd");

    public QAccountSnsToken(String variable) {
        super(AccountSnsToken.class, forVariable(variable));
    }

    public QAccountSnsToken(Path<? extends AccountSnsToken> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountSnsToken(PathMetadata metadata) {
        super(AccountSnsToken.class, metadata);
    }

}

