package com.sparwk.personalizationnode.account.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountGroupDetail is a Querydsl query type for AccountGroupDetail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountGroupDetail extends EntityPathBase<AccountGroupDetail> {

    private static final long serialVersionUID = -1685489741L;

    public static final QAccountGroupDetail accountGroupDetail = new QAccountGroupDetail("accountGroupDetail");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath accountGroupName = createString("accountGroupName");

    public final StringPath addr1 = createString("addr1");

    public final StringPath addr2 = createString("addr2");

    public final StringPath city = createString("city");

    public final StringPath locationCd = createString("locationCd");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath postCd = createString("postCd");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final StringPath region = createString("region");

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QAccountGroupDetail(String variable) {
        super(AccountGroupDetail.class, forVariable(variable));
    }

    public QAccountGroupDetail(Path<? extends AccountGroupDetail> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountGroupDetail(PathMetadata metadata) {
        super(AccountGroupDetail.class, metadata);
    }

}

