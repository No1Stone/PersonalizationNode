package com.sparwk.personalizationnode.account.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompany is a Querydsl query type for ProfileCompany
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompany extends EntityPathBase<ProfileCompany> {

    private static final long serialVersionUID = 1826499332L;

    public static final QProfileCompany profileCompany = new QProfileCompany("profileCompany");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath bio = createString("bio");

    public final StringPath comInfoAddress = createString("comInfoAddress");

    public final StringPath comInfoCountryCd = createString("comInfoCountryCd");

    public final StringPath comInfoEmail = createString("comInfoEmail");

    public final StringPath comInfoFound = createString("comInfoFound");

    public final StringPath comInfoLat = createString("comInfoLat");

    public final StringPath comInfoLocation = createString("comInfoLocation");

    public final StringPath comInfoLon = createString("comInfoLon");

    public final StringPath comInfoOverview = createString("comInfoOverview");

    public final StringPath comInfoPhone = createString("comInfoPhone");

    public final StringPath comInfoWebsite = createString("comInfoWebsite");

    public final StringPath headline = createString("headline");

    public final StringPath ipiNumber = createString("ipiNumber");

    public final StringPath ipiNumberVarifyYn = createString("ipiNumberVarifyYn");

    public final StringPath mattermostId = createString("mattermostId");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath profileBgdImgUrl = createString("profileBgdImgUrl");

    public final StringPath profileCompanyName = createString("profileCompanyName");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImgUrl = createString("profileImgUrl");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath vatNumber = createString("vatNumber");

    public final StringPath vatNumberVarifyYn = createString("vatNumberVarifyYn");

    public QProfileCompany(String variable) {
        super(ProfileCompany.class, forVariable(variable));
    }

    public QProfileCompany(Path<? extends ProfileCompany> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompany(PathMetadata metadata) {
        super(ProfileCompany.class, metadata);
    }

}

