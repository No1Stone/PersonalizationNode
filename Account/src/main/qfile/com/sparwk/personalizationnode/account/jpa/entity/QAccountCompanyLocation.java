package com.sparwk.personalizationnode.account.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAccountCompanyLocation is a Querydsl query type for AccountCompanyLocation
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountCompanyLocation extends EntityPathBase<AccountCompanyLocation> {

    private static final long serialVersionUID = -1005563435L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAccountCompanyLocation accountCompanyLocation = new QAccountCompanyLocation("accountCompanyLocation");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final QAccount account;

    public final StringPath locationCd = createString("locationCd");

    public QAccountCompanyLocation(String variable) {
        this(AccountCompanyLocation.class, forVariable(variable), INITS);
    }

    public QAccountCompanyLocation(Path<? extends AccountCompanyLocation> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QAccountCompanyLocation(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QAccountCompanyLocation(PathMetadata metadata, PathInits inits) {
        this(AccountCompanyLocation.class, metadata, inits);
    }

    public QAccountCompanyLocation(Class<? extends AccountCompanyLocation> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.account = inits.isInitialized("account") ? new QAccount(forProperty("account"), inits.get("account")) : null;
    }

}

