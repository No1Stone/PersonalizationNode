package com.sparwk.personalizationnode.account.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAccountPassport is a Querydsl query type for AccountPassport
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountPassport extends EntityPathBase<AccountPassport> {

    private static final long serialVersionUID = -977556753L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAccountPassport accountPassport = new QAccountPassport("accountPassport");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final QAccount account;

    public final StringPath countryCd = createString("countryCd");

    public final StringPath passportFirstName = createString("passportFirstName");

    public final StringPath passportImgFileUrl = createString("passportImgFileUrl");

    public final StringPath passportLastName = createString("passportLastName");

    public final StringPath passportMiddleName = createString("passportMiddleName");

    public final StringPath passportVerifyYn = createString("passportVerifyYn");

    public final StringPath personalInfoCollectionYn = createString("personalInfoCollectionYn");

    public QAccountPassport(String variable) {
        this(AccountPassport.class, forVariable(variable), INITS);
    }

    public QAccountPassport(Path<? extends AccountPassport> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QAccountPassport(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QAccountPassport(PathMetadata metadata, PathInits inits) {
        this(AccountPassport.class, metadata, inits);
    }

    public QAccountPassport(Class<? extends AccountPassport> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.account = inits.isInitialized("account") ? new QAccount(forProperty("account"), inits.get("account")) : null;
    }

}

