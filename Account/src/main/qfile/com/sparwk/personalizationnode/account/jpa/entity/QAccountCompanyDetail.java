package com.sparwk.personalizationnode.account.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAccountCompanyDetail is a Querydsl query type for AccountCompanyDetail
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountCompanyDetail extends EntityPathBase<AccountCompanyDetail> {

    private static final long serialVersionUID = 1240519281L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAccountCompanyDetail accountCompanyDetail = new QAccountCompanyDetail("accountCompanyDetail");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final QAccount account;

    public final StringPath addr1 = createString("addr1");

    public final StringPath addr2 = createString("addr2");

    public final StringPath city = createString("city");

    public final StringPath companyName = createString("companyName");

    public final StringPath contctEmail = createString("contctEmail");

    public final StringPath contctFirstName = createString("contctFirstName");

    public final StringPath contctLastName = createString("contctLastName");

    public final StringPath contctMidleName = createString("contctMidleName");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath postCd = createString("postCd");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final StringPath region = createString("region");

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QAccountCompanyDetail(String variable) {
        this(AccountCompanyDetail.class, forVariable(variable), INITS);
    }

    public QAccountCompanyDetail(Path<? extends AccountCompanyDetail> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QAccountCompanyDetail(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QAccountCompanyDetail(PathMetadata metadata, PathInits inits) {
        this(AccountCompanyDetail.class, metadata, inits);
    }

    public QAccountCompanyDetail(Class<? extends AccountCompanyDetail> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.account = inits.isInitialized("account") ? new QAccount(forProperty("account"), inits.get("account")) : null;
    }

}

