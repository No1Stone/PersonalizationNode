package com.sparwk.personalizationnode.account.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountCompanyType is a Querydsl query type for AccountCompanyType
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountCompanyType extends EntityPathBase<AccountCompanyType> {

    private static final long serialVersionUID = 1785024858L;

    public static final QAccountCompanyType accountCompanyType = new QAccountCompanyType("accountCompanyType");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath companyCd = createString("companyCd");

    public final StringPath companyLicenseFileName = createString("companyLicenseFileName");

    public final StringPath companyLicenseFileUrl = createString("companyLicenseFileUrl");

    public final StringPath companyLicenseVerifyYn = createString("companyLicenseVerifyYn");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QAccountCompanyType(String variable) {
        super(AccountCompanyType.class, forVariable(variable));
    }

    public QAccountCompanyType(Path<? extends AccountCompanyType> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountCompanyType(PathMetadata metadata) {
        super(AccountCompanyType.class, metadata);
    }

}

