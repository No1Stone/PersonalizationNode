package com.sparwk.personalizationnode.account.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.personalizationnode.account.jpa.dto.AccountPassportBaseDTO;
import com.sparwk.personalizationnode.account.jpa.entity.QAccountPassport;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class AccountPassportRepositoryDynamicImpl implements AccountPassportRepositoryDynamic {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(AccountPassportRepositoryDynamicImpl.class);
    private QAccountPassport qAccountPassport = QAccountPassport.accountPassport;

    private AccountPassportRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }


    @Override
    public Long AccountPassportRepositoryDynamicUpdate(AccountPassportBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qAccountPassport);


        if (entity.getPassportFirstName() == null || entity.getPassportFirstName().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountPassport.passportFirstName, entity.getPassportFirstName());
        }
        if (entity.getPassportMiddleName() == null || entity.getPassportMiddleName().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountPassport.passportMiddleName, entity.getPassportMiddleName());
        }
        if (entity.getPassportLastName() == null || entity.getPassportLastName().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountPassport.passportLastName, entity.getPassportLastName());
        }
        if (entity.getPassportImgFileUrl() == null || entity.getPassportImgFileUrl().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountPassport.passportImgFileUrl, entity.getPassportImgFileUrl());
        }
        if (entity.getPersonalInfoCollectionYn() == null || entity.getPersonalInfoCollectionYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountPassport.personalInfoCollectionYn, entity.getPersonalInfoCollectionYn());
        }
        if (entity.getCountryCd() == null || entity.getCountryCd().isEmpty()) {
        } else
        {
            jpaUpdateClause.set(qAccountPassport.countryCd, entity.getCountryCd());
        }

        jpaUpdateClause.where(qAccountPassport.accntId.eq(entity.getAccntId()));

        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
