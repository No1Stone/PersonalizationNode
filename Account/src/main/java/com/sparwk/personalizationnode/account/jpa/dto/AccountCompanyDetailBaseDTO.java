package com.sparwk.personalizationnode.account.jpa.dto;

import lombok.*;
import org.springframework.lang.NonNull;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyDetailBaseDTO {
    @NonNull
    private Long accntId;
    private String companyName;
    private String postCd;
    private String region;
    private String city;
    private String addr1;
    private String addr2;
    private String countryCd;

    private String phoneNum;

    private String contctFirstName;

    private String contctMidleName;

    private String contctLastName;

    private String contctEmail;

    private String companyLicenseFileUrl;

    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

}
