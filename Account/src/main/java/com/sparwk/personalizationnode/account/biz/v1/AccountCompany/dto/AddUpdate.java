package com.sparwk.personalizationnode.account.biz.v1.AccountCompany.dto;

import lombok.*;

@Getter@Setter
@AllArgsConstructor@NoArgsConstructor
@ToString
public class AddUpdate {
    private String region;
    private String city;
    private String postCd;
    private String addr1;
    private String addr2;

}
