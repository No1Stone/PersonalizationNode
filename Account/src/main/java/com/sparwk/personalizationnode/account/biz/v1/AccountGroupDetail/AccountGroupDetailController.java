package com.sparwk.personalizationnode.account.biz.v1.AccountGroupDetail;

import com.sparwk.personalizationnode.account.biz.v1.AccountGroupDetail.dto.AccountGroupDetailUpdateRequest;
import com.sparwk.personalizationnode.account.config.filter.responsepack.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/group")
@CrossOrigin("*")
@Api(tags = "Account Server 그룹 정보")
@RequiredArgsConstructor
public class AccountGroupDetailController {

    private final Logger logger = LoggerFactory.getLogger(AccountGroupDetailController.class);
    private final AccountGroupDetailService accountGroupDetailService;

    @ApiOperation(
            value = "그룹 디테일 조회",
            notes = "그룹 디테일 조회"
    )
    @GetMapping(path = "/info")
    public Response AccountGroupDetatilSelectController() {
        Response res = accountGroupDetailService.ProfileGroupContactSelectService();
        return res;
    }

    @ApiOperation(
            value = "그룹 주소 업데이트",
            notes = "그룹 주소 업데이트"
    )
    @PostMapping(path = "/update/add")
    public Response AccountGroupDetatilAddUpdateController(@Valid @RequestBody AccountGroupDetailUpdateRequest dto) {
        Response res = accountGroupDetailService.AccountGroupDetatilAddUpdateService(dto);
        return res;
    }


}
