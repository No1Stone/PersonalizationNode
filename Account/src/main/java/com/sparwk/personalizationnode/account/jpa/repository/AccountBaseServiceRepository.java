package com.sparwk.personalizationnode.account.jpa.repository;

import com.sparwk.personalizationnode.account.jpa.dto.*;
import com.sparwk.personalizationnode.account.jpa.entity.*;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AccountBaseServiceRepository {

    private final Logger logger = LoggerFactory.getLogger(AccountBaseServiceRepository.class);
    @Autowired
    private AccountCompanyDetailRepository accountCompanyDetailRepository;
    @Autowired
    private AccountCompanyLocationRepository accountCompanyLocationRepository;
    @Autowired
    private AccountCompanyTypeRepository accountCompanyTypeRepository;
    @Autowired
    private AccountPassportRepository accountPassportRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private AccountSnsTokenRepository accountSnsTokenRepository;
    @Autowired
    private MediaVerifyRepository mediaVerifyRepository;
    @Autowired
    private ModelMapper modelMapper;

    public Long AccountDynamicUpdateService(AccountBaseDTO entity) {
        long result = accountRepository.AccountRepositoryDynamicUpdate(entity);
        return result;
    }

    public Long AccountCompanyDetailDynamicUpdateService(AccountCompanyDetailBaseDTO entity) {
        long result = accountCompanyDetailRepository.AccountCompanyDetailRepositoryDynamicUpdate(entity);
        return result;
    }


    public Long AccountPassportDynamicUpdate(AccountPassportBaseDTO entity) {
        long result = accountPassportRepository.AccountPassportRepositoryDynamicUpdate(entity);
        return result;
    }

    public AccountBaseDTO AccountSaveService(AccountBaseDTO DTO) {
        Account entity = modelMapper.map(DTO, Account.class);
        accountRepository.save(entity);
        AccountBaseDTO result = modelMapper.map(entity, AccountBaseDTO.class);
        return result;
    }

    public AccountCompanyDetailBaseDTO AccountCompanyDetailSaveService(AccountCompanyDetailBaseDTO DTO) {
        AccountCompanyDetail entity = modelMapper.map(DTO, AccountCompanyDetail.class);
        accountCompanyDetailRepository.save(entity);
        AccountCompanyDetailBaseDTO result = modelMapper.map(entity, AccountCompanyDetailBaseDTO.class);
        return result;
    }

    public AccountCompanyLocationBaseDTO AccountCompanyLocationSaveService(AccountCompanyLocationBaseDTO DTO) {
        AccountCompanyLocation entity = modelMapper.map(DTO, AccountCompanyLocation.class);
        accountCompanyLocationRepository.save(entity);
        AccountCompanyLocationBaseDTO result = modelMapper.map(entity, AccountCompanyLocationBaseDTO.class);
        return result;
    }

    public AccountCompanyTypeBaseDTO AccountCompanyTypeSaveService(AccountCompanyTypeBaseDTO DTO) {
        AccountCompanyType entity = modelMapper.map(DTO, AccountCompanyType.class);
        accountCompanyTypeRepository.save(entity);
        AccountCompanyTypeBaseDTO result = modelMapper.map(entity, AccountCompanyTypeBaseDTO.class);
        return result;
    }

    public AccountPassportBaseDTO AccountPassportSaveService(AccountPassportBaseDTO DTO) {
        AccountPassport entity = modelMapper.map(DTO, AccountPassport.class);
        accountPassportRepository.save(entity);
        AccountPassportBaseDTO result = modelMapper.map(entity, AccountPassportBaseDTO.class);
        return result;
    }

    public AccountSnsTokenBaseDTO AccountSnsTokenSaveService(AccountSnsTokenBaseDTO DTO) {
        AccountSnsToken entity = modelMapper.map(DTO, AccountSnsToken.class);
        accountSnsTokenRepository.save(entity);
        AccountSnsTokenBaseDTO result = modelMapper.map(entity, AccountSnsTokenBaseDTO.class);
        return result;
    }


    public MediaVerifyBaseDTO MediaVerifySaveService(MediaVerifyBaseDTO DTO) {
        MediaVerify entity = modelMapper.map(DTO, MediaVerify.class);
        mediaVerifyRepository.save(entity);
        MediaVerifyBaseDTO result = modelMapper.map(entity, MediaVerifyBaseDTO.class);
        return result;
    }

    public List<AccountBaseDTO> AccountSelectService(Long accountId) {
        return accountRepository
                .findById(accountId).stream().map(e -> modelMapper.map(e, AccountBaseDTO.class))
                .collect(Collectors.toList());
    }

    public AccountBaseDTO AccountFindByEmailService(String email) {
        logger.info("===find email");
        return accountRepository
                .findByAccntEmail(email).map(e -> modelMapper.map(e, AccountBaseDTO.class)).get()
                ;
    }




    public List<AccountCompanyDetailBaseDTO> AccountCompanyDetailSelectService(Long accountId) {
        return accountCompanyDetailRepository
                .findById(accountId).stream().map(e -> modelMapper.map(e, AccountCompanyDetailBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<AccountCompanyLocationBaseDTO> AccountCompanyLocationSelectService(Long accountId) {
        return accountCompanyLocationRepository
                .findById(accountId).stream().map(e -> modelMapper.map(e, AccountCompanyLocationBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<AccountCompanyTypeBaseDTO> AccountCompanyTypeSelectService(Long accountId) {
        return accountCompanyTypeRepository
                .findById(accountId).stream().map(e -> modelMapper.map(e, AccountCompanyTypeBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<AccountPassportBaseDTO> AccountPassportSelectService(Long accountId) {
        return accountPassportRepository.findById(accountId).stream().map(e -> modelMapper.map(e, AccountPassportBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<AccountSnsTokenBaseDTO> AccountSnsTokenSelectService(Long accountId) {
        return accountSnsTokenRepository
                .findById(accountId).stream().map(e -> modelMapper.map(e, AccountSnsTokenBaseDTO.class))
                .collect(Collectors.toList());
    }


    public List<MediaVerifyBaseDTO> MediaVerifySelectService(Long accountId) {
        return mediaVerifyRepository
                .findById(accountId).stream().map(e -> modelMapper.map(e, MediaVerifyBaseDTO.class))
                .collect(Collectors.toList());
    }

    public List<AccountBaseDTO> FindByAccountRepository(Long accountRepositoryId){
        return accountRepository
                .findByAccntRepositoryId(accountRepositoryId).stream()
                .map(e -> modelMapper.map( e, AccountBaseDTO.class)).collect(Collectors.toList());
    }
    public AccountBaseDTO FindByAccountId(Long accountId) {
        return accountRepository.findByAccntId(accountId).map(e -> modelMapper.map(e, AccountBaseDTO.class)).get();
    }

}
