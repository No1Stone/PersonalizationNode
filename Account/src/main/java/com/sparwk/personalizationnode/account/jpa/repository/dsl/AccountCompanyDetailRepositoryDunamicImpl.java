package com.sparwk.personalizationnode.account.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.personalizationnode.account.jpa.dto.AccountCompanyDetailBaseDTO;
import com.sparwk.personalizationnode.account.jpa.entity.QAccountCompanyDetail;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class AccountCompanyDetailRepositoryDunamicImpl implements AccountCompanyDetailRepositoryDunamic {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(AccountCompanyDetailRepositoryDunamicImpl.class);
    private QAccountCompanyDetail qAccountCompanyDetail = QAccountCompanyDetail.accountCompanyDetail;

    private AccountCompanyDetailRepositoryDunamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long AccountCompanyDetailRepositoryDynamicUpdate(AccountCompanyDetailBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qAccountCompanyDetail);
        logger.info(" 어카운트------{}",entity.getAccntId());
        logger.info(" 컴퍼니 네임------{}",entity.getCompanyName());
        if (entity.getCompanyName() == null || entity.getCompanyName().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountCompanyDetail.companyName, entity.getCompanyName());
        }
        if (entity.getPostCd() == null || entity.getPostCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountCompanyDetail.postCd, entity.getPostCd());
        }
        if (entity.getRegion() == null || entity.getRegion().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountCompanyDetail.region, entity.getRegion());
        }
        if (entity.getCity() == null || entity.getCity().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountCompanyDetail.city, entity.getCity());
        }
        if (entity.getAddr1() == null || entity.getAddr1().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountCompanyDetail.addr1, entity.getAddr1());
        }
        if (entity.getAddr2() == null || entity.getAddr2().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountCompanyDetail.addr2, entity.getAddr2());
        }
        if (entity.getContctFirstName() == null || entity.getContctFirstName().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountCompanyDetail.contctFirstName, entity.getContctFirstName());
        }
        if (entity.getContctMidleName() == null || entity.getContctMidleName().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountCompanyDetail.contctMidleName, entity.getContctMidleName());
        }
        if (entity.getContctLastName() == null || entity.getContctLastName().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountCompanyDetail.contctLastName, entity.getContctLastName());
        }
        if (entity.getContctEmail() == null || entity.getContctEmail().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccountCompanyDetail.contctEmail, entity.getContctEmail());
        }
        if (entity.getRegUsr() != null) {
            jpaUpdateClause.set(qAccountCompanyDetail.regUsr, entity.getRegUsr());
        }
        if (entity.getRegDt() != null) {
            jpaUpdateClause.set(qAccountCompanyDetail.regDt, entity.getRegDt());
        }
        if (entity.getModUsr() != null) {
            jpaUpdateClause.set(qAccountCompanyDetail.modUsr, entity.getModUsr());
        }
        if (entity.getModDt() != null) {
            jpaUpdateClause.set(qAccountCompanyDetail.modDt, entity.getModDt());
        }

        jpaUpdateClause.where(qAccountCompanyDetail.accntId.eq(entity.getAccntId()));
        logger.info(" - - - - - - {}",jpaUpdateClause);
        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }


}
