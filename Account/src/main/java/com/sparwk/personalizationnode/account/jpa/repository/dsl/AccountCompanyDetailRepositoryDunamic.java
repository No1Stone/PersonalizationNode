package com.sparwk.personalizationnode.account.jpa.repository.dsl;

import com.sparwk.personalizationnode.account.jpa.dto.AccountCompanyDetailBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface AccountCompanyDetailRepositoryDunamic {
    @Transactional
    Long AccountCompanyDetailRepositoryDynamicUpdate(AccountCompanyDetailBaseDTO entity);
}
