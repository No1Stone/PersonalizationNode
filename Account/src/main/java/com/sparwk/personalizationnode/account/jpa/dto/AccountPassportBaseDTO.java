package com.sparwk.personalizationnode.account.jpa.dto;

import lombok.*;
import org.springframework.lang.NonNull;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountPassportBaseDTO {
    @NonNull
    private Long accntId;
    @Size(max = 50)
    private String passportFirstName;
    @Size(max = 50)
    private String passportMiddleName;
    @Size(max = 50)
    private String passportLastName;
    @Size(max = 200)
    private String passportImgFileUrl;
    @Size(max = 1)
    private String personalInfoCollectionYn;
    @Size(max = 9)
    private String countryCd;
    private String passportVerifyYn;

}
