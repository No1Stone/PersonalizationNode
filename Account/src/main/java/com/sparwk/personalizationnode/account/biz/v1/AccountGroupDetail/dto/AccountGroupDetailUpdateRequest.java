package com.sparwk.personalizationnode.account.biz.v1.AccountGroupDetail.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountGroupDetailUpdateRequest {
    private String postCd;
    private String locationCd;
    private String region;
    private String city;
    private String addr1;
    private String addr2;
}
