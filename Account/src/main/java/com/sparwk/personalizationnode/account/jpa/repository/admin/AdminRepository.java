package com.sparwk.personalizationnode.account.jpa.repository.admin;

import com.sparwk.personalizationnode.account.jpa.entity.admin.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository extends JpaRepository<Admin, Long> {

    boolean existsByAdminId(Long adminId);
    boolean existsByAdminEmail(String email);
}
