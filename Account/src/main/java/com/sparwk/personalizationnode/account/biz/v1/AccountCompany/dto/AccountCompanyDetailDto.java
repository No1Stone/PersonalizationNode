package com.sparwk.personalizationnode.account.biz.v1.AccountCompany.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyDetailDto {

    @Schema(description = "회사이름")
    private String companyName;

    @Schema(description = "우편번호")
    @Size(max = 20)
    private String postCd;

    @Schema(description = "지역")
    @Size(max = 50)
    private String region;

    @Schema(description = "도시")
    @Size(max = 20)
    private String city;

    @Schema(description = "주소1")
    @Size(max = 200)
    private String addr1;

    @Schema(description = "주소2")
    @Size(max = 200)
    private String addr2;

    @Schema(description = "담당자 firstName")
    @Size(max = 50)
    private String contctFirstName;

    @Schema(description = "담당자 midleName")
    @Size(max = 50)
    private String contctMidleName;

    @Schema(description = "담당자 lastName")
    @Size(max = 50)
    private String contctLastName;

    @Schema(description = "담당자 email")
    @Size(max = 100)
    private String contctEmail;


}
