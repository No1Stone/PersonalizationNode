package com.sparwk.personalizationnode.account.biz.v1.AccountPassport.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountPassportDto {

    @Schema(description = "여권 firstName")
    @Size(max = 50)
    private String passportFirstName;
    @Schema(description = "여권 midleName")
    @Size(max = 50)
    private String passportMiddleName;
    @Schema(description = "여권 lastName")
    @Size(max = 50)
    private String passportLastName;
    @Schema(description = "여권 스캔파일 저장 url")
    @Size(max = 200)
    private String passportImgFileUrl;
    @Schema(description = "개인정보 저장 여부")
    @Size(max = 1)
    private String personalInfoCollectionYn;
    @Schema(description = "패스포트 국가 코드")
    @Size(max = 9)
    private String countryCd;

    @Schema(description = "패스포트 국가 코드")
    @Size(max = 9)
    private String passportVerifyYn;

}
