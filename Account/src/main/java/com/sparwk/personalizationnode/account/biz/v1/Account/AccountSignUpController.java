package com.sparwk.personalizationnode.account.biz.v1.Account;

import com.sparwk.personalizationnode.account.biz.v1.Account.dto.*;
import com.sparwk.personalizationnode.account.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.account.jpa.repository.AccountBaseServiceRepository;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/signUp")
@CrossOrigin("*")
@Api(tags = "Account Server Email중복체크, SignUp(토큰이 필요하지 않습니다.)")
public class AccountSignUpController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private AccountBaseServiceRepository AccountBaseServiceRepository;
    @Autowired
    private ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(AccountSignUpController.class);

    @PostMapping(path = "/info/save")
    public ResponseEntity TestController( @RequestBody AccountDto dto){
     return ResponseEntity.ok(accountService.AccountSaveTestController(dto));
    }


    @ApiOperation(
            value = "회원가입 정보입력"
    )
    @Operation(
            description = "Login Request",
            parameters = {
                    @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "로그인 정보")
            }
    )
    @PostMapping(path = "/signUp")
    public Response SignUpContoller(@Valid @RequestBody SignUpDto dto) {
        logger.info("dto - {}", dto);
        Response res = accountService.SignUp(dto);
        return res;
    }


    @ApiOperation(
            value = "이메일 중복체크"
    )
    @Operation(
            description = "이메일 중복체크"
    )
    @GetMapping(path = "/overlab/{accEmail}")
    public Response EmailOverlabCheck(@PathVariable(name = "accEmail")String accEmail ){
        Response res = accountService.OverlabCheckServiceResponse(accEmail);
        return res;
    }

    @ApiOperation(
            value = "서버통신용"
    )
    @Operation(
            description = "서버통신용"
    )
    @GetMapping(path = "/getEmail/{accId}")
    public String EmailGetController(@PathVariable(name = "accId")Long accId){
    return accountService.EmailGetService(accId);
    }

    @ApiOperation(
            value = "서버통신용"
    )
    @Operation(
            description = "서버통신용"
    )
    @GetMapping(path = "/getPhone/{accId}")
    public String PhoneGetController(@PathVariable(name = "accId")Long accId){
        return accountService.PhoneGetService(accId);
    }

    @ApiOperation(
            value = "서버통신용"
    )
    @Operation(
            description = "서버통신용"
    )
    @GetMapping(path = "/getPhoneEmail/{accId}")
    public EmailPhoneDto PhoneEmailGetController(@PathVariable(name = "accId")Long accId){
        return accountService.PhoneEmailGetService(accId);
    }

//
//    @ApiOperation(
//            value = "회원가입 정보입력"
//    )
//    @Operation(
//            description = "Login Request",
//            parameters = {
//                    @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "로그인 정보")
//            }
//    )
//    @PostMapping(path = "/admin/password")
//    public Response AdminPasswordSaveContoller(@Valid @RequestBody AdminPasswordRequest dto) {
//        logger.info("dto - {}", dto);
//        Response res = accountService.AdminPasswordSaveContoller(dto);
//        return res;
//    }

    @PostMapping(path = "/admin/password")
    public Response AdminPasswordCreateController(@RequestBody AdminPasswordCreateRequest dto){
        logger.info("========={}",dto);
        Response res =  accountService.AdminPasswordSaveContoller(dto);
        return res;
    }
}
