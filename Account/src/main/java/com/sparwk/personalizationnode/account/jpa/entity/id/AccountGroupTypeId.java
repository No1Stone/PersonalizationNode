package com.sparwk.personalizationnode.account.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class AccountGroupTypeId implements Serializable {
    private Long accntId;
    private String groupCd;
}
