package com.sparwk.personalizationnode.account.biz.v1.Account.dto;


import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AdminPasswordRequest {

    private Long adminId;
    private String adminPassword;


}
