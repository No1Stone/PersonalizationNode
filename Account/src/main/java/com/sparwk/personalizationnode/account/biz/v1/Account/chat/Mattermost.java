package com.sparwk.personalizationnode.account.biz.v1.Account.chat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Mattermost {

    private Long sparwkProfileId;
    private String imgUrl;
    private MmUser mmUser;

    public Mattermost ofMmProfile(Long profileId,String profileImg, String email, String firstName, String lastName){
    this.sparwkProfileId = profileId;
    this.imgUrl = profileImg;
    this.mmUser.setEmail(email);
    this.mmUser.setFirstname(firstName);
    this.mmUser.setLastname(lastName);
    return this;
    }

}
