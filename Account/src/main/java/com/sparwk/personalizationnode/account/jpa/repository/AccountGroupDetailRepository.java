package com.sparwk.personalizationnode.account.jpa.repository;

import com.sparwk.personalizationnode.account.jpa.entity.AccountGroupDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface AccountGroupDetailRepository extends JpaRepository<AccountGroupDetail, Long> {

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_account_group_detail set " +
                    "post_cd = :post, " +
                    "location_cd= :location, " +
                    "region= :region, " +
                    "city= :city, " +
                    "addr1= :addr1, " +
                    "addr2= :addr2 " +
                    "where accnt_id = :accountId "
            ,nativeQuery = true
    )
    int UpdateAccountGroupDetailAdd(
            @Param("accountId") Long accountId,
            @Param("post") String post,
            @Param("location") String location,
            @Param("region") String region,
            @Param("city") String city,
            @Param("addr1") String addr1,
            @Param("addr2") String addr2
    );


}
