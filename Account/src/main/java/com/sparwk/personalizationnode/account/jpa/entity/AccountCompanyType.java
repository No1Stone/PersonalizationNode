package com.sparwk.personalizationnode.account.jpa.entity;

import com.sparwk.personalizationnode.account.jpa.entity.id.AccountCompanyTypeId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_company_type")
@IdClass(AccountCompanyTypeId.class)
@DynamicUpdate
public class AccountCompanyType {

    @Id
    @Column(name = "accnt_id")
    private Long accntId;
    @Id
    @Column(name = "company_cd")
    private String companyCd;
    @Column(name = "company_license_file_url", nullable = true)
    private String companyLicenseFileUrl;
    @Column(name = "company_license_verify_yn", nullable = true)
    private String companyLicenseVerifyYn;
    @Column(name = "company_license_file_name", nullable = true)
    private String companyLicenseFileName;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    AccountCompanyType
            (
                    Long accntId,
                    String companyCd,
                    String companyLicenseFileUrl,
                    String companyLicenseVerifyYn,
                    String companyLicenseFileName,
                    Long regUsr,
                    LocalDateTime regDt,
                    Long modUsr,
                    LocalDateTime modDt
            ) {
        this.accntId = accntId;
        this.companyCd = companyCd;
        this.companyLicenseFileUrl = companyLicenseFileUrl;
        this.companyLicenseVerifyYn = companyLicenseVerifyYn;
        this.companyLicenseFileName = companyLicenseFileName;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }


}
