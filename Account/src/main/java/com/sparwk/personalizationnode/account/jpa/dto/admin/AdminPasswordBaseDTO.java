package com.sparwk.personalizationnode.account.jpa.dto.admin;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AdminPasswordBaseDTO {
    private Long adminId ;
    private String adminPassword ;
    private String adminPrevPassword ;
    private LocalDateTime lastConnect;
}
