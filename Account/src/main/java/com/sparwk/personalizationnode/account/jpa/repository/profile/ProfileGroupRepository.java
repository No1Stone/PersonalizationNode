package com.sparwk.personalizationnode.account.jpa.repository.profile;

import com.sparwk.personalizationnode.account.jpa.entity.profile.ProfileGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileGroupRepository extends JpaRepository<ProfileGroup,Long> {
}
