package com.sparwk.personalizationnode.account.biz.v1.AccountPassport;

import com.sparwk.personalizationnode.account.biz.v1.AccountPassport.dto.AccountPassportDto;
import com.sparwk.personalizationnode.account.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.account.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/passport")
@CrossOrigin("*")
@Api(tags = "Account Server 여권 정보")
public class AccountPassportController {

    @Autowired
    private AccountPassportService accountPassportService;
    private final Logger logger = LoggerFactory.getLogger(AccountPassportController.class);

    @PostMapping(path = "/info")
    public Response AccountPassportSaveController(@Valid @RequestBody AccountPassportDto dto
    ,HttpServletRequest req) {
        logger.info("controller -{}",dto);
        Response res = accountPassportService.AccountPassportSave(dto, req);
        return res;
    }

    @GetMapping(path = "/info")
    public Response AccountPassportSelect(
           HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = accountPassportService.AccountPassportSelect(userInfoDTO.getAccountId());
        return res;
    }


    @PostMapping(path = "/info/update")
    public Response AccountPassportUpdateController(@Valid @RequestBody AccountPassportDto dto, HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = accountPassportService.AccountPassportUpdate(dto, req);
        return res;
    }


}
