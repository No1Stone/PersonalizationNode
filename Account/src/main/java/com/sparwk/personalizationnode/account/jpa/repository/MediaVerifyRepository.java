package com.sparwk.personalizationnode.account.jpa.repository;

import com.sparwk.personalizationnode.account.jpa.entity.MediaVerify;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediaVerifyRepository extends JpaRepository<MediaVerify, Long> {
    boolean existsByAccntIdAndMediaValidString(Long id, String key);
}
