package com.sparwk.personalizationnode.account.biz.v1.AccountSnstoken.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountSnsTokenDto {

    private String snsTypeCd;
    @Schema(description = "sns 토큰")
    @Size(max = 200)
    private String snsToken;
}
