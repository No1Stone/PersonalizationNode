package com.sparwk.personalizationnode.account.jpa.repository.profile;

import com.sparwk.personalizationnode.account.jpa.entity.profile.ProfileCompanyContact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileCompanyContactRepository extends JpaRepository<ProfileCompanyContact, Long> {

}
