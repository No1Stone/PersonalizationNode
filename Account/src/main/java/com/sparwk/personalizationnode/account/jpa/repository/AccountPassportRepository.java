package com.sparwk.personalizationnode.account.jpa.repository;

import com.sparwk.personalizationnode.account.jpa.entity.AccountPassport;
import com.sparwk.personalizationnode.account.jpa.repository.dsl.AccountPassportRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountPassportRepository extends JpaRepository<AccountPassport, Long>, AccountPassportRepositoryDynamic {
}
