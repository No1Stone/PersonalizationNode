package com.sparwk.personalizationnode.account.jpa.entity.id;

import com.sparwk.personalizationnode.account.jpa.entity.Account;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Data
public class AccountCompanyTypeId implements Serializable {

    private Long accntId;
    private String companyCd;

}
