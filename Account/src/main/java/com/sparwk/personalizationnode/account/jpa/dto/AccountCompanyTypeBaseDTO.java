package com.sparwk.personalizationnode.account.jpa.dto;

import lombok.*;
import org.springframework.lang.NonNull;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyTypeBaseDTO {

    private Long accntId;
    private String companyCd;
    private String companyLicenseFileUrl;
    private String companyLicenseVerifyYn;
    private String companyLicenseFileName;

}
