package com.sparwk.personalizationnode.account.biz.v1.AccountCompany.dto;


import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyContactBaseDTO {

    private Long profileId;
    private String profileContactImgUrl;
    private String profileContactDescription;
    private String contctFirstName;
    private String contctMidleName;
    private String contctLastName;
    private String contctEmail;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

}
