package com.sparwk.personalizationnode.account.biz.v1.Account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter@Setter
@AllArgsConstructor@NoArgsConstructor
public class PasswordReset {

    private String currentPassword;
    private String passwordToChange;

}
