package com.sparwk.personalizationnode.account.jpa.repository.admin;

import com.sparwk.personalizationnode.account.jpa.entity.admin.AdminPassword;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminPasswordRepository extends JpaRepository<AdminPassword, Long> {

    boolean existsByAdminPassword(String paasword);

}
