package com.sparwk.personalizationnode.account.jpa.dto;

import lombok.*;
import org.springframework.lang.NonNull;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyLocationBaseDTO {
    @NonNull
    private Long accntId;
    @Size(max = 9)
    private String locationCd;
}
