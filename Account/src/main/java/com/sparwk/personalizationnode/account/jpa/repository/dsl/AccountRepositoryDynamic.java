package com.sparwk.personalizationnode.account.jpa.repository.dsl;

import com.sparwk.personalizationnode.account.jpa.dto.AccountBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface AccountRepositoryDynamic {
    @Transactional
    Long AccountRepositoryDynamicUpdate(AccountBaseDTO entity);
}
