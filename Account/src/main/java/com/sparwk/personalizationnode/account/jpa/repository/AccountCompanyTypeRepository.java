package com.sparwk.personalizationnode.account.jpa.repository;

import com.sparwk.personalizationnode.account.jpa.entity.AccountCompanyType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AccountCompanyTypeRepository extends JpaRepository<AccountCompanyType, Long> {

    @Transactional
    void deleteByAccntIdAndCompanyCd(Long accountId, String cd);

    List<AccountCompanyType> findByAccntId(Long accntId);
}
