package com.sparwk.personalizationnode.account.biz.v1.Account.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder@ToString
@AllArgsConstructor@NoArgsConstructor
public class AdminPasswordCreateRequest {

    @Schema(description = "password")
    private String password;
    @Schema(description = "key")
    private String key;
    @Schema(description = "어드민아이디")
    private Long adminId;

}
