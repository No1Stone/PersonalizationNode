package com.sparwk.personalizationnode.account.biz.v1.Account.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyTypeRequest {

    @Size(max = 9)
    @Schema(description = "회사 코드 퍼블리셔인지 스트링 a,b,c,d")
    private String companyCd;
    @Schema(description = "회사 인증파일")
    private String companyLicenseFileUrl;
    private String companyLicenseFileName;


}
