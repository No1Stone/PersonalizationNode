package com.sparwk.personalizationnode.account.jpa.repository.dsl;


import com.sparwk.personalizationnode.account.jpa.dto.AccountPassportBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface AccountPassportRepositoryDynamic {
    @Transactional
    Long AccountPassportRepositoryDynamicUpdate(AccountPassportBaseDTO dto);
}
