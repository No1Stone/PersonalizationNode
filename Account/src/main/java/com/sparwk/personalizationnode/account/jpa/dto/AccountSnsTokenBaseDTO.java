package com.sparwk.personalizationnode.account.jpa.dto;

import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountSnsTokenBaseDTO {

    private Long accntId;
    @Size(max = 50)
    private String snsTypeCd;
    @Size(max = 200)
    private String snsToken;
}
