package com.sparwk.personalizationnode.account.biz.v1.Account.dto.groupcreate;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class GroupCreateRequest {

    private String ProfileGroupName;
    private List<AccountGroupTypeRequest> accountGroupTypeRequestList;
    private AccountGroupDetailRequest accountGroupDetailRequest;
    private ProfileGroupContactRequest profileGroupContactRequest;

}
