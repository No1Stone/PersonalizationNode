package com.sparwk.personalizationnode.account.jpa.repository.profile;

import com.sparwk.personalizationnode.account.jpa.entity.profile.ProfileGroupContact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileGroupContactRepository extends JpaRepository<ProfileGroupContact, Long> {
}
