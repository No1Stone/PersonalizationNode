package com.sparwk.personalizationnode.account.biz.v1.Account.dto.groupcreate;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileGroupContactRequest {
    private String profileContactImgUrl;
    private String profileContactDescription;
    private String contctFirstName;
    private String contctMidleName;
    private String contctLastName;
    private String contctEmail;
    private String contctPhoneNumber;
    private String verifyPhoneYn;
    private String countryCd;

}
