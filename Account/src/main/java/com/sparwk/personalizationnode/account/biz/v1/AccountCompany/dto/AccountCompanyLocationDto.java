package com.sparwk.personalizationnode.account.biz.v1.AccountCompany.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyLocationDto {
    @Schema(description = "회사 활동 지역")
    @Size(max = 9)
    private String locationCd;
}
