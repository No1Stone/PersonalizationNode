package com.sparwk.personalizationnode.account.biz.v1.AccountGroupDetail;

import com.sparwk.personalizationnode.account.biz.v1.AccountGroupDetail.dto.AccountGroupDetailUpdateRequest;
import com.sparwk.personalizationnode.account.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.account.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.account.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.account.jpa.dto.AccountGroupDetailBaseDTO;
import com.sparwk.personalizationnode.account.jpa.repository.AccountGroupDetailRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@RequiredArgsConstructor
public class AccountGroupDetailService {
    private final Logger logger = LoggerFactory.getLogger(AccountGroupDetailService.class);
    private final ModelMapper modelMapper;
    private final HttpServletRequest httpServletRequest;
    private final MessageSourceAccessor messageSourceAccessor;
    private final AccountGroupDetailRepository accountGroupDetailRepository;

    public Response ProfileGroupContactSelectService() {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        res.setResult(accountGroupDetailRepository
                .findById(userInfoDTO.getAccountId()).map(e -> modelMapper.map(e, AccountGroupDetailBaseDTO.class)).get());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response AccountGroupDetatilAddUpdateService(AccountGroupDetailUpdateRequest dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        int save = accountGroupDetailRepository.UpdateAccountGroupDetailAdd(
                userInfoDTO.getAccountId(),
                dto.getPostCd(),
                dto.getLocationCd(),
                dto.getRegion(),
                dto.getCity(),
                dto.getAddr1(),
                dto.getAddr2()
        );
        res.setResult(save);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
