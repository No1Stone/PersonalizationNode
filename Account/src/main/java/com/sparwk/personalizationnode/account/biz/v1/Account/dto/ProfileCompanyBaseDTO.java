package com.sparwk.personalizationnode.account.biz.v1.Account.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class ProfileCompanyBaseDTO {

    private Long profileId;
    private Long accntId;
    private String profileCompanyName;
    private String mattermostId;
    private String profileImgUrl;
    private String profileBgdImgUrl;
    private String headline;
    private String bio;
    private String comInfoOverview;
    private String comInfoWebsite;
    private String comInfoPhone;
    private String comInfoEmail;
    private String comInfoFound;
    private String comInfoCountryCd;
    private String comInfoAddress;
    private String comInfoLat;
    private String comInfoLon;
    private String ipiNumber;
    private String ipiNumberVarifyYn;
    private String vatNumber;
    private String vatNumberVarifyYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

    @Builder
    ProfileCompanyBaseDTO(
            Long profileId,
            Long accntId,
            String profileCompanyName,
            String mattermostId,
            String profileImgUrl,
            String profileBgdImgUrl,
            String headline,
            String bio,
            String comInfoOverview,
            String comInfoWebsite,
            String comInfoPhone,
            String comInfoEmail,
            String comInfoFound,
            String comInfoCountryCd,
            String comInfoAddress,
            String comInfoLat,
            String comInfoLon,
            String ipiNumber,
            String ipiNumberVarifyYn,
            String vatNumber,
            String vatNumberVarifyYn,

            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt

    ) {
        this.profileId = profileId;
        this.accntId = accntId;
        this.profileCompanyName = profileCompanyName;
        this.mattermostId = mattermostId;
        this.profileImgUrl = profileImgUrl;
        this.profileBgdImgUrl = profileBgdImgUrl;
        this.headline = headline;
        this.bio = bio;
        this.comInfoOverview = comInfoOverview;
        this.comInfoWebsite = comInfoWebsite;
        this.comInfoPhone = comInfoPhone;
        this.comInfoEmail = comInfoEmail;
        this.comInfoFound = comInfoFound;
        this.comInfoCountryCd = comInfoCountryCd;
        this.comInfoAddress = comInfoAddress;
        this.comInfoLat = comInfoLat;
        this.comInfoLon = comInfoLon;
        this.ipiNumber = ipiNumber;
        this.ipiNumberVarifyYn = ipiNumberVarifyYn;
        this.vatNumber = vatNumber;
        this.vatNumberVarifyYn = vatNumberVarifyYn;

        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;

    }

}
