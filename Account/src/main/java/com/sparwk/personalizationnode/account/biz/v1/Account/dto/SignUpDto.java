package com.sparwk.personalizationnode.account.biz.v1.Account.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter@Setter@Builder
@NoArgsConstructor@AllArgsConstructor
@ToString
public class SignUpDto {

    @Schema(description = "사용자 이메일")
    private String accntEmail;
    @Schema(description = "사용자 비밀번호")
    private String accntPass;
    @Schema(description = "유저 타입 사람/회사/그룹/단체")
    private String accntTypeCd;

    @Schema(description = "SignUp개인정보 동의1 마케팅 ")
    private String receiveMarketingInfoYn ;

    @Schema(description = "SignUp개인정보 동의2 제 3자 제공")
    private String provideInfoTothirdYn ;

    @Schema(description = "SignUp개인정보 동의3 개인정보 수집동의 I agree to the")
    private String personalInfoCollectionYn ;

    @Schema(description = "SignUp개인정보 동의4 스파크가 내정보를 이용해도되는지 iagree that Sparwk May")
    private String provideInfoTosparwkYn ;

    @Schema(description = "SignUp개인정보 동의5 스파크가 내정보를 해외에 이관해도되는지 iagree that Sparwk May")
    private String transferInfoToabroadYn ;



}
