package com.sparwk.personalizationnode.account.config.filter.token;

import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Service
public class UserInfoDTO {

    private Long accountId;
    private String accntTypeCd;
    private Long lastUseProfileId;
    private List<Long> ProfileList;
    private HashMap<String, String> permission;

}
