package com.sparwk.personalizationnode.account.jpa.entity.admin;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_admin")
public class Admin {

    @Id
    @GeneratedValue(generator = "accnt_id_seq")
    @Column(name = "admin_id", nullable = true)
    private Long adminId;
    @Column(name = "admin_email", nullable = true)
    private String adminEmail;
    @Column(name = "description", nullable = true)
    private String description;
    @Column(name = "use_yn", nullable = true)
    private String useYn;

    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;


    @Column(name = "dial", nullable = true)
    private String dial;
    @Column(name = "phone_number", nullable = true)
    private String phoneNumber;
    @Column(name = "lock_yn", nullable = true)
    private String lockYn;
    @Column(name = "permission_assign_seq", nullable = true)
    private Long permissionAssignSeq;
    @Column(name = "full_name", nullable = true)
    private String fullName;

    @Builder
    Admin(
            Long adminId,
            String adminEmail,
            String description,
            String useYn,

            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt,

            String dial,
            String phoneNumber,
            String lockYn,
            Long permissionAssignSeq,
            String fullName
    ) {
        this.adminId = adminId;
        this.adminEmail = adminEmail;
        this.description = description;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
        this.dial = dial;
        this.phoneNumber = phoneNumber;
        this.lockYn = lockYn;
        this.permissionAssignSeq = permissionAssignSeq;
        this.fullName = fullName;
    }
}
