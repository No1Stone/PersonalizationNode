package com.sparwk.personalizationnode.account.biz.v1.AccountSnstoken;

import com.sparwk.personalizationnode.account.biz.v1.AccountSnstoken.dto.AccountSnsTokenDto;
import com.sparwk.personalizationnode.account.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.account.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/sns")
@CrossOrigin("*")
@Api(tags = "Account Server SnsToken정보 입력")
public class AccountSnsTokenController {

    @Autowired
    private AccountSnsTokenService AccountSnsTokenService;
    private final Logger logger = LoggerFactory.getLogger(AccountSnsTokenController.class);

    @PostMapping(path = "/token")
    public Response AccountSnsTokenSaveController(@Valid @RequestBody AccountSnsTokenDto DTO
    ,HttpServletRequest req) {
        Response res = AccountSnsTokenService.AccountSnsTokenSave(DTO, req);
        return res;
    }

    @GetMapping(path = "/token")
    public Response AccountSnsTokenSelect(
           HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res =  AccountSnsTokenService.AccountSnsTokenSelect(userInfoDTO.getAccountId());
        return res;
    }

}
