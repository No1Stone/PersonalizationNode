package com.sparwk.personalizationnode.account.jpa.repository;

import com.sparwk.personalizationnode.account.jpa.entity.Account;
import com.sparwk.personalizationnode.account.jpa.repository.dsl.AccountRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> , AccountRepositoryDynamic {

    Optional<Account> findByAccntEmail(String email);
    List<Account> findByAccntRepositoryId(Long accRepositoryId);
    Optional<Account> findByAccntId(Long accountId);
    Long countByAccntEmail(String email);

}
