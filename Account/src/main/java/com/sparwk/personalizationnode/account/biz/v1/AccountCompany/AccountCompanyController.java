package com.sparwk.personalizationnode.account.biz.v1.AccountCompany;

import com.sparwk.personalizationnode.account.biz.v1.AccountCompany.dto.*;
import com.sparwk.personalizationnode.account.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.account.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/company")
@CrossOrigin("*")
@Api(tags = "Account Server 회사 정보")
public class AccountCompanyController {

    @Autowired
    private AccountCompanyService accountCompanyService;
    private final Logger logger = LoggerFactory.getLogger(AccountCompanyController.class);

    @PostMapping(path = "/location")
    public Response AccountCompanyLocationSaveController(
            @Valid @RequestBody AccountCompanyLocationDto dto, HttpServletRequest req) {

        Response res = accountCompanyService.AccountCompanyLocationSave(dto, req);
        return res;
    }


    @GetMapping(path = "/location")
    public Response AccountCompanyLocationSelect(HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = accountCompanyService.AccountCompanyLocationSelect(userInfoDTO.getAccountId());
        return res;
    }

    @PostMapping(path = "/type")
    public Response AccountCompanyTypeSaveController(@Valid @RequestBody AccountCompanyTypeDto dto
            , HttpServletRequest req) {
        Response res = accountCompanyService.AccountCompanyTypeSave(dto, req);
        return res;
    }

    @PostMapping(path = "/type/list")
    public Response AccountCompanyTypeListSaveController(@Valid @RequestBody AccountCompanyTypeListDto dto
            , HttpServletRequest req) {
        Response res = accountCompanyService.AccountCompanyTypeListSave(dto, req);
        return res;
    }

    @GetMapping(path = "/type")
    public Response AccountCompanyTypeSelect(
            HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = accountCompanyService.AccountCompanyTypeSelect(userInfoDTO.getAccountId());
        return res;
    }

    @PostMapping(path = "/detail")
    public Response AccountCompanyDetailSaveController(@Valid @RequestBody AccountCompanyDetailDto dto,
    HttpServletRequest req) {
        Response res = accountCompanyService.AccountCompanyDetailSave(dto, req);
        return res;
    }

    @GetMapping(path = "/detail")
    public Response AccountCompanyDetailSelect(
            HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = accountCompanyService.AccountCompanyDetailSelect(userInfoDTO.getAccountId());
        return res;
    }

    @PostMapping(path = "/detail/update")
    public Response AccountCompanyDetailUpdateController(
            @Valid @RequestBody
                    AccountCompanyDetailDto dto
                  , HttpServletRequest req) {
        Response res = accountCompanyService.AccountCompanyDetailUpdate(dto, req);
        return res;
    }

    @PostMapping(path = "/add/update")
    public Response AccountCompanyAddressUpdateController(
            @Valid @RequestBody
                    AddUpdate dto
            , HttpServletRequest req) {
        Response res = accountCompanyService.AccountCompanyAddressUpdateService(dto, req);
        return res;
    }
}
