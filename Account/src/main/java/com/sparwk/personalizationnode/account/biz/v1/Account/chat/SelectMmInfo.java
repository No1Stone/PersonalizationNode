package com.sparwk.personalizationnode.account.biz.v1.Account.chat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SelectMmInfo {
    private Long accountId;
    private Long profileId;
    private String mattermostId;
    private String profileImgUrl;
    private String email;
    private String firstname;
    private String lastname;

    public MmUser ofMmUser(){
        MmUser mm = new MmUser();
        mm.setEmail(this.email);
        mm.setFirstname(this.firstname);
        mm.setLastname(this.lastname);
        return mm;
    }

}
