package com.sparwk.personalizationnode.account.biz.v1.Account.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel(value = "어드민 생성")
public class AdminCreateRequest {

    @ApiModelProperty(position = 1, required = true
            , notes = "UseYn?"
            , value = "Value?"
            , example = "Y"
            , name = "name?"
    )
    private String useYn;
    @ApiModelProperty( position = 2,required = true)
    private String adminEmail;
    @ApiModelProperty(required = true, position = 3)
    private Long permissionAssignSeq;
    @ApiModelProperty(required = true, position = 4)
    private String fullName;
    @ApiModelProperty(required = true, position = 5)
    private String dial;
    @ApiModelProperty(required = true, position = 6)
    private String phoneNumber;
    @ApiModelProperty(position = 7)
    private String description;

}
