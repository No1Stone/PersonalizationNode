package com.sparwk.personalizationnode.account.biz.v1.Account.dto.groupcreate;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountGroupTypeRequest {

    private String groupCd;
    private String groupLicenseFileUrl;
    private String groupLicenseFileName;
    private String songDataURL;

}
