package com.sparwk.personalizationnode.account.biz.v1.AccountCompany.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyTypeDto {
    @Schema(description = "회사 유형 스트링 [a,b,c,d]")
    @Size(max = 9)
    private String companyCd;
    @Schema(description = "라이센스 파일")
    private String companyLicenseFileUrl;
    private String companyLicenseFileName;
}
