package com.sparwk.personalizationnode.account.jpa.repository;

import com.sparwk.personalizationnode.account.jpa.entity.AccountGroupType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountGroupTypeRepository extends JpaRepository<AccountGroupType, Long> {


}
