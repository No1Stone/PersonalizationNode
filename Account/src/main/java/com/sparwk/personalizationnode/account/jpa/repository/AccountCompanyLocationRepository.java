package com.sparwk.personalizationnode.account.jpa.repository;

import com.sparwk.personalizationnode.account.jpa.entity.AccountCompanyLocation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountCompanyLocationRepository extends JpaRepository<AccountCompanyLocation, Long> {
}
