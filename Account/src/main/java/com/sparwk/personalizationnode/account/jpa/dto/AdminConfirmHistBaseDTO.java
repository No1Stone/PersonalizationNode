package com.sparwk.personalizationnode.account.jpa.dto;

import lombok.*;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AdminConfirmHistBaseDTO {
    private Long confirmHistSeq;
    private Long confirmTargetId;
    @Size(max = 9)
    private String confirmTypeCd;
    private String confirmResult;
    private Long confirmAccntId;
    private String confirmAccntIp;
    private String confirmAccntMac;
    private String remark;
    private LocalDateTime confirmAccntDt;
    private LocalDateTime regDt;
}
