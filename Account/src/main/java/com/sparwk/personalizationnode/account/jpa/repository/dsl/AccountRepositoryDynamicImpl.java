package com.sparwk.personalizationnode.account.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.personalizationnode.account.jpa.dto.AccountBaseDTO;
import com.sparwk.personalizationnode.account.jpa.entity.QAccount;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class AccountRepositoryDynamicImpl implements AccountRepositoryDynamic {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(AccountRepositoryDynamicImpl.class);
    private QAccount qAccount = QAccount.account;

    private AccountRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long AccountRepositoryDynamicUpdate(AccountBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qAccount);
        logger.info("dynamic init - {}", entity.toString());
        if (entity.getAccntTypeCd() == null || entity.getAccntTypeCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.accntTypeCd, entity.getAccntTypeCd());
        }
        if (entity.getAccntEmail() == null || entity.getAccntEmail().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.accntEmail, entity.getAccntEmail());
        }
        if (entity.getAccntPass() == null || entity.getAccntPass().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.accntPass, entity.getAccntPass());
        }
        if (entity.getCountryCd() == null || entity.getCountryCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.countryCd, entity.getCountryCd());
        }
        if (entity.getPhoneNumber() == null || entity.getPhoneNumber().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.phoneNumber, entity.getPhoneNumber());
        }
        if (entity.getAccntLoginActiveYn() == null || entity.getAccntLoginActiveYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.accntLoginActiveYn, entity.getAccntLoginActiveYn());
        }
        if (entity.getVerifyYn() == null || entity.getVerifyYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.verifyYn, entity.getVerifyYn());
        }
        if (entity.getUseYn() == null || entity.getUseYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.useYn, entity.getUseYn());
        }
        if (entity.getVerifyPhoneYn() == null || entity.getVerifyPhoneYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.verifyPhoneYn, entity.getVerifyPhoneYn());
        }
        if (entity.getLastUseProfileId() != null) {
            jpaUpdateClause.set(qAccount.lastUseProfileId, entity.getLastUseProfileId());
        }
        if (entity.getRegUsr() != null) {
            jpaUpdateClause.set(qAccount.regUsr, entity.getRegUsr());
        }
        if (entity.getRegDt() != null) {
            jpaUpdateClause.set(qAccount.regDt, entity.getRegDt());
        }
        if (entity.getModUsr() != null) {
            jpaUpdateClause.set(qAccount.modUsr, entity.getModUsr());
        }
        if (entity.getModDt() != null) {
            jpaUpdateClause.set(qAccount.modDt, entity.getModDt());
        }

        if (entity.getReceiveMarketingInfoYn() == null || entity.getReceiveMarketingInfoYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.receiveMarketingInfoYn, entity.getReceiveMarketingInfoYn());
        }
        if (entity.getProvideInfoTothirdYn() == null || entity.getProvideInfoTothirdYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.provideInfoTothirdYn, entity.getProvideInfoTothirdYn());
        }
        if (entity.getPersonalInfoCollectionYn() == null || entity.getPersonalInfoCollectionYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.personalInfoCollectionYn, entity.getPersonalInfoCollectionYn());
        }
        if (entity.getProvideInfoTosparwkYn() == null || entity.getProvideInfoTosparwkYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.provideInfoTosparwkYn, entity.getProvideInfoTosparwkYn());
        }
        if (entity.getTransferInfoToabroadYn() == null || entity.getTransferInfoToabroadYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.transferInfoToabroadYn, entity.getTransferInfoToabroadYn());
        }


        jpaUpdateClause.where(qAccount.accntId.eq(entity.getAccntId()));

        if (entity.getAccntRepositoryId() != null) {
            jpaUpdateClause.where(qAccount.accntRepositoryId.eq(entity.getAccntRepositoryId()));
        }


        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
