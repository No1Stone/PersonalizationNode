package com.sparwk.personalizationnode.account.biz.v1.Account.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyCreateRequest {

    private AccountCompanyDetailRequest accountCompanyDetailRequest;
    private AccountCompanyLocationRequest accountCompanyLocationRequest;
    private AccountCompanyTypeRequest accountCompanyTypeRequest;

}
