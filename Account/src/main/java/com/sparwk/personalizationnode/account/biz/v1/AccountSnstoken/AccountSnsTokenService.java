package com.sparwk.personalizationnode.account.biz.v1.AccountSnstoken;

import com.sparwk.personalizationnode.account.biz.v1.AccountSnstoken.dto.AccountSnsTokenDto;
import com.sparwk.personalizationnode.account.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.account.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.account.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.account.jpa.dto.AccountSnsTokenBaseDTO;
import com.sparwk.personalizationnode.account.jpa.repository.AccountBaseServiceRepository;
import com.sparwk.personalizationnode.account.jpa.repository.AccountSnsTokenRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountSnsTokenService {

    @Autowired
    private AccountBaseServiceRepository accountBaseServiceRepository;
    @Autowired
    private AccountSnsTokenRepository accountSnsTokenRepository;
    @Autowired
    private ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(AccountSnsTokenService.class);
    private final MessageSourceAccessor messageSourceAccessor;

    public Response AccountSnsTokenSave(AccountSnsTokenDto dto, HttpServletRequest req) {

        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        AccountSnsTokenBaseDTO accountSnsTokenBaseDTO = modelMapper.map(dto, AccountSnsTokenBaseDTO.class);
        accountSnsTokenBaseDTO.setAccntId(userInfoDTO.getAccountId());
        AccountSnsTokenBaseDTO result = accountBaseServiceRepository.AccountSnsTokenSaveService(accountSnsTokenBaseDTO);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AccountSnsTokenSelect(Long accountId) {
       List<AccountSnsTokenBaseDTO> result = accountSnsTokenRepository.findByAccntId(accountId)
               .stream().map(e -> modelMapper.map(e, AccountSnsTokenBaseDTO.class))
               .collect(Collectors.toList());
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


}
