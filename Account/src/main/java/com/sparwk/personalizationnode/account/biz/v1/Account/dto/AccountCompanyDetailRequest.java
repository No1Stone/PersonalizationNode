package com.sparwk.personalizationnode.account.biz.v1.Account.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyDetailRequest {
    private String companyName;
    private String postCd;
    private String region;
    private String city;
    private String addr1;
    private String addr2;
    private String countryCd;
    private String phoneNum;
    private String contctFirstName;
    private String contctMidleName;
    private String contctLastName;
    private String contctEmail;
    private String profileContactImgUrl;
    private String profileContactDescription;
    private String contctPhoneNumber;
}
