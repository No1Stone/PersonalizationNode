package com.sparwk.personalizationnode.account.biz.v1.Account.dto.groupcreate;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountGroupDetailRequest {
    private String postCd;
    private String locationCd;
    private String region;
    private String city;
    private String addr1;
    private String addr2;
}
