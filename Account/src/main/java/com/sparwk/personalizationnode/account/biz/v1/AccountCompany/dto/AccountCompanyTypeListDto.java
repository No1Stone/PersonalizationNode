package com.sparwk.personalizationnode.account.biz.v1.AccountCompany.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyTypeListDto {

    private List<AccountCompanyTypeDto> AccountCompanyTypeDtoList;

}
