package com.sparwk.personalizationnode.account.jpa.repository;

import com.sparwk.personalizationnode.account.jpa.entity.AccountSnsToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountSnsTokenRepository extends JpaRepository<AccountSnsToken, Long> {

    List<AccountSnsToken> findByAccntId(Long accId);

}
