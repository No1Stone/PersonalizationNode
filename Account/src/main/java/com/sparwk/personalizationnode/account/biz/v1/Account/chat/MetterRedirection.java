package com.sparwk.personalizationnode.account.biz.v1.Account.chat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MetterRedirection {

    //연결하는 사용자의 mattermost_id
    private String fromId;
    //연결대상이 되는 사용자의 mattermost_id
    private String toId;
    //로그인 사용자의 mattermost_id
    private String me;
    //대화하려는 사용자의 mattermost_id
    private String talkTo;
    //project에 연결된 team_id
    private String teamId;
    //사용자의 mattermost_id
    private String id;

}
