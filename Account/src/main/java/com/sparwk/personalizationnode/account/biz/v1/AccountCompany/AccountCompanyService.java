package com.sparwk.personalizationnode.account.biz.v1.AccountCompany;

import com.sparwk.personalizationnode.account.biz.v1.AccountCompany.dto.*;
import com.sparwk.personalizationnode.account.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.account.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.account.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.account.jpa.dto.AccountCompanyDetailBaseDTO;
import com.sparwk.personalizationnode.account.jpa.dto.AccountCompanyLocationBaseDTO;
import com.sparwk.personalizationnode.account.jpa.dto.AccountCompanyTypeBaseDTO;
import com.sparwk.personalizationnode.account.jpa.entity.AccountCompanyDetail;
import com.sparwk.personalizationnode.account.jpa.repository.AccountBaseServiceRepository;
import com.sparwk.personalizationnode.account.jpa.repository.AccountCompanyDetailRepository;
import com.sparwk.personalizationnode.account.jpa.repository.AccountCompanyLocationRepository;
import com.sparwk.personalizationnode.account.jpa.repository.AccountCompanyTypeRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountCompanyService {
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(AccountCompanyService.class);

    @Autowired
    private AccountBaseServiceRepository accountBaseServiceRepository;
    @Autowired
    private AccountCompanyLocationRepository accountCompanyLocationRepository;
    @Autowired
    private AccountCompanyTypeRepository accountCompanyTypeRepository;
    @Autowired
    private AccountCompanyDetailRepository accountCompanyDetailRepository;
    @Autowired
    private ModelMapper modelMapper;

    public Response AccountCompanyLocationSave(AccountCompanyLocationDto dto, HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        AccountCompanyLocationBaseDTO accountCompanyLocationBaseDTO = modelMapper.map(dto, AccountCompanyLocationBaseDTO.class);
        accountCompanyLocationBaseDTO.setAccntId(userInfoDTO.getAccountId());

        AccountCompanyLocationBaseDTO result =
                accountBaseServiceRepository.AccountCompanyLocationSaveService(accountCompanyLocationBaseDTO);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AccountCompanyLocationSelect(Long accountId) {
        List<AccountCompanyLocationBaseDTO> result =
                accountBaseServiceRepository.AccountCompanyLocationSelectService(accountId);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AccountCompanyTypeSave(AccountCompanyTypeDto dto, HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        AccountCompanyTypeBaseDTO accountCompanyTypeBaseDTO = modelMapper.map(dto, AccountCompanyTypeBaseDTO.class);
        accountCompanyTypeBaseDTO.setAccntId(userInfoDTO.getAccountId());
        AccountCompanyTypeBaseDTO result =
                accountBaseServiceRepository.AccountCompanyTypeSaveService(accountCompanyTypeBaseDTO);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AccountCompanyTypeListSave(AccountCompanyTypeListDto dto, HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();

        //현재 입력값
//        List<String> befoData = new ArrayList<>();
//        logger.info("조회는햇니??1");
//        List<AccountCompanyTypeBaseDTO> asd = accountCompanyTypeRepository.findAllByAccntId(userInfoDTO.getAccountId())
//                .stream().map(e -> modelMapper.map(e, AccountCompanyTypeBaseDTO.class)).collect(Collectors.toList());
//        logger.info("조회는햇니??2");
//        for(AccountCompanyTypeBaseDTO e: asd){
//            befoData.add(e.getCompanyCd());
//        }
       List<String> befoData = accountCompanyTypeRepository.findByAccntId(userInfoDTO.getAccountId())
                .stream().map(e -> modelMapper.map(e.getCompanyCd(), String.class)).collect(Collectors.toList());
        logger.info("현재입력값에러인가?");
        //유저가 입력한 신규값 우선저장시킴
        List<String> afterData = new ArrayList<>();
        for(AccountCompanyTypeDto e : dto.getAccountCompanyTypeDtoList()){
            afterData.add(e.getCompanyCd());
            logger.info("입력값 -{}",e.getCompanyCd());
            AccountCompanyTypeBaseDTO accountCompanyTypeBaseDTO = modelMapper.map(e, AccountCompanyTypeBaseDTO.class);
            accountCompanyTypeBaseDTO.setCompanyCd(e.getCompanyCd());
            accountCompanyTypeBaseDTO.setAccntId(userInfoDTO.getAccountId());
            accountCompanyTypeBaseDTO.setCompanyLicenseVerifyYn("N");
            logger.info("========타입확인==={}",accountCompanyTypeBaseDTO);
            AccountCompanyTypeBaseDTO result =
                    accountBaseServiceRepository.AccountCompanyTypeSaveService(accountCompanyTypeBaseDTO);

        }
        logger.info("비포--{}",befoData.toString());
        logger.info("에프--{}",afterData.toString());
        befoData.removeAll(afterData);
        logger.info("지운후",befoData.toString());


        for(String e : befoData){
            accountCompanyTypeRepository.deleteByAccntIdAndCompanyCd(userInfoDTO.getAccountId(), e);
        }

        res.setResult(accountBaseServiceRepository.FindByAccountId(userInfoDTO.getAccountId()));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AccountCompanyTypeSelect(Long accountId) {
        List<AccountCompanyTypeBaseDTO> result = accountCompanyTypeRepository.findByAccntId(accountId).stream().map(
                e-> modelMapper.map(e, AccountCompanyTypeBaseDTO.class)).collect(Collectors.toList());
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response AccountCompanyDetailSave(AccountCompanyDetailDto dto, HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        AccountCompanyDetailBaseDTO accountCompanyDetailBaseDTO = modelMapper.map(dto, AccountCompanyDetailBaseDTO.class);
        accountCompanyDetailBaseDTO.setAccntId(userInfoDTO.getAccountId());
        AccountCompanyDetailBaseDTO result = accountBaseServiceRepository
                .AccountCompanyDetailSaveService(accountCompanyDetailBaseDTO);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AccountCompanyDetailSelect(Long accountId) {
        List<AccountCompanyDetailBaseDTO> result = accountBaseServiceRepository.AccountCompanyDetailSelectService(accountId);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AccountCompanyDetailUpdate(AccountCompanyDetailDto dto, HttpServletRequest req) {

        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        logger.info("-------{}",dto);
        AccountCompanyDetailBaseDTO AccountCompanyDetailBaseDTO =  modelMapper.map(dto, AccountCompanyDetailBaseDTO.class);
        AccountCompanyDetailBaseDTO.setAccntId(userInfoDTO.getAccountId());
        Long result = accountBaseServiceRepository.AccountCompanyDetailDynamicUpdateService(
                AccountCompanyDetailBaseDTO
        );
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AccountCompanyAddressUpdateService(AddUpdate dto, HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();

        AccountCompanyDetailBaseDTO befo = accountCompanyDetailRepository.findById(userInfoDTO.getAccountId())
                .map(e -> modelMapper.map(e, AccountCompanyDetailBaseDTO.class)).get();
        AccountCompanyDetail saveEntity = modelMapper.map(befo, AccountCompanyDetail.class);
        saveEntity.setAddr1(dto.getAddr1());
        saveEntity.setAddr2(dto.getAddr2());
        saveEntity.setPostCd(dto.getPostCd());
        saveEntity.setCity(dto.getCity());
        saveEntity.setRegion(dto.getRegion());

        accountCompanyDetailRepository.save(saveEntity);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
