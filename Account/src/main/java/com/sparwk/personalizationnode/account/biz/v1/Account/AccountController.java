package com.sparwk.personalizationnode.account.biz.v1.Account;

import com.sparwk.personalizationnode.account.biz.v1.Account.dto.AccountCompanyCreateRequest;
import com.sparwk.personalizationnode.account.biz.v1.Account.dto.AccountDto;
import com.sparwk.personalizationnode.account.biz.v1.Account.dto.AdminCreateRequest;
import com.sparwk.personalizationnode.account.biz.v1.Account.dto.PasswordReset;
import com.sparwk.personalizationnode.account.biz.v1.Account.dto.groupcreate.GroupCreateRequest;
import com.sparwk.personalizationnode.account.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.account.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.account.jpa.repository.AccountBaseServiceRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/account")
@CrossOrigin("*")
@Api(tags = "Account Server 계정 정보")
public class AccountController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private AccountBaseServiceRepository AccountBaseServiceRepository;
    @Autowired
    private ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired(required=true)
    private HttpServletRequest request;




    @ApiOperation(
            value = "계정정보 조회"
    )
    @Operation(
            description = "계정정보 조회"
    )
    @GetMapping(path = "/info")
    public Response AccountSelect(HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = accountService.AccountSelectServiceResponse(userInfoDTO.getAccountId());
        return res;
    }

    @ApiOperation(
            value = "계정정보 조회 RepositoryId"
    )
    @Operation(
            description = "계정정보 조회"
    )
    @GetMapping(path = "/info/{accountRepositoryId}")
    public Response AccountRepositorySelect(
            @PathVariable(name = "accountRepositoryId") Long accountRepositoryId, HttpServletRequest req) {
        Response res = accountService.FindByAccountRepositoryIdSelect(accountRepositoryId);
        return res;
    }

    @ApiOperation(
            value = "회원가입 정보업데이트"
    )
    @Operation(
            description = "회원정보 업데이트",
            parameters = {
                    @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "회원정보 업데이트")
            }
    )
    @PostMapping(path = "/info/update")
    public Response AccountDynamicUpdateController(@Valid @RequestBody AccountDto dto
            , HttpServletRequest req) {

        Response res = accountService.AccountDynamicUpdateServiceResponse(dto, req);
        return res;
    }

    @ApiOperation(
            value = "프로필 변경"
    )
    @Operation(
            description = "프로필 변경",
            parameters = {
                    @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "회원정보 업데이트")
            }
    )
    @GetMapping(path = "/last/profile/{profileId}")
    public Response LastProfileUpdate(@PathVariable(name = "profileId") Long profileId, HttpServletRequest req) {
        Response res = accountService.LastProfileUpdate(profileId, req);
        return res;
    }

    @PostMapping(path = "/company/create")
    public Response CompanyVerifyInfoSave(@Valid @RequestBody AccountCompanyCreateRequest dto, HttpServletRequest req) {
        Response res = accountService.CompanyCreateService(dto, req);
        return res;
    }

    @ApiOperation(
            value = "그룹 어카운트및 프로필 생성"
    )
    @Operation(
            description = "그룹 어카운트및 프로필 생성",
            parameters = {
                    @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "그룹 어카운트및 프로필 생성")
            }
    )
    @PostMapping(path = "/group/create")
    public Response GroupVarifyInfoSave(@Valid @RequestBody GroupCreateRequest dto, HttpServletRequest req){
        Response res = accountService.GroupVarifyInfoSaveService(dto, req);
        return res;
    }

    @PostMapping(path = "/passwordReset")
    public Response PasswordReset(@RequestBody PasswordReset passwordReset, HttpServletRequest req){
        Response res = accountService.PasswordResetService(passwordReset, req);
        return res;
    }

    @GetMapping(path = "/test222")
    public void test11111(){
        UserInfoDTO user = (UserInfoDTO) request.getAttribute("UserInfoDTO");
        logger.info("-----널나오지마라라라----{}",user);
    }


    @ApiOperation(
            value = "어드민 생성 정보입력"

    )
//    @Operation(
//            description = "Admin Create Request",
//            parameters = {
//                    @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "admin create")
//            }
//            )
    @PostMapping(path = "/admin/create")
    public Response AdminCreateContoller(@Valid @RequestBody AdminCreateRequest dto) {
        logger.info("dto - {}", dto);
        Response res = accountService.AdminCreateService(dto);
        return res;
    }




}
