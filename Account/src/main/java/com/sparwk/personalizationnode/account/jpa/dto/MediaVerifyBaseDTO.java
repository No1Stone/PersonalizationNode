package com.sparwk.personalizationnode.account.jpa.dto;


import lombok.*;
import org.springframework.lang.NonNull;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MediaVerifyBaseDTO {
    @NonNull
    private Long accntId;
    private String mediaVerifyReqId;
    @Size(max = 9)
    private String mediaTypeCd;
    private String mediaValidString;
    @Size(max = 9)
    private String verifyTypeCd;
    private LocalDateTime expiredDt;
    private LocalDateTime regDt;
}
