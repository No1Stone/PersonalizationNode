package com.sparwk.personalizationnode.account.biz.v1.Account;

import com.google.gson.Gson;
import com.sparwk.personalizationnode.account.biz.v1.Account.chat.CreateMmRes;
import com.sparwk.personalizationnode.account.biz.v1.Account.chat.Mattermost;
import com.sparwk.personalizationnode.account.biz.v1.Account.chat.MmUser;
import com.sparwk.personalizationnode.account.biz.v1.Account.dto.*;
import com.sparwk.personalizationnode.account.biz.v1.Account.dto.groupcreate.AccountGroupTypeRequest;
import com.sparwk.personalizationnode.account.biz.v1.Account.dto.groupcreate.GroupCreateRequest;
import com.sparwk.personalizationnode.account.config.common.RestCall;
import com.sparwk.personalizationnode.account.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.account.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.account.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.account.jpa.dto.AccountBaseDTO;
import com.sparwk.personalizationnode.account.jpa.dto.AccountCompanyDetailBaseDTO;
import com.sparwk.personalizationnode.account.jpa.dto.AccountCompanyLocationBaseDTO;
import com.sparwk.personalizationnode.account.jpa.dto.AccountCompanyTypeBaseDTO;
import com.sparwk.personalizationnode.account.jpa.entity.Account;
import com.sparwk.personalizationnode.account.jpa.entity.AccountGroupDetail;
import com.sparwk.personalizationnode.account.jpa.entity.AccountGroupType;
import com.sparwk.personalizationnode.account.jpa.entity.admin.Admin;
import com.sparwk.personalizationnode.account.jpa.entity.admin.AdminPassword;
import com.sparwk.personalizationnode.account.jpa.entity.profile.ProfileCompanyContact;
import com.sparwk.personalizationnode.account.jpa.entity.profile.ProfileGroup;
import com.sparwk.personalizationnode.account.jpa.entity.profile.ProfileGroupContact;
import com.sparwk.personalizationnode.account.jpa.repository.*;
import com.sparwk.personalizationnode.account.jpa.repository.admin.AdminPasswordRepository;
import com.sparwk.personalizationnode.account.jpa.repository.admin.AdminRepository;
import com.sparwk.personalizationnode.account.jpa.repository.profile.ProfileCompanyContactRepository;
import com.sparwk.personalizationnode.account.jpa.repository.profile.ProfileGroupContactRepository;
import com.sparwk.personalizationnode.account.jpa.repository.profile.ProfileGroupRepository;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final Logger logger = LoggerFactory.getLogger(AccountService.class);

    @Autowired
    private AccountBaseServiceRepository accountServiceRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private ModelMapper modelMapper;
    private final MessageSourceAccessor messageSourceAccessor;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ProfileCompanyContactRepository profileCompanyContactRepository;
    @Autowired
    private AccountGroupDetailRepository accountGroupDetailRepository;
    @Autowired
    private AccountGroupTypeRepository accountGroupTypeRepository;
    @Autowired
    private ProfileGroupContactRepository profileGroupContactRepository;
    @Autowired
    private ProfileGroupRepository profileGroupRepository;
    @Autowired
    private ProfileCompanyRepository profileCompanyRepository;

    private final MediaVerifyRepository mediaVerifyRepository;
    private final AdminRepository adminRepository;
    private final AdminPasswordRepository adminPasswordRepository;
    private final HttpServletRequest httpServletRequest;

    @Value("${sparwk.node.project.projectip}")
    private String projectIp;
    @Value("${sparwk.node.personalization.accountip}")
    private String accountIp;
    @Value("${sparwk.node.personalization.authip}")
    private String authIp;
    @Value("${sparwk.node.personalization.profileip}")
    private String profileIp;
    @Value("${sparwk.node.schema}")
    private String schema;
    @Value("${sparwk.node.externalconnect.mailingip}")
    private String mailingip;
    @Value("${sparwk.node.communication.chat}")
    private String chat;

    @Async
    public Long OverlabCheckService(String accEmail) {
        return accountRepository.countByAccntEmail(accEmail);
    }

    public Response OverlabCheckServiceResponse(String accEmail) {
        long countAcc = OverlabCheckService(accEmail);
        Response res = new Response();
        if (countAcc > 0) {
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        } else {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        res.setResult(countAcc);
        return res;
    }

    @Transactional
    public Response SignUp(SignUpDto signDto) {
        AccountBaseDTO accountDTO = modelMapper.map(signDto, AccountBaseDTO.class);
        Response res = new Response();
        AccountBaseDTO result = null;
        logger.info("value test -----{}", passwordEncoder.encode(signDto.getAccntPass()));
        Long emailC = accountRepository.countByAccntEmail(signDto.getAccntEmail());
        if (emailC > 0) {
            res.setResultCd(ResultCodeConst.BAD_REQUEST_EMAIL_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        AccountBaseDTO saveResult = accountServiceRepository.AccountSaveService(accountDTO);
        long accId = saveResult.getAccntId();
        accountDTO.setAccntId(accId);
        accountDTO.setAccntPass(passwordEncoder.encode(signDto.getAccntPass()));
        accountDTO.setModDt(LocalDateTime.now());
        accountDTO.setModUsr(accId);
        accountDTO.setRegDt(LocalDateTime.now());
        accountDTO.setRegUsr(accId);
        accountDTO.setAccntLoginActiveYn("Y");
//            accountDTO.setAccntRepositoryId();
//            accountDTO.setCountryCd();
        accountDTO.setUseYn("Y");
        accountDTO.setVerifyYn("N");
        accountDTO.setVerifyPhoneYn("N");

        long updareResult = accountServiceRepository.AccountDynamicUpdateService(accountDTO);
        if (updareResult > 0) {
            result = accountServiceRepository.FindByAccountId(saveResult.getAccntId());
            res.setResult(result);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        } else {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }


        return res;
    }


    public Response AccountSelectServiceResponse(Long accountId) {
        Response res = new Response();
        AccountDto accResult = modelMapper.map(accountServiceRepository.FindByAccountId(accountId), AccountDto.class);
        res.setResult(accResult);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AccountDynamicUpdateServiceResponse(AccountDto dto, HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        AccountBaseDTO accountBaseDTO = modelMapper.map(dto, AccountBaseDTO.class);
        accountBaseDTO.setAccntId(userInfoDTO.getAccountId());

        long result = accountServiceRepository.AccountDynamicUpdateService(accountBaseDTO);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public AccountDto AccountSaveTestController(AccountDto dto) {
        AccountBaseDTO accBase = accountServiceRepository
                .AccountSaveService(modelMapper.map(dto, AccountBaseDTO.class));
        AccountDto accResponse = modelMapper.map(accBase, AccountDto.class);
        return accResponse;
    }

    public Response FindByAccountRepositoryIdSelect(Long RepositoryId) {
        List<AccountDto> result = accountServiceRepository.FindByAccountRepository(RepositoryId)
                .stream().map(e -> modelMapper.map(e, AccountDto.class)).collect(Collectors.toList());
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response LastProfileUpdate(Long profileId, HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = new Response();
        long updateResult = accountServiceRepository
                .AccountDynamicUpdateService(
                        AccountBaseDTO
                                .builder()
                                .accntId(userInfoDTO.getAccountId())
                                .lastUseProfileId(profileId)
                                .build());
        res.setResult(updateResult);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response CompanyCreateService(AccountCompanyCreateRequest dto, HttpServletRequest req) {
        Response res = new Response();
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        AccountCompanyDetailBaseDTO accountCompanyDetailBaseDTO =
                modelMapper.map(dto.getAccountCompanyDetailRequest(), AccountCompanyDetailBaseDTO.class);
        accountCompanyDetailBaseDTO.setAccntId(userInfoDTO.getAccountId());
        accountCompanyDetailBaseDTO.setModUsr(userInfoDTO.getAccountId());
        accountCompanyDetailBaseDTO.setRegUsr(userInfoDTO.getAccountId());
        accountCompanyDetailBaseDTO.setModDt(LocalDateTime.now());
        accountCompanyDetailBaseDTO.setRegDt(LocalDateTime.now());

        AccountCompanyDetailBaseDTO accountCompanyDetailResult = accountServiceRepository
                .AccountCompanyDetailSaveService(accountCompanyDetailBaseDTO);

        String[] localtionStringArray = dto.getAccountCompanyLocationRequest().getLocationCd().trim().split(",");

        AccountBaseDTO abaseDTO = AccountBaseDTO
                .builder()
                .accntId(userInfoDTO.getAccountId())
                .countryCd(dto.getAccountCompanyDetailRequest().getCountryCd())
                .build();
        accountServiceRepository.AccountDynamicUpdateService(abaseDTO);

        for (String e : localtionStringArray) {
            AccountCompanyLocationBaseDTO accountCompanyLocationBaseDTO = AccountCompanyLocationBaseDTO
                    .builder()
                    .accntId(userInfoDTO.getAccountId())
                    .locationCd(e)
                    .build();
            AccountCompanyLocationBaseDTO accountCompanyLocationResult = accountServiceRepository
                    .AccountCompanyLocationSaveService(accountCompanyLocationBaseDTO);
        }

        String[] companyTypeStringArray = dto.getAccountCompanyTypeRequest().getCompanyCd().trim().split(",");
        for (String e : companyTypeStringArray) {
            AccountCompanyTypeBaseDTO accountCompanyTypeBaseDTO = AccountCompanyTypeBaseDTO
                    .builder()
                    .accntId(userInfoDTO.getAccountId())
                    .companyCd(e)
                    .companyLicenseFileUrl(dto.getAccountCompanyTypeRequest().getCompanyLicenseFileUrl())
                    .companyLicenseVerifyYn("N")
                    .build();
            AccountCompanyTypeBaseDTO AccountCompanyTypeResult = accountServiceRepository
                    .AccountCompanyTypeSaveService(accountCompanyTypeBaseDTO);
        }

        ProfileCompanyBaseDTO profileCompanyBaseDTO = ProfileCompanyBaseDTO
                .builder()
                .accntId(userInfoDTO.getAccountId())
                .profileCompanyName(dto.getAccountCompanyDetailRequest().getCompanyName())
                .modUsr(userInfoDTO.getAccountId())
                .regUsr(userInfoDTO.getAccountId())
                .modDt(LocalDateTime.now())
                .regDt(LocalDateTime.now())
                .build();

        logger.info("profileCompanyBaseDTO - ---------{}", profileCompanyBaseDTO.toString());

        HttpHeaders headers = new HttpHeaders();
        String bearertoken = req.getHeader("Authorization");
        logger.info("token", bearertoken);
        String token = bearertoken.substring(7);
        StringBuffer sb = new StringBuffer();
        sb.append("Bearer ");
        sb.append(token);
        headers.set("Authorization", sb.toString());
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)
                        .host(profileIp)
                        .path("V1/profile/company/info");
        UriComponents uriComponents = builder.build();
        logger.info("accountLast profile tostring - {}", uriComponents.toString());
        HttpEntity<ProfileCompanyBaseDTO> entity = new HttpEntity<>(profileCompanyBaseDTO, headers);
//        ResponseEntity<String> tokenResponse =
//                restTemplate.exchange(uriComponents.toUriString(), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> resultResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), entity, String.class);
        JSONObject obj = new JSONObject(resultResponse.getBody());
        logger.info("object - {}", obj.toString());

        Gson gson = new Gson();
        Response responseAccount = gson.fromJson(String.valueOf(obj), Response.class);
        ProfileCompanyBaseDTO rofileCompanyResult = modelMapper.map(responseAccount.getResult(), ProfileCompanyBaseDTO.class);
        logger.info("-------- {}", rofileCompanyResult.getProfileId());

        if (rofileCompanyResult.getProfileId() == null) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } else {

            AccountBaseDTO accountLastProfile = AccountBaseDTO.builder()
                    .accntId(rofileCompanyResult.getAccntId())
                    .lastUseProfileId(rofileCompanyResult.getProfileId())
                    .build();
            Long lastResultUp = accountServiceRepository.AccountDynamicUpdateService(accountLastProfile);


            Account account = accountRepository.findById(userInfoDTO.getAccountId()).orElse(null);
//            Profile profileSe = profileRepository.findById(userInfoDTO.getLastUseProfileId()).orElse(null);

            Mattermost mm = new Mattermost();
            MmUser mu = new MmUser();
            mu.setEmail(account.getAccntEmail()+String.valueOf(rofileCompanyResult.getProfileId()));
            mu.setFirstname(rofileCompanyResult.getProfileCompanyName());
            mm.setSparwkProfileId(rofileCompanyResult.getProfileId());
            mm.setImgUrl(rofileCompanyResult.getProfileImgUrl());
            mm.setMmUser(mu);
            //로컬버전은 http 라서 고정스트링으로 설정
            String chatCreateURL = "https://"+chat+"/V1/company/create";
            logger.info("chatCreateURL - -{}",chatCreateURL);
            logger.info("mm - -{}",new Gson().toJson(mm));
            logger.info("mu - -{}",new Gson().toJson(mu));

            String mmRes = RestCall.postRes(chatCreateURL, new Gson().toJson(mm));
            String getData = new JSONObject(mmRes).getJSONObject("data").toString();
//        String getId =  new JSONObject(getData).getJSONArray("id").toString();
            CreateMmRes cm = new Gson().fromJson(getData, CreateMmRes.class);
            profileCompanyRepository
                    .UpdateProfileCompanyMattermostId(rofileCompanyResult.getProfileId(),cm.getId());

            if (lastResultUp > 0) {
                HttpHeaders headersa = new HttpHeaders();
                String bearertokena = req.getHeader("Authorization");
                logger.info("positionTokenCreate - {}", bearertoken);
                String tokena = bearertoken.substring(7);
                StringBuffer sba = new StringBuffer();
                sb.append("Bearer ");
                sb.append(token);
                headers.set("Authorization", bearertoken);

                UriComponentsBuilder buildera =
                        UriComponentsBuilder
                                .newInstance()
                                .scheme(schema)
                                .host(authIp)
                                .path("/V1/token/positionGet");
                UriComponents uriComponentsa = buildera.build();
                HttpEntity<String> entitya = new HttpEntity<>(headers);
                ResponseEntity<String> resultResponsea =
                        restTemplate.exchange(uriComponentsa.toUriString(), HttpMethod.GET, entitya, String.class);

                JSONObject obja = new JSONObject(resultResponsea.getBody());
                logger.info("object - {}", obja.toString());
                Response responseAccounta = gson.fromJson(String.valueOf(obja), Response.class);
                logger.info("-------- {}", responseAccounta.getResult().toString());

                ProfileCompanyContact profileCompanyContact = profileCompanyContactRepository.save(
                        ProfileCompanyContact.builder()
                                .profileId(rofileCompanyResult.getProfileId())
                                .contctFirstName(dto.getAccountCompanyDetailRequest().getContctFirstName())
                                .contctMidleName(dto.getAccountCompanyDetailRequest().getContctMidleName())
                                .contctLastName(dto.getAccountCompanyDetailRequest().getContctLastName())
                                .contctEmail(dto.getAccountCompanyDetailRequest().getContctEmail())
                                .contctPhoneNumber(dto.getAccountCompanyDetailRequest().getContctPhoneNumber())
                                .build());

                res.setResult(responseAccounta.getResult());
                res.setResultCd(ResultCodeConst.SUCCESS.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }
        }

        return res;
    }

    public Response PasswordResetService(PasswordReset passwordReset, HttpServletRequest req) {
        Response res = new Response();
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        if (passwordEncoder.matches(passwordReset.getCurrentPassword(),
                accountRepository.findByAccntId(userInfoDTO.getAccountId()).get().getAccntPass())) {
            AccountBaseDTO baseDTO = AccountBaseDTO
                    .builder()
                    .accntId(userInfoDTO.getAccountId())
                    .accntPass(passwordEncoder.encode(passwordReset.getPasswordToChange()))
                    .modUsr(userInfoDTO.getAccountId())
                    .modDt(LocalDateTime.now())
                    .build();
            long result = accountServiceRepository.AccountDynamicUpdateService(baseDTO);
            res.setResult(result);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } else {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
    }


    public String EmailGetService(Long accId) {
        AccountBaseDTO baseDto = accountRepository.findByAccntId(accId)
                .map(e -> modelMapper.map(e, AccountBaseDTO.class)).get();
        logger.info("---_------{}", baseDto.getAccntEmail());
        return baseDto.getAccntEmail();
    }

    public String PhoneGetService(Long accId) {
        AccountBaseDTO baseDto = accountRepository.findByAccntId(accId)
                .map(e -> modelMapper.map(e, AccountBaseDTO.class)).get();
        logger.info("---_------{}", baseDto.getAccntEmail());
        return baseDto.getPhoneNumber();
    }

    public EmailPhoneDto PhoneEmailGetService(Long accId) {
        AccountBaseDTO baseDto = accountRepository.findByAccntId(accId)
                .map(e -> modelMapper.map(e, AccountBaseDTO.class)).get();
        logger.info("---_------{}", baseDto.getAccntEmail());
        return EmailPhoneDto.builder()
                .accntEmail(baseDto.getAccntEmail())
                .phoneNumber(baseDto.getPhoneNumber())
                .build();
    }

    public Response GroupVarifyInfoSaveService(GroupCreateRequest dto, HttpServletRequest req) {
        Response res = new Response();
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        AccountGroupDetail agdSave = modelMapper.map(dto.getAccountGroupDetailRequest(), AccountGroupDetail.class);
        agdSave.setAccntId(userInfoDTO.getAccountId());
        agdSave.setModUsr(userInfoDTO.getAccountId());
        agdSave.setRegUsr(userInfoDTO.getAccountId());
        agdSave.setModDt(LocalDateTime.now());
        agdSave.setRegDt(LocalDateTime.now());
        accountGroupDetailRepository.save(agdSave);

        for (AccountGroupTypeRequest e : dto.getAccountGroupTypeRequestList()) {
            accountGroupTypeRepository.save(
                    AccountGroupType.builder()
                            .accntId(userInfoDTO.getAccountId())
                            .groupCd(e.getGroupCd())
                            .groupLicenseFileUrl(e.getGroupLicenseFileUrl())
                            .groupLicenseVarifyYn("N")
                            .groupLicenseFileName(e.getGroupLicenseFileName())
                            .modDt(LocalDateTime.now())
                            .regDt(LocalDateTime.now())
                            .modUsr(userInfoDTO.getAccountId())
                            .regUsr(userInfoDTO.getAccountId())
                            .build()
            );
        }

        ProfileGroup pgSaveResult = profileGroupRepository.save(ProfileGroup.builder()
                .accntId(userInfoDTO.getAccountId())
                .profileGroupName(dto.getProfileGroupName())
                .modDt(LocalDateTime.now())
                .regDt(LocalDateTime.now())
                .modUsr(userInfoDTO.getAccountId())
                .regUsr(userInfoDTO.getAccountId())
                .build()
        );

        profileGroupContactRepository.save(
                ProfileGroupContact.builder()
                        .profileId(pgSaveResult.getProfileId())
                        .profileContactImgUrl(dto.getProfileGroupContactRequest().getProfileContactImgUrl())
                        .profileContactDescription(dto.getProfileGroupContactRequest().getProfileContactDescription())
                        .contctFirstName(dto.getProfileGroupContactRequest().getContctFirstName())
                        .contctMidleName(dto.getProfileGroupContactRequest().getContctMidleName())
                        .contctLastName(dto.getProfileGroupContactRequest().getContctLastName())
                        .contctEmail(dto.getProfileGroupContactRequest().getContctEmail())
                        .contctPhoneNumber(dto.getProfileGroupContactRequest().getContctPhoneNumber())
                        .verifyPhoneYn(dto.getProfileGroupContactRequest().getVerifyPhoneYn())
                        .countryCd(dto.getProfileGroupContactRequest().getCountryCd())
                        .modDt(LocalDateTime.now())
                        .regDt(LocalDateTime.now())
                        .modUsr(userInfoDTO.getAccountId())
                        .regUsr(userInfoDTO.getAccountId())
                        .build()
        );


        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AdminCreateService(AdminCreateRequest dto) {
        Response res = new Response();
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        if (adminRepository.existsByAdminEmail(dto.getAdminEmail())) {
            //중복
            res.setResultCd(ResultCodeConst.BAD_REQUEST_EMAIL_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Admin adminSave = modelMapper.map(dto, Admin.class);
        adminSave.setLockYn("N");
        adminSave.setRegUsr(userInfoDTO.getAccountId());
        adminSave.setRegDt(LocalDateTime.now());
        adminSave.setModUsr(userInfoDTO.getAccountId());
        adminSave.setModDt(LocalDateTime.now());
        Admin adminSaveResult = adminRepository.save(adminSave);
        String[] email = {adminSaveResult.getAdminEmail()};

        AdminEmailSendRequest aesr = new AdminEmailSendRequest();
        aesr.setContents(String.valueOf(adminSaveResult.getAdminId()));
        aesr.setReceiver(email);
        aesr.setSubject("Sparwk Admin");
        HttpHeaders headers = new HttpHeaders();

        headers.set("Authorization", httpServletRequest.getHeader("Authorization"));
        UriComponentsBuilder builder =
                UriComponentsBuilder
                        .newInstance()
                        .scheme(schema)
                        .host(mailingip)
                        .path("/V1/admin/create");
        UriComponents uriComponents = builder.build();
        logger.info("admin tostring - {}", uriComponents.toString());
        HttpEntity<AdminEmailSendRequest> entity = new HttpEntity<>(aesr, headers);
//        ResponseEntity<String> tokenResponse =
//                restTemplate.exchange(uriComponents.toUriString(), HttpMethod.POST, entity, String.class);
        ResponseEntity<String> resultResponse =
                restTemplate.postForEntity(uriComponents.toUriString(), entity, String.class);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AdminPasswordSaveContoller(AdminPasswordCreateRequest dto) {
        Response res = new Response();
//        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        // 1. 어카운트가 있는지 체크

        if(!mediaVerifyRepository.existsByAccntIdAndMediaValidString(dto.getAdminId(), dto.getKey())){
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        if (adminRepository.existsByAdminId(dto.getAdminId())) {
            adminPasswordRepository.save(
                    AdminPassword.builder()
                            .adminId(dto.getAdminId())
                            .adminPassword(passwordEncoder.encode(dto.getPassword()))
                            .adminPrevPassword(passwordEncoder.encode(dto.getPassword()))
                            .lastConnect(LocalDateTime.now())
                            .build());
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } else {
            res.setResultCd(ResultCodeConst.NOT_FOUND_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
    }

    public Response AdminPasswordChangageContoller() {
        Response res = new Response();

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
