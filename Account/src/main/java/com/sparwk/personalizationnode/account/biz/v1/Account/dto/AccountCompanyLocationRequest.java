package com.sparwk.personalizationnode.account.biz.v1.Account.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountCompanyLocationRequest {

    @Size(max = 9)
    @Schema(description = "회사 활동지역 스트링 a,b,c,d")
    private String locationCd;
}
