package com.sparwk.personalizationnode.account.jpa.entity;

import com.sparwk.personalizationnode.account.jpa.entity.id.AccountSnsTokenId;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_sns_token")
@DynamicUpdate
@IdClass(AccountSnsTokenId.class)
public class AccountSnsToken {
    @Id
    @Column(name = "accnt_id")
    private Long accntId;
    @Id
    @Column(name = "sns_type_cd", nullable = true, length = 50)
    private String snsTypeCd;
    @Column(name = "sns_token", nullable = true, length = 200)
    private String snsToken;

    AccountSnsToken(
            Long accntId,
            String snsTypeCd,
            String snsToken
    ) {
        this.accntId = accntId;
        this.snsTypeCd = snsTypeCd;
        this.snsToken = snsToken;
    }

}
