package com.sparwk.personalizationnode.account.biz.v1.AccountPassport;

import com.sparwk.personalizationnode.account.biz.v1.AccountPassport.dto.AccountPassportDto;
import com.sparwk.personalizationnode.account.config.filter.responsepack.Response;
import com.sparwk.personalizationnode.account.config.filter.responsepack.ResultCodeConst;
import com.sparwk.personalizationnode.account.config.filter.token.UserInfoDTO;
import com.sparwk.personalizationnode.account.jpa.dto.AccountPassportBaseDTO;
import com.sparwk.personalizationnode.account.jpa.repository.AccountBaseServiceRepository;
import com.sparwk.personalizationnode.account.jpa.repository.AccountPassportRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountPassportService {

    @Autowired
    private AccountBaseServiceRepository accountBaseServiceRepository;
    @Autowired
    private AccountPassportRepository accountPassportRepository;
    @Autowired
    private ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(AccountPassportService.class);
    private final MessageSourceAccessor messageSourceAccessor;

    public Response AccountPassportSave(AccountPassportDto dto, HttpServletRequest req) {

        AccountPassportBaseDTO accResult = null;
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        AccountPassportBaseDTO accountPassportBaseDTO =   modelMapper.map(dto, AccountPassportBaseDTO.class);
        accountPassportBaseDTO.setAccntId(userInfoDTO.getAccountId());

        AccountPassportBaseDTO result = accountBaseServiceRepository
                .AccountPassportSaveService(accountPassportBaseDTO);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response AccountPassportSelect(Long accountId) {
       List<AccountPassportBaseDTO> result = accountBaseServiceRepository.AccountPassportSelectService(accountId);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    public Response AccountPassportUpdate(AccountPassportDto dto, HttpServletRequest req) {
        AccountPassportBaseDTO accResult = null;
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());

        AccountPassportBaseDTO accountPassportBaseDTO = modelMapper.map(dto, AccountPassportBaseDTO.class);
        accountPassportBaseDTO.setAccntId(userInfoDTO.getAccountId());
        long result = accountBaseServiceRepository.AccountPassportDynamicUpdate(accountPassportBaseDTO);

        if(result <=0){
            accResult = accountPassportRepository
                    .findById( userInfoDTO.getAccountId()).map(e -> modelMapper.map(e, AccountPassportBaseDTO.class)).get();
        }
        Response res = new Response();
        res.setResult(accResult);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
