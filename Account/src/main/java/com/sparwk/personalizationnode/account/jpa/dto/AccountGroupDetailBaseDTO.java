package com.sparwk.personalizationnode.account.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountGroupDetailBaseDTO {
    private Long accntId;
    private String AccountGroupName;
    private String postCd;
    private String locationCd;
    private String region;
    private String city;
    private String addr1;
    private String addr2;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
