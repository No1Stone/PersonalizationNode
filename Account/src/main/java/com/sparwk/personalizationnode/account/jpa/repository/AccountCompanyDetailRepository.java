package com.sparwk.personalizationnode.account.jpa.repository;

import com.sparwk.personalizationnode.account.jpa.entity.AccountCompanyDetail;
import com.sparwk.personalizationnode.account.jpa.repository.dsl.AccountCompanyDetailRepositoryDunamic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountCompanyDetailRepository extends JpaRepository<AccountCompanyDetail, Long> ,
        AccountCompanyDetailRepositoryDunamic {
}
