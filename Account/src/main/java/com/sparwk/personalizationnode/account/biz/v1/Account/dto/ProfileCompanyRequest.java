package com.sparwk.personalizationnode.account.biz.v1.Account.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileCompanyRequest {

    private String mattermostId;
    private String profileImgUrl;
    private String profileBgdImgUrl;
    private String headline;
    private String bio;
    private String comInfoOverview;
    private String comInfoWebsite;
    private String comInfoPhone;
    private String comInfoEmail;
    private String comInfoFound;
    private String comInfoCountryCd;
    private String comInfoAddress;
    private String comInfoLat;
    private String comInfoLon;
    private String ipiNumber;
    private String ipiNumberVarifyYn;
    private String vatNumber;
    private String vatNumberVarifyYn;

}
